var errorCodes = {
	"-1" 	: "An error has occured, please try later.",
	"15"	: "You have already rated this business.Thanks",
	"39"	: "Invalid Mobile or Password",
	"46"	: "Invalid OTP Please try again.",
	"27"	: "No data available.",
  	"19"  	: "Email id already exist. Please use different email id for registration",
  	"55"  	: "Mobile number already exist",
  	"58"	: "Sorry 😯 No such listings exist in this city. Petbubs eagerly awaits it 🙂",
  	"66"	: "Pet Deleted Successfully",
  	"67"	: "😯 Error in Pet Deleted ",

};

var allStates = [
	{"type":"AN","mode":"Andaman and Nicobar"},
	{"type":"AP","mode":"Andhra Pradesh"},
	{"type":"AR","mode":"Arunachal Pradesh"},
	{"type":"AS","mode":"Assam"},
	{"type":"BR","mode":"Bihar"},
	{"type":"CH","mode":"Chandigarh"},
	{"type":"CT","mode":"Chhattisgarh"},
	{"type":"DD","mode":"Daman and Diu"},
	{"type":"DL","mode":"Delhi"},
	{"type":"DN","mode":"Dadra and Nagar Haveli"},
	{"type":"GA","mode":"Goa"},
	{"type":"GJ","mode":"Gujarat"},
	{"type":"HP","mode":"Himachal Pradesh"},
	{"type":"HR","mode":"Haryana"},
	{"type":"JH","mode":"Jharkhand"},
	{"type":"JK","mode":"Jammu and Kashmir"},
	{"type":"KA","mode":"Karnataka"},
	{"type":"KL","mode":"Kerala"},
	{"type":"LD","mode":"Lakshadweep"},
	{"type":"MH","mode":"Maharashtra"},
	{"type":"ML","mode":"Meghalaya"},
	{"type":"MN","mode":"Manipur"},
	{"type":"MP","mode":"Madhya Pradesh"},
	{"type":"MZ","mode":"Mizoram"},
	{"type":"NL","mode":"Nagaland"},
	{"type":"OR","mode":"Odisha"},
	{"type":"PB","mode":"Punjab"},
	{"type":"PY","mode":"Puducherry"},
	{"type":"RJ","mode":"Rajasthan"},
	{"type":"SK","mode":"Sikkim"},
	{"type":"TG","mode":"Telangana"},
	{"type":"TN","mode":"Tamil Nadu"},
	{"type":"TR","mode":"Tripura"},
	{"type":"UA","mode":"Uttarakhand"},
	{"type":"UP","mode":"Uttar Pradesh"},
	{"type":"WB","mode":"West Bengal"}
];

var allmonths = [
	{"type":"01","mode":"JAN"},
	{"type":"02","mode":"FEB"},
	{"type":"03","mode":"MAR"},
	{"type":"04","mode":"APR"},
	{"type":"05","mode":"MAY"},
	{"type":"06","mode":"JUN"},
	{"type":"07","mode":"JUL"},
	{"type":"08","mode":"AUG"},
	{"type":"09","mode":"SEP"},
	{"type":"10","mode":"OCT"},
	{"type":"11","mode":"NOV"},
	{"type":"12","mode":"DEC"}
];

var depositOptions = [
	{"type":"1","mode":"All funding account holders"},
	{"type":"2","mode":"For Self Only"}
];

var yesNoJson = [
	{"type":"0","mode":"No"},
	{"type":"1","mode":"Yes"}
];

var maturityInstructions = [
	{"type":"2","mode":"Transfer to Another Account"},
	{"type":"5","mode":"No Action, No Renewal"}
];

var reInvstMatInst = [
	{"type":"2","mode":"Transfer to Another Account"},
	{"type":"3","mode":"Automatically Renew"},
	{"type":"5","mode":"No Action, No Renewal"}
];

