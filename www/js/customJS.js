/*** DO NOT REMOVE ***/
function customValidation(form, control, customValidator) {
    var validationType = customValidator.value;
    switch (validationType) {
        case "isNumber":
            var reg = new RegExp('^\\d+$');
            var isValid = reg.test(control[0].value);
            if (!isValid) {
                form.$invalid = "true";
                form.$valid = "false";
                control.addClass('ng-invalid');
            }
            break;
        default:
            break;
    }
}

function SortJson(a, b) {
    return parseInt(a["postId"]) - parseInt(b["postId"]);
}



/*** DO NOT REMOVE ***/
function controlValidation(form, control, controlValidator) {
    var value = controlValidator.value;
    var values = value.split('|');

    var validationType = values[0];
    var control1 = form[values[1]];
    var control2 = form[values[2]];

    switch (validationType) {
        case "equalTo":
            var isValid = false;
            if (control1.$modelValue === control2.$modelValue) {
                isValid = true;
            }

            if (!isValid) {
                // form.$invalid = "true";					
                // form.$valid = "false";					
                control.addClass('ng-invalid');
            }
            break;
        default:
            break;
    }
}

function getCustomerMobile() {
    var c = window.localStorage.getItem('gCustomerMobile') ? window.localStorage.getItem('gCustomerMobile') : "";
    var d = decryptText(c);
    return d;
}

function setCustomerMobile(mobile) {
    var e = encryptText(mobile);
    window.localStorage.setItem('gCustomerMobile', e);
}

function customCache(scope, targetLeaf) {
    if (targetLeaf === "MainMenu") {
        setCustomerMobile(scope.data.mobileNo);
    }
}

function setSpecialRequestParams(inputParamComponentName, requestParams, key) {
    switch (inputParamComponentName) {
        case "CustomerNumber":
            requestParams.push({
                k: key,
                v: getCustomerMobile()
            });
            break;
        case "Breeder_Kennels":
            requestParams.push({
                k: key,
                v: "0"
            });
            break;
    }
    return requestParams;
}

function addToGlobalData() {
    try {

        gGlobalData["CustomerId"] = "2";
        gGlobalData["emptyString"] = "";
        gGlobalData["Name"] = "";
        gGlobalData["isActive"] = "";
        gGlobalData["adoptRequest"] = "1";
        gGlobalData['city1'] = "Mumbai";
        gGlobalData['dogsVital'] = "0";
        gGlobalData['catsVital'] = "1";
        gGlobalData['mediaId'] = "2";
        gGlobalData["Like"] = "";
        gGlobalData["subCategory"] = "0";
        gGlobalData["Petnews"] = "";
        gGlobalData["ViewAgency"] = "0";
        gGlobalData["VetActivity"] = [];

    } catch (e) {
        showErrorMessage('addToDynamicList', e.message);
    }
}
var TypesOfPets = [
    { name: "Dog", value: "Dog" },
    { name: "Cat", value: "Cat" },
    { name: "Fish", value: "Fish" },
    { name: "Turtle", value: "Turtle" },
    { name: "Bird", value: "Bird" },
    { name: "Rabbit", value: "Rabbit" },
    { name: "Hamster", value: "Hamster" },
    { name: "Other", value: "Other" },
]

var genderType = [
    { name: "Male", value: "Male" },
    { name: "Female", value: "Female" }
]

var DogBreedCategory = [
    { name: "Small", value: "Small" },
    { name: "Medium", value: "Medium" },
    { name: "Large", value: "Large" }
]

var Activity = [
    { name: "Birthday", value: "Birthday" },
    { name: "Food", value: "Food" },
    { name: "Fun", value: "Fun" },
    { name: "Grooming", value: "Grooming" },
    { name: "MISC", value: "MISC" },
    { name: "Veterinary Care", value: "Veterinary Care" },
]

var PrefedActivity = [
    { name: "Deworming", value: "Deworming" },
    { name: "Rabies Vaccination", value: "Rabies Vaccination" },
    { name: "DHPPi & Leptosporosis", value: "DHPPi & Leptosporosis" },
    { name: "Feline calicivirus vaccination", value: "Feline calicivirus vaccination" },
    { name: "Feline panleukopenia virus", value: "Feline panleukopenia virus" },
    { name: "Feline rhinotracheitis virus", value: "Feline rhinotracheitis virus" },
    { name: "Other", value: "PreFedOther" },
]

var city = [
    { name: "Bangalore", value: "Bangalore" },
    { name: "Chennai", value: "Chennai" },
    { name: "Chandigarh", value: "Chandigarh" },
    { name: "Delhi", value: "Delhi" },
    { name: "Goa", value: "Goa" },
    { name: "Hyderabad", value: "Hyderabad" },
    { name: "Indore", value: "Indore" },
    { name: "Jaipur", value: "Jaipur" },
    { name: "Karjat", value: "Karjat" },
    { name: "Kolkata", value: "Kolkata" },
    { name: "Kochi", value: "Kochi" },
    { name: "Mumbai", value: "Mumbai" },
    { name: "Pune", value: "Pune" }


]

var Country = [
    { name: "India", value: "India" },
]

var myPets = [
    { name: "Zulu", value: "Zulu" },
    { name: "Jack", value: "Jack" },
    { name: "Lieca", value: "Lieca" },
    { name: "Rocky", value: "Rocky" }
]

var DogVitals = [
    { title: "Breathing rate", source: "https://www.rover.com/blog/dogs-vital-signs/", para1: "A healthy dog, depending on breed, takes between 12 and 24 breaths per minute. To measure breathing rate, count the number of times the chest expands in 10 seconds and multiply by 6.", para2: "You can do this either by watching your dog or resting your hand on the ribs. Normal respirations should not make any noise, and should require very little effort. Of course, if you have a brachycephalic breed like a Pug or English Bulldog, a little snort from time to time can be expected.", para3: "" },
    { title: "Heart rate", source: "https://www.rover.com/blog/dogs-vital-signs/", para1: "A normal heart rate for dogs is between 60 and 140 beats per minute. To determine your dog’s heart rate, put your hand to his chest and count how many pulses you feel in 15 seconds, then multiply by 4 to get the number of beats per minute.If you have trouble detecting heart beats in the chest area, try placing two fingers on the middle of your dog’s thigh near where the leg joins the body. There, you should be able to feel the femoral artery pulsing each time the heart beats.", para2: "", para3: "" },
    { title: "Temperature", source: "http://pets.webmd.com/dogs/high-fever-in-dogs", para1: "The normal body temperature for dogs is between 101 and 102.5 F, compared to 97.6 to 99.6 F for humans. This means your dog may feel feverish to you even when his temperature is completely normal.", para2: "The only accurate way to tell if your dog has an increased body temperature is to take his rectal temperature. Experts recommend using a digital thermometer specifically designed for rectal use in dogs.", para3: "To take your dog’s temperature, first coat the thermometer with a lubricant such as petroleum gel or baby oil. Next, gently insert the thermometer about one inch into your dog’s anus and wait for results. Most thermometers sold for this purpose will take less than 60 seconds to register.", para3: "" },
    { title: "Dehydration", source: "http://pets.webmd.com/dogs/guide/dog-dehydration-water-needs", para1: "Dehydration occurs when fluid levels drop to less than normal. This is due to either reduced water intake or increased fluid loss. Fluid loss can be due to overheating in hot weather or a bout of vomiting or diarrhea, especially in puppies.", para2: "If you suspect that your dog is dehydrated, take him to a veterinarian immediately.", para3: "" },
    { title: "Pulse rate", source: "https://www.rover.com/blog/dogs-vital-signs/ ", para1: "A normal heart rate for dogs is between 60 and 140 beats per minute. To determine your dog’s heart rate, put your hand to his chest and count how many pulses you feel in 15 seconds, then multiply by 4 to get the number of beats per minute.", para2: "", para3: "" },
]

var CatVitals = [
    { title: "Breathing rate", source: "http://www.vetstreet.com/dr-marty-becker/feline-fine-how-to-read-your-cats-vital-signs", para1: "Cats at rest (their default state) normally take 20 to 30 breaths per minute. Breathing should be smooth, with exhales taking longer than inhales. To check your cat’s respiratory rate, count the number of times his chest rises and falls over a one-minute period.Be concerned if your cat’s breathing is abnormal. That means it’s unusually slow, fast or noisy; has a high, harsh or whistling sound; or the cat is having difficulty breathing. And remember that a purr signals more than pleasure; it can also be a sign that a cat is in distress. Just because a cat is purring doesn’t mean he’s feeling good.", para2: "", para3: "" },
    { title: "Heart rate", source: "http://consciouscat.net/2015/02/23/how-to-check-your-cats-vital-signs/", para1: "The normal heart rate for a healthy cat is bewteen 140 and 220 beats per minute. A relaxed cat’s heart rate will be on the low end of the range. Feel your cat’s heartbeat with one hand over his left side, just behind his front leg. Count the number of beats for 15 seconds and multiply by four.", para2: "", para3: "" },
    { title: "Temperature", source: "http://consciouscat.net/2015/02/23/how-to-check-your-cats-vital-signs/", para1: "Normal body temperature in a healthy cat is between 100 and 102.5 degrees Fahrenheit. The easiest way to check your cat’s temperature is with an ear thermometer. If you’re going to take your cat’s temperature with a traditional glass thermometer, be sure to lubricate the thermometer with a water-based lubricant, and gently insert it into your cat’s rectum. Don’t insert it further than about an inch. Since a traditional thermometer needs to stay in for at least two minutes (or until it beeps if you’re using a digital one), this is not the easiest thing to do with most cats.", para2: "", para3: "" },
    { title: "Dehydration", source: "http://pets.webmd.com/dogs/guide/dog-dehydration-water-needs", para1: "The classic sign for dehydration is skin tenting. If you take a pinch of skin over the cat's shoulders and pull up gently, the skin should snap back into place when released. As the cat gets more dehydrated, the skin goes back in place more and more slowly. If the pinch of skin stays up (the \"tent\"), it is a sign of severe dehydration. The cat should be seen by a veterinarian immediately.", para2: "", para3: "" },
    { title: "Pulse rate", source: "http://www.vetstreet.com/dr-marty-becker/check-your-cats-vital-signs-at-home ", para1: "You’ll need a watch with a second hand, a stopwatch or … your smartphone! Feel your cat’s heartbeat with one hand over his left side, just behind his front leg. Count the number of beats in 15 seconds and multiply by four to get the heart rate in beats per minute (bpm). A normal cat's heart rate is between 140 and 220 bpm, with a relaxed cat measuring on the low end. Call your veterinarian if the heart rate is too rapid, too slow or irregular.", para2: "", para3: "" },
]

function addToDynamicList() {
    try {
        var dynamicList = getDynamicListData();
        dynamicList["cityList"] = city;
        dynamicList["selectedCity"] = 'Mumbai';
        dynamicList["Activity"] = Activity;
        dynamicList["Country"] = Country;
        dynamicList["PrefedActivity"] = PrefedActivity;
        dynamicList["TypesOfPets"] = TypesOfPets;
        dynamicList["genderType"] = genderType;
        dynamicList["DogBreedCategory"] = DogBreedCategory;
        dynamicList["myPets"] = myPets;
        dynamicList["DogVitals"] = DogVitals;
        dynamicList["CatVitals"] = CatVitals;
        dynamicList['cat'] = "1";
        dynamicList['dog'] = "0";
        dynamicList['others'] = "2";

        dynamicList["Vaccination"] = "5";
        dynamicList["Feeding"] = "2";
        dynamicList["Travel"] = "1";
        dynamicList["Training"] = "3";
        dynamicList["Grooming"] = "4";
        dynamicList["Mating"] = "0";

        dynamicList["Crematorium"] = "";

        dynamicList["set"] = false;
        setDynamicListData(dynamicList);

    } catch (e) {
        showErrorMessage('addToDynamicList', e.message);
    }
}

function showCustomError(errCode) {
    //console.log('errCode', errCode);
    try {
        return errorCodes[errCode];
    } catch (e) {
        showErrorMessage('addToDynamicList', e.message);
    }

}


/***** APP SPECIFIC CODE - START *****/


var randomColor = function (CatTitle) {
    switch (CatTitle) {
        case "Birthday":
            return { 'background-color': '#fc7aad' }
            break;

        case "Veterinary Care":
            return { 'background-color': '#7680d2' }
            break;

        case "Grooming":
            return { 'background-color': '#995cb3' }
            break;

        case "Food":
            return { 'background-color': '#38ca75' }
            break;

        case "Other":
            return { 'background-color': '#f6b864' }
            break;

        default:
            return { 'background-color': '#f6b864' }
            break;
    }
}

var alertTime = function (timeString) {
    switch (timeString) {
        case 'At time of event':
            return '0';
            break;
        case '5 minutes before':
            return '5';
            break;
        case '15 minutes before':
            return '15';
            break;
        case '30 minutes before':
            return '30';
            break;
        case '1 hour before':
            return '60';
            break;
        case '2 hour befor':
            return '120';
            break;
        case '1 day before':
            return '1440';
            break;
        case '2 days before':
            return '2880';
            break;
        case '1 week before':
            return '10080';
            break;
        default:
            return '';
            break;
    }
}

var petName = function (petid, petlist) {
    for (var i = 0; i < petlist.length; i++) {
        switch (petid) {
            case petlist[i].petid:
                return petlist[i].name;
                break;
        }
    }
}

var petId = function (name, petlist) {
    for (var i = 0; i < petlist.length; i++) {
        switch (name) {
            case petlist[i].name:
                return petlist[i].petid;
                break;
        }
    }
}

var randomIcon = function (CatTitle) {
    switch (CatTitle) {
        case "Birthday":
            return "images/cat1.png";
            break;

        case "Veterinary Care":
            return "images/cat2.png";
            break;

        case "Grooming":
            return "images/cat3.png";
            break;

        case "Food":
            return "images/cat4.png";
            break;

        case "Other":
            return "images/cat6.png";
            break;

        default:
            return "images/cat5.png";
            break;

    }
}

var randomColor = function (CatTitle) {
    switch (CatTitle) {
        case "Birthday":
            return { 'background-color': '#fc7aad' };
            break;

        case "Veterinary Care":
            return { 'background-color': '#7680d2' };
            break;

        case "Grooming":
            return { 'background-color': '#995cb3' };
            break;

        case "Food":
            return { 'background-color': '#38ca75' };
            break;

        case "Other":
            return { 'background-color': '#f6b864' };
            break;

        default:
            return { 'background-color': '#f6b864' };
            break;

    }
}


function onClickAddEvent(data, gGlobalData, Global, GlobalConstants, showAlert) {
    if (data.addEventEligible) {
        var startDate = new Date(data["startDate"]);
        var endDate = (data["endDate"]) ? new Date(data["endDate"]) : new Date(data["startDate"]);
        var endRepeatDate = data.selectedDate != "" ? data.selectedDate : "undefined";
        var myPet = data["myPet"];
        var selectActivity = data["selectActivity"];
        var addRepeat = (typeof data.choice !== "undefined" && data.choice !== "");
        var addAlert = (typeof data.alertTime !== "undefined" && data.alertTime !== "");
        var DataselectEndDate = "undefined";
        var Datachoice = "Never";
        var DataalertTime = "undefined";
        var DataalertTimeText = "Never";
        var imgLoc = data.activityname === undefined ? gGlobalData["selectedCalendarimg"] : randomIcon(data.activityname);
        var CatTitle = data.activityname === undefined ? gGlobalData["CatTitle"] : data.activityname;
        var petid = data.petId;

        if (endDate < startDate) {
            showAlert("Petbubs", "Ends should be greater than Starts");
            return false;
        }

        if (addRepeat == true) {
            DataselectEndDate = endRepeatDate;
            Datachoice = data["choice"];

            if (new Date(DataselectEndDate).getTime() < startDate.getTime()) {
                showAlert("Petbubs", "End Repeat time should be greater than Starts");
                return false;
            }
        }

        function success(result) {
            showAlert("Petbubs", "Event created successfully");
        }

        function error(result) {
            alert(result);
        }
        return true;
    }
}
/*PEt deleteDateList*/
var deleteDateList = function (list) {

    for (var i = 0; i < list.length; i++) {
        list[i] = new Date(list[i]);
    }
    return list;
}


/*Calendar Add event*/
function onClickAddEventFromServer(data, gGlobalData, Global, GlobalConstants, showAlert, isEdit, eId, isAdd) {
    if (data.addEventEligible || (data.editEventStarted && isEdit)) {
        // var startDate = new Date(data["startDate"]);
        // var endDate = (data["endDate"]) ? new Date(data["endDate"]) : new Date(data["startDate"]);
        // var endRepeatDate = "undefined";
        // var myPet = data["myPet"];
        // var selectActivity = data["selectActivity"];
        // var addRepeat = (typeof data.choice !== "undefined" && data.choice !== "");
        // var addAlert = (typeof data.alertTime !== "undefined" && data.alertTime !== "");
        // var DataselectEndDate = "undefined";
        // var Datachoice = "Never";
        // var DataalertTime = "undefined";
        // var DataalertTimeText = "Never";
        // var imgLoc = gGlobalData["selectedCalendarimg"];
        // var CatTitle = gGlobalData["CatTitle"];

        var startDate = new Date(data["startDate"]);
        var endDate = (data["endDate"]) ? new Date(data["endDate"]) : new Date(data["startDate"]);
        var endRepeatDate = data.selectedDate != "" ? data.selectedDate : "undefined";
        var myPet = data["myPet"];
        var selectActivity = data["selectActivity"];
        var addRepeat = (typeof data.choice !== "undefined" && data.choice !== "");
        var addAlert = (typeof data.alertTime !== "undefined" && data.alertTime !== "");
        var DataselectEndDate = "undefined";
        var Datachoice = "Never";
        var DataalertTime = "undefined";
        var DataalertTimeText = "Never";
        var imgLoc = data.activityname === undefined ? gGlobalData["selectedCalendarimg"] : randomIcon(data.activityname);
        var CatTitle = data.activityname === undefined ? gGlobalData["CatTitle"] : data.activityname;
        var petid = data.petId;
        var deletedate = data.deletedate === undefined ? '' : deleteDateList(data.deletedate);
        var deletedaterepeat = data.deletedaterepeat === undefined ? '' : data.deletedaterepeat;


        var events = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);

        // if (endDate < startDate) {
        //     showAlert("Petbubs", "Ends should be greater than Starts");
        //     return false;
        // }

        if (addRepeat == true) {
            DataselectEndDate = endRepeatDate;
            Datachoice = data["choice"];

            // if (new Date(DataselectEndDate).getTime() < startDate.getTime()) {
            //     showAlert("Petbubs", "End Repeat time should be greater than Starts");
            //     return false;
            // }
        }

        if (addAlert == true) {
            DataalertTime = data["alertTime"];
            DataalertTimeText = data["alertTimeText"];
        }

        //var endDate=new Date();
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yy = startDate.getFullYear();
        var hh = startDate.getHours();
        var ms = startDate.getMinutes();
        var x = yy + ',' + mm + ',' + dd + ' ' + hh + ':' + ms;
        var date = new Date(x);

        var editedData = false;
        var _EID = eId;
        if (isEdit) {
            // convert repeat eid to parent eid
            // var splitEid = eId.toString().split("_");
            // if (splitEid[0] === "REPEAT") {
            //     eId = parseInt(splitEid[2]);
            // }
            // delete repeat eid of this if any
            deleteRepeatEvent(eId, Global, GlobalConstants);

            _EID = eId;


            var currentEvents = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
            main_loop: for (var i = 0; i < currentEvents.length; i++) {
                if (currentEvents[i].eId === eId) {
                    editedData = {
                        eId: eId,
                        title: selectActivity,
                        date: date,
                        startDate: new Date(startDate),
                        endDate: new Date(endDate),
                        alert: DataalertTimeText,
                        alertTime: DataalertTime,
                        repeat: Datachoice,
                        endRepeat: ((DataselectEndDate === "undefined") ? false : new Date(DataselectEndDate)),
                        color: randomColor(CatTitle),
                        img: imgLoc,
                        myPet: myPet,
                        type: CatTitle,
                        petid: petid,
                        deletedate: deletedate,
                        deletedaterepeat: deletedaterepeat
                    };
                    console.log(gGlobalData["selectedCalendarimg"])
                    currentEvents[i] = editedData;
                    break main_loop;
                }
            }
            Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, currentEvents);
        } else {
            _EID = eId;
            events.push({
                eId: _EID,
                title: selectActivity,
                date: date,
                startDate: new Date(startDate),
                endDate: new Date(endDate),
                alert: DataalertTimeText,
                alertTime: DataalertTime,
                repeat: Datachoice,
                endRepeat: ((DataselectEndDate === "undefined") ? false : new Date(DataselectEndDate)),
                color: randomColor(CatTitle),
                img: imgLoc,
                myPet: myPet,
                type: CatTitle,
                petid: petid,
                deletedate: deletedate,
                deletedaterepeat: deletedaterepeat
            });

            Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, events);

        }

        if (addRepeat) {
            manageRepeat(_EID, Datachoice, DataselectEndDate, Global, GlobalConstants, deletedate, deletedaterepeat);
        } else if (deletedate.length > 0) {

            for (var z = 0; z < events.length; z++) {

                // if(events[z].deletedate.length!=undefined){
                var datelength = events[z].deletedate.length;
                for (var x = 0; x < datelength; x++) {

                    var dxt = new Date(events[z].deletedate[x]);
                    if (JSON.stringify(events[z].startDate) === JSON.stringify(dxt) || JSON.stringify(events[z].startDate) === JSON.stringify(events[z].deletedate[x])) {
                        events.splice(z, 1);
                        if (events[z] === undefined) {
                            break;
                        }
                        else {
                            z--;
                        }
                        break;
                    }
                }


                // }else{
                //     break;
                // }
            }


            Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, events);

        }

        //var calOptions = window.plugins.calendar.getCalendarOptions(); // grab the defaults
        var calOptions = {};

        if (DataalertTime != "undefined") {
            calOptions.firstReminderMinutes = DataalertTime; // default is 60, pass in null for no reminder (alarm)    
        } else {
            calOptions.firstReminderMinutes = null; // default is 60, pass in null for no reminder (alarm)    
        }

        if (DataselectEndDate != "undefined") {
            var dateStringTodate = new Date(DataselectEndDate);
            endRepeatDate = dateStringTodate;
            if (Datachoice == "undefined") {
            } else {
                calOptions.recurrence = Datachoice; // supported are: daily, weekly, monthly, yearly    
                calOptions.recurrenceEndDate = dateStringTodate;
            }
        } else {
            var today = new Date();
            var dd = new Date(today).getDate();
            var mm = new Date(today).getMonth() + 1;
            var yy = new Date(today).getFullYear();
            var z = yy + ',' + mm + ',' + dd + ' ' + 11 + ':' + 59;
            var newDate = new Date(z);
            endRepeatDate = newDate;
        }

        //window.plugins.calendar.createEventWithOptions(selectActivity,"Location","notes",startDate,endDate,calOptions,success,error);


        function success(result) {
            showAlert("Petbubs", "Event created successfully");
        }

        function error(result) {
            alert(result);
        }

        try {
            if (isEdit) {
                showAlert("Petbubs", "Event updated successfully");
            } else if (isAdd) {
                showAlert("Petbubs", "Event created successfully");
            }
        } catch (e) {
            console.log(e);
        }

        return (editedData) ? editedData : true;
    }
}


function manageRepeat(eId, Datachoice, DataselectEndDate, Global, GlobalConstants, deletedate, deletedaterepeat) {
    var currentEvents = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
    main_loop: for (var i = 0; i < currentEvents.length; i++) {
        if (currentEvents[i].eId === eId) {
            var targetEvent = currentEvents[i];
            var startDate = new Date(targetEvent.startDate);
            var endDate = new Date(targetEvent.endDate);
            var diff_time = endDate.getTime() - startDate.getTime();
            //var repeatLap = getRepeatLap(Datachoice);
            var endRepeatTime = new Date(DataselectEndDate).getTime();

            // get repeat array of time
            getRepeatArray(Datachoice, endRepeatTime, startDate, endDate, [], function (repeatArray) {
                if (repeatArray.length === 0) return false;

                var events = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
                // console.log(events);
                for (var r = 0; r < repeatArray.length; r++) {
                    var repeatEvent = {
                        eId: targetEvent.eId,
                        repeatParent: targetEvent.eId,
                        title: targetEvent.title,
                        date: repeatArray[r].startDate,
                        startDate: repeatArray[r].startDate,
                        endDate: repeatArray[r].endDate,
                        alert: targetEvent.alert,
                        alertTime: targetEvent.alertTime,
                        repeat: targetEvent.repeat,
                        endRepeat: targetEvent.endRepeat,
                        color: targetEvent.color,
                        img: targetEvent.img,
                        myPet: targetEvent.myPet,
                        type: targetEvent.type,
                        petid: targetEvent.petid,
                        deletedate: targetEvent.deletedate,
                        deletedaterepeat: targetEvent.deletedaterepeat,
                    }
                    // console.log(repeatEvent);
                    events.push(repeatEvent);
                }

                for (var z = 0; z < events.length; z++) {

                    // if(events[z].deletedate.length!=undefined){
                    var datelength = events[z].deletedate.length;
                    for (var x = 0; x < datelength; x++) {

                        var dxt = new Date(events[z].deletedate[x]);
                        if (JSON.stringify(events[z].startDate) === JSON.stringify(dxt) || JSON.stringify(events[z].startDate) === JSON.stringify(events[z].deletedate[x])) {
                            events.splice(z, 1);
                            if (events[z] === undefined) {
                                break;
                            }
                            else {
                                z--;
                            }
                            break;
                        }
                    }


                    // }else{
                    //     break;
                    // }
                }


                Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, events);

            });

            break main_loop;
        }
    }

}

function getRepeatArray(choice, endRepeatTime, startDate, endDate, repeatArray, callback, deletedate, deletedaterepeat) {
    var newJump = {};
    var lap = 0;

    // for(var i=0; i < deletedate.length; i++){
    // if(deletedate[i]!=startDate){
    switch (choice) {
        case "hourly":
            lap = (1000 * 60 * 60);
            break;
        case "daily":
            lap = (1000 * 60 * 60 * 24);
            break;
        case "weekly":
            lap = (1000 * 60 * 60 * 24 * 7);
            break;
    }

    newJump.startDate = new Date(startDate.getTime() + lap);
    newJump.endDate = new Date(endDate.getTime() + lap);

    if (choice === "monthly") {
        newJump.startDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate(), startDate.getHours(), startDate.getMinutes(), startDate.getSeconds());
        newJump.endDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, endDate.getDate(), endDate.getHours(), endDate.getMinutes(), endDate.getSeconds());
    }

    if (choice === "yearly") {
        newJump.startDate = new Date(startDate.getFullYear() + 1, startDate.getMonth(), startDate.getDate(), startDate.getHours(), startDate.getMinutes(), startDate.getSeconds());
        newJump.endDate = new Date(endDate.getFullYear() + 1, endDate.getMonth(), endDate.getDate(), endDate.getHours(), endDate.getMinutes(), endDate.getSeconds());
    }

    // }}

    if (newJump.startDate.getTime() < endRepeatTime) {
        repeatArray.push(newJump);
        getRepeatArray(choice, endRepeatTime, newJump.startDate, newJump.endDate, repeatArray, callback);
    } else {
        callback(repeatArray);
    }

    return lap;
}


function deleteEvent(eId, Global, GlobalConstants) {
    // console.log(eId)
    // var splitEid = eId.toString().split("_");
    // if this is repeat event we are deleting, then just replace eId with it's parent's eId.
    // so that parent will delete and all its repeat will also delete
    // if (splitEid[0] === "REPEAT") {
    //     eId = parseInt(splitEid[2]);
    // }

    var currentEvents = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
    var targetIndex = false;


    for (var z = 0; z < currentEvents.length; z++) {

        // if(events[z].deletedate.length!=undefined){
        for (var x = 0; x < currentEvents[z].deletedate.length; x++) {

            var dxt = new Date(currentEvents[z].deletedate[x]);
            if (JSON.stringify(currentEvents[z].startDate) === JSON.stringify(dxt) || JSON.stringify(currentEvents[z].startDate) === JSON.stringify(currentEvents[z].deletedate[x])) {
                currentEvents.splice(z, 1);
                if (currentEvents[z] === undefined) {
                    break;
                }
                else {
                    z--;
                }
            }
        }

    }

    // main_loop: for (var i = 0; i < currentEvents.length; i++) {
    //     if (currentEvents[i].eId === eId) {
    //         targetIndex = i;
    //         break main_loop;
    //     }
    // }

    // if (targetIndex !== false) {
    //     currentEvents.splice(targetIndex, 1);
    Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, currentEvents);
    // }

    // delete repeat events if any
    deleteRepeatEvent(eId, Global, GlobalConstants);

    return true;
}

// here eId will be parent's eid
function deleteRepeatEvent(eId, Global, GlobalConstants) {
    // console.log("delete repeat eid", eId)
    var currentEvents = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
    var targetIndex = [];
    for (var i = 0; i < currentEvents.length; i++) {
        // var splitEid = currentEvents[i].eId.toString().split("_");
        // if (splitEid[0] === "REPEAT" && eId === parseInt(splitEid[2])) {
        //     targetIndex.push(i);
        // }
        if (eId === currentEvents[i].eId) {
            targetIndex.push(i);
        }
    }

    if (targetIndex.length === 0) return false;
    targetIndex.sort(function (a, b) { return b - a; });


    // for (var j = 0; j < targetIndex.length; j++) {
    //     currentEvents.splice(targetIndex[j], 1);
    // }
    for (var z = 0; z < currentEvents.length; z++) {

        // if(events[z].deletedate.length!=undefined){
        for (var x = 0; x < currentEvents[z].deletedate.length; x++) {

            var dxt = new Date(currentEvents[z].deletedate[x]);
            if (JSON.stringify(currentEvents[z].startDate) === JSON.stringify(dxt) || JSON.stringify(currentEvents[z].startDate) === JSON.stringify(currentEvents[z].deletedate[x])) {
                currentEvents.splice(z, 1);
                if (currentEvents[z] === undefined) {
                    break;
                }
                else {
                    z--;
                }
            }
        }


        // }else{
        //     break;
        // }
    }

    Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, currentEvents);

    return true;
}

function addEventByGivenData(data, name, id, Global, GlobalConstants, showAlert) {
    if (data && typeof data === "object") {
        var id = name + "_" + id + "_" + data.activityname;
        var isEdit = false;

        var event = {};
        event.alert = data.activityReminder1;
        event.alertTime = data.activityReminder1_time;
        event.color = randomColor("Other");
        event.date = getDateFromString(data.activitydate);
        event.startDate = getDateFromString(data.activitydate);
        // event.eId = id;
        event.repeat = false; // because there is no repeat
        event.endRepeat = false; // because there is no repeat
        event.type = "Other"; // category is fix: other
        event.endDate = new Date(event.startDate.getTime() + (60 * 60 * 1000)); // because there is no repeat
        event.img = "images/cat6.png"; // category is fix: other
        event.myPet = name;
        event.title = data.activityname;

        var events = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
        // loop: for (var i = 0; i < events.length; i++) {
        //     if (events[i].eid === id) {
        //         isEdit = true;
        //         events[i] = event;
        //         break loop;
        //     }
        // }

        if (!isEdit) {
            events.push(event);
        }

        Global.setObjectInLocalStorage(GlobalConstants.ls_calendarEvents, events);
    }
}

function getDateFromString(str) {
    if (!str) return false;

    var date = false;
    if (str.length > 13) {
        date = new Date(str)
    } else {
        var strSpl = str.split("-");
        date = new Date(strSpl[2], parseInt(strSpl[1]) - 1, strSpl[0]);
    }

    return date;
}

function formatDate(object, datekey) {

    for (var index = 0; index < object.length; index++) {
        var Newsdate = new Date(object[index][datekey]);
        var today = new Date();
        var milliseconds = Math.floor((today - (Newsdate)) / 60000);
        var seconds = Math.floor((today - (Newsdate)) / 1000);
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        var months = Math.floor(days / 30);
        if (parseInt(months) > 0) {
            // object[index][datekey] = months + " months ago";
            object[index][datekey] = moment(Newsdate).format("MMMM DD YYYY") + " at " + moment(Newsdate).format("h:mm a");
        } else if (parseInt(days) < 2 && parseInt(days) > 0) {
            object[index][datekey] = days + " day ago";
        } else if (parseInt(days) > 0) {
            object[index][datekey] = days + " days ago";
        } else if (parseInt(hours) < 2 && parseInt(hours) > 0) {
            object[index][datekey] = hours + " hour ago";
        } else if (parseInt(hours) > 0) {
            object[index][datekey] = hours + " hours ago";
        } else if (parseInt(minutes) > 0) {
            object[index][datekey] = minutes + " min ago";
        } else if (parseInt(seconds) > 0) {
            object[index][datekey] = seconds + " sec ago";
        } else if (parseInt(milliseconds) > 0) {
            opValue[index].date = milliseconds + " seconds ago";
        }
    }
    return object;
}


function bubtalkthreadCategory(object) {
    for (var index = 0; index < object.length; index++) {
        switch (object[index].topic) {
            case "1":
                object[index].topicName = "General Chats";
                break;
            case "2":
                object[index].topicName = "FAQs";
                break;
            case "3":
                object[index].topicName = "Adoption";
                break;
            case "4":
                object[index].topicName = "Lost & Found";
                break;
            case "5":
                object[index].topicName = "Breeding/Mating";
                break;
            case "6":
                object[index].topicName = "Grooming";
                break;
            case "7":
                object[index].topicName = "Health & Nutrition";
                break;
            case "8":
                object[index].topicName = "Training";
                break;
            case "9":
                object[index].topicName = "Boarding & Kennels";
                break;
        }
    }

    return object;
}

function getFileName(uri) {
    return uri.substring(uri.lastIndexOf('/') + 1);
}

// To create unique file name.
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function getImagesFromPicker($scope, imagesCounts, objKey) {
    $scope.dynamicList.SelectedMedia = [];
    $scope.isMediaSelected = objKey;
    $scope.dynamicList.SelectedMediaType = '';
    $scope.dynamicList.fileName = [];
    $scope.dynamicList.video = {};
    $scope.dynamicList.videoName = '';

    window.imagePicker.getPictures(
        function (results) {
            $scope.$apply(function () {
                $scope.dynamicList.SelectedMediaType = 'image';
                $scope.dynamicList.SelectedMedia.length = 0;
                $scope.dynamicList.fileName.length = 0;
            });

            for (var i = 0; i < results.length; i++) {
                // Extracts File name from URI.
                var fileName = getFileName(results[i]);// results[i].substring(results[i].lastIndexOf('/') + 1);
                var uniqueFileName = guid() + fileName;
                $scope.$apply(function () {
                    $scope.dynamicList.fileName.push(uniqueFileName);
                    $scope.dynamicList.SelectedMedia.push(results[i]);
                });
            }
        }, function (error) {
            console.log('Error: ' + error);
        }, {
            maximumImagesCount: imagesCounts,
            width: 600,
            height: 600
        });
}


function getEditImagesFromPicker($scope, imagesCounts, objKey) {
    $scope.dynamicList.editThreadMedia = [];
    $scope.dynamicList.SelectedMediaType = '';
    $scope.dynamicList.editfileName = [];


    window.imagePicker.getPictures(
        function (results) {
            $scope.$apply(function () {
                $scope.dynamicList.SelectedMediaType = 'image';
                $scope.dynamicList.editThreadMedia.length = 0;
                $scope.dynamicList.editfileName.length = 0;
            });

            for (var i = 0; i < results.length; i++) {
                // Extracts File name from URI.
                var fileName = getFileName(results[i]);// results[i].substring(results[i].lastIndexOf('/') + 1);
                var uniqueFileName = guid() + fileName;
                $scope.$apply(function () {
                    $scope.dynamicList.editfileName.push(uniqueFileName);
                    $scope.dynamicList.editThreadMedia.push(results[i]);
                });
            }
        }, function (error) {
            console.log('Error: ' + error);
        }, {
            maximumImagesCount: imagesCounts,
            width: 600,
            height: 600
        });
}


function storeFileToLocal(foldername, url, $cordovaFile, $cordovaFileTransfer, showToast, callback) {
    if (!$cordovaFile || !$cordovaFileTransfer || typeof cordova === 'undefined') return false;

    $cordovaFile.checkDir(cordova.file.externalRootDirectory, foldername).then(function (success) {
        // directory exists
        copyFileLocal();
    }, function (error) {
        // error
        if (error.code === 1) { // directory not exists error code
            // so here is making of directory
            $cordovaFile.createDir(
                cordova.file.externalRootDirectory,
                foldername,
                false
            ).then(function (success) {
                copyFileLocal();
            }, function (error) {
                console.log(error);
            });
        } else {
            console.log(error)
        }
    });

    function copyFileLocal() {
        var sourceFileName = url.substring(url.lastIndexOf('/') + 1, url.length);
        showToast("Downloading...");
        $cordovaFileTransfer.download(url, cordova.file.externalRootDirectory + foldername + '/' + sourceFileName, {}, true)
            .then(function (result) {
                // Success!
                showToast("Your file has been downloaded and you can find it at " + foldername + '/' + sourceFileName);
                console.log("result : " + JSON.stringify(result));
                callback(foldername + '/' + sourceFileName);

            }, function (err) {
                // Error
                console.log("error : " + JSON.stringify(err));
                if (err.code === 1) {
                    showToast("Permission Denied: Please give this app access to store files in your storage.");
                } else if (err.code === 3) {
                    showToast("Can't connect with server. Please check your internet connection");
                }

            }, function (progress) {
                /*$timeout(function () {
                  $scope.downloadProgress = (progress.loaded / progress.total) * 100;
    
                })*/
            });
    }
}



// means in array named target, we will search for searchKey which value should be searchValue, and then we will return value of returnKey
function getObjectByKeyFromArray(target, searchKey, searchValue, returnKey) {
    var result = false;
    // var result = '';
    main_loop: for (var i = 0; i < target.length; i++) {
        if (target[i][searchKey] === searchValue) {
            result = returnKey ? target[i][returnKey] : target[i];
            break main_loop;
        }
    }

    return result;
}



function getBiggestValueFromArray(arr, key) {
    var val = 0;
    for (var i = 0; i < arr.length; i++) {
        console.log(arr[i][key], val)
        if (parseInt(arr[i][key]) > val) {
            val = parseInt(arr[i][key]);
        }
    }

    return val;
}