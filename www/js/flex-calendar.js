(function () {
  "use strict";
  angular.module('flexcalendar', [])
    .directive('flexCalendar', flexCalendar);

  function flexCalendar() {

    var template =
      '<div id="divCalendarDiv" class="bar bar-header item-input-inset" style="display: none"><label class="item-input-wrapper"><input type="search" id="Calendarbox" ng-model="searchBoxCalendar" placeholder="Search"></label><button class="button ion-android-close input-button button-small" ng-click="clearText()" ng-show="searchBoxCalendar.length"></button></div>' +
      '<div id="flexCalendar" class="flex-calendar">' +
      '<img ng-src={{monthSrc}} style="width: 100%;">' +
      '<div class="month row">' +
      '<div style="display: flex;">' +
      '<div id="preMonth" class="arrow-year calendar-year-arrows {{arrowPrevClass}}" ng-click="prevYear()" ></div>' +
      '<div id="selectedMonth" class="label calendar-date-text-color padd-top-4 font-size-3 font-titi titi-500">{{selectedYear}}</div>' +
      '<div id="nextMonth"  class="arrow-next-year calendar-year-arrows {{arrowNextClass}}" ng-click="nextYear()" ></div>' +
      '</div>' +
      '<div class="c-label" style="display: flex;">' +
      '<div id="preMonth" class="col arrow {{arrowPrevClass}} float-left"  ng-click="prevMonth()" ></div>' +
      '<div id="selectedMonth" class="col label font-titi titi-600 float-left" >{{ selectedMonth | translate }}</div>' +
      '<div id="nextMonth"  class="col arrow-next float-left {{arrowNextClass}}" ng-click="nextMonth()" ></div>' +
      '</div>' +
      '<div class="float-left" style="display: flex;">' +
      '<a href="#" class="mg-top-3" ng-click="addEvent()">' +
      '<img class="plus-circled" src="images/shadow-button-v1.png"/>' +
      '</a>' +
      '</div>' +
      '</div>' +
      '<div class="week">' +
      '<div class="day" ng-repeat="day in weekDays(options.dayNamesLength) track by $index">{{ day }}</div>' +
      '</div>' +
      '<div class="days" ng-repeat="week in weeks">' +
      '<div class="day"' +
      'ng-repeat="day in week track by $index"' +
      'ng-class="{istoday: isToday(day), selected: isDefaultDate(day), event: day.event[0], disabled: day.disabled, out: !day}"' +
      'ng-click="onClick(day, $index, $event)">' +
      '<div class="number"><span ng-style="getMultipleEvents(day)">{{day.day}}</span></div>' +
      '</div>' +
      '</div>' +
      '<div class="mid-container-calendar margin-topt-15" ng-if="EventDetail.length>0">' +
      '<div class="events border-bottom">' +
      '<div class="row padding-right margin-top-10m">' +
      '<div class="col-20 center padd-bottom-4">' +
      '<p ng-click="jumpToday()" class=" today-text" style="" ng-if="calendarTitle !== \'Upcoming Events\' && !onToday">Today</p>' +
      '</div>' +
      '<div class="col-60 center">' +
      '<p class="color-date font-size-14 padd-bottom-5">{{calendarTitle}}</p>' +
      '</div>' +
      '<div class="col-20 center">' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '<div id="EventData"><div id="EventList" ng-repeat="item in EventDetail | orderBy:\'date\' | filter: searchBoxCalendar" >' +
      // '<div ng-repeat="item in EventDetail">'+
      '<ion-list class="ion-option-calendar" show-delete="false" can-swipe="true" swipe-direction="both">' +
      '<ion-item>' +
      '<div class="event-scroll">' +
      //'<div class="col-10"></div>'+
      '<div class="row border-bottom" ng-click="viewEventItem(item,\'click\')">' +
      '<div class="col-20 padd-top-bottom-10 event-border center"><p class="up-name-date">{{item.date | date:"dd"}} <span class="up-day-act">{{item.date | date:"MMM" | uppercase}}</span></p> <p class="up-day-act">{{item.date | date:"EEEE"}}</p></div>' +
      '<div class="col-60 event-border"><img class="up-event" ng-src="{{item.img}}" width="40" height="40">' +
      '<p  class="padd-top-10 up-name-date">{{item.myPet}}</p><p class="up-day-act">{{item.title ? item.title : ""}}</p>' +
      '</div>' +
      '<div class="col-20 padd-right-zero padd-top-10 center"><p class="padd-top-10 up-day-act">{{item.date | date:"hh:mm a" }}</p></div>' +
      '</div>' +
      '</div>' +
      '<ion-option-button class="icon ion-ios-trash-outline" side="right" ng-click="deletePetEvent(item.eId, item.petid, item.startDate, item.repeat,$index)"></ion-option-button>' +
      '<ion-option-button class="ion-edit" side="left" ng-click="viewEventItem(item,\'swipe\')"></ion-option-button>' +
      // '<ion-option-button class="ion-edit" side="left" ng-click="onButtonClick(nextleaf,EditEventLeaf)"></ion-option-button>'+
      '</ion-item>' +
      '</ion-list>' +
      '</div></div>';

    var directive = {
      restrict: 'E',
      scope: {
        options: '=?',
        events: '=?',
        outerbuttonclick: "="
      },
      template: template,
      controller: Controller
    };

    return directive;
  }

  Controller.$inject = ['$scope', '$filter', '$ionicNativeTransitions', '$ionicScrollDelegate', '$timeout', '$state', 'Global', 'GlobalConstants', 'globalService'];

  function Controller($scope, $filter, $ionicNativeTransitions, $ionicScrollDelegate, $timeout, $state, Global, GlobalConstants, globalService) {

    // $scope.showList=true;
    $scope.days = [];
    $scope.options = $scope.options || {};
    $scope.events = $scope.events || [];
    $scope.options.dayNamesLength = $scope.options.dayNamesLength || 1;
    $scope.options.mondayIsFirstDay = $scope.options.mondayIsFirstDay || false;

    $scope.onClick = onClick;
    $scope.allowedPrevMonth = allowedPrevMonth;
    $scope.allowedNextMonth = allowedNextMonth;
    $scope.weekDays = weekDays;
    $scope.isDefaultDate = isDefaultDate;
    $scope.isToday = isToday;
    $scope.jumpToday = jumpToday;
    $scope.prevMonth = prevMonth;
    $scope.nextMonth = nextMonth;
    $scope.prevYear = prevYear;
    $scope.nextYear = nextYear;
    $scope.viewEventItem = viewEventItem;
    $scope.addEvent = addEvent;
    $scope.deletePetEvent = deletePetEvent;
    $scope.onButtonClick = onButtonClick;

    $scope.calendarTitle = "Upcoming Events";
    $scope.onToday = true;
    $scope.arrowPrevClass = "visible";
    $scope.arrowNextClass = "visible";
    $scope.showme={
      hide:true
    };

    var $translate = $filter('translate');
    var monthSrc = ['img/Calendar/Jan.jpg', 'img/Calendar/Feb.jpg', 'img/Calendar/Mar.jpg', 'img/Calendar/April.jpg', 'img/Calendar/May.jpg', 'img/Calendar/June.jpg', 'img/Calendar/July.jpg', 'img/Calendar/Aug.jpg', 'img/Calendar/Sept.jpg', 'img/Calendar/Oct.jpg', 'img/Calendar/Nov.jpg', 'img/Calendar/Dec.jpg'];
    var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var WEEKDAYS = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];

    if ($scope.options.mondayIsFirstDay) {
      var sunday = WEEKDAYS.shift();
      WEEKDAYS.push(sunday);
    }

    if ($scope.options.minDate) {
      $scope.options.minDate = new Date($scope.options.minDate);
    }

    if ($scope.options.maxDate) {
      $scope.options.maxDate = new Date($scope.options.maxDate);
    }

    if ($scope.options.disabledDates) {
      createMappedDisabledDates();
    }

    if ($scope.events) {
      createMappedEvents();
    }

    function createMappedDisabledDates() {
      $scope.mappedDisabledDates = $scope.options.disabledDates.map(function (date) {
        return new Date(date);
      });
    }

    function createMappedEvents() {
      $scope.mappedEvents = $scope.events.map(function (obj) {
        obj.date = new Date(obj.date);
        return obj;
      });

    }

    $scope.$watch('options.defaultDate', function () {
      calculateSelectedDate();
    });

    $scope.$watch('options.disabledDates', function () {
      createMappedDisabledDates();
      calculateDisabledDates();
    });

    $scope.$watch('events', function () {
      createMappedEvents();
      calculateWeeks();
    });

    /////////////////

    $scope.clearText = function () {
      if ($state.current.name === "PetCalendar") {
        $scope.searchBoxCalendar = '';
        document.getElementById('flexCalendar').setAttribute("style", "display:block");
        document.getElementById('divCalendarDiv').setAttribute('style', "display:none");
      }
    };

    

    $('.search-window').keyup(function () {
      var text = $('#' + this.id).val();
 
      if (text.length > 0) {
        $scope.searchBoxCalendar =text;
        document.getElementById('EventData').setAttribute("style", "display:block");
      } else {
        $scope.searchBoxCalendar = '';
        document.getElementById('EventData').setAttribute("style", "display:none");
      }

     // if (text.length > 0) {
     //    document.getElementById("EventList").setAttribute('style', "display:none");
     //    $scope.EventDetail = sort($scope.events, text);
     //  } else {
     //    $scope.EventDetail = $scope.events;
     //    $scope.searchBoxCalendar = '';
     //  }

    });

    function sort(objects, searchText) {
      searchText = searchText.toLowerCase();
      var filtered = [];
      angular.forEach(objects, function (item) {
        if (item.myPet.toLowerCase().indexOf(searchText) >= 0 ||
          item.title.toLowerCase().indexOf(searchText) >= 0
        ) {
          filtered.push(item);
        }
      });
      return filtered;
    }

    function onClick(date, index, domEvent) {
      if (!date || date.disabled) { return; }
      $scope.options.defaultDate = date.date;
      if (date.event.length != 0) {
        $timeout(function () {
          $scope.calendarTitle = date.day + "-" + (date.month + 1) + "-" + date.year;
          $scope.onToday = isToday(date);
          $scope.EventDetail = date.event;
          if (!$scope.$$phase) {
            $scope.$apply();
          }
          $ionicScrollDelegate.resize();
        });
        $scope.options.eventClick(date, domEvent);

      }
      else {
        $scope.calendarTitle = "Upcoming Events";
        $scope.options.dateClick(date, domEvent);
      }
    }

    $scope.getMultipleEvents = function (date) {
      if (date && date.event && date.event[0]) {
        var bgColor = date.event[0].color;
        return {
          'background-color': bgColor["background-color"],
          'background-image': (date.event.length > 1) ? "url(img/color.png)" : "",
          'background-repeat': 'no-repeat',
          'background-position': 'center center',
          'background-size': 'cover'
        };
      }

    }

    function bindEvent(date) {
      if (!date || !$scope.mappedEvents) { return; }
      date.event = [];
      $scope.mappedEvents.forEach(function (event) {
        if (date.date.getFullYear() === event.date.getFullYear()
          && date.date.getMonth() === event.date.getMonth()
          && date.date.getDate() === event.date.getDate()) {
          date.event.push(event);
        }
      });
    }

    function allowedDate(date) {
      if (!$scope.options.minDate && !$scope.options.maxDate) {
        return true;
      }
      var currDate = date.date;
      if ($scope.options.minDate && (currDate < $scope.options.minDate)) { return false; }
      if ($scope.options.maxDate && (currDate > $scope.options.maxDate)) { return false; }
      return true;
    }

    function disabledDate(date) {
      if (!$scope.mappedDisabledDates) return false;
      for (var i = 0; i < $scope.mappedDisabledDates.length; i++) {
        if (date.year === $scope.mappedDisabledDates[i].getFullYear() && date.month === $scope.mappedDisabledDates[i].getMonth() && date.day === $scope.mappedDisabledDates[i].getDate()) {
          return true;
          break;
        }
      }
    }

    function allowedPrevMonth() {
      var prevYear = null;
      var prevMonth = null;
      if (!$scope.options.minDate) { return true; }
      var currMonth = MONTHS.indexOf($scope.selectedMonth);
      if (currMonth === 0) {
        prevYear = ($scope.selectedYear - 1);
        prevMonth = 11;
      } else {
        prevYear = $scope.selectedYear;
        prevMonth = (currMonth - 1);
      }
      if (prevYear < $scope.options.minDate.getFullYear()) { return false; }
      if (prevYear === $scope.options.minDate.getFullYear()) {
        if (prevMonth < $scope.options.minDate.getMonth()) { return false; }
      }
      return true;
    }

    function allowedNextMonth() {
      var nextYear = null;
      var nextMonth = null;
      if (!$scope.options.maxDate) { return true; }
      var currMonth = MONTHS.indexOf($scope.selectedMonth);
      if (currMonth === 11) {
        nextYear = ($scope.selectedYear + 1);
        nextMonth = 0;
      } else {
        nextYear = $scope.selectedYear;
        nextMonth = (currMonth + 1);
      }
      if (nextYear > $scope.options.maxDate.getFullYear()) { return false; }
      if (nextYear === $scope.options.maxDate.getFullYear()) {
        if (nextMonth > $scope.options.maxDate.getMonth()) { return false; }
      }
      return true;
    }

    function calculateWeeks() {
      $scope.weeks = [];
      var week = null;
      var daysInCurrentMonth = new Date($scope.selectedYear, MONTHS.indexOf($scope.selectedMonth) + 1, 0).getDate();

      for (var day = 1; day < daysInCurrentMonth + 1; day += 1) {
        var date = new Date($scope.selectedYear, MONTHS.indexOf($scope.selectedMonth), day);
        var dayNumber = new Date($scope.selectedYear, MONTHS.indexOf($scope.selectedMonth), day).getDay();
        if ($scope.options.mondayIsFirstDay) {
          dayNumber = (dayNumber + 6) % 7;
        }
        week = week || [null, null, null, null, null, null, null];
        week[dayNumber] = {
          year: $scope.selectedYear,
          month: MONTHS.indexOf($scope.selectedMonth),
          day: day,
          date: date,
          _month: date.getMonth() + 1
        };

        if (allowedDate(week[dayNumber])) {
          if ($scope.mappedEvents) { bindEvent(week[dayNumber]); }
        } else {
          week[dayNumber].disabled = true;
        }

        if (week[dayNumber] && disabledDate(week[dayNumber])) {
          week[dayNumber].disabled = true;
        }

        if (dayNumber === 6 || day === daysInCurrentMonth) {
          $scope.weeks.push(week);
          week = undefined;
        }
      }
      (!$scope.allowedPrevMonth()) ? $scope.arrowPrevClass = "hidden" : $scope.arrowPrevClass = "visible";
      (!$scope.allowedNextMonth()) ? $scope.arrowNextClass = "hidden" : $scope.arrowNextClass = "visible";
    }

    function calculateSelectedDate() {
      if ($scope.options.defaultDate) {
        $scope.options._defaultDate = new Date($scope.options.defaultDate);
      } else {
        $scope.options._defaultDate = new Date();
      }

      $scope.selectedYear = $scope.options._defaultDate.getFullYear();
      $scope.monthSrc = monthSrc[$scope.options._defaultDate.getMonth()];
      $scope.selectedMonth = MONTHS[$scope.options._defaultDate.getMonth()];
      $scope.selectedDay = $scope.options._defaultDate.getDate();
      $scope.EventDetail = [];
      $scope.mappedEvents.forEach(function (event) {
        if ($scope.options._defaultDate.getMonth() === event.date.getMonth() && $scope.options._defaultDate.getFullYear() === event.date.getFullYear()) {
          $scope.EventDetail.push(event);
        }
      });
      calculateWeeks();
    }

    function calculateDisabledDates() {
      if (!$scope.mappedDisabledDates || $scope.mappedDisabledDates.length === 0) return;
      for (var i = 0; i < $scope.mappedDisabledDates.length; i++) {
        $scope.mappedDisabledDates[i] = new Date($scope.mappedDisabledDates[i]);
      }
      calculateWeeks();
    }

    function weekDays(size) {
      return WEEKDAYS.map(function (name) { return $translate(name).slice(0, size); });
    }

    function isDefaultDate(date) {
      if (!date) { return; }
      var result = date.year === $scope.options._defaultDate.getFullYear() &&
        date.month === $scope.options._defaultDate.getMonth() &&
        date.day === $scope.options._defaultDate.getDate();
      return result;
    }

    function isToday(date) {
      if (!date) { return; }
      var today = new Date();
      var result = date.year === today.getFullYear() &&
        date.month === today.getMonth() &&
        date.day === today.getDate();
      return result;
    }

    function jumpToday() {
      $scope.onToday = true;
      var today = new Date();
      $scope.selectedYear = today.getFullYear();
      var month = today.getMonth();

      $scope.selectedMonth = MONTHS[month];
      $scope.monthSrc = monthSrc[month];

      $scope.EventDetail = [];
      $scope.mappedEvents.forEach(function (event) {
        if ((month) === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
          $scope.EventDetail.push(event);
        }
      });

      calculateWeeks();
      $ionicScrollDelegate.resize();
      $timeout(function () {
        angular.element(document.getElementsByClassName('istoday')).triggerHandler('click');
      })
    }


    function prevYear() {
      $scope.selectedYear -= 1;
      $scope.EventDetail = [];
      var currIndex = MONTHS.indexOf($scope.selectedMonth);
      $scope.mappedEvents.forEach(function (event) {
        if ((currIndex) === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
          $scope.EventDetail.push(event);
        }
      });

      calculateWeeks();
      $ionicScrollDelegate.resize();
    }


    function prevMonth() {
      if (!$scope.allowedPrevMonth()) { return; }
      var currIndex = MONTHS.indexOf($scope.selectedMonth);
      if (currIndex === 0) {
        $scope.selectedYear -= 1;
        $scope.selectedMonth = MONTHS[11];
        $scope.monthSrc = monthSrc[11];
        $scope.EventDetail = [];
        $scope.mappedEvents.forEach(function (event) {
          if (11 === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
            $scope.EventDetail.push(event);
          }
        });
      } else {
        $scope.selectedMonth = MONTHS[currIndex - 1];
        $scope.monthSrc = monthSrc[currIndex - 1];
        $scope.EventDetail = [];
        $scope.mappedEvents.forEach(function (event) {
          if ((currIndex - 1) === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
            $scope.EventDetail.push(event);
          }
        });
      }
      var month = { name: $scope.selectedMonth, index: currIndex - 1, _index: currIndex + 2 };
      $scope.options.changeMonth(month, $scope.selectedYear);
      calculateWeeks();
      $ionicScrollDelegate.resize();

    }

    function nextYear() {
      $scope.selectedYear += 1;
      $scope.EventDetail = [];
      var currIndex = MONTHS.indexOf($scope.selectedMonth);
      $scope.mappedEvents.forEach(function (event) {
        if ((currIndex) === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
          $scope.EventDetail.push(event);
        }
      });

      calculateWeeks();
      $ionicScrollDelegate.resize();
    }

    function nextMonth() {
      if (!$scope.allowedNextMonth()) { return; }
      var currIndex = MONTHS.indexOf($scope.selectedMonth);
      if (currIndex === 11) {
        $scope.selectedYear += 1;
        $scope.selectedMonth = MONTHS[0];
        $scope.monthSrc = monthSrc[0];
        $scope.EventDetail = [];
        $scope.mappedEvents.forEach(function (event) {
          if (0 === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
            $scope.EventDetail.push(event);
          }
        });
      } else {
        $scope.EventDetail = [];
        $scope.mappedEvents.forEach(function (event) {
          if ((currIndex + 1) === event.date.getMonth() && $scope.selectedYear === event.date.getFullYear()) {
            $scope.EventDetail.push(event);
          }
        });
        $scope.selectedMonth = MONTHS[currIndex + 1];
        $scope.monthSrc = monthSrc[currIndex + 1];
      }
      var month = { name: $scope.selectedMonth, index: currIndex + 1, _index: currIndex + 2 };
      $scope.options.changeMonth(month, $scope.selectedYear);
      calculateWeeks();
      $ionicScrollDelegate.resize();
    }

    function viewEventItem(item, triggerEvent) {
      var dynamicList = getDynamicListData();
      dynamicList["viewEventItem"] = item;
      setDynamicListData(dynamicList);
      if (triggerEvent === "click") {
        $state.go('ViewEventLeaf');
      }

      if (triggerEvent === "swipe") {
        $state.go('EditEventLeaf');
      }
    }

    function addEvent() {
      var appData = getAppData();
      var proceed = true;
      var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

      var serviceInfo = null;

      // if(proceed){
      //     for (var template in appData.views) {
      //         if(appData.views[template].title === componentTargetLeaf){
      //             serviceInfo = appData.views[template].networkService;
      //             setControllerData(appData.views[template].data);
      //         }
      //     }
      // }
      // $state.go('AddEventLeaf');
      // globalService.showEventConfirmation("<center>Welcome to the <br/>Pet Calendar!</center>", "<center>A smart & fun way of keeping track of your pet’s activities<br/>Please click below to add your pet’s details.</center>", eventConfirm);
      $scope.outerbuttonclick('', 'AddEventLeaf');

    }

    function deletePetEvent(eId, petid, petdeletedate, petdeletedaterepeat,index) {
      // var result = deleteEvent(eId, Global, GlobalConstants);
      // if (result) {
        // $scope.options.deletePetActivity(eId, petid);
        $scope.options.deleteSinglePetActivity(eId, petid, petdeletedate, petdeletedaterepeat);
        $scope.EventDetail.splice(index,1);
        // $ionicNativeTransitions.stateGo($state.current.name, {}, { reload: true });
        // $state.go($state.current.name, {}, { reload: true });
        globalService.showAlert('Petbubs', 'Event deleted successfully.');
        
        // $state.go("PetCalendar");
      // }
    }

    function onButtonClick(componentSubType, componentTargetLeaf, key) {
      var appData = getAppData();
      var proceed = true;
      var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

      var serviceInfo = null;

      if (proceed) {
        for (var template in appData.views) {
          if (appData.views[template].title === componentTargetLeaf) {
            serviceInfo = appData.views[template].networkService;
            setControllerData(appData.views[template].data);
          }
        }
      }
      $state.go('EditEventLeaf');
    }

  }

})();
