/*! App Studio - v0.0.1 - 26 June 2014
* http://www.mobilewaretech.com
* Includes: 
* Copyright 2014 Mobileware Technologies Pvt Ltd. 
* 
* Author: Rohan Deshpande
* Date  : 26 June 2014
* Desc  : iOS
*/

/*********************************GLOBAL: STARTS HERE*********************************************/
var gAppDefXML = null;
var gContainerDiv = null;
var gContainerDivId = "wrapper";
var gSliderMenuLeftDivId = "sliderMenuLeft";
var gSliderMenuRightDivId = "sliderMenuRight";
var gAppDefXMLFileName = "AppDef.xml";
var gfirstLeafXPath = "/app/appdes/firstleaf";
var gleafXPath = "/app/leaf";
var gcomponentXPath = "/component";
var gElementXPath = "/elements/element";
var gHomeLeafName = null;
var gImageFolderPath = "./img/";
var gDataTheme = "custom"; //Define JQuery UI Theme
//A (50/50),  B (33/33/33), C (25/25/25/25), D (20/20/20/20/20)
var gGridRowCss = ["","","ui-grid-a", "ui-grid-b", "ui-grid-c", "ui-grid-d"]; //Classes for Grid Rows
var gGridColumnCss = ["ui-block-a", "ui-block-b", "ui-block-c", "ui-block-d", "ui-block-e"]; //Classes for Grid Columns
var validator = null;
/*********************************GLOBAL: ENDS HERE*********************************************/

$(document).ready(function(){
    $(function() {
        $( "body>[data-role='panel']" ).panel();
    });
    $(function(){
		$( ".date-input-css" ).datepicker();
	})
    validator = $("#formMain").validate({
        onfocusout: false,
        onkeyup: true,
        onclick: true,
        focusInvalid: true,
        messages: {
			userId: {
				required: " (required)",
				minlength: " (must be at least 3 characters)"
			},
			password: {
				required: " (required)",
				minlength: " (must be between 5 and 12 characters)",
				maxlength: " (must be between 5 and 12 characters)"
			}
		},
        errorPlacement: function(error,element){
            //error.insertAfter(element);
            //alert(error.html());
            element.parent().addClass("ui-input-text-error");
        },
        showErrors: function(errorMap, errorList) {
            var summary = "Please check following errors:";
            $.each(errorList, function() {
            	console.log(this);
                //summary += "\n * " + this.element.parentElement.previousElementSibling.htmlFor + " -" + this.message + "\n" ;
                summary += "\n * " + this.element.id + " -" + this.message + "\n" ;
            });
            //$('.errorbox').html(summary);
            //$( ":mobile-pagecontainer" ).pagecontainer( "change", "#popupDialog", { role: "dialog" } );
            this.defaultShowErrors();
            if(this.numberOfInvalids()){
            	alert(summary);	
            	//$.mobile.changePage("#popupDialog", { transition: 'slidedown', role: "dialog" } );
            	//$( ":mobile-pagecontainer" ).pagecontainer( "change", "#popupDialog", { transition: 'slidedown', role: "dialog", data-rel: "external" } );
            }
            
            //$("#popupDialog").page({ dialog: true });
        },
        submitHandler: function( form ){
            alert("submitted!");
        }, 

    });             

});

/** 
 * initialiseAppStudio()  
 * The code below will initialise the start screen 
 *  
 * @param   
 */
var initialiseAppStudio = function(){
	gContainerDiv = document.getElementById(gContainerDivId);
	gAppDefXML = loadXMLFromFile("include/xml/" + gAppDefXMLFileName);
	var firstLeafValue = getNodeValueByXPath(gAppDefXML, gfirstLeafXPath);
	var startupScreenXPath = gleafXPath + "[@name='" + firstLeafValue + "']" + gcomponentXPath;
	var componentList= getNodesByXPath(gAppDefXML, startupScreenXPath);
	var pageResults = page.render(firstLeafValue);
	var homePage = pageResults[0];
	var homePageContainer = pageResults[1];
    //var homePageContainer = pageContainer.render(firstLeafValue);
	gHomeLeafName = firstLeafValue;
    //homePage.appendChild(homePageContainer);
	//gContainerDiv.appendChild(homePage);
    var sliderLeftMenuScreenXPath = gleafXPath + "[@name='" + gSliderMenuLeftDivId + "']" + gcomponentXPath;
    var sliderLeftComponentList= getNodesByXPath(gAppDefXML, sliderLeftMenuScreenXPath);
    var sliderRightMenuScreenXPath = gleafXPath + "[@name='" + gSliderMenuRightDivId + "']" + gcomponentXPath;
    var sliderRightComponentList= getNodesByXPath(gAppDefXML, sliderRightMenuScreenXPath);
    for (var i=0; i<sliderLeftComponentList.length; i++){
		createControls(document.getElementById(gSliderMenuLeftDivId), sliderLeftComponentList[i], sliderLeftMenuScreenXPath, homePage);
	}
	for (var i=0; i<sliderRightComponentList.length; i++){
		createControls(document.getElementById(gSliderMenuRightDivId), sliderRightComponentList[i], sliderRightMenuScreenXPath, homePage);
	}
	for (var i=0; i<componentList.length; i++){
		createControls(homePageContainer, componentList[i], startupScreenXPath, homePage);
	}
};

var createControls = function(varHTMLDiv, varComponent, varCurrentLeafXPath, varPage){	
		var componentNode = varComponent;
		var componentName = componentNode.getAttribute("name");
		var componentType = componentNode.getAttribute("type");		
		var componentSubType = componentNode.getAttribute("subtype");
		var componentTheme = componentNode.getAttribute("theme");
		var componentTitle = componentNode.getAttribute("title");		
		var ctrl = null;
		switch(componentType){
			case "input":				
				ctrl =  input.render(componentName, componentSubType, "", "", "width:100%;", componentTheme, "false", componentNode);				
				break;
			case "label":
				ctrl = label.render(componentName,componentTitle, componentTheme);
				break;
			case "panel":
				var panelComponentXPath = varCurrentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
				ctrl = panel.render(componentNode, panelComponentXPath);				
				break;
			case "button":	
				var componentTargetLeaf = componentNode.getAttribute("targetLeaf");		
				ctrl = button.render(componentName, componentTitle, componentTheme, componentSubType, componentTargetLeaf);
				break;
			case "command":	
				//var componentTargetLeaf = componentNode.getAttribute("targetLeaf");		
				ctrl = button.render(componentName, componentTitle, componentTheme, componentSubType, componentTargetLeaf);
				break;	
			case "list":
				var listComponentXPath = varCurrentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
				ctrl = list.render(componentName, componentNode, listComponentXPath, componentSubType);				
				break;
			case "date":					
				ctrl = datePicker.render(componentName, "", "", "width:100%;", "form-control", "false");
				break;
			case "radiobuttongroup":
				var radioComponentXPath = varCurrentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
				ctrl = radioButton.render(componentName, componentNode, radioComponentXPath, componentSubType);
				break;
            case "checkboxgroup":
				var checkboxComponentXPath = varCurrentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
				ctrl = checkbox.render(componentName, componentNode, checkboxComponentXPath, componentSubType);
				break;
			case "image":					
				ctrl = image.render(componentNode, componentTheme);
				break;
			case "h1":					
				ctrl = h1.render(componentName,componentTitle, componentTheme);
				break;	
			case "header":	
				var headerComponentXPath = varCurrentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;				
				pageHeader.render(varPage, componentNode, headerComponentXPath);
				break;		
			default:
				break;
		}
		if(ctrl){
			varHTMLDiv.appendChild(ctrl);		
		}
		
};

var touchScroll = function( event ) {
    event.preventDefault();
};

var page = {
	render:function(varId){
		var page = document.createElement("div");
		page.setAttribute("id",varId);
		page.setAttribute("data-role","page");
		page.setAttribute("data-theme",gDataTheme);
        page.setAttribute("data-add-back-btn","true");
        //$(page).bind( 'touchmove', touchScroll );
		//page.setAttribute("class", "container");

		/*
		var pageHeader = document.createElement("div");
		pageHeader.setAttribute("data-role","header");
        pageHeader.setAttribute("data-position","fixed");
		pageHeader.setAttribute("class","ui-header-bar");
		var headerTitle = document.createElement("h1");
		headerTitle.innerText = varId;
		pageHeader.appendChild(headerTitle);
        var headerButtonLeft = document.createElement("a");
        headerButtonLeft.setAttribute("href","#leftpanel3");
        headerButtonLeft.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-bullets ui-btn-icon-notext ui-btn-left");
        pageHeader.appendChild(headerButtonLeft);
        var headerButtonRight = document.createElement("a");
        headerButtonRight.setAttribute("href","#rightpanel2");
        headerButtonRight.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-gear ui-btn-icon-notext ui-btn-right");
        pageHeader.appendChild(headerButtonRight);
		page.appendChild(pageHeader);
		*/

        var pageContainer = document.createElement("div");
        pageContainer.setAttribute("id",varId+"Container");
        pageContainer.setAttribute("class", "ui-page-container");
        pageContainer.setAttribute("data-role","content");
        page.appendChild(pageContainer);
        gContainerDiv.appendChild(page);
        
        var results = [];
        results[0] = page;
        results[1] = pageContainer;
		return results;
	},
	previousPage: function(){
		var prevPageObject = $.mobile.activePage.prev('[data-role=page]');
		var prevPageId = null;
		if(prevPageObject.length !== 0){
			prevPageId = '#' + prevPageObject[0].id;
		}else{
			prevPageId = '#' + gHomeLeafName;
		}
				
		var currentPage = '#'+$.mobile.activePage.attr('id');
		$(currentPage).remove();
		$.mobile.changePage(prevPageId, {
		   transition: 'slide',
		   reverse: true,
		   changeHash: false
		});
		
	},
	nextPage: function(varNextLeafName){
		var nextScreenXPath = gleafXPath + "[@name='" + varNextLeafName + "']" + gcomponentXPath;
		var componentList= getNodesByXPath(gAppDefXML, nextScreenXPath);
		if(componentList.length > 0){
			
			//var validator = $( "#formMain" ).validate();
			//validator.resetForm();
			this.resetValidationErrors();
			if(validator.form()){
				var nextPageResults = page.render(varNextLeafName);
				var nextPage = nextPageResults[0];
				var nextPageContainer = nextPageResults[1];			
				for (var i=0; i<componentList.length; i++){
					createControls(nextPageContainer, componentList[i], nextScreenXPath, nextPage);
				}
				//gContainerDiv.appendChild(nextPage);
				$( ":mobile-pagecontainer" ).pagecontainer( "change", '#'+varNextLeafName, { transition: "slide", changeHash: false } );
				//$.mobile.changePage( '#'+varNextLeafName, { transition: "slide", changeHash: false });			
			}			
			//validator.formValidate();
		}
		
	},
	resetValidationErrors : function(page) {
		$('#' + gContainerDivId).find('.ui-input-text-error').each(function() {
			$(this).removeClass('ui-input-text-error');			
		});
		validator.resetForm();
	},	
};

var pageContainer = {
render:function(varId){
    var pageContainer = document.createElement("div");
    pageContainer.setAttribute("id",varId+"Container");
    pageContainer.setAttribute("class", "ui-page-container");
    return pageContainer;
},
    
};

var pageHeader = {
	render: function(varPage,varHeaderNode, varHeaderComponentXPath){
		var componentList= getNodesByXPath(gAppDefXML, varHeaderComponentXPath);

		var pageHeader = document.createElement("div");
		pageHeader.setAttribute("data-role","header");
        pageHeader.setAttribute("data-position","fixed");
		pageHeader.setAttribute("class","ui-header-bar");
		varPage.appendChild(pageHeader);

		for (var j = 0; j < componentList.length; j++) {
			createControls(pageHeader, componentList[j], null);
			// var componentNode = componentList[j];
			// var componentType = componentNode.getAttribute("type");
			// var componentSubType = componentNode.getAttribute("subtype");
			// if(componentType === "h1"){
			// 	var headerTitle = document.createElement("h1");
			// 	headerTitle.innerText = componentNode.getAttribute("title");
			// 	pageHeader.appendChild(headerTitle);
			// }else if(componentType === "image" && componentSubType === "leftSlider" ){
			// 	var headerButtonLeft = document.createElement("a");
		 //        headerButtonLeft.setAttribute("href","#leftpanel3");
		 //        headerButtonLeft.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-bullets ui-btn-icon-notext ui-btn-left");
		 //        pageHeader.appendChild(headerButtonLeft);
			// }else if(componentType === "image" && componentSubType === "rightSlider" ){
			// 	var headerButtonRight = document.createElement("a");
		 //        headerButtonRight.setAttribute("href","#rightpanel2");
		 //        headerButtonRight.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-gear ui-btn-icon-notext ui-btn-right");
		 //        pageHeader.appendChild(headerButtonRight);
			// }
		}
	
	}
};

var input = {
	render: function(varId,varType,varValue,varPlaceHolder,varStyle,varCssClass,varReadOnly, varComponentNode){
			if(!varType || varType == "any"){
				varType = "text";
			}
			var isRequired = varComponentNode.getAttribute("mandatory");
			var maxLength = varComponentNode.getAttribute("maxchar");
			var minLength = varComponentNode.getAttribute("minchar");
			var input = document.createElement("input");
			input.setAttribute("id",varId);			
			input.setAttribute("name",varId);			
			input.setAttribute("type",varType);
			input.setAttribute("value",varValue);
			input.setAttribute("placeholder",varPlaceHolder);
			input.setAttribute("style",varStyle);
			if(varCssClass && varCssClass.length > 0){
				//input.setAttribute("data-role","none");
				input.setAttribute("class",varCssClass);
			}
			
			if(varReadOnly == "true"){
				input.setAttribute("readOnly","readOnly");
			}
			if(isRequired == "true"){
				input.required = true;
			}
			if(maxLength){
				input.setAttribute("maxlength",maxLength);
			}
			if(minLength){
				input.setAttribute("minlength",minLength);
			}
        //alert(varId);
			return input;
	},
	setValue: function(varInput, varValue){
		varInput.setAttribute("value",varValue);
	},
	getValue: function(varInput){
		return varInput.value;
	},
};

var label = {
	render: function(varId, varText, varCssClass){
			var label = document.createElement("label");
			label.setAttribute("id",varId);
			label.setAttribute("for",varText.replace(":",""));
			//label.setAttribute("class", varCssClass);
			label.innerText = varText;
        //alert(varText);
			return label;
		},
};

var panel = {
	render: function(varPanelNode, varPanelComponentXPath){
		var panelLayout = varPanelNode.getAttribute("layout");
		var componentName = varPanelNode.getAttribute("name");
		switch(panelLayout){
			case "grid":
				var numOfColumns = varPanelNode.getAttribute("nocolumns");				
				var componentList= getNodesByXPath(gAppDefXML, varPanelComponentXPath);
				var numOfRows = Math.ceil(componentList.length/parseInt(numOfColumns));
				console.log(numOfRows);
				// var table = document.createElement("TABLE"); 
				// var index=0;
				// for (var i = 0; i < numOfRows; i++) {
				// 	var row = table.insertRow(i);					
				// 	for (var j = 0; j < numOfColumns; j++) {
				// 		var cell = row.insertCell(j);	
				// 		console.log(index+j);
				// 		if(componentList[index+j]){
				// 			createControls(cell, componentList[index+j], varPanelComponentXPath);	
				// 		}						
				// 	};					
				// 	index = index+2;
				// };
				// return table;

				var gridDiv = document.createElement("div"); 
				var index=0;		
				for (var i = 0; i < numOfRows; i++) {
					var gridRowDiv = document.createElement("div"); 
					
					gridRowDiv.setAttribute("class", gGridRowCss[parseInt(numOfColumns)]);
					for (var j = 0; j < numOfColumns; j++) {
						var gridBlockDiv = document.createElement("div"); 
						gridBlockDiv.setAttribute("class", gGridColumnCss[j]);
						console.log(index+j);
						if(componentList[index+j]){
							createControls(gridBlockDiv, componentList[index+j], varPanelComponentXPath);	
							gridRowDiv.appendChild(gridBlockDiv);
						}
												
					};					
					gridDiv.appendChild(gridRowDiv);
					index = index+parseInt(numOfColumns);
				};
				return gridDiv;
				break;
			case "repeatable":	
				var elementList = ['test1','test2','test3', 'test4'];
				var componentList= getNodesByXPath(gAppDefXML, varPanelComponentXPath);
				var list = document.createElement('ul');
				list.setAttribute("id",componentName);
				list.setAttribute("data-role","listview");
				list.setAttribute("data-inset","true");
				for (var i = 0; i < elementList.length; i++) {
					var listItem = document.createElement('li');
					// var anchor = document.createElement('a');
					// //var targetLeaf = elementList[i].getAttribute("targetLeaf");
					// //anchor.setAttribute("onclick", "list.onListItemClick('"+ targetLeaf +"')");
					// anchor.innerText = elementList[i];
					// listItem.appendChild(anchor);
					for (var j = 0; j < componentList.length; j++) {
						createControls(listItem, componentList[j], varPanelComponentXPath);												
					};
					list.appendChild(listItem);
				};
				return list;
				break;
			default:
				break;
		}
	},
};

var button = {
	render: function(varID, varTitle, varCssClass, varSubType, varTargetLeaf){
			var btn = document.createElement("button");
			btn.setAttribute("type", "button");
			btn.setAttribute("id", varID);			
			btn.setAttribute("class", "ui-btn ui-shadow " + varCssClass);
			btn.setAttribute("onclick", "button.onButtonClick('"+ varSubType +"','"+ varTargetLeaf +"')");
			btn.innerHTML = varTitle;
        //alert(varTitle);
			return btn;
		},
	onButtonClick: function(varSubType, varTargetLeaf){		
		switch(varSubType){
			case "nextleaf":				
				page.nextPage(varTargetLeaf);
				break;
			case "back":
				page.previousPage();
				break;		
			case "exit":
				break;	
			default:
				break;
		}
	},
};

var list = {
	render: function(varComponentName, varListNode, varListComponentXPath, varComponentSubType){		
		switch(varComponentSubType){
			case "icontextlist":
				var elementList= getNodesByXPath(gAppDefXML, varListComponentXPath);
				var list = document.createElement('ul');
				list.setAttribute("id",varComponentName);
				list.setAttribute("data-role","listview");
				list.setAttribute("data-inset","true");
				for (var i = 0; i < elementList.length; i++) {
					var listItem = document.createElement('li');
					var anchor = document.createElement('a');
					var targetLeaf = elementList[i].getAttribute("targetLeaf");
					anchor.setAttribute("onclick", "list.onListItemClick('"+ targetLeaf +"')");
					var image = document.createElement('img');
					var h2 = document.createElement('h2');
					var imageSrc = elementList[i].getAttribute("image");
					image.setAttribute("src", gImageFolderPath+imageSrc);
                    image.setAttribute("class", "ui-li-icon");
					h2.innerText = elementList[i].getAttribute("title");
                    anchor.setAttribute("class", "ui-li-anchor-center");
					anchor.appendChild(image);
					anchor.appendChild(h2);
					listItem.appendChild(anchor);
					list.appendChild(listItem);
				};
				return list;
				break;
			case "plaintextlist":
				var elementList= getNodesByXPath(gAppDefXML, varListComponentXPath);
				var list = document.createElement('ul');
				list.setAttribute("id",varComponentName);
				list.setAttribute("data-role","listview");
				list.setAttribute("data-inset","true");
				for (var i = 0; i < elementList.length; i++) {
					var listItem = document.createElement('li');
					var anchor = document.createElement('a');
					var targetLeaf = elementList[i].getAttribute("targetLeaf");
					anchor.setAttribute("onclick", "list.onListItemClick('"+ targetLeaf +"')");
					anchor.innerText = elementList[i].getAttribute("title");
					listItem.appendChild(anchor);
					list.appendChild(listItem);
				};
				return list;
				break;	
			default:
				break;
		}
	},
	onListItemClick: function(varTargetLeaf){
		page.nextPage(varTargetLeaf);
	},
};

var datePicker = {
	render: function(varId,varValue,varPlaceHolder,varStyle,varCssClass,varReadOnly){
		var datePicker = document.createElement("input");
			datePicker.setAttribute("id",varId);			
			datePicker.setAttribute("data-role","date");
			datePicker.setAttribute("type","text");
			if(varReadOnly == "true"){
				datePicker.setAttribute("readOnly","readOnly");
			}
			$(datePicker).datepicker();
			return datePicker;		
	},
};

var radioButton = {
	render: function(varComponentName, varListNode, varListComponentXPath, varComponentSubType){		
		var elementList= getNodesByXPath(gAppDefXML, varListComponentXPath);		
		var fieldSet = document.createElement('fieldset');
		fieldSet.setAttribute("id",varComponentName);
		fieldSet.setAttribute("data-role","controlgroup");
		//fieldSet.setAttribute("data-type","horizontal");


		for (var i = 0; i < elementList.length; i++) {
			var input = document.createElement('input');
			var label = document.createElement('label');
			
			input.setAttribute("name", "radio_" + varComponentName);
			input.setAttribute("id", elementList[i].getAttribute("name"));
			input.setAttribute("value", elementList[i].getAttribute("title"));
			input.setAttribute("type", "radio");
			
			label.setAttribute("for", elementList[i].getAttribute("name"));
			label.innerText = elementList[i].getAttribute("title");
			
			
			
			fieldSet.appendChild(input);
			fieldSet.appendChild(label);
			
			
		};
		
		return fieldSet;
	},
};

var checkbox = {
render: function(varComponentName, varListNode, varListComponentXPath, varComponentSubType){
    var elementList= getNodesByXPath(gAppDefXML, varListComponentXPath);
    var fieldSet = document.createElement('fieldset');
    fieldSet.setAttribute("id",varComponentName);
    fieldSet.setAttribute("data-role","controlgroup");
    //fieldSet.setAttribute("data-type","horizontal");
    
    
    for (var i = 0; i < elementList.length; i++) {
        var input = document.createElement('input');
        var label = document.createElement('label');
        
        input.setAttribute("name", "checkbox_" + varComponentName);
        input.setAttribute("id", elementList[i].getAttribute("name"));
        input.setAttribute("value", elementList[i].getAttribute("title"));
        input.setAttribute("type", "checkbox");
        
        label.setAttribute("for", elementList[i].getAttribute("name"));
        label.innerText = elementList[i].getAttribute("title");
        
        
        
        fieldSet.appendChild(input);
        fieldSet.appendChild(label);
        
        
    };
    
    return fieldSet;
},
};

var image = {
	render: function(varComponent, varCssClass){
		var componentSubtype = varComponent.getAttribute("subtype")
		switch(componentSubtype){
			case "leftSlider":				
				var headerButtonLeft = document.createElement("a");
		        headerButtonLeft.setAttribute("href","#sliderMenuLeft");		        
		        headerButtonLeft.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-grid ui-btn-icon-notext ui-btn-left");				
		        return headerButtonLeft;
				break;
			case "rightSlider":				
				var headerButtonRight = document.createElement("a");
		        headerButtonRight.setAttribute("href","#sliderMenuRight");
		        headerButtonRight.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-gear ui-btn-icon-notext ui-btn-right");				
		        return headerButtonRight;
				break;
			case "backButton":				
				var headerButtonBack = document.createElement("a");
		        headerButtonBack.setAttribute("onclick", "image.previousPage()");
		        headerButtonBack.setAttribute("class","ui-btn ui-shadow ui-corner-all ui-icon-back ui-btn-icon-notext ui-btn-left");				
		        return headerButtonBack;
				break;		
			default:
				var anchor = document.createElement('a');		
				anchor.setAttribute("onclick", "image.onImageClick('"+ varComponent.getAttribute("targetLeaf") +"')");
				var image = document.createElement('img');		
				var imageSrc = varComponent.getAttribute("image");
				image.setAttribute("src", gImageFolderPath+imageSrc);
				image.setAttribute("class", varCssClass);		
				anchor.appendChild(image);
				return anchor;
				break;	
		}
				
	},
	onImageClick: function(varTargetLeaf){
		page.nextPage(varTargetLeaf);
	},
	previousPage: function(){
		var prevPageObject = $.mobile.activePage.prev('[data-role=page]');
		var prevPageId = null;
		if(prevPageObject.length !== 0){
			prevPageId = '#' + prevPageObject[0].id;
		}else{
			prevPageId = '#' + gHomeLeafName;
		}
				
		var currentPage = '#'+$.mobile.activePage.attr('id');
		$(currentPage).remove();
		$.mobile.changePage(prevPageId, {
		   transition: 'slide',
		   reverse: true,
		   changeHash: false
		});
		
	},
};

var h1 = {
	render: function(varId, varText, varCssClass){
			var h1Title = document.createElement("h1");
			h1Title.innerText = varText;
			return h1Title;
		},
};

jQuery.extend(jQuery.validator.messages, {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
    
});