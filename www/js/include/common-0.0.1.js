/*! Common - v0.0.1 - 26 June 2014
 * http://www.mobilewaretech.com
 * Includes: 
 * Copyright 2014 Mobileware Technologies Pvt Ltd. 
 * 
 * Author: Rohan Deshpande
 * Date  : 26 June 2014
 * Desc  : Include all Common reusable functions here
 */

function loadXMLFromFile(varFileName) {
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    } else // code for IE5 and IE6
    {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET", varFileName, false);
    try {
        xhttp.responseType = "msxml-document"
    } catch (err) {} // Helping IE
    xhttp.send("");
    return xhttp.responseXML;
}

function getXMLHttpRequest() {
    var xhttp = null;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    } else // code for IE5 and IE6
    {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xhttp;
}

//get a node based on value of specific attribute
function getNodeBasedOnAttributeValue(varXmlDoc, varNodeName, varAttributeName, varAttributeValue) {
    try {
        var nodes = varXmlDoc.getElementsByTagName(varNodeName);

        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].getAttribute(varAttributeName).toString().toUpperCase() == varAttributeValue.toUpperCase()) {
                return nodes[i];
            }
        }

        return null;

    } catch (e) {
        showErrorMessage('getNodeBasedOnAttributeValue', e.message);
    }
}

function getNodesByXPath(xml, path) {
    try {
        var nodesArr = [];
        // code for IE
        var xhttp = getXMLHttpRequest(); //new XMLHttpRequest();
        if (window.ActiveXObject || xhttp.responseType == "msxml-document") {
            xml.setProperty("SelectionLanguage", "XPath");
            var nodes = xml.selectNodes(path);
            for (i = 0; i < nodes.length; i++) {
                nodesArr.push(nodes[i]);
            }
        }

        // code for Chrome, Firefox, Opera, etc.
        else if (document.implementation && document.implementation.createDocument) {
            //debugger;
            var nodes = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null);

            /*var pathList = path.split("/");
		var reqNode = xml;

		for(var i=1;i<pathList.length-1;i++){
			
			if(i==1){
				reqNode = getReqNode(xml,pathList[i])
			}else if(i < pathList.length-1){
				reqNode = getReqNode(reqNode[0],pathList[i])
			}else if(i == pathList.length-1){
				nodes  = getReqNode(reqNode[0],pathList[i])
			}
			
		}
*/

            /*
		XPathJS.bindDomLevel3XPath();
		var nodes = xml.evaluate(
		    path , // XPath expression
		    xml, // context node
		    null, // namespace resolver
		    XPathResult.ANY_TYPE
		);*/

            var result = nodes.iterateNext();

            while (result) {
                nodesArr.push(result);
                result = nodes.iterateNext();
            }
        }
        return nodesArr;
    } catch (e) {
        showErrorMessage('getNodesByXPath', e.message);
    }
}

function getReqNode(dom, path) {
    var node = dom.getElementsByName(path);
    return node
}

function getNodeValueByXPath(xml, path) {
    try {
        var nodeValue = "";
        // code for IE
        var xhttp = getXMLHttpRequest(); //new XMLHttpRequest();
        if (window.ActiveXObject || xhttp.responseType == "msxml-document") {
            xml.setProperty("SelectionLanguage", "XPath");
            var nodes = xml.selectNodes(path);
            nodeValue = nodes[0].childNodes[0].nodeValue;
        }

        // code for Chrome, Firefox, Opera, etc.
        else if (document.implementation && document.implementation.createDocument) {
            var nodes = xml.evaluate(path, xml, null, XPathResult.ANY_TYPE, null);
            var result = nodes.iterateNext();
            nodeValue = result.childNodes[0].nodeValue;

            /*XPathJS.bindDomLevel3XPath();
		var nodes = xml.evaluate(
		    path , // XPath expression
		    xml, // context node
		    null, // namespace resolver
		    XPathResult.ANY_TYPE
		);
		
		var result=nodes.iterateNext();
		nodeValue = result.childNodes[0].nodeValue;*/
            // loop through results
            /*for (var i = 0; i < result.snapshotLength; i++) {
		    var node = result.snapshotItem(i);
		    //alert(node);
		}*/

        }
        return nodeValue;
    } catch (e) {
        showErrorMessage('getNodeValueByXPath', e.message);
    }
}

function textToXML(text) {
    var xmlDoc = null;
    try {
        if (window.DOMParser) {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(text, "text/xml");
        } else // Internet Explorer
        {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(text);
        }
        return (new XMLSerializer()).serializeToString(xmlDoc);
    } catch (e) {
        showErrorMessage('textToXML', e.message);
    }
}


/*
var viewData = { 
    employees : [] 
};
function onGeneratedRow(columnsResult)
{
    var jsonData = {};
    columnsResult.forEach(function(column) 
    {
        var columnName = column.metadata.colName;
        jsonData[columnName] = column.value;
    });
    viewData.employees.push(jsonData);
 }
 */



function getSessionTimer() {
    var session = sessionStorage.getItem('st') ? sessionStorage.getItem('st') : '';
    return session;
}

function setSessionTimer(st) {
    sessionStorage.setItem('st', st);
}




function getDynamicListData() {
    var dynamicListData = sessionStorage.getItem('dynamicListData') ? JSON.parse(sessionStorage.getItem('dynamicListData')) : {};
    return dynamicListData;
}

function setDynamicListData(newDynamicListData) {
    sessionStorage.setItem('dynamicListData', JSON.stringify(newDynamicListData));
}

function resetDynamicListData() {
    sessionStorage.removeItem('dynamicListData');
}

function getControllerData() {
    var controllerData = sessionStorage.getItem('controllerData') ? JSON.parse(sessionStorage.getItem('controllerData')) : {};
    return controllerData;
}

function setControllerData(newControllerData) {
    sessionStorage.setItem('controllerData', JSON.stringify(newControllerData));
}

function resetControllerData() {
    sessionStorage.removeItem('controllerData');
}

function getApplicationListData() {
    var appListData = sessionStorage.getItem('applicationListData') ? JSON.parse(sessionStorage.getItem('applicationListData')) : {};
    return appListData;
}

function setApplicationListData(newAppListData) {
    sessionStorage.setItem('applicationListData', JSON.stringify(newAppListData));
}

function resetApplicationListData() {
    sessionStorage.removeItem('applicationListData');
}

function getSelectedAppData() {
    var selectedAppData = sessionStorage.getItem('selectedAppData') ? JSON.parse(sessionStorage.getItem('selectedAppData')) : {};
    return selectedAppData;
}

function setSelectedAppData(newSelectedAppData) {
    sessionStorage.setItem('selectedAppData', JSON.stringify(newSelectedAppData));
}

function resetSelectedAppData() {
    sessionStorage.removeItem('selectedAppData');
}

function getPreviousStateName() {
    var previousStateName = sessionStorage.getItem('previousStateName') ? sessionStorage.getItem('previousStateName') : "";
    return previousStateName;
}

function setPreviousStateName(newPreviousStateName) {
    sessionStorage.setItem('previousStateName', newPreviousStateName);
}

function resetPreviousStateName() {
    sessionStorage.removeItem('previousStateName');
}

function getAppId() {
    var appId = localStorage.getItem('appId') ? localStorage.getItem('appId') : "";
    return appId;
}

function setAppId(newAppId) {
    localStorage.setItem('appId', newAppId);
}

function resetAppId() {
    localStorage.removeItem('appId');
}

function getFirstLeafName() {
    var firstLeafName = sessionStorage.getItem('firstLeafName') ? sessionStorage.getItem('firstLeafName') : "";
    return firstLeafName;
}

function setFirstLeafName(newFirstLeafName) {
    sessionStorage.setItem('firstLeafName', newFirstLeafName);
}

function resetFirstLeafName() {
    sessionStorage.removeItem('firstLeafName');
}

function getFirstLeafNameChildApp() {
    var firstLeafNameChildApp = localStorage.getItem('firstLeafNameChildApp') ? localStorage.getItem('firstLeafNameChildApp') : "";
    return firstLeafNameChildApp;
}

function setFirstLeafNameChildApp(newFirstLeafNameChildApp) {
    localStorage.setItem('firstLeafNameChildApp', newFirstLeafNameChildApp);
}

function resetFirstLeafNameChildApp() {
    localStorage.removeItem('firstLeafNameChildApp');
}

function getHomeLeafName() {
    var homeLeafName = sessionStorage.getItem('homeLeafName') ? sessionStorage.getItem('homeLeafName') : "";
    return homeLeafName;
}

function setHomeLeafName(newHomeLeafName) {
    sessionStorage.setItem('homeLeafName', newHomeLeafName);
}

function resetHomeLeafName() {
    sessionStorage.removeItem('homeLeafName');
}

function getHomeLeafNameChildApp() {
    var homeLeafNameChildApp = localStorage.getItem('homeLeafNameChildApp') ? localStorage.getItem('homeLeafNameChildApp') : "";
    return homeLeafNameChildApp;
}

function setHomeLeafNameChildApp(newHomeLeafNameChildApp) {
    localStorage.setItem('homeLeafNameChildApp', newHomeLeafNameChildApp);
}

function resetHomeLeafNameChildApp() {
    localStorage.removeItem('homeLeafNameChildApp');
}

function getMobileUserId() {
    var mobileUserId = localStorage.getItem('mobileUserId') ? localStorage.getItem('mobileUserId') : "";
    return mobileUserId;
}

function setMobileUserId(newMobileUserId) {
    localStorage.setItem('mobileUserId', newMobileUserId);
}

function resetMobileUserId() {
    localStorage.removeItem('mobileUserId');
}

function getDeviceId() {
    var deviceId = localStorage.getItem('deviceId') ? localStorage.getItem('deviceId') : 0;
    return deviceId;
}

function setDeviceId(newDeviceId) {
    localStorage.setItem('deviceId', newDeviceId);
}

function resetDeviceId() {
    localStorage.removeItem('deviceId');
}

function getDeviceOSVersion() {
    var osVersion = localStorage.getItem('OSVersion') ? localStorage.getItem('OSVersion') : 0;
    return osVersion;
}

function setDeviceOSVersion(newOSVersion) {
    localStorage.setItem('OSVersion', newOSVersion);
}

function resetDeviceOSVersion() {
    localStorage.removeItem('OSVersion');
}

function getAppVersion() {
    var appVersion = localStorage.getItem('appVersion') ? localStorage.getItem('appVersion') : "";
    return appVersion;
}

function setAppVersion(newAppVersion) {
    localStorage.setItem('appVersion', newAppVersion);
}

function resetAppVersion() {
    localStorage.removeItem('appVersion');
}

function getCNO() {
    var c = localStorage.getItem('gCNO') ? localStorage.getItem('gCNO') : "";
    var d = decryptText(c);
    return d;
}

function setCNO(cNO) {
    var e = encryptText(cNO);
    localStorage.setItem('gCNO', e);
}

function getAppData() {
    var appData = sessionStorage.getItem('appData') ? JSON.parse(sessionStorage.getItem('appData')) : {};
    return appData;
}

function getPIN() {
    var c = sessionStorage.getItem('gPin') ? sessionStorage.getItem('gPin') : "";
    var d = decryptText(c);
    return d;
}

function setPIN(pin) {
    var e = encryptText(pin);
    sessionStorage.setItem('gPin', e);
}

function getAppData() {
    var appData = sessionStorage.getItem('appData') ? JSON.parse(sessionStorage.getItem('appData')) : {};
    return appData;
}

function setAppData(appData) {
    sessionStorage.setItem('appData', JSON.stringify(appData));
}

function resetAppData() {
    sessionStorage.removeItem('appData');
}

function showErrorMessage(functionName, errorMessage) {
    console.log(functionName + ' : ' + errorMessage);
    // alert(functionName + ' : '+ errorMessage);	
}


var httpPOST = function(options) {
    try {
        var initInjector = angular.injector(["ng"]);
        var $http = initInjector.get("$http");
        var $q = initInjector.get("$q");
        var deferred = $q.defer();
        var success = options.success;
        var error = options.error;
        $http.post(options.url, options.data)
            .success(function(data, status, headers, config) {
                deferred.resolve(success(data, status, headers, config));
            }).error(function(data, status, headers, config) {
                deferred.reject(data);
                error(data, status, headers, config);
            });
        return deferred.promise;
    } catch (e) {
        showErrorMessage('httpPOST', e.message);
    }
}

function resetLeafData(leafName, appData, exceptionFields) {
    try {
        for (var template in appData.views) {
            if (appData.views[template].title === leafName) {
                var dataNode = appData.views[template].data;
                //console.log(dataNode);

                for (var keyName in dataNode) {

                    if (exceptionFields.indexOf(keyName) < 0) {
                        //dataNode[keyName] = "";
                        //console.log(keyName);
                        delete dataNode[keyName];
                        /*var dynamicList = getDynamicListData();

	      			if(dynamicList[keyName]){
	      				dynamicList[keyName] = "";
	      			}*/
                    }
                }
            }
        }

        return appData;
    } catch (e) {
        showErrorMessage('resetLeafData', e.message);
    }
}

function resetScopeData(scopeData, exceptionFields) {
    try {

        //var dataNode = appData.views[template].data;
        //console.log(dataNode);

        for (var keyName in scopeData) {

            if (exceptionFields.indexOf(keyName) < 0) {
                scopeData[keyName] = "";
                //console.log(keyName);
                //delete dataNode[keyName];
                /*var dynamicList = getDynamicListData();

				if(dynamicList[keyName]){
					dynamicList[keyName] = "";
				}*/
            }
        }


        return scopeData;
    } catch (e) {
        showErrorMessage('resetScopeData', e.message);
    }
}

function getFormattedTime() {
    try {
        var d = new Date();
        var x = document.getElementById("demo");
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var dt = h + ":" + m + ":" + s + ":" + ms;
        return dt;
    } catch (e) {
        showErrorMessage('getFormattedTime', e.message);
    }
}

function addZero(x, n) {
    if (x.toString().length < n) {
        x = "0" + x;
    }
    return x;
}

function encryptText(text2Encrypt) {
    try {
        var encryptedText = CryptoJS.TripleDES.encrypt(text2Encrypt, CryptoJS.enc.Hex.parse(key), {
            iv: CryptoJS.enc.Hex.parse(iv),
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString();

        return encryptedText;
    } catch (e) {
        showErrorMessage('encryptText', e.message);
    }
}

function decryptText(text2Decrypt) {
    try {
        var decryptedText = CryptoJS.TripleDES.decrypt(text2Decrypt, CryptoJS.enc.Hex.parse(key), {
            iv: CryptoJS.enc.Hex.parse(iv),
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        //alert('utf8 decrypted ' + decrypted.toString(CryptoJS.enc.Utf8));
        return decryptedText.toString(CryptoJS.enc.Utf8);
    } catch (e) {
        showErrorMessage('decryptText', e.message);
    }
}

function generateSMS() {
    try {
        var separator = "|";
        var dynamicList = getDynamicListData();

        sessionStorage.setItem('regDeviceTime', getFormattedTime());
        var unEncryptedSMS = dynamicList['customerNo'] + "|" + device.uuid + "|" + sessionStorage.getItem('regDeviceTime');
        var encSMSPart = encryptText(unEncryptedSMS);

        var SMS = "CIFVAL " + encSMSPart;



        var messageInfo = {
            phoneNumber: "+919212438888",
            textMessage: SMS
        };

        sms.sendMessage(messageInfo, function(message) {
            //console.log("success: " + message);
            //alert("Success");
        }, function(error) {
            //console.log("code: " + error.code + ", message: " + error.message);
            //alert("error.message");
        });


        //CONFIGURATION
        /*var options = {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
                //intent: 'INTENT'  // send SMS with the native android SMS messaging
                intent: '' // send SMS without open any other app
            }
        };

        //9212438888
        var success = function () {  };
        var error = function (e) { alert('Message Failed:' + e); };
        sms.send("919212438888", SMS, options, success, error);*/
    } catch (e) {
        showErrorMessage('generateSMS', e.message);
    }
}

function getDataValueFromLeaf(appData, inputParamLeafName, inputParamComponentName) {
    try {
        var value = "";

        for (var template in appData.views) {
            if (appData.views[template].title === inputParamLeafName) {
                var leafData = appData.views[template].data;
                
                value = leafData[inputParamComponentName];

                if (!value) {
                    value = "";
                }
            }
        }

        return value;
    } catch (e) {
        showErrorMessage('getDataValueFromLeaf', e.message);
    }
}

function setDataValueInLeaf(appData, inputParamLeafName, inputParamComponentName,value) {
    try {
        
        for (var template in appData.views) {
            if (appData.views[template].title === inputParamLeafName) {
                appData.views[template].data[inputParamComponentName]=value;
            }
        }
        setAppData(appData);

    } catch (e) {
        showErrorMessage('setDataValueInLeaf', e.message);
    }
}

function onInputChange(e, name) {
    try {
        var textValue = e.value;
        var re = /^(?!.*([0-9])\1{2})(?=.*\d)[0-9]+$/;
        var output = re.test(textValue);


        if (output) {
            if (e.value.length < 6) {
                var textValue = e.value;
                var lasDigit = textValue.substring(0, textValue.length - 1);

            }

            setSessionTimer(0);

            var value = e.value;
            console.log(e.value);
            value = value.replace("*", "");

            //sessionStorage.setItem("password" ,)

            console.log(value);
            var i = 0;
            var maskPassword = "";
            while (i < e.value.length) {
                maskPassword += "*";
                i++;
            }
            e.value = maskPassword;
        } else {
            e.value = textValue.substring(0, textValue.length - 1);
        }
    } catch (e) {
        showErrorMessage('onInputChange', e.message);
    }

}

function getYears() {
    try {
        var d = new Date();
        var n = parseInt(d.getFullYear());
        var genYears = [];
        var yrs = {};
        yrs["type"] = n.toString().substring(2);
        yrs["mode"] = n;
        genYears.push(yrs);


        for (var k = 0; k < 10; k++) {
            n = n + 1;
            var yrs = {};
            yrs["type"] = n.toString().substring(2);
            yrs["mode"] = n;
            genYears.push(yrs);
        }

        return genYears;
    } catch (e) {
        showErrorMessage('getYears', e.message);
    }
}

function getNoJSON(varMaxCount) {
    try {
        var genJson = [];

        for (var k = 0; k < varMaxCount; k++) {
            var data = {};
            data["type"] = k
            data["mode"] = k;
            genJson.push(data);
        }

        return genJson;
    } catch (e) {
        showErrorMessage('getNoJSON', e.message);
    }
}

function getNoJSONWO0(varMaxCount) {
    try {
        var genJson = [];

        for (var k = 1; k < varMaxCount; k++) {
            var data = {};
            data["type"] = k
            data["mode"] = k;
            genJson.push(data);
        }

        return genJson;
    } catch (e) {
        showErrorMessage('getNoJSONWO0', e.message);
    }
}


