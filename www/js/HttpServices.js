
var appServices = angular.module('appStudioStarter.petBubHttpService', []);
appServices.factory('PetBubHttpService', function ($http, $q, GlobalConstants) {
    var caledarServFactory = {};
    var deferred = $q.defer();
    var _calendarService = function (data) {
        var deferred = $q.defer();
        $http.post(GlobalConstants.defaultEndPoint + '/n', data).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
    caledarServFactory.calendarService = _calendarService;
    return caledarServFactory;
})