var db = null;

//define(['angular', 'angularIonic', 'controllers', 'services'], function (angular) {
define(['controllers', 'services'], function () {
    // Ionic Starter App
    var $stateProviderRef = null;
    var $urlRouterProviderRef = null;
    window.routerStateList = [];
    window.providers = {};

    var appStudioStarter = angular.module('appStudioStarter', ['ionic', 'appStudioStarter.controllers', 'appStudioStarter.services', 'appStudioStarter.petBubHttpService', 'flexcalendar', 'pascalprecht.translate', 'ngCordova', 'ion-gallery', 'ionic.rating', 'ionic-native-transitions', 'starter.constants'], function ($controllerProvider, $compileProvider, $provide) {
        window.providers = {
            $controllerProvider: $controllerProvider,
            $compileProvider: $compileProvider,
            $provide: $provide
        };
    });

    appStudioStarter.config(function ($ionicNativeTransitionsProvider) {
        $ionicNativeTransitionsProvider.setDefaultOptions({
            type: 'slide',
            direction: 'left',
            duration: 500, // in milliseconds (ms), default 800, 
            backInOppositeDirection: true
        });
    })

    appStudioStarter.config(function ($locationProvider, $stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider, $httpProvider) {
        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.swipeBackEnabled(false);
        $ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
        $ionicConfigProvider.backButton.text('').icon('bmbBack');
        $ionicConfigProvider.platform.android.navBar.alignTitle('center');
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);
        //$ionicConfigProvider.platform.android.backButton.previousTitleText('').icon('ion-android-arrow-back');


        $stateProvider.state("Abstract", {
            cache: false,
            url: "/",
            template: "<div ui-view></div>",
            controller: "AbstractController"

        });

        $urlRouterProviderRef = $urlRouterProvider;
        $stateProviderRef = $stateProvider;
        $locationProvider.html5Mode(false);
        window.gStateProviderRef = $stateProviderRef;
        //$urlRouterProvider.otherwise('/'); // if none of the above states are matched, use this as the fallback
    });

    appStudioStarter.run(['$q', '$rootScope', '$state', '$ionicPopup', '$ionicPlatform', 'globalService', '$cordovaFile', '$cordovaSQLite', 'Global', 'GlobalConstants',
        function ($q, $rootScope, $state, $ionicPopup, $ionicPlatform, globalService, $cordovaFile, $cordovaSQLite, Global, GlobalConstants) {
            $ionicPlatform.ready(function () {
                // db = window.sqlitePlugin.openDatabase({name: 'Petbubs.db', location: 'default'});
                // $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS UserWall (id integer primary key, PId integer,customerId text,PhotoId text,MyLike text,VideoId text)");
                // console.log(db);

                if (window.cordova && window.cordova.plugins.Keyboard) {

                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(false);
                    $cordovaFile.createDir(cordova.file.externalRootDirectory, "Petbubs", false).then(function (success) {
                        // alert('success');
                    }, function (error) {
                        //alert('error '+JSON.stringify(error));
                    });
                    $cordovaFile.createDir(cordova.file.externalRootDirectory, "Petbubs/Videos", false).then(function (success) {
                        // alert('success');
                    }, function (error) {
                        //alert('error '+JSON.stringify(error));
                    });
                    
                }


                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    StatusBar.styleDefault();
                }
                $ionicPlatform.isFullScreen = true;
            });

            var firstLeafName = getFirstLeafName(); //""; //localStorage.getItem('firstLeafName');
            var homeLeafName = getHomeLeafName(); //localStorage.getItem('homeLeafName');
            $ionicPlatform.registerBackButtonAction(function (event) {
                if ($state.current.name === "Login" || $state.current.name === firstLeafName) {
                    globalService.showConfirmation('Exit Application', 'Are you sure. Continue?', function () {
                        resetAppData();
                        navigator.app.exitApp();
                    }, function () {
                        event.preventDefault();
                    });
                } else {
                    event.preventDefault();
                }

            }, 100);
            var testInternet = function () {
                $.ajax({
                    url: "http://www.google.fr/blank.html",
                    timeout: 5000, //timeout to 5s
                    type: "GET",
                    cache: false
                }).done(function () {
                    alert('true');
                    globalService.setOnlineStatus(true);
                }).fail(function () {
                    alert('false');
                    globalService.setOnlineStatus(false);
                });
            }
            document.addEventListener("online", function () {
                globalService.setOnlineStatus(true); //testInternet();
            }, false);
            document.addEventListener("offline", function () {
                globalService.setOnlineStatus(false);
            }, false);

            if (navigator && navigator.connection && navigator.connection.type === "none") {
                globalService.setOnlineStatus(false);
            } else {
                globalService.setOnlineStatus(true);
            }

            var appData = getAppData();
            for (var template in appData.views) {
                if (window.routerStateList.indexOf(appData.views[template].title.toString()) < 0) {
                    window.routerStateList.push(appData.views[template].title.toString());
                    $stateProviderRef.state(appData.views[template].title, {
                        cache: false,
                        url: appData.views[template].url,
                        template: appData.views[template].template,
                        controller: appData.views[template].controller
                    });
                }
            }

            var showFirstScreen = function () {

                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

                if (isContainerApp) {
                    setPreviousStateName($state.current.name);
                    if (getMobileUserId().toString().length > 0) {
                        getAppsForContainer();
                    }
                } else if (customerId.length > 0) {
                    $state.go(homeLeafName);
                } else {
                    $state.go(firstLeafName);
                }
            }

            if (isContainerApp) {
                var deviceInfoRequest = function () {
                    console.log('in deviceInfoRequest');
                    try {
                        var uid = null;
                        if (device.platform.toString() === "Android") {
                            uid = cordova.plugins.uid;
                        }
                        var serviceRequest = {};
                        var request = {};
                        var requestParams = [];
                        if (uid) {
                            requestParams.push({
                                k: "IMEI",
                                v: uid.IMEI
                            });
                        } else {
                            requestParams.push({
                                k: "IMEI",
                                v: device.uuid
                            });
                        }

                        requestParams.push({
                            k: "platform",
                            v: device.platform
                        });
                        requestParams.push({
                            k: "deviceMake",
                            v: device.model
                        });
                        requestParams.push({
                            k: "deviceModel",
                            v: device.model
                        });
                        requestParams.push({
                            k: "screenWidth",
                            v: (window.innerWidth * window.devicePixelRatio).toString()
                        });
                        requestParams.push({
                            k: "screenHeight",
                            v: (window.innerHeight * window.devicePixelRatio).toString()
                        });
                        requestParams.push({
                            k: "OSVersion",
                            v: device.version.toString()
                        });
                        request["params"] = requestParams;
                        request["deviceId"] = getDeviceId();
                        request["service"] = "DeviceReg";
                        serviceRequest["req"] = request;
                        return serviceRequest;
                    } catch (e) {
                        showErrorMessage('deviceInfoRequest', e.message);
                    }
                }

                var deviceVersionRequest = function () {
                    console.log('in deviceVersionRequest');
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    requestParams.push({
                        k: "OSVersion",
                        v: device.version.toString()
                    });
                    requestParams.push({
                        k: "deviceId",
                        v: getDeviceId().toString()
                    });
                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["service"] = "UpdateOS";
                    serviceRequest["req"] = request;
                    return serviceRequest;
                }

                var remoteWipeRequest = function (servicename) {
                    console.log('in remoteWipeRequest');
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    requestParams.push({
                        k: "deviceId",
                        v: getDeviceId().toString()
                    });
                    requestParams.push({
                        k: "deviceStatus",
                        v: "0"
                    });
                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["service"] = servicename;
                    serviceRequest["req"] = request;
                    return serviceRequest;
                }

                var getAppsForContainerRequest = function () {
                    console.log('in getAppsForContainerRequest');
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    requestParams.push({
                        k: "userId",
                        v: getMobileUserId().toString()
                    });
                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["service"] = "GetAppsForContainer";
                    serviceRequest["req"] = request;
                    return serviceRequest;
                }

                var getAppsForContainer = function () {
                    if (globalService.getOnlineStatus()) {
                        var appListRequest = getAppsForContainerRequest();
                        httpPOST({
                            url: gAppStudioServerURL,
                            data: appListRequest,
                            success: function (data, status, headers, config) {
                                alert('success callback ' + JSON.stringify(data) + ' ' + status + ' ' + headers + ' ' + config);
                                var output = data.op;
                                if (output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                                    for (var opIndex = 0; opIndex < output.length; opIndex++) {
                                        var opKey = output[opIndex].k;
                                        var opValue = output[opIndex].v;
                                        switch (opKey.toString()) {
                                            case "AppList":
                                                var appDetails = opValue;
                                                //alert('isContainerApp ' + isContainerApp);
                                                if (isContainerApp) {
                                                    //alert('appData ' + targetLeaf);
                                                    appStudioDB.openDBInstance();
                                                    appStudioDB.createTablesRecursively(0);
                                                    appStudioDB.getAllApps(function (tx, rs) {
                                                        //Check if App exists
                                                        if (rs && rs.rows.length > 0) {
                                                            //for(var rowIndex=0; rowIndex<rs.rows.length; rowIndex++){
                                                            var dbAppId = rs.rows.item(0).AppId;
                                                            var appExists = false;
                                                            for (var appIndex = 0; appIndex < appDetails.length; appIndex++) {
                                                                var appId = appDetails[appIndex].appId;
                                                                if (appId.toString() === dbAppId.toString()) {
                                                                    appExists = true;
                                                                }
                                                            }
                                                            if (appExists === false) {
                                                                appStudioDB.deleteApplication(dbAppId, function () {
                                                                    deleteApplication(1, rs.rows, appDetails);
                                                                });
                                                            } else {
                                                                deleteApplication(1, rs.rows, appDetails);
                                                            }


                                                            //}
                                                        } else {
                                                            appStudioDB.setAppListForContainter(appDetails, appDetails.length, 0, function () {
                                                                getAppListFromDB();
                                                            });
                                                        }

                                                    });

                                                    // appStudioDB.selectAllRecords("Application", "AppId", function(tx, rs){
                                                    //   //Uncomment for normal listing
                                                    //   var appList = [];
                                                    //   for(var rowIndex=0; rowIndex<rs.rows.length; rowIndex++){
                                                    //     var appItem = rs.rows.item(rowIndex);
                                                    //     if(appItem.AppLogo.toString().length === 0){
                                                    //       appItem.AppLogo = "default_app_icon.png";
                                                    //     }
                                                    //     //alert(JSON.stringify(appItem));
                                                    //     appList.push(appItem);
                                                    //   }
                                                    //   var dynamicList = getDynamicListData();
                                                    //   dynamicList[opKey] = appList;
                                                    //   setDynamicListData(dynamicList);
                                                    // });


                                                } else {

                                                }
                                                break;
                                        }
                                    }
                                }
                            },
                            error: function (data, status, headers, config) {
                                console.log('error callback ' + data + '-' + status);
                                //$globalService.hideLoading();
                            },
                        });
                    } else {
                        globalService.showAlert('Internet Disconnected', 'The internet is disconnected on your device.', getAppListFromDB);
                    }
                }


                var deleteApplication = function (rowIndex, dbApps, appDetails) {
                    if (rowIndex < dbApps.length) {
                        var dbAppId = dbApps.item(rowIndex).AppId;
                        var appExists = false;
                        for (var appIndex = 0; appIndex < appDetails.length; appIndex++) {
                            var appId = appDetails[appIndex].appId;
                            if (appId.toString() === dbAppId.toString()) {
                                appExists = true;
                            }
                        }
                        if (appExists === false) {
                            appStudioDB.deleteApplication(dbAppId, function () {
                                deleteApplication(rowIndex + 1, dbApps, appDetails);
                            });
                        } else {
                            deleteApplication(rowIndex + 1, dbApps, appDetails);
                        }
                    } else {
                        appStudioDB.setAppListForContainter(appDetails, appDetails.length, 0, function () {
                            getAppListFromDB();
                        });
                    }
                }

                var checkRemoteWipeStatus = function () {
                    if (globalService.getOnlineStatus()) {
                        var rwRequest = remoteWipeRequest("CheckDevice");
                        httpPOST({
                            url: gAppStudioServerURL,
                            data: rwRequest,
                            success: function (data, status, headers, config) {
                                //alert('success callback ' + JSON.stringify(data) + ' ' + status + ' ' + headers + ' ' + config);
                                var output = data.op;
                                if (output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                                    //showFirstScreen();
                                    if (output[1].k.toString() === "DeviceStatus" && output[1].v.toString() === "1") {
                                        setMobileUserId("");
                                        globalService.showConfirmation('Remote Wipe Enabled', 'This will clear all application data stored locally. Continue?', remoteWipeSuccess, showAppExit);
                                    } else {
                                        showFirstScreen();
                                    }

                                } else {
                                    globalService.showAlert('Remote wipe', 'Invalid request. Please try again.', showFirstScreen);
                                }
                            },
                            error: function (data, status, headers, config) {
                                console.log('error callback ' + data + '-' + status);
                                //$globalService.hideLoading();
                            },
                        });
                    } else {
                        globalService.showAlert('Internet Disconnected', 'The internet is disconnected on your device.', showFirstScreen);
                    }
                }

                var currentDeviceId = getDeviceId();
                if (parseInt(currentDeviceId) === 0) {
                    if (globalService.getOnlineStatus()) {
                        var request = deviceInfoRequest();
                        console.log(JSON.stringify(request));
                        httpPOST({
                            url: gAppStudioServerURL,
                            data: request,
                            success: function (data, status, headers, config) {
                                alert('success callback ' + JSON.stringify(data) + ' ' + status + ' ' + headers + ' ' + config);
                                var output = data.op;
                                if (output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                                    if (output[1].k.toString() === "DeviceId") {
                                        setDeviceId(output[1].v.toString());
                                    }
                                    setDeviceOSVersion(device.version.toString());
                                    checkRemoteWipeStatus();
                                } else {
                                    globalService.showAlert('Register Device', 'Invalid request. Please try again.');
                                }
                            },
                            error: function (data, status, headers, config) {
                                console.log('error callback ' + data + '-' + status);
                                //$globalService.hideLoading();
                            },
                        });
                    } else {
                        $globalService.showAlert('Internet Disconnected', 'The internet is disconnected on your device.');
                    }
                } else {
                    var currentOSVersion = getDeviceOSVersion();
                    if (currentOSVersion.toString() !== device.version.toString()) {
                        var request = deviceVersionRequest();
                        httpPOST({
                            url: gAppStudioServerURL,
                            data: request,
                            success: function (data, status, headers, config) {
                                alert('success callback ' + JSON.stringify(data) + ' ' + status + ' ' + headers + ' ' + config);
                                var output = data.op;
                                if (output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                                    if (output[1].k.toString() === "OSUpdated") {
                                        setDeviceOSVersion(device.version.toString());
                                    }
                                    checkRemoteWipeStatus();
                                } else {
                                    globalService.showAlert('Update OS', 'Invalid request. Please try again.');
                                }
                            },
                            error: function (data, status, headers, config) {
                                console.log('error callback ' + data + '-' + status);
                                //$globalService.hideLoading();
                            },
                        });
                    } else {
                        if (isContainerApp) {
                            checkRemoteWipeStatus();
                        }
                    }
                }

                var remoteWipeSuccess = function () {
                    appStudioDB.openDBInstance();
                    appStudioDB.dropTablesRecursively(0);
                    var rwRequest = remoteWipeRequest("UpdateDevice");
                    httpPOST({
                        url: gAppStudioServerURL,
                        data: rwRequest,
                        success: function (data, status, headers, config) {
                            //alert('success callback ' + JSON.stringify(data) + ' ' + status + ' ' + headers + ' ' + config);
                            var output = data.op;
                            if (output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                                if (output[1].k.toString() === "DeviceWiped") {
                                    globalService.showAlert('Remote Wipe', 'Data cleared successfully', showFirstScreen);
                                }
                            } else {
                                globalService.showAlert('Remote wipe', 'Invalid request. Please try again.');
                            }
                        },
                        error: function (data, status, headers, config) {
                            console.log('error callback ' + data + '-' + status);
                            //$globalService.hideLoading();
                        },
                    });
                }

                var getAppListFromDB = function () {
                    appStudioDB.openDBInstance();
                    appStudioDB.selectAllRecords("Application", "AppId", function (tx, rs) {
                        //Uncomment for normal listing
                        var appList = [];
                        for (var rowIndex = 0; rowIndex < rs.rows.length; rowIndex++) {
                            var appItem = rs.rows.item(rowIndex);
                            if (appItem.AppLogo.toString().length === 0) {
                                appItem.AppLogo = "default_app_icon.png";
                            }
                            //alert(JSON.stringify(appItem));
                            appList.push(appItem);
                        }
                        var dynamicList = getDynamicListData();
                        dynamicList["AppList"] = appList;
                        setDynamicListData(dynamicList);
                        $state.go(homeLeafName);

                    });
                }


            } else {
                showFirstScreen();
            }
            var showAppExit = function () {
                globalService.showAlert('Exit', 'Closing application', function () {
                    navigator.app.exitApp();
                });
            }
            //$state.go(firstLeafName);
            //showFirstScreen();

            // $ionicPopup.alert({
            //    title: "Internet Disconnected",
            //    content:  "The internet is disconnected on your device."
            //  }).then(function(res) {

            //  });



        }
    ]);

    /*appStudioStarter.run(function($ionicPlatform) {
		$ionicPlatform.ready(function() {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			if(window.cordova && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			}
			if(window.StatusBar) {
				StatusBar.styleDefault();
			}

		});
	});*/
    appStudioStarter.directive('sidebarDirective', function () {
        return {
            link: function (scope, element, attr) {

                // scope.$watch(attr.sidebarDirective, function(newVal) {
                //       console.log("D");
                //       if(newVal){
                //         element.addClass('show');
                //         document.getElementById('overlay').className="sidemenu-background-overlay";
                //         document.getElementById('navigation-toggle').className="";
                //         document.getElementById('overlay').style.opacity = 0.7;
                //         return;
                //       }
                //       document.getElementById('overlay').classList.remove("sidemenu-background-overlay");
                //       document.getElementById('overlay').style.opacity = 0;

                //       setTimeout(function(){
                //             document.getElementById('navigation-toggle').classList.add("ion-navicon-round")
                //       },400);

                //       element.removeClass('show');
                //     });
            }
        };
    })

    appStudioStarter.directive('resize', function ($window) {
        return {
            link: function (scope, element, attr) {
                var size = attr.resize;
                if (size === 'imgSize') {
                    var imgHeight = (window.screen.width - (window.screen.width / 4));
                    element.css('height', imgHeight + 'px');
                } else {
                    var noHeader = (window.innerHeight - 55);
                    element.css('height', noHeader * size / 100 + 'px');
                }
            }
        }
    })

    appStudioStarter.directive('autoNext', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr, form) {
                var tabindex = parseInt(attr.tabindex);
                var maxLength = parseInt(attr.ngMaxlength);
                element.bind('keyup', function (e) {
                    if (e.which == 8 || e.which == 229) {
                        if (element.val().length == 0) {
                            var prev = $('[tabindex=' + (tabindex - 1) + ']');
                            if (prev.length > 0) {
                                prev.focus();
                                return prev.triggerHandler('keypress', { which: e.which });
                            }
                        }
                    } else if (element.val().length > maxLength - 1) {
                        var next = $('[tabindex=' + (tabindex + 1) + ']');
                        if (next.length > 0) {
                            next.focus();
                            return next.triggerHandler('keypress', { which: e.which });
                        } else {
                            return false;
                        }
                    }
                    return true;
                });

            }
        }
    })

    appStudioStarter.directive('ionRadioFix', function () {
        return {
            restrict: 'E',
            replace: true,
            require: '?ngModel',
            transclude: true,
            template:
            '<label class="item item-radio">' +
            '<input type="radio" name="radio-group">' +
            '<div class="radio-content">' +
            '<div class="item-content disable-pointer-events" ng-transclude></div>' +
            '<i class="radio-icon disable-pointer-events icon ion-checkmark"></i>' +
            '</div>' +
            '</label>',

            compile: function (element, attr) {
                if (attr.icon) {
                    var iconElm = element.find('i');
                    iconElm.removeClass('ion-checkmark').addClass(attr.icon);
                }

                var input = element.find('input');
                angular.forEach({
                    'name': attr.name,
                    'value': attr.value,
                    'disabled': attr.disabled,
                    'ng-value': attr.ngValue,
                    'ng-model': attr.ngModel,
                    'ng-disabled': attr.ngDisabled,
                    'ng-change': attr.ngChange,
                    'ng-required': attr.ngRequired,
                    'required': attr.required
                }, function (value, name) {
                    if (angular.isDefined(value)) {
                        input.attr(name, value);
                    }
                });

                return function (scope, element, attr) {
                    scope.getValue = function () {
                        return scope.ngValue || attr.value;
                    };
                };
            }
        };
    })

    appStudioStarter.directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            var ensureCompileRunsOnce = scope.$watch(
                function (scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function (value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);

                    // Use un-watch feature to ensure compilation happens only once.
                    ensureCompileRunsOnce();
                }
            );
        };
    }])

    appStudioStarter.directive('fadeIn', function ($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $element, attrs) {
                $element.addClass("ng-hide-remove");
                $element.on('load', function () {
                    $element.addClass("ng-hide-add");
                });
            }
        };
    })
    appStudioStarter.directive('showErrors', function ($timeout) {
        return {
            restrict: 'A',
            require: '^form',
            link: function (scope, el, attrs, formCtrl) {
                // find the text box element, which has the 'name' attribute
                var inputEl = el[0].querySelector("[name]");
                // convert the native text box element to an angular element
                var inputNgEl = angular.element(inputEl);
                // get the name on the text box
                var inputName = inputNgEl.attr('name');

                // only apply the has-error class after the user leaves the text box
                inputNgEl.bind('blur', function () {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });

                scope.$on('show-errors-check-validity', function () {
                    el.toggleClass('has-error', formCtrl[inputName].$invalid);
                });

                scope.$on('show-errors-reset', function () {
                    $timeout(function () {
                        el.removeClass('has-error');
                    }, 0, false);
                });
            }
        }
    })

    appStudioStarter.directive('onValidSubmit', ['$parse', '$timeout', function ($parse, $timeout) {
        return {
            require: '^form',
            restrict: 'A',
            link: function (scope, element, attrs, form) {
                form.$submitted = false;
                var fn = $parse(attrs.onValidSubmit);
                element.on('submit', function (event) {
                    scope.$apply(function () {
                        element.addClass('ng-submitted');
                        form.$submitted = true;
                        if (form.$valid) {
                            if (typeof fn === 'function') {
                                fn(scope, { $event: event });
                            }
                        }
                    });
                });
            }
        }
    }]);

    appStudioStarter.directive('validated', ['$parse', function ($parse) {
        return {
            restrict: 'AEC',
            require: '^form',
            link: function (scope, element, attrs, form) {
                var inputs = element.find("*");
                for (var i = 0; i < inputs.length; i++) {
                    (function (input) {
                        var attributes = input.attributes;
                        if (attributes.getNamedItem('ng-model') != void 0 && attributes.getNamedItem('name') != void 0) {
                            var field = form[attributes.name.value];
                            if (field != void 0) {
                                scope.$watch(function () {
                                    return form.$submitted + "_" + field.$valid;
                                }, function () {
                                    if (form.$submitted != true) return;
                                    var inp = angular.element(input);
                                    if (inp.hasClass('ng-invalid')) {
                                        element.removeClass('has-success');
                                        element.addClass('has-error');
                                    } else {
                                        element.removeClass('has-error').addClass('has-success');
                                    }
                                });
                            }
                        }
                    })(inputs[i]);
                }
            }
        }
    }]);

    appStudioStarter.directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == undefined) return ''
                    var transformedInput = inputValue.replace(/[^0-9+.]/g, '');
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    });

    appStudioStarter.directive('compareTo', function () {
        return {
            require: '?ngModel',
            restrict: 'A',
            scope: {
                compareTo: '='
            },
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    console && console.warn('Match validation requires ngModel to be on the element');
                    return;
                }

                scope.$watch(function () {
                    var modelValue = angular.isUndefined(ctrl.$modelValue) ? ctrl.$$invalidModelValue : ctrl.$modelValue;
                    return (ctrl.$pristine && angular.isUndefined(modelValue)) || scope.compareTo === modelValue;
                }, function (currentValue) {
                    ctrl.$setValidity('compareTo', currentValue);
                });
            }
        };
    });

    appStudioStarter.directive('notEqual', function () {
        return {
            require: '?ngModel',
            restrict: 'A',
            scope: {
                notEqual: '='
            },
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    console && console.warn('Match validation requires ngModel to be on the element');
                    return;
                }

                scope.$watch(function () {
                    var modelValue = angular.isUndefined(ctrl.$modelValue) ? ctrl.$$invalidModelValue : ctrl.$modelValue;
                    return (ctrl.$pristine && angular.isUndefined(modelValue)) || scope.notEqual != modelValue;
                }, function (currentValue) {
                    ctrl.$setValidity('notEqual', currentValue);
                });
            }
        };
    });

    appStudioStarter.directive('noScroll', function ($document) {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attr) {

                $document.on('touchmove', function (e) {
                    e.preventDefault();
                });
            }
        }
    })
    appStudioStarter.directive('dateLowerThan', ["$filter", function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                var validateDateRange = function (inputValue) {
                    var fromDate = $filter('date')(inputValue, 'short');
                    var toDate = $filter('date')(attrs.dateLowerThan, 'short');
                    var isValid = isValidDateRange(fromDate, toDate);
                    ctrl.$setValidity('dateLowerThan', isValid);
                    return inputValue;
                };

                ctrl.$parsers.unshift(validateDateRange);
                ctrl.$formatters.push(validateDateRange);
                attrs.$observe('dateLowerThan', function () {
                    validateDateRange(ctrl.$viewValue);
                });
            }
        };
    }])

    appStudioStarter.directive('dateGreaterThan', ["$filter", function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                var validateDateRange = function (inputValue) {
                    var fromDate = $filter('date')(attrs.dateGreaterThan, 'short');
                    var toDate = $filter('date')(inputValue, 'short');
                    var isValid = isValidDateRange(fromDate, toDate);
                    ctrl.$setValidity('dateGreaterThan', isValid);
                    return inputValue;
                };

                ctrl.$parsers.unshift(validateDateRange);
                ctrl.$formatters.push(validateDateRange);
                attrs.$observe('dateGreaterThan', function () {
                    validateDateRange(ctrl.$viewValue);

                });
            }
        };
    }])

    appStudioStarter.directive('easyTab', ['$state', '$http', 'globalService', 'netCallService', 'GlobalConstants', function ($state, $http, globalService, netCallService, GlobalConstants) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                if ($state.current.name != "BubHubWallFindFriends" || $state.current.name != "SocialMedia") {
                    $('#tab-container-1').easytabs({ updateHash: false });
                    $('#easy-tab-container-1').easytabs({ updateHash: false });
                }
            }
        }
    }])


    appStudioStarter.filter('inrCurrency', function () {
        return function (n) {
            if (n) {
                var x = n;
                x = x.toString();
                var afterPoint = '';
                if (x.indexOf('.') > 0) {
                    afterPoint = x.substring(x.indexOf('.'), x.length);
                    if (afterPoint.length == 2) {
                        afterPoint = afterPoint + "0";
                    }
                } else {
                    afterPoint = ".00";
                }
                x = Math.floor(x);
                x = x.toString();
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
                return res;
            }
        };
    })

    appStudioStarter.directive('tiSegmentedControl', function () {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                onSelect: "&"
            },
            template: '<div class=\"buttons\"><div id="BubHubWallFindFriendsTabs" class="button-bar" ng-transclude></div></div>',

            controller: function ($scope) {
                this.buttons = [];
                this.setSelectedButton = function (title) {
                    $scope.onSelect({ $index: this.buttons.indexOf(title) });
                }
                var style = window.document.createElement('style');
                style.type = 'text/css';
                style.innerHTML += '.button.button-block.ti-segmented-control:first-child {}';
                style.innerHTML += '.button.button-block.ti-segmented-control { }';
                style.innerHTML += '.button.button-blockoutline.ti-segmented-control:last-child {}';
                style.innerHTML += '.button.button-block.ti-segmented-control.activated {}';
                window.document.getElementsByTagName('head')[0].appendChild(style);
            },
            link: function (scope) {
            }
        }
    })

    appStudioStarter.directive('tiSegmentedControlButton', function () {

        return {
            replace: true,
            require: '^tiSegmentedControl',
            scope: {
                title: '='
            },
            template: '<a class=\"button button-block ti-segmented-control \">{{title}}</a>',
            link: function (scope, element, attr, segmentedControlCtrl) {
                segmentedControlCtrl.buttons.push(scope.title);
                if (attr.selected != undefined) element.addClass('active');

                element.bind('click', function () {

                    segmentedControlCtrl.setSelectedButton(scope.title);
                    var buttons = angular.element(angular.element(element.parent()[0]).children());
                    for (var i = 0; i < buttons.length; i++) {
                        angular.element(buttons[i]).removeClass('active');
                    }
                    element.addClass('active');
                });
            }
        }
    });


    var isValidDate = function (dateStr) {
        if (dateStr == undefined)
            return false;
        var dateTime = Date.parse(dateStr);

        if (isNaN(dateTime)) {
            return false;
        }
        return true;
    };

    var getDateDifference = function (fromDate, toDate) {
        return Date.parse(toDate) - Date.parse(fromDate);
    };

    var isValidDateRange = function (fromDate, toDate) {
        if (fromDate == "" || toDate == "")
            return true;
        if (isValidDate(fromDate) == false) {
            return false;
        }
        if (isValidDate(toDate) == true) {
            var days = getDateDifference(fromDate, toDate);
            if (days < 0) {
                return false;
            }
        }
        return true;
    };


    window.gAppStudioStarter = appStudioStarter;
    return appStudioStarter;
})

