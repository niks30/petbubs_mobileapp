/*********************************GLOBAL: STARTS HERE*********************************************/
var bmbAppVersion = "1.0.0";
var isLocal = true;
var gContainerAppDefXML = null;
var gAppDefXML = null;
var gContainerDiv = null;
var gContainerDivId = "wrapper";
var gAppDefXMLFileName = "xml/Petbubs.xml";
var gContainerAppDefXMLFileName = "xml/Container.xml";
var gfirstLeafXPath = "/app/appdes/firstleaf";
var gHomeLeafXPath = "/app/appdes/homeleaf";
var gServerCommURLXPath = "/app/appdes/commUrl";
var gEnableEncryption = "/app/appdes/enableEncryption";
var gleafXPath = "/app/leaf";
var gcomponentXPath = "/component";
var gprocedureXPath = "/procedure";
var gparamXPath = "/param";
var gNetworkServiceXPath = "/networkservice";
var gElementXPath = "/elements/element";
var gSidemenuXPath = "/sidebar/component";
var gFooterXPath = "/footer/component";

var gHomeLeafName = null;
var gImageFolderPath = "images/";
var isContainerApp = false;
var gUserId = "13"; // REQUIRED FOR CONTAINER APP
var gAppId = "5";
var gAppVersion = "1";
var gAppStudioServerURL = "";
var gAppStudioServerEncryptURL = "http://183.87.102.124:7070/AppStudio/eunt";
var currentLeafName = null;
var gCurrentView = null;
var ionBar = null;  /// ion-bar to initialise header contents..(only applicable for leaf that has SideMenu Bar).
var gGlobalData = {};
var gDynamicList = {};
var gRequestEncryptionEnabled = false;
var iv = "3ad5485e60a4fecd";
var key = "0a948a068f5d4d8b9cc45df90b58d382d2b916c25822b6f74ea96fe6823132f4"

/*********************************GLOBAL: ENDS HERE***********************************************/
/**
* Following function is the starting point for AppStudio. It loads the local AppXML and sends for processing
* @param NIL
*/
var initAppStudio = function () {
    try {
        gGlobalData['isContainer'] = isContainerApp;
        gGlobalData['appId'] = gAppId;
        gGlobalData['userId'] = gUserId;
        gGlobalData['appVersion'] = gAppVersion;
        gGlobalData['userId'] = gUserId;
        addToGlobalData();
        addToDynamicList();
        resetAppData();

        if (isContainerApp) {
            var initInjector = angular.injector(["ng"]);
            var $http = initInjector.get("$http");
            return $http.get(gContainerAppDefXMLFileName).then(function (response) {
                gContainerAppDefXML = stringToXML(response.data);
                processAppXML(gContainerAppDefXML, false);
                console.log('$http get done');
            }, function (errorResponse) {
                console.log('$http get error');
            });
        } else {
            if (isLocal) {
                var initInjector = angular.injector(["ng"]);
                var $http = initInjector.get("$http");
                return $http.get(gAppDefXMLFileName).then(function (response) {
                    gAppDefXML = stringToXML(response.data);
                    //  console.log(gAppDefXML);
                    processAppXML(gAppDefXML, false);
                    //bootstrapAppStudio();
                    //  console.log('$http get done');
                }, function (errorResponse) {
                    // Handle error case
                    console.log('$http get error');
                });
            } else {
                //appStudioDB.deleteDB();
                appStudioDB.openDBInstance();
                //appStudioDB.createTables();
                appStudioDB.createTablesRecursively(0);
                //appStudioDB.createTable("Resources");
                appStudioDB.getAppDetails(gAppId, gAppVersion, getAppDetailsCallBack);
                //getAppXMLFromServer();
            }
        }
    } catch (e) {
        showErrorMessage('initAppStudio', e.message);
    }
}

/**
* Following function helps in setting the Device Info like IMEI, Platform, OS version etc
* @param NIL
*/
var setDeviceInfo = function () {
    try {
        var uid = cordova.plugins.uid;
        var deviceInfo = 'device.cordova ' + device.cordova + ' device.version ' + device.version + ' device.uuid ' + device.uuid + ' device.platform ' + device.platform + ' device.model ' + device.model;
        deviceInfo += ' uid.UUID ' + uid.UUID + ' uid.IMEI ' + uid.IMEI + ' uid.IMSI ' + uid.IMSI + ' uid.ICCID ' + uid.ICCID + ' uid.IMSI ' + uid.IMSI + ' screen.width ' + screen.width;
    } catch (e) {
        showErrorMessage('setDeviceInfo', e.message);
    }
}

/**
* Following function is NOT USED
* @param NIL
*/
var createTablesCallback = function () {
    try {
        appStudioDB.openDBInstance();
        appStudioDB.getAppDetails(gAppId, gAppVersion, getAppDetailsCallBack);
    } catch (e) {
        showErrorMessage('createTablesCallback', e.message);
    }
}

/**
* Following function is callback for getting App details from DB based on AppId and AppVersion
* @param tx - transaction object
* @param rs - result set containing Rows returned from DB
*/
var getAppDetailsCallBack = function (tx, rs) {
    try {
        if (rs && rs.rows.length > 0) {
            resetAppData(); // Called from common.js
            setAppData(JSON.parse(rs.rows.item(0).AppData)); // Called from common.js
            bootstrapAppStudio();
        } else {
            getAppXMLFromServer();
        }
    } catch (e) {
        showErrorMessage('getAppDetailsCallBack', e.message);
    }
}

/**
* Following function helps in creating web service request to AppXML from Server based on AppId, AppVersion, UserId
* @param NIL
*/
var appXMLRequest = function () {
    try {
        var serviceRequest = {};
        var request = {};
        var requestParams = [];
        requestParams.push({
            k: "userId",
            v: gUserId
        });
        requestParams.push({
            k: "appId",
            v: gAppId
        });
        requestParams.push({
            k: "isContainer",
            v: isContainerApp
        });
        requestParams.push({
            k: "appVersion",
            v: gAppVersion
        });
        request["params"] = requestParams;
        request["deviceId"] = getDeviceId();
        request["service"] = "GetAppsOnDevice";
        serviceRequest["req"] = request;
        console.log('serviceRequest ' + JSON.stringify(serviceRequest));
        return serviceRequest;
    } catch (e) {
        showErrorMessage('appXMLRequest', e.message);
    }
}

/**
* Following function helps in getting AppXML from server
* @param NIL
*/
var getAppXMLFromServer = function () {
    try {
        ActivityIndicator.show('Updating Application, Please wait ...');
        var request = appXMLRequest();

        httpPOST({
            url: gAppStudioServerURL,
            data: request,
            success: function (data, status, headers, config) {
                //alert('success callback ' + JSON.stringify(data) + ' ' + status + ' ' + headers + ' ' + config);
                var output = data.op;
                if (output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                    if (output[1].k.toString() === "AppDef") {
                        //alert('appXML ' + output[1].v[0].appXML);
                        var appDetails = output[1].v;
                        for (var i = 0; i < appDetails.length; i++) {
                            appStudioDB.insertApplication(appDetails[i], setAppDetailsinLocalStorage);
                            if (!isContainerApp) {
                                gAppDefXML = stringToXML(appDetails[i].appXML);
                                processAppXML(gAppDefXML, false);
                                //bootstrapAppStudio();
                                var appData = JSON.stringify(getAppData());
                                appStudioDB.updateApplication(appDetails[i], appData, bootstrapAppStudio);
                            } else {

                            }
                        }
                    }
                } else {
                    alert('Error in server operation');
                    ActivityIndicator.hide();
                }
            },
            error: function (data, status, headers, config) {
                alert('error callback ' + data + '-' + status);
            },
        });
    } catch (e) {
        showErrorMessage('getAppXMLFromServer', e.message);
    }
}

/**
* Following function helps in updating AppId and AppVersion in local Storage
* @param appDetails - Object containing details of the App from DB
*/
var setAppDetailsinLocalStorage = function (appDetails) {
    try {
        resetAppId();
        setAppId(appDetails.appId);
        resetAppVersion();
        setAppVersion(appDetails.appVersion);
    } catch (e) {
        showErrorMessage('setAppDetailsinLocalStorage', e.message);
    }
}

/**
* Following function helps in boot strapping AppStudio using Angular
* @param NIL
*/
var bootstrapAppStudio = function () {
    try {
        require(['app'], function (app) {
            'use strict';

            var start = function () {
                angular.bootstrap(document, [app['name']]);
            }
            start();
        });
    } catch (e) {
        showErrorMessage('setAppDetailsinLocalStorage', e.message);
    }
}

/**
* Following function helps in converting xml string to XML DOM
* @param oString - xml string
*/
var stringToXML = function (oString) {
    try {
        //code for IE
        if (window.ActiveXObject) {
            var oXML = new ActiveXObject("Microsoft.XMLDOM");
            oXML.loadXML(oString);
            return oXML;
        }
        // code for Chrome, Safari, Firefox, Opera, etc.
        else {
            return (new DOMParser()).parseFromString(oString, "text/xml");
        }
    } catch (e) {
        showErrorMessage('stringToXML', e.message);
    }
}

/**
* Following function helps in converting xml string to XML DOM
* @param appDefXML - XML DOM
* @param isChildApp - Used to differentiate between ContainerApp and its Child Apps. Helps in setting Start page
*/
var processAppXML = function processAppXML(appDefXML, isChildApp) {
    try {
        var appData = getAppData(); //{};
        var leafViews = {};
        gAppDefXML = appDefXML;
        var firstLeafName = getNodeValueByXPath(gAppDefXML, gfirstLeafXPath);
        gAppStudioServerURL = getNodeValueByXPath(gAppDefXML, gServerCommURLXPath);
        gRequestEncryptionEnabled = getNodeValueByXPath(gAppDefXML, gEnableEncryption) === "1" ? true : false;
        if (isChildApp) {
            setFirstLeafNameChildApp(firstLeafName);
        } else {
            setFirstLeafName(firstLeafName);
        }

        var homeLeafName = getNodeValueByXPath(gAppDefXML, gHomeLeafXPath);
        if (isChildApp) {
            //localStorage.setItem('homeLeafNameChildApp', homeLeafName);
            setHomeLeafNameChildApp(homeLeafName);
        } else {
            //localStorage.setItem('homeLeafName', homeLeafName);
            setHomeLeafName(homeLeafName);
        }
        //var startupScreenXPath = gleafXPath + "[@name='" + firstLeafName + "']" + gcomponentXPath;
        var leafList = getNodesByXPath(gAppDefXML, gleafXPath);

        for (var leafIndex = 0; leafIndex < leafList.length; leafIndex++) {
            var currentLeafNode = leafList[leafIndex];
            var leafName = currentLeafNode.getAttribute("name");
            var leafTitle = currentLeafNode.getAttribute("title");
            var childView = currentLeafNode.getAttribute("childView");
            var parent = currentLeafNode.getAttribute("parent");
            var leafNameXPath = gleafXPath + "[@name='" + leafName + "']";
            var leafXPath = gleafXPath + "[@name='" + leafName + "']" + gcomponentXPath;
            var leafNetworkXPath = gleafXPath + "[@name='" + leafName + "']" + gNetworkServiceXPath;
            var componentList = getNodesByXPath(gAppDefXML, leafXPath);
            var componentData = {};
            componentData["leafName"] = leafName;
            currentLeafName = leafName;
            componentData["title"] = leafTitle;

            if (leafName === "Login") {
                componentData["userId"] = getCNO();
            }

            var template = createViewTemplate(currentLeafNode, componentList, leafXPath, componentData, leafNameXPath);
            var networkServiceData = getNetworkServiceData(leafNetworkXPath);
            var leafComponents = {};
            leafComponents["title"] = leafName;
            leafComponents["controller"] = leafName + "Controller";
            leafComponents["template"] = template.outerHTML; //.replace(/"/g, "'");
            leafComponents["url"] = "/" + leafName + "/:prevData"; // prevData required to send $stateParams
            leafComponents["data"] = componentData;
            leafComponents["childView"] = childView == "true" ? true : false;
            leafComponents["parent"] = parent == "true" ? true : false;
            leafComponents["ViewName"] = currentLeafNode.getAttribute("ViewName");
            leafComponents["networkService"] = networkServiceData;
            leafViews[leafName + "Data"] = leafComponents;
        }

        if (appData.views) {
            var prevViews = appData.views;
            var concatViews = {};
            for (var _obj in prevViews) concatViews[_obj] = prevViews[_obj];
            for (var _obj in leafViews) concatViews[_obj] = leafViews[_obj];
            appData["views"] = concatViews;
        } else {
            appData["views"] = leafViews; //Default
        }

        resetAppData();
        setAppData(appData);
    } catch (e) {
        showErrorMessage('processAppXML', e.message);
    }
}

/**
* Following function helps in reading Network Service Node from AppXMl and adding details in AppData for respective leaf
* @param currentLeafNetworkXPath - Contains XPath till NetworkService node of a Leaf
*/
var getNetworkServiceData = function (currentLeafNetworkXPath) {
    try {
        var networkServiceData = {};
        var inputParams = [];

        var networkServiceNode = getNodesByXPath(gAppDefXML, currentLeafNetworkXPath);
        if (networkServiceNode && networkServiceNode.length > 0) {
            var serviceId = getNodeValueByXPath(gAppDefXML, currentLeafNetworkXPath + "/serviceid");
            // var commUrl = getNodeValueByXPath(gAppDefXML, currentLeafNetworkXPath + "/commUrl");
            // var enableEncryption = getNodeValueByXPath(gAppDefXML, currentLeafNetworkXPath + "/enableEncryption");
            var progressInfo = getNodeValueByXPath(gAppDefXML, currentLeafNetworkXPath + "/progressinfo");
            var inputParamNodes = getNodesByXPath(gAppDefXML, currentLeafNetworkXPath + "/inputparams/param");
            for (var index = 0; index < inputParamNodes.length; index++) {
                var param = {};
                param["key"] = inputParamNodes[index].getAttribute("name");
                var value = inputParamNodes[index].firstChild.nodeValue;
                if (value.indexOf("@{") >= 0) {
                    value = value.replace("@{", "");
                    value = value.replace("}", "");
                }
                param["value"] = value;
                inputParams.push(param);
            }
            networkServiceData["inputParams"] = inputParams;
            networkServiceData["service"] = serviceId;
            // networkServiceData["commUrl"] = commUrl;
            // networkServiceData["enableEncryption"] = enableEncryption;
            networkServiceData["progressInfo"] = progressInfo;
        }
        //  console.log('networkServiceData', JSON.stringify(networkServiceData));
        return networkServiceData;
    } catch (e) {
        showErrorMessage('getNetworkServiceData', e.message);
    }
}

/**
* Following function helps creating view for each Leaf
* @param currentLeafNode - Contains XML Node of Leaf from AppXML
* @param componentList - Contains a nodeList of Component Nodes within a Leaf
* @param currentLeafXPath - Contains XPath till current Leaf
* @param componentData - Contains JSON object which holds data for each leaf
*/
var createViewTemplate = function (currentLeafNode, componentList, currentLeafXPath, componentData, leafNameXPath) {
    try {

        var isSideMenuBar = currentLeafNode.getAttribute("hasSideMenuBar");
        var ionView;
        if (isSideMenuBar == "true") {
            ionView = document.createElement("div");
            sideMenu = create_sideMenus(currentLeafNode, componentList, currentLeafXPath, componentData, leafNameXPath);
            ioninsideview = create_ionView(currentLeafNode, componentList, currentLeafXPath, componentData, leafNameXPath);
            ionView.appendChild(sideMenu);
            ionView.appendChild(ioninsideview);
        } else {
            ionView = create_ionView(currentLeafNode, componentList, currentLeafXPath, componentData, leafNameXPath);
        }
        return ionView;

    } catch (e) {
        showErrorMessage('createViewTemplate', e.message);
    }
}


var create_sideMenus = function (currentLeafNode, componentList, leafXPath, componentData, currentLeafXPath) {
    try {

        var deviceHeight = window.innerHeight;
        var container = document.createElement("div");
        //container.setAttribute("ng-controller","AbstractCntrl");
        var overlay = document.createElement("div");
        //overlay.setAttribute("class","sidemenu-background-overlay");
        overlay.setAttribute("id", "overlay");
        overlay.setAttribute("ng-click", "falseToggle()")

        container.setAttribute("class", "container");

        container.setAttribute("on-swipe-right", "Truetoggle()");
        container.setAttribute("on-swipe-left", "falseToggle()");

        var sidebar = document.createElement("div");

        sidebar.setAttribute("class", "sidebar");
        sidebar.setAttribute("id", "sidebarContainer");
        sidebar.setAttribute("sidebar-directive", "Sidebarstate");

        // var navigation=document.createElement("a");
        // //navigation.setAttribute("ng-class","{{Sidebarstate}}");
        // navigation.setAttribute("id","navigation-toggle");
        // //navigation.setAttribute("ng-click","toggleState()");
        // //navigation.setAttribute("on-drag-right", 'onSwipeRight()');
        // navigation.setAttribute("class","ion-navicon-round");

        var sideMenu = document.createElement("div");/*contain side bar header and sideView*/
        sideMenu.setAttribute("class", "sidemenu-background whiteBg");
        var deviceWidth = parseInt(window.innerWidth) - 80;

        sidebar.setAttribute("style", "left:-" + deviceWidth + "px");
        //navigation.setAttribute("style","min-height:"+deviceHeight+"px;left:"+deviceWidth+"px");
        sideMenu.setAttribute("style", "min-height:" + deviceHeight + "px;width:" + deviceWidth + "px");

        var ionContent = document.createElement("ion-content");

        var currentXPath = currentLeafXPath + gSidemenuXPath;
        componentList = getNodesByXPath(gAppDefXML, currentXPath);

        for (var index = 0; index < componentList.length; index++) {
            create_ionControl(ionContent, componentList[index], null, currentXPath, componentData, null);
        }

        //sidebar.appendChild(navigation);

        // making header
        var header = document.createElement("div");
        header.setAttribute("class", "bar bar-header bar bar-stable bg-color bg-76c");
        var heading = document.createElement("h1");
        heading.setAttribute("class", "title white font-size-20 titi-400");
        heading.innerText = "MENU";
        var link = document.createElement("a");
        link.setAttribute("href", "#");
        link.setAttribute("class", "button icon-left ion-chevron-right button-clear white");
        link.setAttribute("ng-click", "falseToggle()")
        header.appendChild(heading);
        header.appendChild(link);

        sideMenu.appendChild(header);
        sideMenu.appendChild(ionContent);
        sidebar.appendChild(sideMenu);
        container.appendChild(overlay);
        container.appendChild(sidebar);


        return container;


    } catch (e) {
        showErrorMessage('create_sideMenus', e.message);
    }

}

/**
* Following function helps creating ionView for each Leaf
* @param currentLeafNode - Contains XML Node of Leaf from AppXML
* @param componentList - Contains a nodeList of Component Nodes within a Leaf
* @param currentLeafXPath - Contains XPath till current Leaf
* @param componentData - Contains JSON object which holds data for each leaf
*/
var create_ionView = function (currentLeafNode, componentList, currentLeafXPath, componentData) {
    try {
        var hideBackButton = currentLeafNode.getAttribute("hideBackButton");
        var hideHeader = currentLeafNode.getAttribute("hideHeader");
        var hideSubHeader = currentLeafNode.getAttribute("hideSubHeader");
        var headerImage = currentLeafNode.getAttribute("headerImage");
        var headerTitle = currentLeafNode.getAttribute("title");
        var CustomHeader = currentLeafNode.getAttribute("CustomHeader");
        var hasSideMenuBar = currentLeafNode.getAttribute("hasSideMenuBar");
        var isFloatingbtn = currentLeafNode.getAttribute("isFloatingbtn");
        var hasStickyPost = currentLeafNode.getAttribute("hasStickyPost");
        var stickyPostClick = currentLeafNode.getAttribute("onClickStickyPost");
        var outOfIonContent = currentLeafNode.getAttribute("outOfIonContent");

        var ionView = document.createElement("ion-view");

        ionView.appendChild(create_ionNavBar(currentLeafNode, componentList, currentLeafXPath, componentData));

        var customSubHeaderName = currentLeafNode.getAttribute("customSubHeaderName");
        if (customSubHeaderName && customSubHeaderName.length) {
            var customHeaderXPath = currentLeafXPath + "[@name='" + customSubHeaderName + "']";
            var customHeader = getNodesByXPath(gAppDefXML, customHeaderXPath);
            create_ionControl(ionView, customHeader[0], null, currentLeafXPath);
        }


        if (hideSubHeader && hideSubHeader === "false") {
            ionView.appendChild(create_ionSubHeaderBar(currentLeafNode, componentList, currentLeafXPath, componentData));
        }

        if (isFloatingbtn && isFloatingbtn == "true") {
            var a = document.createElement("a");
            a.setAttribute("class", "a-plus");
            var icon = document.createElement("img");
            icon.setAttribute("class", "plus-circled");
            icon.setAttribute("src", "images/shadow-button.png");
            a.appendChild(icon);
            a.setAttribute("ng-click", "onButtonClick('nextleaf','AddEventLeaf')")
            ionView.appendChild(a);
        }

        gCurrentView = ionView;

        var searchbutton = currentLeafNode.getAttribute("searchbutton");
        if (searchbutton) { ////////////// Search Box for Pet directory
            var sdiv = document.createElement("div");
            sdiv.setAttribute("id", "searchBar");
            sdiv.setAttribute("class", "search-bar");
            sdiv.setAttribute("style","display:none");
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("class", "search-window font-titi titi-400 size-14 black-s2 ng-pristine ng-untouched ng-valid");
            input.setAttribute("style", "width: 92%");
            // input.setAttribute("class","autofocus");
            input.setAttribute("id", "searchbox");
            input.setAttribute("placeholder", "Search");
            input.setAttribute("ng-model", "searchText");
            //input.setAttribute("style","background-color:#11c1f3");

            //input.setAttribute("disabled","true");
            sdiv.appendChild(input);

            var a = document.createElement("a");
            a.setAttribute("class", "close-icon-1");
            //a.setAttribute("ng-if","searchText.length>0");
            a.setAttribute("ng-click", "clearText()");
            a.setAttribute("href", "javascript:;");

            var img = document.createElement("img");
            img.setAttribute("width", "13");
            img.setAttribute("height", "13");
            img.setAttribute("src", "images/close-icon-1.png");
            img.setAttribute("class", "searchBox");
            a.appendChild(img);

            var sp = document.createElement("br");
            sdiv.appendChild(a);
            ionView.appendChild(sdiv);
        }

        ionView.appendChild(create_ionContent(currentLeafNode, componentList, currentLeafXPath, componentData, customSubHeaderName, outOfIonContent));

        if (outOfIonContent && outOfIonContent.length) {
            var outContentXPath = currentLeafXPath + "[@name='" + outOfIonContent + "']";
            var outContent = getNodesByXPath(gAppDefXML, outContentXPath);
            create_ionControl(ionView, outContent[0], null, currentLeafXPath);
        }

        //Sticky Post
        // console.log(componentData.leafName);
        if (hasStickyPost && hasStickyPost.length > 0 && hasStickyPost == "true") {

            var stickyDiv = document.createElement('div');
            stickyDiv.setAttribute('class', 'sticky-post');
            var rowDiv = document.createElement('div');
            rowDiv.setAttribute('class', 'row padd-zero');
            var firstColDiv = document.createElement('div');
            firstColDiv.setAttribute('class', 'col-75');
            var commentInput = document.createElement('input');
            commentInput.setAttribute('type', 'text');
            commentInput.setAttribute('ng-model', 'data.commentInput');
            commentInput.setAttribute('class', 'comment-box size-14 font-titi titi-400');
            commentInput.setAttribute('placeholder', 'Type your comment');
            firstColDiv.appendChild(commentInput);
            var secondColDiv = document.createElement('div');
            secondColDiv.setAttribute('class', 'col-25 overflow-h');
            var postButton = document.createElement('button');
            if (stickyPostClick) {
                postButton.setAttribute('ng-click', stickyPostClick);
            }
            postButton.setAttribute('class', 'margin-right-10 button post-btn-1 size-14 font-titi titi-700 lh-22');
            postButton.innerText = 'Post';
            secondColDiv.appendChild(postButton);
            rowDiv.appendChild(firstColDiv);
            rowDiv.appendChild(secondColDiv);
            stickyDiv.appendChild(rowDiv);
            ionView.appendChild(stickyDiv);
        }
        return ionView;

    } catch (e) {

        showErrorMessage('create_ionView', e.message);
    }
}



/**
* Following function helps creating action Buttons within NavBar
* @param componentNode - Contains component node of type Command
*/
var create_NewNavBarButton = function (componentNode, currentLeafNode) {
    try {

        var subType = componentNode.getAttribute("subtype");
        var theme = componentNode.getAttribute("theme");
        if (subType === "searchPD") {
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("id", "searchPD");

        }
        var anchorTag = document.createElement("a");
        anchorTag.setAttribute("class", "button icon button-clear button-dark " + theme)

        switch (subType) {
            case "search":
                anchorTag.setAttribute("class", "button icon ion-search button-clear button-dark " + theme);
                var onclick = componentNode.getAttribute('onclick');
                if (onclick) {
                    anchorTag.setAttribute('ng-click', onclick);
                }
                break;
            case "close":
                var targetLeaf = componentNode.getAttribute("targetLeaf");
                anchorTag.setAttribute("class", "button icon ion-close button-clear button-dark " + theme);
                anchorTag.setAttribute("ng-click", "onButtonClick('nextleaf','" + targetLeaf + "')");
                break;
            case "sideMenu":
                anchorTag.setAttribute("class", "button icon ion-navicon button-clear button-dark " + theme);
                break;
            case "sideMenuRounded":
                anchorTag.setAttribute("class", "button icon button-clear ion-navicon-round button-dark " + theme);
                anchorTag.setAttribute("ng-click", "Truetoggle()");
                anchorTag.setAttribute("on-drag-right", 'onSwipeRight()');
                anchorTag.setAttribute("style", "margin-top: 3px")
                break;
            case "bookmark":
                var targetLeaf = componentNode.getAttribute("targetLeaf");
                anchorTag.setAttribute("class", "margin-top-1-up button button-clear button-positive header-profile " + theme);
                var image = document.createElement("img");
                image.setAttribute("src", gImageFolderPath + "news-heart.png");
                image.setAttribute("ng-if", "globalData.noBookmark");
                image.setAttribute("class", "news-heart");
                anchorTag.appendChild(image);
                anchorTag.setAttribute("ng-click", "onButtonClick('nextleaf','" + targetLeaf + "')");
                break;
            case "blank":
                anchorTag.setAttribute("class", "icon-right ");
                break;
            case "button":
                var text = componentNode.getAttribute("buttontext");
                anchorTag = document.createElement("button");
                anchorTag.innerHTML = text;
                anchorTag.setAttribute("class", "button header-btn");
                var targetLeaf = componentNode.getAttribute("targetLeaf");
                anchorTag.setAttribute("ng-click", "onButtonClick('nextleaf','" + targetLeaf + "')");
                var disabled = componentNode.getAttribute("disabled");
                if (disabled && disabled.length) {
                    anchorTag.setAttribute("disabled", disabled);
                }
                break;

            case "roundedProfileButton":
                var iconPath = componentNode.getAttribute("iconPath");
                var iconTheme = componentNode.getAttribute("iconTheme");
                var imageFromUrl = componentNode.getAttribute("imageFromUrl");
                var img = document.createElement("img");
                var imageFromUrl = componentNode.getAttribute("imageFromUrl");

                if (iconTheme) {
                    img.setAttribute('class', iconTheme);
                }
                if (imageFromUrl) {
                    img.setAttribute('ng-src', iconPath);
                } else {
                    img.setAttribute('src', gImageFolderPath + iconPath);
                }

                var onClick = componentNode.getAttribute("onIconClick");
                anchorTag = document.createElement("button");
                anchorTag.setAttribute('class', ' button button-clear button-positive header-profile'); //margin-top-1-up
                anchorTag.appendChild(img);
                if (onClick) {
                    anchorTag.setAttribute('ng-click', onClick);
                }
                break;

            case "dropbox":
                anchorTag = document.createElement("button");
                anchorTag.setAttribute("class", "button btn-drop");
                var div = document.createElement("div");
                div.setAttribute("class", "select-list");
                var label = document.createElement("label");
                label.setAttribute("class", "item-select-arrow item main-section-right-bar");
                label.setAttribute("id", "city_select");
                label.setAttribute("value", "Mumbai");
                label.setAttribute("ng-click", "showModal('templates/AddCity.html')");
                //var dynamicList = getDynamicListData();
                console.log(window.localStorage.getItem("selectedCity"));
                label.innerHTML = window.localStorage.getItem("selectedCity");
                // var select = document.createElement("select");
                // select.setAttribute("class", "select-drop-btn first-letter-cap font-size-12-600 btn-drp-p");
                // select.setAttribute("id","city_select");
                // //select.setAttribute("onchange","CloseCity(options[selectedIndex].value)");
                // select.setAttribute("ng-click","showModal('templates/AddCity.html')");
                // // currently creating options hardcoded
                // //var options = ["Bangalore", "Chennai", "Chandigarh", "Delhi", "Goa", "Hyderabad", "Indore", "Jaipur", "Karjat", "Kolkata", "Kochi", "Mumbai", "Pune"];
                // var dynamicList = getDynamicListData();
                // var options=dynamicList.city;
                // for (var i = 0; i < options.length; i++) {
                //     var opt = document.createElement("option");
                //     opt.setAttribute("value", options[i].value);
                //     //opt.setAttribute("ng-click","CloseCity(options[i].value)");
                //     if(options[i].value==="Mumbai"){
                //     opt.setAttribute("selected","selected");
                //     }
                //     opt.innerText = options[i].value;
                //     select.appendChild(opt);
                // }

                // label.appendChild(select);

                div.appendChild(label);
                anchorTag.appendChild(div);
                break;

            case "imageButton":
                var anchorTag = document.createElement("button");
                anchorTag.setAttribute("type", "button");
                anchorTag.setAttribute("class", "margin-top-1-up button button-clear button-positive header-profile");
                var src = componentNode.getAttribute("src");
                if (src && src.length) {
                    var image = document.createElement("img");
                    image.setAttribute("class", "setting-icon");
                    image.setAttribute("src", gImageFolderPath + src);
                    anchorTag.appendChild(image);
                }
                var targetLeaf = componentNode.getAttribute("targetLeaf");
                anchorTag.setAttribute("ng-click", "onButtonClick('nextleaf','" + targetLeaf + "')");
                break;
        }
        if (subType === "searchPD") {
            // var sp =document.createElement("br");
            // ionNavBar.appendChild(sp);  
            return input;
        } else {
            return anchorTag;
        }

    } catch (e) {
        showErrorMessage('create_NewNavBarButton', e.message);
    }
}

/**
* Following function helps creating ionNavBar
* @param currentLeafNode - Contains XML Node of Leaf from AppXML
* @param componentList - Contains a nodeList of Component Nodes within a Leaf
* @param currentLeafXPath - Contains XPath till current Leaf
* @param componentData - Contains JSON object which holds data for each leaf
*/
var create_ionNavBar = function (currentLeafNode, componentList, currentLeafXPath, componentData) {
    try {
        var ionNavBar = document.createElement("div");
        var headerTheme = currentLeafNode.getAttribute("headerTheme");
        var hideBackButton = currentLeafNode.getAttribute("hideBackButton");
        var navbuttonTheme = currentLeafNode.getAttribute("navbuttontheme");
        var navbuttonNGTheme = currentLeafNode.getAttribute("navbuttonNGtheme");
        var navHeadingTheme = currentLeafNode.getAttribute("navheadingtheme");
        var navHeadingNGTheme = currentLeafNode.getAttribute("navheadingNGtheme");
        var headImageTheme = currentLeafNode.getAttribute("headimagetheme");
        var imageDimention = currentLeafNode.getAttribute("headimagedimention");
        var componentNgclass = currentLeafNode.getAttribute("ng-class");
        var title = currentLeafNode.getAttribute("title");
        var headerImage = currentLeafNode.getAttribute("headerImage");

        ionNavBar.setAttribute("class", "bar bar-header " + headerTheme);
        ionNavBar.setAttribute('id', 'navbarid');
        if (componentNgclass) {
            ionNavBar.setAttribute("ng-class", componentNgclass);
        }

        //ionNavBar.setAttribute("ng-controller", "AbstractCntrl");

        if (headerImage) {
            var h1 = document.createElement("h1");
            h1.setAttribute("class", "title " + navHeadingTheme);
            var img = document.createElement("img");
            img.setAttribute("src", gImageFolderPath + headerImage);
            img.setAttribute("class", "title-image " + headImageTheme);
            if (imageDimention && imageDimention.length) {
                var dimSplitted = imageDimention.split(" ");
                var width = parseInt(dimSplitted[0]);
                var height = parseInt(dimSplitted[1]);
                img.setAttribute("width", width);
                img.setAttribute("height", height);
            }
            h1.appendChild(img);
            ionNavBar.appendChild(h1);
        } else {
            var h1 = document.createElement("h1");
            h1.setAttribute("class", "title " + navHeadingTheme);
            h1.setAttribute("ng-class", navHeadingNGTheme);
            h1.innerText = title;
            ionNavBar.appendChild(h1);
        }

        if (hideBackButton != "true") {
            var ionBackButton = document.createElement("a");
            ionBackButton.setAttribute("class", "button icon-left ion-chevron-left button-clear button-dark " + navbuttonTheme);
            ionBackButton.setAttribute("ng-class", navbuttonNGTheme);
            ionBackButton.setAttribute("ng-click", "myGoBack()");
            ionNavBar.appendChild(ionBackButton);
        }



        for (var index = 0; index < componentList.length; index++) {
            var componenttype = componentList[index].getAttribute("type");
            if (componenttype == "command") {
                var ionNavButton = create_NewNavBarButton(componentList[index], currentLeafNode);
                ionNavBar.appendChild(ionNavButton);
            }
        }

        return ionNavBar;
    } catch (e) {
        showErrorMessage('create_ionNavBar', e.message);
    }
}

/**
* Following function helps creating action Buttons within NavBar
* @param componentNode - Contains component node of type Command
*/
var create_NavBarButton = function (componentNode, currentLeafNode) {
    try {

        // var hasSideMenuBar=currentLeafNode.getAttribute("hasSideMenuBar");
        var cmdSide = componentNode.getAttribute("side");
        var subType = componentNode.getAttribute("subtype");
        var param = componentNode.getAttribute("param");
        var componentTargetLeaf = componentNode.getAttribute("targetLeaf");
        var ionNavButton = document.createElement("ion-nav-buttons");
        ionNavButton.setAttribute("style", "position:absolute");
        ionNavButton.setAttribute("side", cmdSide);
        var button = document.createElement("button");
        button.setAttribute("class", "button headerButtons");
        var buttonIcon = document.createElement("i");
        if (subType === "logout") {
            //buttonIcon.setAttribute("class", "icon ionIcon ion-power");
            //var buttonIcon = document.createElement("img");
            buttonIcon.setAttribute("class", " icon logOut");
            button.setAttribute("ng-click", "onLogoutClick()");
        } else if (subType === "customBack") {
            button.setAttribute("class", "icon bmbBack customBackButton");
            button.setAttribute("ng-click", "onBackbuttonClick('" + componentTargetLeaf + "')");
        } else if (subType === "home") {
            buttonIcon.setAttribute("class", "icon ionIcon ion-home");
            button.setAttribute("ng-click", "onHomeClick()");
        } else if (subType === "homeChildApp") {
            buttonIcon.setAttribute("class", "icon ionIcon ion-home");
            button.setAttribute("ng-click", "onChildAppHomeClick()");
        } else if (subType === "sync") {
            buttonIcon.setAttribute("class", "icon ionIcon ion-loop");
            button.setAttribute("ng-click", "onSyncClick()");
        } else if (subType === "checkmark") {
            buttonIcon.setAttribute("class", "icon ionIcon ion-checkmark");
            button.setAttribute("ng-click", "onCheckmarkClick()");
        } else if (subType === "close") {
            buttonIcon.setAttribute("class", "icon ionIcon ion-close");
            button.setAttribute("ng-click", "CancelEvent()");
        } else if (subType === "SideBarbtn") {
            button.setAttribute("class", "button button-icon button-clear ion-navicon");
            button.setAttribute("ng-click", "toggleState()");
            button.setAttribute("on-drag-right", 'onSwipeRight()');
        } else if (subType === "bookmark") {
            button.setAttribute("class", "button icon-right ion-android-bookmark button-clear button-calm bookmarkIcon");
            button.setAttribute("ng-click", "onButtonClick('nextleaf','bookmark')");
        } else if (subType === "Searchbtn") {
            button.setAttribute("class", "button icon-right icon ion-search button-clear button-dark");
            button.setAttribute("ng-click", "onSearchbtnClick()");
        } else if (subType === "editbtn") {
            button.setAttribute("class", " button headerButtons ion-edit");
            button.setAttribute("style", "font-size:25px;color:#3e3e3e;right:15%;margin-top:9%");
            button.setAttribute("ng-click", "onClickProcedureCall('onEditBtnClick','null'," + param + ")");
        } else if (subType === "dropdown") {
            var div = document.createElement("div");
            div.setAttribute("class", "selectComboDiv mwtSelectTextGradient");
            var selectTag = document.createElement('select');
            selectTag.setAttribute('class', 'selectCombo');
            selectTag.setAttribute("style", "border: none;padding-top: 2px;color: #333333;")
            selectTag.setAttribute("ng-model", 'data.city');
            selectTag.setAttribute("ng-options", 'item.value as item.name for item in dynamicList.city');
            selectTag.setAttribute("ng-change", "onComboChange(data.city,'city')");

            var optionTag = document.createElement('option');
            optionTag.setAttribute("value", "");
            optionTag.setAttribute("style", "display:none;");
            //optionTag.innerText = "-- Select --";
            selectTag.appendChild(optionTag);
            div.appendChild(selectTag);
        }

        if (subType === "dropdown") {
            ionNavButton.appendChild(div);
        } else {
            button.appendChild(buttonIcon);
            ionNavButton.appendChild(button);
        }

        if (currentLeafNode != null) {
            return ionNavButton;
        }
        else {
            gCurrentView.appendChild(ionNavButton);
        }
    } catch (e) {
        showErrorMessage('create_NavBarButton', e.message);
    }
}

/**
* Following function helps creating ionSubHeader. Header below NavBar (Main Header)
* @param currentLeafNode - Contains XML Node of Leaf from AppXML
* @param componentList - Contains a nodeList of Component Nodes within a Leaf
* @param currentLeafXPath - Contains XPath till current Leaf
* @param componentData - Contains JSON object which holds data for each leaf
*/
var create_ionSubHeaderBar = function (currentLeafNode, componentList, currentLeafXPath, componentData) {
    try {

        var subHeaderTheme = currentLeafNode.getAttribute("subHeaderTheme");
        subHeaderTheme = subHeaderTheme && subHeaderTheme.length > 0 ? subHeaderTheme : "";

        var subHeaderStyle = currentLeafNode.getAttribute("subHeaderStyle");

        var ionSubHeaderBar = document.createElement("ion-header-bar");


        ionSubHeaderBar.setAttribute("class", "bar bar-subheader " + subHeaderTheme);

        var subTitle = currentLeafNode.getAttribute("subTitle");
        var title = document.createElement("h5");
        if (subHeaderStyle && subHeaderStyle.length > 0) {
            title.setAttribute("style", subHeaderStyle);
        }

        title.innerText = subTitle;
        ionSubHeaderBar.appendChild(title);
        return ionSubHeaderBar;
        /*<ion-header-bar class="bar bar-subheader">
          <h1 class="title">Subheader</h1>
        </ion-header-bar>*/

    } catch (e) {
        showErrorMessage('create_ionSubHeaderBar', e.message);
    }
}


/**
* Following function helps creating ionContent.
* ionContent is the body of a view which contains all the controls defined within a Leaf
* @param currentLeafNode - Contains XML Node of Leaf from AppXML
* @param componentList - Contains a nodeList of Component Nodes within a Leaf
* @param currentLeafXPath - Contains XPath till current Leaf
* @param componentData - Contains JSON object which holds data for each leaf
*/
var create_ionContent = function (currentLeafNode, componentList, currentLeafXPath, componentData, customSubHeaderName, outOfIonContent) {
    try {
        var hideHeader = currentLeafNode.getAttribute("hideHeader");
        var hideSubHeader = currentLeafNode.getAttribute("hideSubHeader");
        var inset = currentLeafNode.getAttribute("inset");
        var theme = currentLeafNode.getAttribute("theme");
        var contentTheme = currentLeafNode.getAttribute("contentTheme");
        var formElement = currentLeafNode.getAttribute("formElement");
        var scroll = currentLeafNode.getAttribute("scroll");

        var hasHeader = hideHeader && hideHeader.length > 0 && hideHeader === "true" ? "false" : "true";
        var hasSubHeader = hideSubHeader && hideSubHeader.length > 0 && hideSubHeader === "true" ? "false" : "true";
        var hasInfiniteScroll = currentLeafNode.getAttribute("hasInfiniteScroll");
        var ionContent = document.createElement("ion-content");
        ionContent.setAttribute("delegate-handle", "scroller");

        if (hasHeader && hasHeader.length > 0 && hasHeader == "tre") {
            ionContent.setAttribute("has-header", hasHeader);
        }

        if (scroll && scroll.length > 0) {
            ionContent.setAttribute("scroll", scroll);
        }

        if (hasHeader == "true") {
            ionContent.setAttribute("class", 'has-header');
        }

        if (contentTheme && contentTheme.length > 0) {
            if (hasHeader == "true") {
                ionContent.setAttribute("class", 'has-header ' + contentTheme);
            } else {
                ionContent.setAttribute("class", contentTheme);
            }
        }


        if (hideSubHeader == "false") {
            ionContent.setAttribute("has-subheader", hasSubHeader);
        }

        var formDiv = document.createElement("form");
        formDiv.setAttribute("novalidate", 'novalidate');

        if (customSubHeaderName && customSubHeaderName.length) {
            componentList = componentList.filter(function (element) {
                return element.getAttribute("name") !== customSubHeaderName;
            })
        }

        if (outOfIonContent && outOfIonContent.length) {
            componentList = componentList.filter(function (element) {
                return element.getAttribute("name") !== outOfIonContent;
            })
        }

        for (var i = 0; i < componentList.length; i++) {
            var componentNode = componentList[i];
            var nextComponentNode = null;
            var componentType = componentNode.getAttribute("type");

            switch (componentType) {
                case "label":
                    nextComponentNode = componentList[i + 1];
                    if (nextComponentNode) {
                        var nextComponentType = nextComponentNode.getAttribute("type");
                        if (nextComponentType === "input" || nextComponentType === "select") {
                            i++;
                        } else {
                            nextComponentNode = null;
                        }
                    }
                    break;
                default:
                    nextComponentNode = null;
                    break;
            };

            if (formElement == "true") {
                create_ionControl(formDiv, componentNode, nextComponentNode, currentLeafXPath, componentData, formDiv);
            } else {
                create_ionControl(ionContent, componentNode, nextComponentNode, currentLeafXPath, componentData, null);
            }
        }

        if (formElement == "true") {
            ionContent.appendChild(formDiv);
        }

        if (hasInfiniteScroll == "true") {
            var ionInfiniteScroll = document.createElement("ion-infinite-scroll");
            var onInfinite = currentLeafNode.getAttribute("onInfinite");
            var ionInfiniteNgIf = currentLeafNode.getAttribute("ionInfiniteNgIf");
            if (onInfinite && onInfinite.length > 0) {
                ionInfiniteScroll.setAttribute("on-infinite", onInfinite);
                ionInfiniteScroll.setAttribute("distance", "0.001%");
            }
            if (ionInfiniteNgIf && ionInfiniteNgIf.length > 0) {
                ionInfiniteScroll.setAttribute("ng-if", ionInfiniteNgIf);
            }
            ionContent.appendChild(ionInfiniteScroll);
        }
        return ionContent;


    } catch (e) {
        showErrorMessage('create_ionContent', e.message);
    }
}

var create_ionControl = function (ionContent, componentNode, nextComponentNode, currentLeafXPath, componentData, formElement) {
    try {
        var componentName = componentNode.getAttribute("name");
        var componentType = componentNode.getAttribute("type");
        var componentSubType = componentNode.getAttribute("subtype");
        var componentTheme = componentNode.getAttribute("theme");
        var componentTitle = componentNode.getAttribute("title");
        var ctrl = null;

        switch (componentType) {
            case "input":
                ctrl = input.render_ionInput(componentNode, nextComponentNode, componentData);
                break;
            case "label":
                if (nextComponentNode) {
                    var nextComponentType = nextComponentNode.getAttribute("type");
                    var nextComponentName = nextComponentNode.getAttribute("name");
                    switch (nextComponentType) {
                        case "input":
                            ctrl = input.render_ionInput(componentNode, nextComponentNode, componentData);
                            break;
                        case "select":
                            var listComponentXPath = currentLeafXPath + "[@name='" + nextComponentName + "']" + gElementXPath;
                            ctrl = selectCombo.render_ionDynamicSelect(componentNode, nextComponentNode, componentData, listComponentXPath);
                            break;
                        case "radiobuttongroup":
                            var radioButtonsComponentXPath = currentLeafXPath + "[@name='" + nextComponentName + "']" + gElementXPath;
                            ctrl = radioButtonGroup.render_ionRadioButton(nextComponentNode, radioButtonsComponentXPath);
                            break;
                    }
                } else {

                    ctrl = label.render_ionLabel(componentNode);
                }

                break;
            case "panel":
                var panelComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = panel.renderPanel(componentNode, panelComponentXPath, componentData, formElement);
                break;
            case "button":
                var buttonComponentXPath = currentLeafXPath + "[@name='" + componentName + "']";
                ctrl = button.render_ionButton(componentNode, formElement, buttonComponentXPath);
                break;
            case "imageTile":
                var buttonComponentXPath = currentLeafXPath + "[@name='" + componentName + "']";
                ctrl = button.render_ionImageTitle(componentNode, buttonComponentXPath);
                break;
            case "radiobuttongroup":
                var radioButtonsComponentXPath = currentLeafXPath + "[@name='" + componentName + "']";
                ctrl = radioButtonGroup.render_ionRadioButton(componentNode, radioButtonsComponentXPath);
                break;
            case "checkbox":
                var checkBoxComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                ctrl = checkBoxGroup.render_ionCheckBox(componentNode, checkBoxComponentXPath);
                break;
            case "image":
                var imageComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                ctrl = image.render_Image(componentNode, imageComponentXPath);
                break;
            case "list":
                var listelementXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                var listComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = list.render_ionList(componentNode, listelementXPath, listComponentXPath);
                break;
            case "dynamicList":
                var listComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                ctrl = dynamicList.render_ionDynamicList(componentNode, listComponentXPath);
                break;
            case "ngInclude":
                var ngIncludeComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                ctrl = ngInclude.render_ngInclude(componentNode, ngIncludeComponentXPath);
                break;
            case "divider":
                var dividerComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = divider.render_ionDivider(componentNode, dividerComponentXPath);
                break;
            case "SlideBox":
                var SlideboxComponentXPath = currentLeafXPath + "[@name='" + componentName + "']";
                ctrl = SlideBox.render_ionSlideBox(componentNode, SlideboxComponentXPath);
                break;
            case "SideBarList":
                var SideBarListComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = SideBarList.render_ionSideBarList(componentNode, SideBarListComponentXPath);
                break;
            case "Iconbutton":
                var IconbuttonComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = Iconbutton.render_Iconbutton(componentNode, IconbuttonComponentXPath);
                break;
            case "gallery":
                var floatbuttonComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = iongallery.render_iongallery(componentNode, floatbuttonComponentXPath);
                break;
            case "Calendar":
                var CalendarComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = Calendar.render_ionCalendar(componentNode, CalendarComponentXPath);
                break;

            /*New Components for Nikhil*/
            case "NewionSilde":
                var NewionSildeXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = NewionSilde.render_NewionSilde(componentNode, NewionSildeXPath);
                break;
            case "NewTitle":
                var NewTitleXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = NewTitle.render_NewTitle(componentNode, NewTitleXPath);
                break;
            case "pTag":
                var pTagXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = pTag.render_pTag(componentNode, pTagXPath);
                break;

            case "span":
                var spanXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = span.render_span(componentNode, spanXPath);
                break;

            case "hTag":
                var htagXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = hTag.render_hTag(componentNode, htagXPath);
                break;

            case "anchorTag":
                var anchorTagXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = anchorTag.render_anchorTag(componentNode, anchorTagXPath);
                break;
            case "anchorImage":
                var anchorTagXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = anchorImageTag.render_anchorImageTag(componentNode, anchorTagXPath);
                break;
            case "icontext":
                var icontextXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = icontext.render_icontext(componentNode, icontextXPath);
                break;
            case "icontext2":
                var icontextXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = icontext2.render_icontext2(componentNode, icontextXPath);
                break;
            case "icontext3":
                var icontextXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = icontext3.render_icontext3(componentNode, icontextXPath);
                break;
            case "ionCheckBox":
                var ionCheckBoxXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = ionCheckBox.render_ionCheckBox(componentNode, ionCheckBoxXPath);
                break;
            case "New_list":
                var listelementXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                ctrl = New_list.render_ionList(componentNode, listelementXPath, listComponentXPath);
                break;

            /*new components by hitesh*/
            case "inputFields":
                var subType = componentNode.getAttribute("subType");
                if (subType && subType.length) {
                    var listelementXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                    ctrl = inputFields.render_fields(subType, componentNode, listelementXPath, listComponentXPath);
                }
                break;
            case "simpleList":
                var listelementXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                var listComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
                ctrl = simpleList.render(componentNode, listelementXPath, listComponentXPath);
                break;
            case "tabs":
                var tabsXPath = currentLeafXPath + "[@name='" + componentName + "']" + gElementXPath;
                ctrl = tabs.render_tabs(componentNode, tabsXPath);
                break;

            default:
                break;
        }
        if (ctrl) {
            ionContent.appendChild(ctrl);
        }
    } catch (e) {
        debugger;
        showErrorMessage('create_ionControl', e.stack);
    }
}


var create_sideList = function (currentLeafNode, componentNode, currentLeafXPath, componentData) {
    try {
        var columnTheme = componentNode.getAttribute("theme");
        var componentName = componentNode.getAttribute("name");
        var userName = componentNode.getAttribute("userName");
        var userImage = componentNode.getAttribute("userImage");
        var componentTargetLeaf = componentNode.getAttribute("targetLeaf");
        var footertitle = componentNode.getAttribute("footertitle");
        var base64 = componentNode.getAttribute("base64");
        var componentNgIf = componentNode.getAttribute("ng-if");

        var panelComponentXPath = currentLeafXPath + "[@name='" + componentName + "']" + gcomponentXPath;
        var div = document.createElement("div");

        var header = document.createElement("header");
        header.setAttribute("class", columnTheme);
        var headertitlediv = document.createElement("div");


        headertitlediv.setAttribute("class", "title customTitle");
        headertitlediv.innerHTML = "<div class='accountinfotitle'>" + userName + "</div><img class='title-image-custome menu-close'  ng-click='onButtonClick(\"nexLeaf\",\"EditProfileImage\")' ng-src='" + userImage + "'>";
        // }else{
        //     headertitlediv.innerHTML="<div class='accountinfotitle'>"+userName+"</div><img class='title-image-custome menu-close' ng-click='onButtonClick(\"nexLeaf\",\"EditProfileImage\")' ng-src='"+gImageFolderPath+ userImage+"'>";
        // }

        var ionContent = document.createElement("ion-content");
        ionContent.setAttribute("class", "has-header customcontent");
        var ionlist = document.createElement("ion-list");
        var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
        for (var item = 0; item < componentList.length; item++) {
            create_ionControl(ionlist, componentList[item], null, panelComponentXPath, componentData, null);
        }

        var footer = document.createElement("ion-footer-bar");
        footer.setAttribute("align-title", "left");
        footer.setAttribute("class", "menu-close logoutbtncolor");
        var h1 = document.createElement("h1");
        h1.setAttribute("class", "title ion-power");
        h1.innerText = "Logout";
        footer.appendChild(h1);
        footer.setAttribute("ng-click", "onLogoutClick()");
        ionContent.appendChild(ionlist);
        header.appendChild(headertitlediv);
        div.appendChild(header);
        div.appendChild(ionContent);
        div.appendChild(footer);
        return div;

    } catch (e) {
        showErrorMessage('create_sideList', e.message);
    }


}

var utils = {
    /**
    * Following function helps in returning Style attributes and CSS classes for a component.
    * @param componentNode - Contains component node of a form Component which is being rendered
    */
    setCssClass: function (componentNode) {
        try {
            var theme = componentNode.getAttribute("theme");
            //var style = componentNode.getAttribute("style");
            var cssClass = theme && theme.length > 0 ? theme : "";
            //cssClass += style && style.length > 0 ? ' ' + style : "";
            return cssClass;
        } catch (e) {
            showErrorMessage('setCssClass', e.message);
        }
    },

    /**
    * Following function helps in returning Style attributes and CSS classes for a component.
    * @param componentNode - Contains component node of a form Component which is being rendered
    */
    setStyleProperties: function (componentNode) {
        try {
            var style = componentNode.getAttribute("style");
            //var cssClass = theme && theme.length > 0 ? theme : "";
            //cssClass += style && style.length > 0 ? ' ' + style : "";
            return style;
        } catch (e) {
            showErrorMessage('setStyleProperties', e.message);
        }
    },

    /**
    * Following function helps setting attributes like name, id, required, etc for a Component
    * @param componentNode - Contains component node of a form Component which is being rendered
    * @param component - Contains HTML component object which is being rendered
    * @param type(String) - Contains type of object being rendered. Label/Textbox etc
    */
    setComponentAttributes: function (componentNode, component, type) {
        try {
            var name = componentNode.getAttribute("name");
            if (name && name.length > 0) {
                component.setAttribute("id", name);
                component.setAttribute("name", name);
            }
            switch (type) {
                case "button":
                    var theme = componentNode.getAttribute("theme");
                    var style = componentNode.getAttribute("style");
                    // var cssClass = this.setCssClass(componentNode);
                    if (theme && theme.length > 0) {
                        component.setAttribute("class", 'button  ' + theme);
                    } else {
                        component.setAttribute("class", 'button  button-light');
                    }
                    if (style && style.length > 0) {
                        component.setAttribute("style", style);
                    }
                    break;
                case "customButton":
                    var theme = componentNode.getAttribute("theme");
                    var style = componentNode.getAttribute("style");
                    // var cssClass = this.setCssClass(componentNode);
                    if (theme && theme.length > 0) {
                        component.setAttribute("class", theme);
                    } else {
                        component.setAttribute("class", 'button  button-light');
                    }
                    if (style && style.length > 0) {
                        component.setAttribute("style", style);
                    }
                    break;
                case "label":
                    var theme = componentNode.getAttribute("theme");
                    var noTheme = componentNode.getAttribute("noTheme");
                    var style = componentNode.getAttribute("style");
                    var cssClass = this.setCssClass(componentNode);
                    if (theme && theme.length > 0) {
                        component.setAttribute("class", 'item item-text-wrap globalfont ' + cssClass);
                    } else {
                        component.setAttribute("class", 'item item-text-wrap globalfont');
                    }

                    if (noTheme && noTheme.length > 0 && noTheme == "true") {
                        component.setAttribute("class", cssClass);
                    }

                    if (style && style.length > 0) {
                        component.setAttribute("style", style);
                    }
                    break;
                case "input":
                    var placeholder = componentNode.getAttribute("alias");
                    if (placeholder && placeholder.length > 0) {
                        component.setAttribute("placeholder", placeholder);
                    }
                    var minchar = componentNode.getAttribute("minchar");
                    if (minchar && minchar.length > 0) {
                        component.setAttribute("ng-minlength", minchar);
                    }
                    var maxchar = componentNode.getAttribute("maxchar");
                    if (maxchar && maxchar.length > 0) {
                        component.setAttribute("ng-maxlength", maxchar);
                        component.setAttribute("maxlength", maxchar);
                    }
                    var greaterThan = componentNode.getAttribute("date-greater-than");
                    if (greaterThan && greaterThan.length > 0) {
                        component.setAttribute("date-greater-than", greaterThan);
                    }
                    var lowerThan = componentNode.getAttribute("date-lower-than");
                    if (lowerThan && lowerThan.length > 0) {
                        component.setAttribute("date-lower-than", lowerThan);
                    }

                    break;
                case "textarea":
                    var placeholder = componentNode.getAttribute("alias");
                    if (placeholder && placeholder.length > 0) {
                        component.setAttribute("placeholder", placeholder);
                    }
                    var minchar = componentNode.getAttribute("minchar");
                    if (minchar && minchar.length > 0) {
                        component.setAttribute("ng-minlength", minchar);
                    }
                    var maxchar = componentNode.getAttribute("maxchar");
                    if (maxchar && maxchar.length > 0) {
                        component.setAttribute("ng-maxlength", maxchar);
                        component.setAttribute("maxlength", maxchar);
                    }
                    component.setAttribute("ng-model", 'data.' + name);
                    break;
                case "select":
                    component.setAttribute("ng-model", 'data.' + name);
                    var notEqualValidator = componentNode.getAttribute("notEqual");
                    if (notEqualValidator && notEqualValidator.length > 0) {
                        component.setAttribute("notEqual", notEqualValidator);

                    }
                    var not_EqualValidator = componentNode.getAttribute("not-equal");
                    if (not_EqualValidator && not_EqualValidator.length > 0) {
                        component.setAttribute("not-equal", not_EqualValidator);
                    }
                    // var componentOnChange = componentNode.getAttribute("onChange");
                    // if (componentOnChange && componentOnChange.length > 0) {

                    // }


                    break;

                default:

                    break;
            }

            var display = componentNode.getAttribute("display");
            if (display && display.length > 0) {
                component.setAttribute("style", "display: none");
            }


            var mandatory = componentNode.getAttribute("mandatory");
            if (mandatory && mandatory === "true") {
                component.setAttribute("required", 'required');
            }

            var readOnly = componentNode.getAttribute("readOnly");
            if (readOnly && readOnly.length > 0) {
                component.setAttribute("disabled", true);
            }
            return component;
        } catch (e) {
            showErrorMessage('setComponentAttributes', e.message);
        }
    }
}

var ngInclude = {
    render_ngInclude: function (ngIncludeComponentNode) {
        try {
            var src = ngIncludeComponentNode.getAttribute("src");

            var ngInclude = document.createElement("ng-include");
            ngInclude.setAttribute("src", src);

            return ngInclude;
        } catch (e) {
            showErrorMessage('render_ionngInclude ', e.message);
        }
    }
}


var label = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_ionLabel: function (labelComponentNode) {
        try {
            var labelTitle = labelComponentNode.getAttribute("title");
            var labelsubTitle = labelComponentNode.getAttribute("subTitle");
            var bindhtml = labelComponentNode.getAttribute("bindhtml");
            var onclick = labelComponentNode.getAttribute("onclick");
            var componentNgIf = labelComponentNode.getAttribute("ng-if");
            var componentNgclass = labelComponentNode.getAttribute("ng-class");
            var ngIncludeComponent = labelComponentNode.getAttribute("ng-include");
            var noTheme = labelComponentNode.getAttribute("noTheme");

            var div = document.createElement("div");

            if (componentNgIf && componentNgIf.length > 0) {
                div.setAttribute("ng-if", componentNgIf);
            }

            var label = document.createElement("h2");
            if (noTheme == undefined) {
                div = utils.setComponentAttributes(labelComponentNode, div, "label");
            }


            if (bindhtml && bindhtml.length > 0) {
                label.setAttribute("compile", "renderHtml(" + labelTitle + ")");
            } else {
                label.innerText = labelTitle;
            }

            if (componentNgclass && componentNgclass.length > 0) {
                div.setAttribute("ng-class", componentNgclass);
            }

            if (onclick && onclick.length > 0) {
                div.setAttribute("ng-click", onclick);
            }

            //create Span
            if (labelsubTitle && labelsubTitle.length > 0) {

                var span = document.createElement("p");
                span.setAttribute("class", 'input-label');
                span.innerText = labelsubTitle;
                div.appendChild(span);
            }
            if (ngIncludeComponent && ngIncludeComponent.length > 0) {
                div.setAttribute("ng-include", ngIncludeComponent);
            } else {

            }
            div.appendChild(label);

            return div;
        } catch (e) {
            showErrorMessage('render_ionLabel', e.message);
        }
    }
}



var selectCombo = {
    /**
    * Following function helps rendering a select component where options are created dynamically
    * @param labelComponentNode - Contains component node of a label Component which is being rendered along with Select
    * @param listComponentNode - Contains component node of a select Component which is being rendered
    */
    render_ionDynamicSelect: function (labelComponentNode, listComponentNode, componentData, listComponentXPath) {
        try {
            var labelTheme = labelComponentNode.getAttribute("theme");
            var labelTitle = labelComponentNode.getAttribute("title");
            var display = labelComponentNode.getAttribute("display");
            var textCSS = labelComponentNode.getAttribute("textCSS");

            //create Label
            var label = document.createElement("label");
            if (labelTheme && labelTheme.length > 0) {
                label.setAttribute("class", 'item item-input item-stacked-label asSelect mwtNoPaddingTopBot validated ' + labelTheme);
            } else {
                label.setAttribute("class", 'item item-input item-stacked-label asSelect mwtNoPaddingTopBot validated');
            }

            if (display && display.length > 0) {
                label.setAttribute("style", "display: none");
            }

            //create Span
            if (labelTitle && labelTitle.length > 0) {
                var span = document.createElement("span");
                if (textCSS && textCSS.length > 0) {
                    span.setAttribute("class", 'input-label globalfont ' + textCSS);
                } else {
                    span.setAttribute("class", 'input-label globalfont');
                }
                span.innerText = labelTitle;
            }

            //create select
            var selectName = listComponentNode.getAttribute("name");
            var selectMandatory = listComponentNode.getAttribute("mandatory");
            var componentSubType = listComponentNode.getAttribute("subtype");
            var componentDataNode = listComponentNode.getAttribute("dataNode");
            var notEqualValidator = listComponentNode.getAttribute("not-equal");
            var compareToValidator = listComponentNode.getAttribute("notEqual");
            var elementList = getNodesByXPath(gAppDefXML, listComponentXPath);
            var divList = document.createElement('div');
            divList.setAttribute("class", 'uiControl');
            //divList.setAttribute("ng-show",'dynamicList.' + componentDataNode);
            var index = 0;
            //for (var index = 0; index < elementList.length; index++) {
            var elementTheme = elementList[index].getAttribute("theme");
            var elementTargetLeaf = elementList[index].getAttribute("targetLeaf");
            var elementTitle = elementList[index].getAttribute("title");
            var elementValue = elementList[index].getAttribute("value");
            var selectTag = document.createElement('select');
            //value.Id as value.Name for value in strengthList
            //item.accountNumber as item.accountNumber for item in dynamicList
            selectTag.setAttribute('class', 'selectCombo');
            selectTag.setAttribute("name", selectName);
            selectTag.setAttribute("id", selectName);
            //selectTag.setAttribute("onchange", "ResetWidth(this)");
            //selectTag.setAttribute("onblur", "ResetWidth(this)");
            //selectTag.setAttribute("onmousedown", "SetWidthToAuto(this)");
            selectTag.setAttribute("style", "overflow: hidden")
            //selectTag.setAttribute("ng-model", "item");
            //selectTag.setAttribute("ng-init", 'data.' + selectName +'=0');
            selectTag.setAttribute("ng-model", 'data.' + selectName);
            selectTag.setAttribute("ng-options", elementValue + ' as ' + elementTitle + ' for item in dynamicList.' + componentDataNode);
            if (notEqualValidator) {
                selectTag.setAttribute("not-equal", notEqualValidator);
            }
            var componentOnChange = listComponentNode.getAttribute("onChange");

            if (componentOnChange && componentOnChange.length > 0) {
                selectTag.setAttribute("ng-change", "onComboChange(data." + selectName + ",'ctrlLabel','" + selectName + "')");
            }

            if (selectMandatory && selectMandatory === "true") {
                selectTag.setAttribute("required", 'required');
            }

            if (compareToValidator && compareToValidator.length > 0) {
                selectTag.setAttribute("notEqual", compareToValidator);
                //input.setAttribute("match", inputCompareToValidator);
            }

            var optionTag = document.createElement('option');
            optionTag.setAttribute("value", "");
            optionTag.setAttribute("style", "display:none;");
            //optionTag.innerText = "-- Select --";

            selectTag.appendChild(optionTag);
            divList.appendChild(selectTag);
            // return divList;
            if (labelTitle && labelTitle.length > 0) {
                label.appendChild(span);
            }
            label.appendChild(divList);

            return label;

        } catch (e) {
            showErrorMessage('render_ionDynamicSelect', e.message);
        }
    }
}

var input = {
    render_ionInput: function (labelComponentNode, inputComponentSubType, componentData) {
        try {
            var labelTheme = labelComponentNode.getAttribute("theme");
            var labelTitle = labelComponentNode.getAttribute("title");
            var display = labelComponentNode.getAttribute("display");
            var componentNgIf = labelComponentNode.getAttribute("ng-if");
            var style = labelComponentNode.getAttribute("style");
            var componentNgShow = labelComponentNode.getAttribute("ng-show");
            var componentNgclass = labelComponentNode.getAttribute("ng-class");
            // var mandatory = labelComponentNode.getAttribute("mandatory");
            //create Label
            var label = document.createElement("label");

            if (display && display.length > 0) {
                label.setAttribute("style", "display: none");
            }

            if (style && style.length > 0) {
                label.setAttribute("style", style);
            }


            if (componentNgIf && componentNgIf.length > 0) {
                label.setAttribute("ng-if", componentNgIf);
            }

            if (labelTheme && labelTheme.length > 0) {
                label.setAttribute("class", 'item item-input item-stacked-label validated ' + labelTheme);
            } else {
                label.setAttribute("class", 'item item-input item-stacked-label validated ');
            }

            if (componentNgShow && componentNgShow.length > 0) {
                label.setAttribute("ng-show", componentNgShow);
            }

            if (componentNgclass && componentNgclass.length > 0) {
                label.setAttribute("ng-class", componentNgclass);
            }

            //create Span
            if (labelTitle && labelTitle.length > 0) {
                var span = document.createElement("span");
                span.setAttribute("class", 'input-label globalfont');
                span.innerText = labelTitle;
            }

            //create Input
            if (inputComponentSubType) {
                var inputSubType = inputComponentSubType.getAttribute("subtype");
                var inputName = inputComponentSubType.getAttribute("name");
                var inputValue = inputComponentSubType.getAttribute("value");
                var inputNgValue = inputComponentSubType.getAttribute("ng-value");
                var inputTheme = inputComponentSubType.getAttribute("theme");
                var inputCustomValidator = inputComponentSubType.getAttribute("customValidator");
                var inputCompareToValidator = inputComponentSubType.getAttribute("compare-to");
                var numericVal = inputComponentSubType.getAttribute("numericValidations");

                if (inputName != "userId") {
                    componentData[inputName] = "";
                }
                var input = document.createElement("input");
                input = utils.setComponentAttributes(inputComponentSubType, input, "input");

                if (inputSubType === "password") {
                    input.setAttribute("type", 'password');
                    //input.setAttribute("ng-pattern", "/^(?!.*([0-9])\1{2})(?=.*\d)[0-9]+$/");
                } else if (inputSubType === "numericpassword") {
                    input.setAttribute("type", 'tel');
                    if (numericVal && numericVal === "true") {
                        input.setAttribute("ng-keyup", "numericKeypUp($event,data." + inputName + ",'" + inputName + "','" + numericVal + "')");
                    } else if (numericVal && numericVal === "false") {
                        input.setAttribute("ng-keyup", "numericKeypUp($event,data." + inputName + ",'" + inputName + "','" + numericVal + "')")
                    } else {
                        input.setAttribute("ng-keyup", "numericKeypUp($event,data." + inputName + ",'" + inputName + "','false')")
                    }

                } else if (inputSubType === "date") {
                    input.setAttribute("type", 'date');
                    //  input.setAttribute("onfocus","(this.type='date')");
                    // input.setAttribute("onblur","(this.type='text')");
                } else if (inputSubType === "datetime") {
                    input.setAttribute("type", 'datetime');
                } else if (inputSubType === "numeric") {
                    input.setAttribute("type", 'tel');
                } else if (inputSubType === "email") {
                    input.setAttribute("type", 'email');
                } else if (inputSubType === "search") {
                    input.setAttribute("type", 'search');
                } else if (inputSubType === "time") {
                    input.setAttribute("type", 'time');
                    // input.setAttribute("onfocus","(this.type='time')");
                    //input.setAttribute("onblur","(this.type='text')");
                } else {
                    input.setAttribute("type", 'text');
                }
                if (inputValue && inputValue.length > 0) {
                    componentData[inputName] = inputValue;
                }
                if (inputNgValue && inputNgValue.length > 0) {
                    input.setAttribute("ng-value", inputNgValue);
                }

                input.setAttribute("class", "mwtTransBackground " + inputTheme);

                if (inputSubType === "numericpassword") {
                    var hiddenInput = document.createElement("input");
                    hiddenInput.setAttribute("type", 'hidden');
                    hiddenInput.setAttribute("name", "hd" + inputName);
                    hiddenInput.setAttribute("ng-model", 'data.' + inputName);
                    hiddenInput.setAttribute("id", "hd" + inputName);


                    if (inputMaxChar && inputMaxChar.length > 0) {
                        hiddenInput.setAttribute("maxlength", inputMaxChar);
                        hiddenInput.setAttribute("ng-maxlength", inputMaxChar);
                    }

                    if (inputCompareToValidator && inputCompareToValidator.length > 0) {
                        hiddenInput.setAttribute("compare-to", inputCompareToValidator);
                        //input.setAttribute("match", inputCompareToValidator);
                    }

                    label.appendChild(hiddenInput);
                } else if (inputSubType === "textarea") {
                    var textarea = document.createElement("textarea");
                    var rows = inputComponentSubType.getAttribute("rows");
                    if (rows && rows.length > 0) {
                        textarea.setAttribute("row", rows);
                    } else {
                        textarea.setAttribute("row", "2");
                    }
                    textarea.setAttribute("class", "mwtTransBackground " + inputTheme);
                    textarea.setAttribute("ng-model", 'data.' + inputName);
                    utils.setComponentAttributes(inputComponentSubType, textarea, "input");


                } else {
                    input.setAttribute("ng-model", 'data.' + inputName);

                    if (inputCompareToValidator && inputCompareToValidator.length > 0) {
                        input.setAttribute("compare-to", inputCompareToValidator);
                        //input.setAttribute("match", inputCompareToValidator);
                    }

                    if (inputCustomValidator && inputCustomValidator.length > 0) {
                        //input.setAttribute("customValidator", inputCustomValidator);
                        if (inputCustomValidator === "isNumber") {
                            input.setAttribute("numbers-only", "numbers-only");
                        }
                    }
                }

                if (labelTitle && labelTitle.length > 0) {
                    label.appendChild(span);
                }
                if (inputSubType === "textarea") {
                    label.appendChild(textarea);
                } else {
                    label.appendChild(input);
                }
            }

            return label;

        } catch (e) {
            showErrorMessage('render_ionInput', e.message);
        }
    },

    render_ionDateInput: function (labelComponentNode, inputComponentSubType, componentData) {
        try {
            var labelTheme = labelComponentNode.getAttribute("theme");
            var labelTitle = labelComponentNode.getAttribute("title");
            //create Label
            var label = document.createElement("label");
            if (labelTheme && labelTheme.length > 0) {
                label.setAttribute("class", 'item item-input mwtTransBackground' + labelTheme);
            } else {
                label.setAttribute("class", 'item item-input mwtTransBackground');
            }

            //create Span
            var span = document.createElement("span");
            span.setAttribute("class", 'input-label');
            span.innerText = labelTitle;

            //create Input
            var inputSubType = inputComponentSubType.getAttribute("subtype");
            var inputName = inputComponentSubType.getAttribute("name");
            componentData[inputName] = "";
            var input = document.createElement("input");
            if (inputSubType === "date") {
                input.setAttribute("type", 'date');
            } else if (inputSubType === "time") {
                input.setAttribute("type", 'time');
            } else if (inputSubType === "datetime") {
                input.setAttribute("type", 'datetime');
            }
            input.setAttribute("class", 'text');
            input.setAttribute("ng-model", 'data.' + inputName);
            input.setAttribute("name", inputName);

            label.appendChild(span);
            label.appendChild(input);
            return label;
        } catch (e) {
            showErrorMessage('render_ionDateInput', e.message);
        }
    }
}

var panel = {
    renderPanel: function (panelNode, panelComponentXPath, componentData, formElement) {
        try {
            var panelLayout = panelNode.getAttribute("layout");
            var componentName = panelNode.getAttribute("name");
            var componentStyle = panelNode.getAttribute("name");
            var columnTheme = panelNode.getAttribute("theme");
            var columnStyle = panelNode.getAttribute("style");
            var padding = panelNode.getAttribute("padding");
            var NumberOfItems = panelNode.getAttribute("Item");
            var cssClass = utils.setCssClass(panelNode);
            var SlideTheme = panelNode.getAttribute("SlideTheme");
            var Animation = panelNode.getAttribute("animation");


            switch (panelLayout) {
                case "grid":
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    var numOfColumns = parseInt(panelNode.getAttribute("nocolumns"));
                    var NoColClass = panelNode.getAttribute("NoColClass");
                    var numOfRows = Math.ceil(componentList.length / parseInt(numOfColumns));

                    var columnsClass = panelNode.getAttribute("columnClass");
                    var componentNgIf = panelNode.getAttribute("ng-if");


                    var columnsClassArr = "";
                    if (columnsClass != undefined) {
                        columnsClassArr = columnsClass.split(" ");
                    }

                    var index = 0;
                    for (var rowIndex = 0; rowIndex < parseInt(numOfRows); rowIndex++) {
                        var row = document.createElement("div");
                        var componentNgclass = panelNode.getAttribute("ng-class");
                        if (componentNgclass && componentNgclass.length > 0) {
                            row.setAttribute("ng-class", componentNgclass);
                        }
                        if (componentNgIf && componentNgIf.length > 0) {
                            row.setAttribute("ng-if", componentNgIf);
                        }
                        row.setAttribute("class", 'row ' + columnTheme);
                        // row.setAttribute("class", 'row mwtNoPadding');
                        if (componentStyle !== "use-all-width") {
                            row.setAttribute("style", columnStyle);
                        }
                        for (var colIndex = 0; colIndex < numOfColumns; colIndex++) {

                            var column = document.createElement("div");

                            // var name = componentList[index + colIndex].getAttribute("name");
                            // if (name && name.length > 0) {
                            //     column.setAttribute("id", name);
                            //     column.setAttribute("name", name);
                            // }

                            var panelComponent = componentList[index + colIndex];
                            if (panelComponent) {
                                var colCSSClass = "";

                                var commonColClass = (NoColClass && NoColClass === "true") ? "" : "col ";

                                if (columnsClassArr != "" && columnsClassArr.length > 0) {
                                    colCSSClass = commonColClass + columnsClassArr[colIndex];
                                } else {
                                    //column.setAttribute("class", 'col');
                                    colCSSClass = commonColClass;
                                }

                                if (padding) {
                                    colCSSClass = colCSSClass + " mwtNoPadding";
                                }

                                column.setAttribute("class", colCSSClass);
                                create_ionControl(column, panelComponent, null, panelComponentXPath, componentData, formElement);

                            }
                            row.appendChild(column);
                        }

                        index += parseInt(numOfColumns);
                    }

                    return row;
                    break;

                case "gridRepeatable":
                    //http://jsfiddle.net/0momap0n/
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    var numOfColumns = parseInt(panelNode.getAttribute("nocolumns"));
                    var componentNgIf = panelNode.getAttribute("ng-if");
                    var theme = panelNode.getAttribute("theme");

                    var componentDataNode = panelNode.getAttribute("dataNode");
                    var dynamicList = panelNode.getAttribute("dynamicList");

                    //var numOfRows = Math.ceil(componentList.length/parseInt(numOfColumns));
                    //console.log(numOfRows);
                    var panel = document.createElement("div");
                    if (dynamicList && dynamicList.length > 0 && dynamicList == "false") {
                        panel.setAttribute("ng-repeat", 'item in ' + componentDataNode);
                    } else if (componentDataNode && componentDataNode.length > 0) {

                        panel.setAttribute("ng-repeat", 'item in dynamicList.' + componentDataNode);
                    }

                    if (componentNgIf && componentNgIf.length > 0) {
                        panel.setAttribute("ng-if", componentNgIf);
                    }
                    panel.setAttribute("class", theme);
                    // var index = 0;
                    // //for(var rowIndex=0; rowIndex<parseInt(numOfRows); rowIndex++){
                    // var row = document.createElement("div");
                    // row.setAttribute("class", 'row');
                    // row.setAttribute("ng-if", '$index%' + numOfColumns.toString() + ' === 0');

                    // if (componentStyle !== "use-all-width") {

                    // }
                    for (var colIndex = 0; colIndex < componentList.length; colIndex++) {

                        //     var column = document.createElement("div");
                        //     column.setAttribute("ng-if", 'dynamicList.' + componentDataNode + '[$index + ' + colIndex + ']');
                        //     var panelComponent = componentList[colIndex];
                        //     if (panelComponent) {
                        //         var colCSSClass = "";
                        //         if (cssClass && cssClass.length > 0) {
                        //             //column.setAttribute("class", 'col ' + cssClass);
                        //             colCSSClass = 'col ' + cssClass;
                        //         } else {
                        //             //column.setAttribute("class", 'col');
                        //             colCSSClass = 'col '
                        //         }

                        //         if (columnsClassArr != "" && columnsClassArr.length > 0) {
                        //             colCSSClass = colCSSClass + ' ' + columnsClassArr[colIndex];
                        //         }
                        //         column.setAttribute("class", colCSSClass);
                        create_ionControl(panel, componentList[colIndex], null, panelComponentXPath, componentData, formElement);

                        //     }
                        //     row.appendChild(column);
                        // }
                        // panel.appendChild(row);
                        // //index += parseInt(numOfColumns);
                    }

                    return panel;
                    break;

                case "vscroll":
                    var scroll = panelNode.getAttribute("scroll");
                    var componentTargetLeaf = panelNode.getAttribute("targetLeaf");
                    var componentSubType = panelNode.getAttribute("subtype");
                    var easyTabs = panelNode.getAttribute("easyTabs");
                    var ngClass = panelNode.getAttribute("ng-class");
                    var resize = panelNode.getAttribute("resize");
                    var theme2 = panelNode.getAttribute("theme2");
                    var componentNgIf = panelNode.getAttribute("ng-if");
                    var componentNgShow = panelNode.getAttribute("ng-show");
                    var componentNgHide = panelNode.getAttribute("ng-hide");
                    var imageResize = panelNode.getAttribute("imageResize");
                    


                    var panel = document.createElement("div");

                    if (resize) {
                        panel.setAttribute("resize", resize);
                    }

                    if (imageResize) {
                        panel.setAttribute("imageResize", imageResize);
                    }

                    if (easyTabs === "true") {
                        panel.setAttribute("easy-tab" + "", easyTabs);
                    }

                    
                    // var editpost = panelNode.getAttribute("name2");
                    // // console.log(editpost);
                    // if(editpost==="xyz"){
                    //     panel.setAttribute("style", "display:none");
                    //     panel.setAttribute("id", panelNode.getAttribute("name"));
                    // }else{
                    //     panel.setAttribute("id", panelNode.getAttribute("name"));
                    // }

                    panel.setAttribute("id", panelNode.getAttribute("name"));

                    var style = panelNode.getAttribute("style");
                    if (style && style.length) {
                        panel.setAttribute("style", style);
                    }

                    if (ngClass) {
                        panel.setAttribute("ng-class", ngClass);
                    }

                    if (componentNgIf) {
                        panel.setAttribute("ng-if", componentNgIf);
                    }


                    if (componentNgHide) {
                        panel.setAttribute("ng-hide", componentNgHide);
                    }


                    if (componentNgShow) {
                        panel.setAttribute("ng-show", componentNgShow);
                    }


                    if (componentTargetLeaf) {
                        var newStructure = panelNode.getAttribute("newStructure");
                        if (newStructure && newStructure === "true") {
                            panel.setAttribute("ng-click", "New_onListItemClick('" + componentSubType + "','" + componentTargetLeaf + "',null,null,item)");
                        } else {
                            panel.setAttribute("ng-click", "New_onListItemClick('" + componentSubType + "','" + componentTargetLeaf + "',null,null,item.map)");
                        }
                    }

                    var onclick = panelNode.getAttribute("onclick");

                    if (onclick && onclick.length > 0) {
                        panel.setAttribute("ng-click", onclick);
                    }

                    if (columnTheme && columnTheme.length > 0) {
                        panel.setAttribute("class", columnTheme);
                    }
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var index = 0; index < componentList.length; index++) {
                        create_ionControl(panel, componentList[index], null, panelComponentXPath, componentData, formElement);
                    }

                    if (scroll && scroll.length > 0 && scroll == "true") {
                        var ionscroll = document.createElement("ion-content");
                        ionscroll.appendChild(panel);
                        return ionscroll;
                    } else {
                        return panel;
                    }

                    break;
                case "SlideBoxPanel":
                    var Normalpanel = document.createElement("div");
                    var DoesContinue = document.createElement("does-continue");
                    var componentTargetLeaf = panelNode.getAttribute("targetLeaf");
                    var componentSubType = panelNode.getAttribute("subtype");
                    var componentCustomEventListener = panelNode.getAttribute("customEventListener");
                    var componentCustomEventListenerArgs = panelNode.getAttribute("customEventListenerArgs");
                    var componentNgIf = panelNode.getAttribute("ng-if");
                    var resize = panelNode.getAttribute("resize");

                    if (resize && resize.length > 0) {
                        Normalpanel.setAttribute("resize", resize);
                    }

                    if (componentNgIf && componentNgIf.length > 0) {
                        Normalpanel.setAttribute("ng-if", componentNgIf);
                    }

                    if (columnTheme != null) {
                        Normalpanel.setAttribute("class", columnTheme);
                    }
                    if (Animation != "") {
                        Normalpanel.setAttribute("animation", Animation);
                    }

                    var span1 = document.createElement("ion-slide-box");
                    if (DoesContinue) {
                        span1.setAttribute("does-continue", "true");
                        span1.setAttribute("slide-interval", "3000");
                        var autoplay = panelNode.getAttribute("autoplay");
                        if (autoplay && autoplay.length > 0) {
                            span1.setAttribute("auto-play", autoplay);
                        } else {
                            span1.setAttribute("auto-play", "true");
                        }

                    }
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var item = 0; item < componentList.length; item++) {
                        //var create_ionControl = function(ionContent, componentNode, nextComponentNode, currentLeafXPath, componentData, formElement)
                        create_ionControl(span1, componentList[item], null, panelComponentXPath, componentData, formElement);
                    }
                    Normalpanel.appendChild(span1);
                    return Normalpanel;
                    break;

                case "collapsible":
                    var panel = document.createElement("div");
                    //panel.setAttribute("style","display: none");
                    var panelTitle = panelNode.getAttribute("title");

                    var divItem = document.createElement('div'); //div for class=item-content
                    divItem.setAttribute("class", 'item item-text-wrap globalfont label use-all-width item item-divider customDividerBar use-all-width');
                    divItem.innerText = panelTitle;

                    var panelId = "panel" + componentName;

                    divItem.setAttribute("ng-click", "onClickCollapsiblePanel('" + panelId + "')");

                    panel.appendChild(divItem);


                    var innPanel = document.createElement("div");
                    innPanel.setAttribute("id", panelId);
                    innPanel.setAttribute("style", "display: none");
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var index = 0; index < componentList.length; index++) {
                        create_ionControl(innPanel, componentList[index], null, panelComponentXPath, componentData, formElement);
                    }
                    panel.appendChild(innPanel);
                    return panel;
                    break;
                case "rPanel":
                    var panel = document.createElement("div");
                    panel.setAttribute("class", columnTheme);
                    var ngIf = panelNode.getAttribute("ng-if");
                    var onclick = panelNode.getAttribute("onclick");

                    if (ngIf && ngIf.length) {
                        panel.setAttribute("ng-if", ngIf);
                    }
                    if (onclick && onclick.length) {
                        panel.setAttribute("ng-click", onclick);
                    }

                    panel.setAttribute("ng-repeat", 'item in dynamicList.' + panelNode.getAttribute("dataNode"));
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var index = 0; index < componentList.length; index++) {
                        create_ionControl(panel, componentList[index], null, panelComponentXPath, componentData, formElement);
                    }
                    return panel;
                    break;
                case "ionOptionPanel":
                    var ionList = document.createElement("ion-list");

                    ionList.setAttribute("show-delete", "false");
                    ionList.setAttribute("can-swipe", "true");
                    ionList.setAttribute("swipe-direction", "both");

                    var ionItem = document.createElement("ion-item");

                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);

                    for (var index = 0; index < componentList.length; index++) {
                        create_ionControl(ionItem, componentList[index], null, panelComponentXPath, componentData, formElement);
                    }

                    var ionOptionButton = document.createElement("ion-option-button");
                    ionOptionButton.setAttribute("class", "icon ion-ios-trash-outline");
                    ionOptionButton.setAttribute("side", "right");
                    ionOptionButton.setAttribute("ng-click", "onBookmarkRemove(item)");

                    ionItem.appendChild(ionOptionButton);
                    ionList.appendChild(ionItem);

                    return ionList;

                    break;
                case "rSubPanel":
                    var panel = document.createElement("div");
                    panel.setAttribute("class", columnTheme);
                    var ngIf = panelNode.getAttribute("ng-if");
                    if (ngIf && ngIf.length) {
                        panel.setAttribute("ng-if", ngIf);
                    }
                    panel.setAttribute("ng-repeat", 'subitem in ' + panelNode.getAttribute("dataNode"));
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var index = 0; index < componentList.length; index++) {
                        create_ionControl(panel, componentList[index], null, panelComponentXPath, componentData, formElement);
                    }
                    return panel;
                    break;
                case "vertical":
                    var panel = document.createElement("div");
                    var componentNgIf = panelNode.getAttribute("ng-if");
                    if (componentNgIf && componentNgIf.length > 0) {
                        panel.setAttribute("ng-if", componentNgIf);
                    }
                    var style = panelNode.getAttribute("style");
                    panel.setAttribute("style", style);
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var index = 0; index < componentList.length; index++) {
                        var componentNode = componentList[index];
                        var nextComponentNode = null;
                        var componentType = componentNode.getAttribute("type");
                        switch (componentType) {
                            case "label":
                                nextComponentNode = componentList[index + 1];
                                if (nextComponentNode) {
                                    var nextComponentType = nextComponentNode.getAttribute("type");
                                    if (nextComponentType === "input" || nextComponentType === "select") {
                                        index++;
                                    } else {
                                        nextComponentNode = null;
                                    }
                                }

                                break;
                            default:
                                nextComponentNode = null;
                                break;
                        };
                        create_ionControl(panel, componentNode, nextComponentNode, panelComponentXPath, componentData, formElement);
                    }
                    return panel;

                case "New_SlideBox":
                    var New_ionlsideBox = document.createElement("ion-slide-box");
                    New_ionlsideBox.setAttribute("on-slide-changed", "slideHasChanged($index)");
                    New_ionlsideBox.setAttribute("does-continue", "true");
                    New_ionlsideBox.setAttribute("slide-interval", "2000");
                    New_ionlsideBox.setAttribute("auto-play", "true");
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);

                    for (var item = 0; item < componentList.length; item++) {
                        create_ionControl(New_ionlsideBox, componentList[item], null, panelComponentXPath, componentData, formElement);
                    }
                    return New_ionlsideBox;
                    break;

                case "Prebox":
                    var preTag = document.createElement("pre");
                    var componentList = getNodesByXPath(gAppDefXML, panelComponentXPath);
                    for (var item = 0; item < componentList.length; item++) {
                        create_ionControl(preTag, componentList[item], null, panelComponentXPath, componentData, formElement);
                    }
                    return preTag;
                    break;
                default:
                    break;
            }
        } catch (e) {
            showErrorMessage('renderPanel', e.message);
        }
    }
}

var button = {
    render_ionButton: function (buttonComponentNode, formElement, buttonComponentXPath) {
        try {
            var componentTargetLeaf = buttonComponentNode.getAttribute("targetLeaf");

            var componentSubType = buttonComponentNode.getAttribute("subtype");
            var componentTheme = buttonComponentNode.getAttribute("theme");
            var componentisNotSubmit = buttonComponentNode.getAttribute("isNotSubmit");
            var button = document.createElement('button');
            var buttonTitle = buttonComponentNode.getAttribute("title");
            var style = buttonComponentNode.getAttribute("style");
            var key = buttonComponentNode.getAttribute("key");
            var componentNgIf = buttonComponentNode.getAttribute("ng-if");
            var onclick = buttonComponentNode.getAttribute("onclick");
            var componentNgclass = buttonComponentNode.getAttribute("ng-class");
            var keyValue = buttonComponentNode.getAttribute("keyValue");
            var disabled = buttonComponentNode.getAttribute("disabled");
            var custom = buttonComponentNode.getAttribute("custom");
            var pst = buttonComponentNode.getAttribute("pst");

            if (buttonTitle == null) {
                buttonTitle = "";
            }

            if (componentNgIf && componentNgIf.length > 0) {
                button.setAttribute("ng-if", componentNgIf);
            }

            if (componentNgclass && componentNgclass.length > 0) {
                button.setAttribute("ng-class", componentNgclass);
            }

            if (custom != null || custom != false) {
                button.setAttribute("class", componentTheme);
                button.setAttribute('ng-click', onclick);
            } else if (componentTheme && componentTheme.length > 0) {
                button.setAttribute("class", componentTheme + ' button ');
            } else {
                button.setAttribute("class", 'button button-light');
            }

            if (disabled && disabled.length) {
                button.setAttribute("disabled", disabled);
            }

            //button=utils.setComponentAttributes(buttonComponentNode,button,"button");

            if (componentisNotSubmit && componentisNotSubmit === "true") {
                if (componentTargetLeaf && componentTargetLeaf.length > 0) {
                    button.setAttribute("ng-click", "onButtonClick('" + componentSubType + "','" + componentTargetLeaf + "')");
                }

                button.setAttribute("type", 'button');
            } else {

                if (pst == "post") {
                    button.setAttribute("ng-click", onclick);
                }
                button.setAttribute("type", 'submit');

                switch (componentSubType) {

                    case "procedure":
                        var procedureComponentXPath = buttonComponentXPath + gprocedureXPath;
                        //console.log('procedureComponentXPath ' + procedureComponentXPath);
                        var procedureNode = getNodesByXPath(gAppDefXML, procedureComponentXPath);
                        var functionName = procedureNode[0].getAttribute("type");
                        //console.log('functionName ' + functionName);
                        var paramComponentXPath = procedureComponentXPath + "[@type='" + functionName + "']" + gparamXPath;

                        var paramList = getNodesByXPath(gAppDefXML, paramComponentXPath);

                        if (formElement) {
                            if (paramList.length > 0) {
                                formElement.setAttribute("on-valid-submit", "onCustomEventHandler('" + componentSubType + "','" + componentTargetLeaf + "','procedureCall'," + functionName + ")");
                            } else {
                                formElement.setAttribute("on-valid-submit", functionName + "()");
                            }
                        }
                        break;
                    case "facebook":
                        var appId = buttonComponentNode.getAttribute("appId");
                        button.setAttribute("ng-click", "fbLogin('" + appId + "','" + componentTargetLeaf + "')");
                        var insertImage = buttonComponentNode.getAttribute("insertIcon");
                        //var insertText=buttonComponentNode.getAttribute("insertText");
                        if (insertImage.length > 0) {
                            var imgText = "<img  src='" + gImageFolderPath + insertImage + "' style=" + style + ">" + buttonTitle;
                            button.innerHTML = imgText;
                            //button.appendChild(insertText);
                        } else {
                            button.innerHTML = buttonTitle;
                        }
                        break;

                    case "googleplus":
                        var appId = buttonComponentNode.getAttribute("appId");
                        button.setAttribute("ng-click", "gplusLogin('" + appId + "','" + componentTargetLeaf + "')");
                        var insertImage = buttonComponentNode.getAttribute("insertIcon");
                        //var insertText=buttonComponentNode.getAttribute("insertText");
                        if (insertImage.length > 0) {
                            var imgText = "<img  src='" + gImageFolderPath + insertImage + "'style=" + style + ">" + buttonTitle;
                            button.innerHTML = imgText;
                            //button.appendChild(insertText);
                        } else {
                            button.innerHTML = buttonTitle;
                        }
                        break;

                    case "twitter":
                        var appId = buttonComponentNode.getAttribute("appId");
                        button.setAttribute("ng-click", "twitterlogin(" + appId + "," + componentTargetLeaf + ")");
                        var insertImage = buttonComponentNode.getAttribute("insertIcon");
                        //var insertText=buttonComponentNode.getAttribute("insertText");
                        if (insertImage.length > 0) {
                            var imgText = "<img  src='" + gImageFolderPath + insertImage + "' style=" + style + ">" + buttonTitle;
                            button.innerHTML = imgText;
                            //button.appendChild(insertText);
                        } else {
                            button.innerHTML = buttonTitle;
                        }
                        break;

                    case "innerSpan":
                        var span = document.createElement("span");
                        var spantheme = buttonComponentNode.getAttribute("spantheme");
                        var spantitle = buttonComponentNode.getAttribute("spantitle");
                        span.setAttribute("class", spantheme);
                        span.innerHTML = spantitle;
                        button.setAttribute("type", buttonComponentNode.getAttribute("buttontype"));
                        button.setAttribute("ng-click", buttonComponentNode.getAttribute("onclick"));
                        button.appendChild(span);
                        break;

                    default:
                        if (formElement && componentTargetLeaf && componentTargetLeaf.length > 0) {
                            formElement.setAttribute("on-valid-submit", "onButtonClick('" + componentSubType + "','" + componentTargetLeaf + "')");
                        }
                        break;
                }
            }

            if (buttonTitle) {
                button.innerText = buttonTitle;
            }


            return button;

        } catch (e) {

            showErrorMessage('render_ionButton', e.message);
        }
    },
    render_ionImageTitle: function (buttonComponentNode, buttonComponentXPath) {
        try {
            var componentTargetLeaf = buttonComponentNode.getAttribute("targetLeaf");
            var componentSubType = buttonComponentNode.getAttribute("subtype");
            var componentTheme = buttonComponentNode.getAttribute("theme");
            var componentNgIf = buttonComponentNode.getAttribute("ng-if");
            var componentTitle = buttonComponentNode.getAttribute("title");
            var componentCustomEventListener = buttonComponentNode.getAttribute("customEventListener");
            var componentCustomEventListenerArgs = buttonComponentNode.getAttribute("customEventListenerArgs");
            var imgSrc = buttonComponentNode.getAttribute("image");
            var iconImage = buttonComponentNode.getAttribute("iconImage");
            var isEditable = buttonComponentNode.getAttribute("isEditable");
            var imageFromUrl = buttonComponentNode.getAttribute("imageFromUrl");
            var onclick = buttonComponentNode.getAttribute("onclick");
            var componentNgclass = buttonComponentNode.getAttribute("ng-class");

            var div = document.createElement('div');
            div.setAttribute("class", componentTheme);

            var img = document.createElement('img');

            if (imageFromUrl && imageFromUrl.length > 0) {
                img.setAttribute("ng-src", imgSrc);
            } else {
                img.setAttribute("src", gImageFolderPath + imgSrc);
            }


            if (componentCustomEventListener && componentCustomEventListenerArgs && componentCustomEventListener.length > 0) {
                div.setAttribute("ng-click", "onCustomEventHandler('" + componentSubType + "','" + componentTargetLeaf + "','" + componentCustomEventListener + "'," + componentCustomEventListenerArgs + ")");
            } else if (componentSubType == "procedure") {
                var procedureComponentXPath = buttonComponentXPath + gprocedureXPath;

                var procedureNode = getNodesByXPath(gAppDefXML, procedureComponentXPath);
                var functionName = procedureNode[0].getAttribute("type");
                //console.log('functionName ' + functionName);
                var paramComponentXPath = procedureComponentXPath + "[@type='" + functionName + "']" + gparamXPath;
                var paramList = getNodesByXPath(gAppDefXML, paramComponentXPath);

                if (paramList && paramList.length > 0) {
                    var value;
                    for (var index = 0; index < paramList.length; index++) {
                        value = paramList[index].getAttribute("value");
                    }
                    div.setAttribute("ng-click", "onClickProcedureCall('" + functionName + "','" + componentTargetLeaf + "'," + value + ")");
                } else {
                    div.setAttribute("ng-click", "onClickProcedureCall('" + functionName + "','" + componentTargetLeaf + "')");
                }
            } else {
                div.setAttribute("ng-click", "onButtonClick('" + componentSubType + "','" + componentTargetLeaf + "')");
            }

            if (componentNgIf && componentNgIf.length > 0) {
                div.setAttribute("ng-if", componentNgIf);
            }

            if (onclick && onclick.length > 0) {
                div.setAttribute("ng-click", onclick);
            }

            if (componentNgclass && componentNgclass.length > 0) {
                div.setAttribute("ng-class", componentNgclass);
            }
            if (iconImage != null) {
                var i = document.createElement("i");
                var iconimage = document.createElement("img");
                iconimage.src = gImageFolderPath + iconImage;
                iconimage.setAttribute("style", "width:30px");
                iconimage.setAttribute("height", "30px");
                i.appendChild(iconimage);
                div.appendChild(i);
            }

            div.appendChild(img);
            if (componentTitle && componentTitle.length > 0) {
                var paragraph = document.createElement("p");
                paragraph.innerText = componentTitle;
                div.appendChild(paragraph);
            }
            //img.innerText =     componentTitle;
            return div;
        } catch (e) {
            showErrorMessage('render_ionImageTitle', e.message);
        }
    }
}

var radioButtonGroup = {
    render_ionRadioButton: function (radioButtonComponentNode, radioButtonsComponentXPath) {
        try {
            var elementList = getNodesByXPath(gAppDefXML, radioButtonsComponentXPath + gElementXPath);


            var divList = document.createElement('div');
            //divList.setAttribute("class", 'list');

            for (var index = 0; index < elementList.length; index++) {

                var theme = elementList[index].getAttribute("theme")

                var labelItem = document.createElement('label'); //label creation for class=item item-radio
                if (theme && theme.length > 0) {
                    labelItem.setAttribute("class", "ng-pristine ng-untouched ng-valid " + theme);
                }
                // labelItem.innerText=elementList[index].getAttribute("name")

                var radioInput = document.createElement('ion-radio-custom'); //input type
                // radioInput.innerText="Cat";
                radioInput.setAttribute("ng-change", 'onRadioButtonChange(data.' + radioButtonComponentNode.getAttribute("name") + ')');
                radioInput.setAttribute("name", radioButtonComponentNode.getAttribute("name"));
                radioInput.setAttribute("ng-model", 'data.' + radioButtonComponentNode.getAttribute("name"));
                radioInput.setAttribute("value", elementList[index].getAttribute("name"));

                var divItem = document.createElement('div'); //div for class=item-content
                divItem.setAttribute("class", 'item-content');
                radioInput.innerText = elementList[index].getAttribute("title");

                var radioIcon = document.createElement('i');
                radioIcon.setAttribute("class", 'radio-icon ion-checkmark'); //class for icon creation

                var elementName = elementList[index].getAttribute("name");

                // if(defaultSelected === elementName){
                //radioInput.setAttribute("value",defaultSelected);
                //  radioInput.setAttribute("ng-value","on");
                //}

                divList.appendChild(radioInput); //appending
                //divList.appendChild(labelItem);
                // labelItem.appendChild(radioIcon);
                //  divList.appendChild(labelItem);
            }

            return divList;
        } catch (e) {
            showErrorMessage('render_ionRadioButton', e.message);
        }
    }
}

var checkBoxGroup = {


    render_ionCheckBox: function (checkBoxComponentNode, checkBoxComponentXPath) {
        try {
            var mandatory = checkBoxComponentNode.getAttribute("mandatory");
            var theme = checkBoxComponentNode.getAttribute("theme");

            var elementList = getNodesByXPath(gAppDefXML, checkBoxComponentXPath);
            //var unorderlist = document.createElement('ul'); //creation of parent unordered list
            //unorderlist.setAttribute("class", 'list');
            //var div = document.createElement('div');
            //div.setAttribute("class",theme);
            // for (var index = 0; index < elementList.length; index++) {
            //   var liItem = document.createElement('li');            //creation of list
            //   liItem.setAttribute("class",'item item-checkbox');

            //   var checkboxLabel = document.createElement('label');      //checkbox label creation
            //   checkboxLabel.setAttribute("class",'checkbox');

            //   var checkboxInput = document.createElement('input');      //input type check box
            //   checkboxInput.setAttribute("type",'checkbox');
            //   liItem.innerText = elementList[index].getAttribute("title");

            //   checkboxLabel.appendChild(checkboxInput);//appending
            //   liItem.appendChild(checkboxLabel);
            //   unorderlist.appendChild(liItem);
            // }

            // var liItem = document.createElement('li'); //creation of list
            // liItem.setAttribute("class", 'item item-toggle inputDate');
            // liItem.innerText=checkBoxComponentNode.getAttribute("title");

            // var checkboxLabel = document.createElement('label'); //checkbox label creation
            // checkboxLabel.setAttribute("class", 'toggle toggle-assertive');

            var checkboxInput = document.createElement('input'); //input type check box
            checkboxInput.setAttribute("type", 'checkbox');
            checkboxInput.setAttribute("class", theme);
            var customModal = checkBoxComponentNode.getAttribute("customModal")
            if (customModal && customModal.length > 0) {
                checkboxInput.setAttribute("ng-model", checkBoxComponentNode.getAttribute("key"));
            } else {
                checkboxInput.setAttribute("ng-model", "data." + checkBoxComponentNode.getAttribute("name"));
            }

            checkboxInput.setAttribute("ng-change", "onCheckboxChange('" + checkBoxComponentNode.getAttribute("name") + "',data." + checkBoxComponentNode.getAttribute("name") + "," + checkBoxComponentNode.getAttribute("key") + ")");
            checkboxInput.setAttribute("ng-value", '{{data.' + checkBoxComponentNode.getAttribute("name") + '}}')

            //var span = document.createElement('div');
            //span.innerText = checkBoxComponentNode.getAttribute("title");
            //span.setAttribute("class", "col30");

            // var div = document.createElement('div');
            // //span.innerText = checkBoxComponentNode.getAttribute("title");
            // div.setAttribute("class", "handle "+theme);
            // //liItem.appendChild(span);

            if (mandatory && mandatory === "true") {
                checkboxInput.setAttribute("required", 'required');
            }

            // div.appendChild(checkboxInput); //appending
            //div.appendChild(checkboxInput);
            //checkboxInput.appendChild();
            //span.appendChild(div);
            //liItem.appendChild(checkboxLabel);
            //unorderlist.appendChild(liItem);

            return checkboxInput;
        } catch (e) {
            showErrorMessage('render_ionCheckBox', e.message);
        }
    }
}

var image = {
    render_Image: function (imageComponentNode, imageComponentXPath) {
        try {
            var imgSrc = imageComponentNode.getAttribute("image");
            var imgStyle = imageComponentNode.getAttribute("style");
            var onclick = imageComponentNode.getAttribute("onclick");
            var name = imageComponentNode.getAttribute("name");
            var width = imageComponentNode.getAttribute("width");
            var height = imageComponentNode.getAttribute("height");
            var componentNgIf = imageComponentNode.getAttribute("ng-if");
            var noParent = imageComponentNode.getAttribute("noParent");
            var isVideo = imageComponentNode.getAttribute("isVideo");
            //var themex =imageComponentNode.getAttribute("theam2");

            /*Css Class*/
            var imageTheme = imageComponentNode.getAttribute("imageTheme");
            var theme = imageComponentNode.getAttribute("theme");
            var componentNgclass = imageComponentNode.getAttribute("ng-class");

            var imgDiv = document.createElement("div");
            if (imgStyle && imgStyle.length > 0) {
                imgDiv.setAttribute("style", imgStyle);
            }

            if (onclick && onclick.length > 0) {
                imgDiv.setAttribute("ng-click", onclick);
            }

            if (componentNgIf && componentNgIf.length > 0) {
                imgDiv.setAttribute("ng-if", componentNgIf);

            }

            var imageFromUrl = imageComponentNode.getAttribute("imageFromUrl");
            var imageResize = imageComponentNode.getAttribute("imageResize");

            var img = document.createElement('img');
            var onclick2 = imageComponentNode.getAttribute("onclick2");

            if (name && name.length > 0) {
                img.setAttribute("id", name);
            }
            if (imageResize) {
                img.setAttribute("imageResize", imageResize);
            }

            if (onclick2 && onclick2.length > 0) {
                img.setAttribute("ng-click", onclick2);
            }

            if (width && width.length > 0) {
                img.setAttribute("width", width);
            }
            if (height && height.length > 0) {
                img.setAttribute("height", height);
            }

            if (componentNgclass && componentNgclass.length > 0) {
                img.setAttribute("ng-class", componentNgclass);
            }

            if (imageFromUrl && imageFromUrl.length > 0) {
                img.setAttribute("ng-src", imgSrc);
            } else {
                img.setAttribute("src", gImageFolderPath + imgSrc);
            }
            imgDiv.setAttribute("class", theme);
            img.setAttribute("class", imageTheme);

            if (noParent && noParent == "true") {
                return img;
            }
            else {
                imgDiv.appendChild(img);
                if (isVideo && isVideo === "true") {
                    //<a href="#"><img class="video" src="images/video-mark.png"></a>
                    playButton = document.createElement("a");
                    playButton.setAttribute("href", "#");
                    var videoMark = document.createElement("img");
                    videoMark.setAttribute("class", "video");
                    videoMark.setAttribute("src", gImageFolderPath + "video-player.png");
                    playButton.appendChild(videoMark);
                    imgDiv.appendChild(playButton);
                }

                return imgDiv;
            }




        } catch (e) {
            showErrorMessage('render_Image', e.message);
        }
    }
}

var list = {
    render_ionList: function (listComponentNode, listelementXPath, listComponentXPath) {
        try {
            var componentSubType = listComponentNode.getAttribute("subtype");
            var elementList = getNodesByXPath(gAppDefXML, listelementXPath);
            var componentList = getNodesByXPath(gAppDefXML, listComponentXPath);
            var ThemeClass = listComponentNode.getAttribute("theme");
            var componentPosition = listComponentNode.getAttribute("componentPosition");
            var componentDataNode = listComponentNode.getAttribute("dataNode");
            var dynamicList = listComponentNode.getAttribute("dynamicList");
            var style = listComponentNode.getAttribute("style");
            var elementTargetLeaf = listComponentNode.getAttribute("targetLeaf");
            var horizontalscroll = listComponentNode.getAttribute("horizontalscroll");
            var listTheme = listComponentNode.getAttribute("listTheme");
            var componentNgIf = listComponentNode.getAttribute("ng-if");
            var anchorTagDisp = listComponentNode.getAttribute("anchorTagDisp");
            var componentNgShow = listComponentNode.getAttribute("ng-show");
            var componentNgForm = listComponentNode.getAttribute("ngForm");
            var divList;
            var anchorTag;

            if (anchorTagDisp && anchorTagDisp.length > 0) {
                anchorTag = document.createElement('a');
            } else {
                anchorTag = document.createElement('div');
            }

            if (componentDataNode && componentDataNode.length > 0) {
                if (dynamicList && dynamicList.length > 0 && dynamicList == "false") {
                    anchorTag.setAttribute("ng-repeat", 'item in ' + componentDataNode);
                } else {
                    anchorTag.setAttribute("ng-repeat", 'item in dynamicList.' + componentDataNode);
                }
            }

            if (componentNgShow && componentNgShow.length > 0) {
                anchorTag.setAttribute("ng-show", componentNgShow);
            }

            if (horizontalscroll && horizontalscroll.length > 0 && horizontalscroll == "true") {
                divList = document.createElement('ion-scroll');
                divList.setAttribute("direction", "x");
            } else {
                divList = document.createElement('div');
                divList.setAttribute("class", "list");
            }

            if (componentNgIf && componentNgIf.length > 0) {
                divList.setAttribute("ng-if", componentNgIf);
            }

            if (componentNgForm && componentNgForm.length > 0) {
                divList.setAttribute("ng-form", "innerForm");
            }
            divList.setAttribute("style", style);

            if (listTheme && listTheme.length > 0) {
                divList.setAttribute("class", "list " + listTheme);
            }

            if (componentPosition == "top") {
                for (var item = 0; item < componentList.length; item++) {
                    var nextComponentNode = componentList[item + 1];
                    if (nextComponentNode) {
                        var nextComponentType = nextComponentNode.getAttribute("type");
                        if (nextComponentType === "input" || nextComponentType === "select") {
                            item++;
                        } else {
                            nextComponentNode = null;
                        }
                    }
                    create_ionControl(anchorTag, componentList[item], nextComponentNode, listComponentXPath, listComponentNode, null);
                }
            }

            for (var index = 0; index < elementList.length; index++) {
                var orderlist = document.createElement("li");
                var elementTheme = elementList[index].getAttribute("theme");
                var componentNgIf = elementList[index].getAttribute("ng-if");
                var isDelete = elementList[index].getAttribute("isDelete");
                var insertbreak = elementList[index].getAttribute("insertbreak");
                var textbold = elementList[index].getAttribute("textbold");
                var insertIcon = elementList[index].getAttribute("insertIcon");
                var badgeText = elementList[index].getAttribute("badgeText");
                var bindhtml = elementList[index].getAttribute("bindhtml");
                var onclick = elementList[index].getAttribute("onclick");

                // var type=elementList[index].getAttribute("type");
                //  console.log(elementTheme);
                switch (componentSubType) {
                    case "icontextlist":
                        if (ThemeClass && ThemeClass.length > 0) {
                            anchorTag.setAttribute("class", "item item-thumbnail-left  " + ThemeClass);
                        } else {
                            anchorTag.setAttribute("class", 'item item-thumbnail-left');
                        }
                        break;
                    case "icontextarrowlist":
                        if (ThemeClass && ThemeClass.length > 0) {
                            orderlist.setAttribute("class", "item item-icon-left item-icon-right " + ThemeClass);
                        } else {
                            orderlist.setAttribute("class", 'item item-icon-left item-icon-right');
                        }

                        break;
                    case "plaintextlist":
                    case "plaintextarrowlist":
                        if (ThemeClass && ThemeClass.length > 0) {
                            // anchorTag.setAttribute("class", "item item-icon-right globalfont customlistelement "+elementTheme );
                            anchorTag.setAttribute("class", "item item-icon-left item-icon-right " + ThemeClass);
                        } else {
                            anchorTag.setAttribute("class", 'item item-icon-left item-icon-right');
                        }
                        break;
                    case "complexList":
                        anchorTag.setAttribute("class", ThemeClass);
                    case "avatar":
                        anchorTag.setAttribute("class", ThemeClass);
                }


                var imageSrc = elementList[index].getAttribute("image");
                if (imageSrc && imageSrc.length > 0) {
                    var imageTheme = elementList[index].getAttribute("imageTheme");
                    var imgIcon = document.createElement('img');
                    var imageFromUrl = elementList[index].getAttribute("imageFromUrl");
                    if (imageFromUrl && imageFromUrl.length > 0) {
                        imgIcon.setAttribute("ng-src", imageSrc);
                    } else {
                        imgIcon.setAttribute("src", gImageFolderPath + imageSrc);
                    }
                    if (componentNgIf && componentNgIf.length > 0) {
                        imgIcon.setAttribute("ng-if", componentNgIf);
                    }
                    if (onclick && onclick.length > 0) {
                        imgIcon.setAttribute("ng-click", onclick);
                    }
                    imgIcon.setAttribute("class", imageTheme);
                    anchorTag.appendChild(imgIcon);

                }

                var title = elementList[index].getAttribute("title");
                //console.log('title'+title);
                if (title && title.length > 0) {
                    var heading = document.createElement('h2');
                    if (componentNgIf && componentNgIf.length > 0) {
                        heading.setAttribute("ng-if", componentNgIf);
                    }

                    if (bindhtml && bindhtml.length > 0) {
                        heading.setAttribute("ng-bind-html", labelTitle)
                    } else {
                        heading.innerHTML = title;
                    }

                    if (onclick && onclick.length > 0) {
                        heading.setAttribute("ng-click", onclick);
                    }

                    if (textbold == "true") {
                        heading.setAttribute("style", "font-weight:900");
                    }
                    heading.setAttribute("class", elementTheme);
                    anchorTag.appendChild(heading);
                }

                var subTitle = elementList[index].getAttribute("subTitle");
                if (subTitle && subTitle.length > 0) {
                    var paragraph = document.createElement('p');
                    if (componentNgIf && componentNgIf.length > 0) {
                        paragraph.setAttribute("ng-if", componentNgIf);
                    }
                    if (onclick && onclick.length > 0) {
                        paragraph.setAttribute("ng-click", onclick);
                    }
                    //paragraph.setAttribute("style","white-space: normal;color:#000")
                    paragraph.setAttribute("class", elementTheme);

                }

                var iconChevron = document.createElement("i");
                var icon = document.createElement("i");

                var rightCSS = "";
                if (isDelete && isDelete.length > 0) {
                    iconChevron.setAttribute("class", 'icon ion-trash-b');

                } else {
                    iconChevron.setAttribute("class", 'icon ion-chevron-right');
                }


                switch (componentSubType) {
                    case "icontextlist":
                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        break;
                    case "plaintextlist":
                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        break;
                    case "icontextarrowlist":
                        anchorTag.appendChild(imgIcon);
                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        anchorTag.appendChild(iconChevron);
                        break;
                    case "plaintextarrowlist":
                        //anchorTag.innerText = elementList[index].getAttribute("title");

                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        anchorTag.appendChild(iconChevron);
                        break;

                    case "complexList":
                        if (subTitle && subTitle.length > 0) {
                            if (insertIcon && insertIcon.length > 0) {
                                var icon = "<img src='" + gImageFolderPath + insertIcon + "'>";
                                paragraph.innerHTML = icon + subTitle;
                            } else if (bindhtml && bindhtml.length > 0) {
                                paragraph.setAttribute("compile", "renderHtml(" + subTitle + ")");
                            } else {
                                paragraph.innerHTML = subTitle;
                            }

                            anchorTag.appendChild(paragraph);
                        }
                        break;
                }
                if (elementTargetLeaf && elementTargetLeaf.length > 0) {
                    var functionName = listComponentNode.getAttribute("functionName");
                    var value = listComponentNode.getAttribute("value");
                    anchorTag.setAttribute("ng-click", "onClickProcedureCall('" + functionName + "','" + elementTargetLeaf + "'," + value + ")");
                }
            }

            if (componentPosition == "bottom") {
                for (var item = 0; item < componentList.length; item++) {
                    var nextComponentNode = componentList[item + 1];
                    if (nextComponentNode) {
                        var nextComponentType = nextComponentNode.getAttribute("type");
                        if (nextComponentType === "input" || nextComponentType === "select") {
                            item++;
                        } else {
                            nextComponentNode = null;
                        }
                    }
                    create_ionControl(anchorTag, componentList[item], nextComponentNode, listComponentXPath, listComponentNode, null);
                }
            }

            divList.appendChild(anchorTag);
            return divList;
        } catch (e) {
            showErrorMessage('render_ionList', e.message);
        }
    }
}


var New_list = {
    render_ionList: function (listComponentNode, listelementXPath, listComponentXPath) {
        try {

            var elementList = getNodesByXPath(gAppDefXML, listelementXPath);
            var componentList = getNodesByXPath(gAppDefXML, listComponentXPath);

            var componentSubType = listComponentNode.getAttribute("subtype");

            /*Class Attributes*/
            var Theme = listComponentNode.getAttribute("theme");
            var style = listComponentNode.getAttribute("style");
            var inlineTheme = listComponentNode.getAttribute("inlineTheme");

            var componentDataNode = listComponentNode.getAttribute("dataNode");

            /*ng Attributes*/
            var componentNgIf = listComponentNode.getAttribute("ng-if");
            var componentNgShow = listComponentNode.getAttribute("ng-show");

            var inline = listComponentNode.getAttribute("inline");

            if (inline) {
                var orderlist = document.createElement("li");
                orderlist.setAttribute("class", inlineTheme);
            }

            divList = document.createElement('ul');
            divList.setAttribute("class", "list " + Theme);

            if (componentNgIf && componentNgIf.length > 0) {
                divList.setAttribute("ng-if", componentNgIf);
            }

            divList.setAttribute("style", style);

            if (Theme && Theme.length > 0) {
                divList.setAttribute("class", Theme);
            }

            for (var index = 0; index < elementList.length; index++) {
                var inlinelist = document.createElement("li");
                var anchorTag = document.createElement("a");

                /*Elements class attributes*/
                var elementTheme = elementList[index].getAttribute("theme");
                var liTheme = elementList[index].getAttribute("liTheme");
                var subTitleTheme = elementList[index].getAttribute("subTitleTheme");
                var titleTheme = elementList[index].getAttribute("titleTheme");
                var componentNgclass = elementList[index].getAttribute("ng-class");

                var componentNgIf = elementList[index].getAttribute("ng-if");
                var elementTargetLeaf = elementList[index].getAttribute("targetLeaf");
                var key = elementList[index].getAttribute("Key");
                var Value = elementList[index].getAttribute("Value");

                if (liTheme) {
                    inlinelist.setAttribute("class", liTheme);
                }

                switch (componentSubType) {
                    case "icontextlist":
                        if (componentNgIf) {
                            inlinelist.setAttribute("ng-if", componentNgIf);
                        }

                        if (elementTheme && elementTheme.length > 0) {
                            anchorTag.setAttribute("class", elementTheme);
                        }
                        break;
                    case "icontextarrowlist":
                        if (elementTheme && elementTheme.length > 0) {
                            anchorTag.setAttribute("class", "item item-icon-left item-icon-right " + elementTheme);
                        } else {
                            anchorTag.setAttribute("class", 'item item-icon-left item-icon-right');
                        }
                        break;
                    case "plaintextlist":
                    case "plaintextarrowlist":
                        if (elementTheme && elementTheme.length > 0) {
                            anchorTag.setAttribute("class", "item item-icon-left item-icon-right " + elementTheme);
                        } else {
                            anchorTag.setAttribute("class", 'item item-icon-left item-icon-right');
                        }
                        break;
                }

                var imageSrc = elementList[index].getAttribute("image");



                if (imageSrc && imageSrc.length > 0) {
                    var imageTheme = elementList[index].getAttribute("imageTheme");
                    var imageHight = elementList[index].getAttribute("imageHight");
                    var imageWidth = elementList[index].getAttribute("imageWidth");

                    var imgIcon = document.createElement('img');
                    var imageFromUrl = elementList[index].getAttribute("imageFromUrl");
                    if (imageFromUrl && imageFromUrl.length > 0) {
                        imgIcon.setAttribute("ng-src", imageSrc);
                    } else {
                        imgIcon.setAttribute("src", gImageFolderPath + imageSrc);
                    }
                    if (componentNgIf && componentNgIf.length > 0) {
                        imgIcon.setAttribute("ng-if", componentNgIf);
                    }

                    if (imageHight && imageHight.length > 0) {
                        imgIcon.setAttribute("height", imageHight);
                    }

                    if (imageWidth && imageWidth.length > 0) {
                        imgIcon.setAttribute("width", imageWidth);
                    }

                    imgIcon.setAttribute("class", imageTheme);
                    anchorTag.appendChild(imgIcon);
                }

                var title = elementList[index].getAttribute("title");

                if (title && title.length > 0) {
                    var heading = document.createElement('h2');
                    if (componentNgIf && componentNgIf.length > 0) {
                        heading.setAttribute("ng-if", componentNgIf);
                    }

                    heading.innerText = title;

                    if (titleTheme) {
                        heading.setAttribute("class", titleTheme);
                    }

                    anchorTag.appendChild(heading);
                }

                var subTitle = elementList[index].getAttribute("subTitle");
                if (subTitle && subTitle.length > 0) {
                    var paragraph = document.createElement('p');

                    if (componentNgIf && componentNgIf.length > 0) {
                        paragraph.setAttribute("ng-if", componentNgIf);
                    }

                    if (subTitleTheme) {
                        paragraph.setAttribute("class", subTitleTheme);
                    }

                }

                var iconChevron = document.createElement("i");
                var icon = document.createElement("i");

                var rightCSS = "";
                iconChevron.setAttribute("class", 'icon ion-chevron-right directory-chevron');

                anchorTag.setAttribute("ng-click", "New_onListItemClick('" + componentSubType + "','" + elementTargetLeaf + "','" + key + "','" + Value + "',item.map)");

                switch (componentSubType) {
                    case "icontextlist":
                        var titleTheme = elementList[index].getAttribute("titleTheme");
                        var value = elementList[index].getAttribute("value");
                        var key = elementList[index].getAttribute("key");
                        var Title = elementList[index].getAttribute("Title");

                        var span = document.createElement('span');
                        span.setAttribute("class", titleTheme);
                        anchorTag.appendChild(span);
                        span.innerText = Title;
                        anchorTag.setAttribute("ng-click", "onIconbuttonClick('" + key + "','" + value + "')");
                        break;
                    case "plaintextlist":
                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        break;
                    case "icontextarrowlist":
                        anchorTag.appendChild(imgIcon);
                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        anchorTag.appendChild(iconChevron);
                        break;
                    case "plaintextarrowlist":
                        //anchorTag.innerText = elementList[index].getAttribute("title");

                        if (subTitle && subTitle.length > 0) {
                            paragraph.innerHTML = subTitle;
                            anchorTag.appendChild(paragraph);
                        }
                        anchorTag.appendChild(iconChevron);
                        break;

                    case "iconButton":

                }
                inlinelist.appendChild(anchorTag);

                if (inline) {
                    orderlist.appendChild(anchorTag);
                    divList.appendChild(orderlist);
                } else {
                    divList.appendChild(inlinelist);
                }

            }

            return divList;
        } catch (e) {
            showErrorMessage('render_ionList', e.message);
        }
    }
}

var dynamicList = {
    render_ionDynamicList: function (listComponentNode, listComponentXPath) {
        try {
            var componentSubType = listComponentNode.getAttribute("subtype");
            var componentDataNode = listComponentNode.getAttribute("dataNode");
            var elementList = getNodesByXPath(gAppDefXML, listComponentXPath);
            var divList = document.createElement('div');
            divList.setAttribute("class", 'list');
            divList.setAttribute("ng-show", 'dynamicList.' + componentDataNode + '.length');
            var index = 0;
            //for (var index = 0; index < elementList.length; index++) {
            var elementTheme = elementList[index].getAttribute("theme");
            var elementTargetLeaf = elementList[index].getAttribute("targetLeaf");
            var isDelete = elementList[index].getAttribute("isDelete");
            var anchorTag = document.createElement('a');
            //anchorTag.setAttribute("ng-repeat", 'item in dynamicList.' + componentDataNode);

            switch (componentSubType) {
                case "icontextlist":
                case "icontextarrowlist":
                    if (elementTheme && elementTheme.length > 0) {
                        anchorTag.setAttribute("class", 'item item-thumbnail-left item-icon-right globalfont ' + elementTheme);
                    } else {
                        anchorTag.setAttribute("class", 'item item-thumbnail-left item-icon-right globalfont');
                    }
                    break;
                case "plaintextlist":
                case "plaintextarrowlist":
                    if (elementTheme && elementTheme.length > 0) {
                        anchorTag.setAttribute("class", 'item item-icon-right globalfont ' + elementTheme);
                    } else {
                        anchorTag.setAttribute("class", 'item item-icon-right globalfont');
                    }
                    break;
            }

            var imageSrc = elementList[index].getAttribute("image");
            var imgIcon = document.createElement('img');
            if (imageSrc && imageSrc.length > 0) {
                imgIcon.setAttribute("src", gImageFolderPath + imageSrc);
            }

            var heading = document.createElement('h2');
            heading.innerText = elementList[index].getAttribute("title");

            var subTitle = elementList[index].getAttribute("subTitle");
            var paragraph = document.createElement('p');

            var iconChevron = document.createElement("i");
            var rightCSS = "";
            if (isDelete && isDelete.length > 0) {
                iconChevron.setAttribute("class", 'icon ion-trash-b');

            } else {
                iconChevron.setAttribute("class", 'icon ion-chevron-right');
            }

            switch (componentSubType) {
                case "icontextlist":
                    anchorTag.appendChild(imgIcon);
                    anchorTag.appendChild(heading);
                    if (subTitle && subTitle.length > 0) {
                        paragraph.innerText = subTitle;
                        anchorTag.appendChild(paragraph);
                    }
                    break;
                case "plaintextlist":
                    anchorTag.appendChild(heading);
                    if (subTitle && subTitle.length > 0) {
                        paragraph.innerText = subTitle;
                        anchorTag.appendChild(paragraph);
                    }
                    break;
                case "icontextarrowlist":
                    anchorTag.appendChild(imgIcon);
                    anchorTag.appendChild(heading);
                    if (subTitle && subTitle.length > 0) {
                        paragraph.innerText = subTitle;
                        anchorTag.appendChild(paragraph);
                    }
                    anchorTag.appendChild(iconChevron);
                    break;
                case "plaintextarrowlist":
                    //anchorTag.innerText = elementList[index].getAttribute("title");
                    anchorTag.appendChild(heading);
                    if (subTitle && subTitle.length > 0) {
                        paragraph.innerText = subTitle;
                        anchorTag.appendChild(paragraph);
                    }
                    anchorTag.appendChild(iconChevron);
                    break;
            }
            //anchorTag.setAttribute("ng-click", "onButtonClick('"+ componentSubType +"','"+ elementTargetLeaf +"')");
            anchorTag.setAttribute("ng-click", "onListItemClick('" + componentSubType + "','" + elementTargetLeaf + "', item)");
            divList.appendChild(anchorTag);

            return divList;
        } catch (e) {
            showErrorMessage('render_ionDynamicList', e.message);
        }
    }
}

var divider = {
    render_ionDivider: function (dividerComponentNode) {
        try {
            var divider = document.createElement('div');
            var cssClass = utils.setCssClass(dividerComponentNode);
            var componentShowDivider = dividerComponentNode.getAttribute("showDivider");
            var reqDate = dividerComponentNode.getAttribute("asDate");
            divider.setAttribute("class", 'item item-divider ' + cssClass);
            if (componentShowDivider) {
                divider.setAttribute("ng-show", componentShowDivider);
            }
            if (reqDate) {
                divider.setAttribute("ng-bind", reqDate);
            } else {
                divider.innerText = dividerComponentNode.getAttribute("title");
            }

            return divider;
        } catch (e) {
            showErrorMessage('render_ionDivider', e.message);
        }
    }
}

var SlideBox = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_ionSlideBox: function (SlideBoxComponentNode, SlideboxComponentXPath) {
        try {

            var SourceAddress = SlideBoxComponentNode.getAttribute("Source");
            var ThemeClass = SlideBoxComponentNode.getAttribute("theme");
            var title = SlideBoxComponentNode.getAttribute("title");
            var subTitle = SlideBoxComponentNode.getAttribute("subTitle");
            var dataNode = SlideBoxComponentNode.getAttribute("dataNode");
            var componentSubType = SlideBoxComponentNode.getAttribute("subtype");
            var componentTargetLeaf = SlideBoxComponentNode.getAttribute("targetLeaf");
            var imageFromUrl = SlideBoxComponentNode.getAttribute("imageFromUrl");
            var noParent = SlideBoxComponentNode.getAttribute("noParent");
            var slideBoxStyle = SlideBoxComponentNode.getAttribute("slideBoxStyle");
            var SourceWidth = SlideBoxComponentNode.getAttribute("SourceWidth");
            var SourceHeight = SlideBoxComponentNode.getAttribute("SourceHeight");
            var Key = SlideBoxComponentNode.getAttribute("Key");
            var Value = SlideBoxComponentNode.getAttribute("Value");
            var resize = SlideBoxComponentNode.getAttribute("resize");
            var SlideBox = document.createElement("ion-slide");
            if (slideBoxStyle && slideBoxStyle.length) {
                SlideBox.setAttribute("class", slideBoxStyle);
                SlideBox.setAttribute("resize", resize);
            }
            if (dataNode && dataNode.length > 0) {
                SlideBox.setAttribute("ng-repeat", "item in " + dataNode);
            }
            // if(componentSubType=="procedure"){
            //     var procedureComponentXPath = SlideboxComponentXPath + gprocedureXPath;
            //     var procedureNode = getNodesByXPath(gAppDefXML, procedureComponentXPath);
            //     var functionName = procedureNode[0].getAttribute("type");
            //     var paramComponentXPath = procedureComponentXPath + "[@type='" + functionName + "']" + gparamXPath;
            //     var paramList = getNodesByXPath(gAppDefXML, paramComponentXPath);

            //     if(paramList && paramList.length>0){
            //         var value;
            //         for(var index=0;index<paramList.length;index++){
            //             value=paramList[index].getAttribute("value");
            //         }
            //         SlideBox.setAttribute("ng-click", "onClickProcedureCall('"+ functionName +"','"+componentTargetLeaf+"',"+value+")");
            //     }
            // }
            if (componentTargetLeaf && componentTargetLeaf.length > 0) {
                SlideBox.setAttribute("ng-click", "New_onListItemClick('icontextarrowlist','" + componentTargetLeaf + "','" + Key + "','" + Value + "',item.map)");
            }

            var Span2 = document.createElement("div");
            Span2.setAttribute("class", ThemeClass);
            var Span3 = document.createElement("img");
            if (imageFromUrl && imageFromUrl.length > 0) {
                Span3.setAttribute("ng-src", SourceAddress);
                // Span3.setAttribute("image-lazy-src",SourceAddress);
                // Span3.setAttribute("image-lazy-distance-from-bottom-to-load","100");
                // Span3.setAttribute("image-lazy-loader","lines");
            } else {
                //Span3.src=gImageFolderPath+SourceAddress;
            }


            Span3.setAttribute("style", "width:100%;height:100%");
            if (SourceWidth && SourceWidth.length) {
                Span3.style.width = SourceWidth;
            }
            if (SourceHeight && SourceHeight.length) {
                Span3.style.height = SourceHeight;
            }


            if (title && title.length > 0) {
                var Span4 = document.createElement("h2");
                Span4.innerHTML = title;
                if (noParent && noParent.length && noParent === "true") {
                    SlideBox.appendChild(Span4);
                } else {
                    Span2.appendChild(Span4);
                }

            }
            if (subTitle && subTitle.length > 0) {
                var ptag = document.createElement("p");
                var subtitleTheme = SlideBoxComponentNode.getAttribute("subtitleTheme");
                if (subtitleTheme && subtitleTheme.length) {
                    ptag.setAttribute("class", subtitleTheme);
                }
                ptag.innerHTML = subTitle;
                if (noParent && noParent.length && noParent === "true") {
                    SlideBox.appendChild(ptag);
                } else {
                    Span2.appendChild(ptag);
                }
            }

            if (noParent && noParent.length && noParent === "true") {
                SlideBox.appendChild(Span3);
            } else {
                Span2.appendChild(Span3);
            }

            if (noParent && noParent.length && noParent === "true") {
                //SlideBox.appendChild(span2);
            } else {
                SlideBox.appendChild(Span2);
            }


            SlideBox = utils.setComponentAttributes(SlideBoxComponentNode, SlideBox, "SlideBox");

            //create Span
            return SlideBox;
        } catch (e) {
            showErrorMessage('render_ionSlideBox', e.message);
        }
    }
}

var SideBarList = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_ionSideBarList: function (SideBarListComponentNode) {
        try {
            var SideBarListComponentNodeTitle = SideBarListComponentNode.getAttribute("title");
            var theme = SideBarListComponentNode.getAttribute("theme");
            var iconImage = SideBarListComponentNode.getAttribute("iconImage");
            var title = SideBarListComponentNode.getAttribute("title");
            var Image = SideBarListComponentNode.getAttribute("Image");
            var componentTargetLeaf = SideBarListComponentNode.getAttribute("targetLeaf");
            var componentSubType = SideBarListComponentNode.getAttribute("subtype");
            var componentStyle = SideBarListComponentNode.getAttribute("style");
            var componentNgIf = SideBarListComponentNode.getAttribute("ng-if");
            var componentCustomEventListener = SideBarListComponentNode.getAttribute("customEventListener");
            var componentCustomEventListenerArgs = SideBarListComponentNode.getAttribute("customEventListenerArgs");
            var subtype = SideBarListComponentNode.getAttribute("subtype");


            var SideBarList = document.createElement("ion-item");
            // SideBarList.setAttribute("menu-close","true");
            SideBarList.setAttribute("class", "item sideListItem menu-close " + theme);
            // SideBarList.setAttribute("ui-sref",componentTargetLeaf);

            switch (subtype) {
                case "button":
                    var Span1 = document.createElement("button");
                    Span1.setAttribute("class", "button " + theme);
                    Span1.setAttribute("style", "width:100%");
                    Span1.innerText = title;
                    Span1.setAttribute("ng-click", "onLogoutClick()");
                    if (componentNgIf && componentNgIf.length > 0) {
                        Span1.setAttribute("ng-if", componentNgIf);
                    }

                    SideBarList.appendChild(Span1);
                    break;
                case "text":
                    var Span3 = "<img src='" + gImageFolderPath + iconImage + "'><div>" + title + "</div>";
                    SideBarList.innerHTML = Span3;
                    break;
            }

            if (componentCustomEventListener && componentCustomEventListenerArgs && componentCustomEventListener.length > 0) {
                SideBarList.setAttribute("ng-click", "onCustomEventHandler('" + componentSubType + "','" + componentTargetLeaf + "','" + componentCustomEventListener + "'," + componentCustomEventListenerArgs + ")");
            } else {
                SideBarList.setAttribute("ng-click", "onButtonClick('" + componentSubType + "','" + componentTargetLeaf + "')");
            }

            if (componentNgIf && componentNgIf.length > 0) {
                SideBarList.setAttribute("ng-if", componentNgIf);
            }
            // SideBarList.setAttribute("menu-close","true");

            return SideBarList;

        } catch (e) {
            showErrorMessage('render_ionSideBarList', e.message);
        }
    }
}


var Calendar = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_ionCalendar: function (CalendarComponentNode) {
        try {
            var EventDate = CalendarComponentNode.getAttribute("EventDate");
            var EventName = CalendarComponentNode.getAttribute("EventName");
            var EventStartTime = CalendarComponentNode.getAttribute("EventStartTime");
            var EventEndTime = CalendarComponentNode.getAttribute("EventEndTime");

            var Calendar = document.createElement("div");
            var div = document.createElement("div");
            div.setAttribute("class", "wrapp");

            var flexcalendar = document.createElement("flex-calendar");
            flexcalendar.setAttribute("options", "options");
            flexcalendar.setAttribute("events", "events");
            flexcalendar.setAttribute("outerbuttonclick", "onButtonClick");

            div.appendChild(flexcalendar);
            Calendar.appendChild(div);
            //Calendar.appendChild(div1);

            Calendar = utils.setComponentAttributes(CalendarComponentNode, Calendar, "Calendar");

            //create Span
            return Calendar;
        } catch (e) {
            showErrorMessage('render_ionCalendar', e.message);
        }
    }
}

var VideoPlayer = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_ionVideoPlayer: function (VideoPlayerComponentNode) {
        try {

            // <iframe src="https://player.vimeo.com/video/35816559?title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/35816559">Wolfmother</a> from <a href="https://vimeo.com/user7198961">Adam Bilsing</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

            var height = VideoPlayerComponentNode.getAttribute("height");
            var width = VideoPlayerComponentNode.getAttribute("width");
            var videosource = VideoPlayerComponentNode.getAttribute("videosource");
            var videotype = VideoPlayerComponentNode.getAttribute("videotype");
            var autoplay = VideoPlayerComponentNode.getAttribute("autoplay");
            var allowfullscreen = VideoPlayerComponentNode.getAttribute("allowfullscreen");

            var VideoPlayer = document.createElement("iframe");
            VideoPlayer.setAttribute("controls", "controls");
            VideoPlayer.setAttribute("style", "display:none");
            VideoPlayer.setAttribute("onload", "this.style.display = 'block'");
            VideoPlayer.setAttribute("height", height);
            VideoPlayer.setAttribute("width", width);

            if (autoplay && autoplay.length > 0) {
                VideoPlayer.setAttribute("autoplay", "true");
            }
            if (allowfullscreen && allowfullscreen.length > 0) {
                VideoPlayer.setAttribute("allowfullscreen", allowfullscreen);
            }

            // var Span1=document.createElement("source");
            VideoPlayer.setAttribute("src", videosource);
            // VideoPlayer.setAttribute("type",videotype);
            //  VideoPlayer.appendChild(Span1);

            VideoPlayer = utils.setComponentAttributes(VideoPlayerComponentNode, VideoPlayer, "VideoPlayer");
            //create Span

            return VideoPlayer;
        } catch (e) {
            showErrorMessage('render_ionVideoPlayer', e.message);
        }
    }
}



var iongallery = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_iongallery: function (iongalleryComponentNode, iongalleryComponentNode) {
        try {
            // var componentList= getNodesByXPath(gAppDefXML, floatbuttonComponentXPath);
            var iongallery = document.createElement("ion-gallery");
            iongallery.setAttribute("ion-gallery-items", "items");
            iongallery.setAttribute("ion-gallery-toggle", "true");
            //create Span

            return iongallery;
        } catch (e) {
            showErrorMessage('render_iongallery', e.message);
        }
    }
}

var ionRating = {
    /**
    * Following function helps rendering a label component
    * @param componentNode - Contains component node of a label Component which is being rendered
    */
    render_ionRating: function (ionRatingComponentNode) {
        try {
            var current = ionRatingComponentNode.getAttribute("currentRating");
            var max = ionRatingComponentNode.getAttribute("maxRating");
            // var componentList= getNodesByXPath(gAppDefXML, floatbuttonComponentXPath);
            var rating = document.createElement("rating");

            rating.setAttribute("ng-model", "data.rating");
            rating.setAttribute("max", "data.max");
            //create Span

            return rating;
        } catch (e) {
            showErrorMessage('render_ionRating', e.message);
        }
    }
}


/*New Components for Nikhil*/
var NewionSilde = {
    render_NewionSilde: function (NewIonSlideComponentNode, slideXpath) {
        try {
            var theme = NewIonSlideComponentNode.getAttribute("theme");
            var image = NewIonSlideComponentNode.getAttribute("image");
            var imageFromUrl = NewIonSlideComponentNode.getAttribute("imageFromUrl");
            var componentTargetLeaf = NewIonSlideComponentNode.getAttribute("targetLeaf");
            var value = NewIonSlideComponentNode.getAttribute("value");
            var isVideo = NewIonSlideComponentNode.getAttribute("isVideo");
            //var span1 = document.createElement("ion-slide-box");
            var ionSlide = document.createElement("ion-slide");

            // ionSlide.setAttribute("does-continue", "true");
            // ionSlide.setAttribute("slide-interval", "300ms");
            // ionSlide.setAttribute("auto-play", "true");


            var div = document.createElement("div");
            div.setAttribute("class", theme);

            var img = document.createElement("img");
            if (imageFromUrl) {
                img.setAttribute("ng-src", image);
            } else {
                img.setAttribute("src", gImageFolderPath + image);
            }


            var playButton = false;
            if (isVideo && isVideo === "true") {
                //<a href="#"><img class="video" src="images/video-mark.png"></a>
                playButton = document.createElement("a");
                playButton.setAttribute("href", "#");
                var videoMark = document.createElement("img");
                videoMark.setAttribute("class", "video");
                videoMark.setAttribute("src", gImageFolderPath + "video-mark.png");
                playButton.appendChild(videoMark);
            }

            if (componentTargetLeaf) {
                div.setAttribute("ng-click", "New_onListItemClick('nextleaf','" + componentTargetLeaf + "',null,null," + value + ")");
            }
            img.setAttribute("width", "100%");
            img.setAttribute("height", "");
            img.setAttribute("border", "0");
            div.appendChild(img);
            if (playButton) {
                div.appendChild(playButton);
            }
            ionSlide.appendChild(div);
            //span1.appendChild(ionSlide);


            return ionSlide;

        } catch (e) {
            showErrorMessage('render_NewionSlide', e.message);
        }
    }
}

var NewTitle = {
    render_NewTitle: function (NewTitleComponentNode, NewTitleXpath) {
        try {

            var title = NewTitleComponentNode.getAttribute("title");
            var theme = NewTitleComponentNode.getAttribute("theme");
            var Headertheme = NewTitleComponentNode.getAttribute("Headertheme");

            var div = document.createElement("div");
            div.setAttribute("class", theme);
            var h1 = document.createElement("h1");
            h1.innerText = title;
            h1.setAttribute("class", Headertheme);
            div.appendChild(h1);
            return div;

        } catch (e) {
            showErrorMessage('render_NewTitle', e.message);
        }
    }
}

var pTag = {
    render_pTag: function (pTagComponentNode, pTagXpath) {
        try {

            var title = pTagComponentNode.getAttribute("title");
            var aftertitle = pTagComponentNode.getAttribute("aftertitle");
            var subTitle = pTagComponentNode.getAttribute("subTitle");
            var theme = pTagComponentNode.getAttribute("theme");
            var subTitleTheme = pTagComponentNode.getAttribute("subTitleTheme");
            var Headertheme = pTagComponentNode.getAttribute("Headertheme");
            var onClick = pTagComponentNode.getAttribute("onclick");
            var ngIf = pTagComponentNode.getAttribute("ng-if");

            var p = document.createElement("p");
            if (ngIf) {
                p.setAttribute('ng-if', ngIf);
            }
            if (onClick) {
                p.setAttribute('ng-click', onClick);
            }
            p.setAttribute("class", theme);
            var titleNode = document.createTextNode(title);
            p.appendChild(titleNode);
            var subtype = pTagComponentNode.getAttribute("subtype");

            switch (subtype) {
                case "withAnchor":
                    var atheme = pTagComponentNode.getAttribute("anchorTheme");
                    var atitle = pTagComponentNode.getAttribute("anchorTitle");
                    var aonclick = pTagComponentNode.getAttribute("anchorOnClick");
                    var aunderline = pTagComponentNode.getAttribute("anchorUnderline");
                    var ahref = pTagComponentNode.getAttribute("anchorHref");

                    var a = document.createElement("a");
                    a.setAttribute("class", atheme);
                    var textNode = "";
                    if (aunderline && aunderline.length && aunderline === "true") {
                        textNode = document.createElement("u");
                        textNode.innerText = atitle;
                    } else {
                        textNode = document.createTextNode(atitle);
                    }
                    a.appendChild(textNode);

                    if (aonclick && aonclick.length) {
                        a.setAttribute("ng-click", aonclick);
                    }

                    a.setAttribute("href", ((ahref && ahref.length) ? ahref : "javascript:;"));

                    p.appendChild(a);
                    break;
                case "withImg":
                    var imgSrc = pTagComponentNode.getAttribute("imgSrc");
                    var ngSrc = pTagComponentNode.getAttribute("ngSrc");
                    var iTheme = pTagComponentNode.getAttribute("iTheme");
                    var iWidth = pTagComponentNode.getAttribute("iWidth");
                    var iHeight = pTagComponentNode.getAttribute("iHeight");
                    var i = document.createElement("img");
                    if (ngSrc && ngSrc.length) {
                        i.setAttribute("ng-src", ngSrc);
                    } else {
                        i.setAttribute("src", "images/" + imgSrc);
                    }

                    i.setAttribute("width", iWidth);
                    i.setAttribute("height", iHeight);
                    i.setAttribute("class", iTheme);
                    p.removeChild(titleNode);
                    p.appendChild(i);
                    p.appendChild(titleNode);

                    break;

                case "withSpan":
                    var span = document.createElement("span");
                    var spanTitle = pTagComponentNode.getAttribute("spanTitle");
                    var spanTheme = pTagComponentNode.getAttribute("spanTheme");
                    if (theme) {
                        span.setAttribute("class", spanTheme);
                    }
                    span.innerText = spanTitle;
                    p.appendChild(span);
                    if (aftertitle && aftertitle.length) {
                        var aftertitleNode = document.createTextNode(aftertitle);
                        p.appendChild(aftertitleNode);
                    }
                    break;

                default:
                    if (subTitle) {
                        var span = document.createElement("span");
                        var subtitleNode = document.createTextNode(subTitle);
                        span.setAttribute("class", subTitleTheme);
                        span.appendChild(subtitleNode);
                        p.appendChild(span);
                    }
                    break;
            }

            return p;

        } catch (e) {
            showErrorMessage('render_pTag', e.message);
        }
    }
}

var span = {
    render_span: function (spanComponentNode, spanXpath) {
        try {

            var title = spanComponentNode.getAttribute("title");
            var theme = spanComponentNode.getAttribute("theme");
            var span = document.createElement("span");
            var onclick = spanComponentNode.getAttribute("onclick");
            span.setAttribute("class", theme);
            span.setAttribute("ng-click", onclick);
            var titleNode = document.createTextNode(title);
            span.appendChild(titleNode);

            var componentList = getNodesByXPath(gAppDefXML, spanXpath);
            for (var item = 0; item < componentList.length; item++) {
                create_ionControl(span, componentList[item], null, spanXpath, null, null);
            }
            return span;

        } catch (e) {
            showErrorMessage('render_span', e.message);
        }
    }
}

var hTag = {
    render_hTag: function (spanComponentNode, spanXpath) {
        try {

            var title = spanComponentNode.getAttribute("title");
            var theme = spanComponentNode.getAttribute("theme");
            var level = spanComponentNode.getAttribute("level");
            var heading = document.createElement("h" + level);
            heading.setAttribute("class", theme);
            var titleNode = document.createTextNode(title);
            heading.appendChild(titleNode);

            var componentList = getNodesByXPath(gAppDefXML, spanXpath);
            for (var item = 0; item < componentList.length; item++) {
                create_ionControl(heading, componentList[item], null, spanXpath, null, null);
            }
            return heading;

        } catch (e) {
            showErrorMessage('render_span', e.message);
        }
    }
}


var anchorTag = {
    render_anchorTag: function (anchorTagComponentNode, anchorTagXpath) {
        try {

            var title = anchorTagComponentNode.getAttribute("title");
            var name = anchorTagComponentNode.getAttribute("name");
            var theme = anchorTagComponentNode.getAttribute("theme");
            var onclick = anchorTagComponentNode.getAttribute("onclick");
            var ngClass = anchorTagComponentNode.getAttribute("ng-class");
            var ngIf = anchorTagComponentNode.getAttribute("ng-if");
            var href = anchorTagComponentNode.getAttribute("href");
            var subType = anchorTagComponentNode.getAttribute("subtype");
            var iIcon = anchorTagComponentNode.getAttribute("iIcon");
            var iconRight = anchorTagComponentNode.getAttribute("iconRight");
            var anchorTag = document.createElement("a");

            if (subType == "withIcon" && (!iconRight || iconRight !== "true")) {
                var iText = document.createElement("i");
                iText.setAttribute("class", iIcon);
                anchorTag.appendChild(iText);
            }

            if (name) {
                anchorTag.setAttribute("id", name);
            }
            anchorTag.setAttribute("class", theme);
            if (title) {
                var titleNode = document.createTextNode(title);
                anchorTag.appendChild(titleNode);
            }

            if (ngClass) {
                anchorTag.setAttribute("ng-class", ngClass);
            }

            if (ngIf) {
                anchorTag.setAttribute("ng-if", ngIf);
            }

            if (onclick) {
                anchorTag.setAttribute("ng-click", onclick);
            }
            var componentList = getNodesByXPath(gAppDefXML, anchorTagXpath);
            for (var item = 0; item < componentList.length; item++) {
                create_ionControl(anchorTag, componentList[item], null, anchorTagXpath, null, null);
            }

            if (subType == "withIcon" && iconRight === "true") {
                var iText = document.createElement("i");
                iText.setAttribute("class", iIcon);
                anchorTag.appendChild(iText);
            }

            anchorTag.setAttribute("href", ((href && href.length) ? href : "javascript:;"));
            return anchorTag;

        } catch (e) {
            showErrorMessage('render_anchorTag', e.message);
        }
    }
}

var anchorImageTag = {
    render_anchorImageTag: function (anchorTagComponentNode, anchorTagXpath) {
        try {

            var title = anchorTagComponentNode.getAttribute("title");
            var theme = anchorTagComponentNode.getAttribute("theme");
            var onclick = anchorTagComponentNode.getAttribute("onclick");
            var ngClass = anchorTagComponentNode.getAttribute("ng-class");
            var ngIf = anchorTagComponentNode.getAttribute("ng-if");
            var href = anchorTagComponentNode.getAttribute("href");
            var anchorTag = document.createElement("a");
            var image = anchorTagComponentNode.getAttribute("image");
            var imageTheme = anchorTagComponentNode.getAttribute("imageTheme");
            var iWidth = anchorTagComponentNode.getAttribute("iWidth");
            var iHeight = anchorTagComponentNode.getAttribute("iHeight");

            if (title) {
                var titleNode = document.createTextNode(title);
                anchorTag.appendChild(titleNode);
            }

            if (image) {
                var imageTag = document.createElement("img");
                imageTag.setAttribute("src", gImageFolderPath + image);
                imageTag.setAttribute("class", imageTheme);
                imageTag.setAttribute("width", iWidth);
                imageTag.setAttribute("height", iHeight);


                anchorTag.appendChild(imageTag);
            }

            anchorTag.setAttribute("class", theme);


            if (ngClass) {
                anchorTag.setAttribute("ng-class", ngClass);
            }

            if (ngIf) {
                anchorTag.setAttribute("ng-if", ngIf);
            }

            if (onclick) {
                anchorTag.setAttribute("ng-click", onclick);
            }

            var componentList = getNodesByXPath(gAppDefXML, anchorTagXpath);
            for (var item = 0; item < componentList.length; item++) {
                create_ionControl(anchorTag, componentList[item], null, anchorTagXpath, null, null);
            }

            anchorTag.setAttribute("href", ((href && href.length) ? href : "javascript:;"));
            return anchorTag;

        } catch (e) {
            showErrorMessage('render_anchorTag', e.message);
        }
    }
}

var icontext = {
    render_icontext: function (icontextComponentNode, icontextXpath) {
        try {

            var title = icontextComponentNode.getAttribute("title");
            var theme = icontextComponentNode.getAttribute("theme");
            var iconClass = icontextComponentNode.getAttribute("icon");

            var p = document.createElement("p");
            if (theme) {
                p.setAttribute("class", theme);
            }

            var icon = document.createElement("i");
            icon.setAttribute("class", iconClass);

            if (title) {
                var span = document.createElement("span");
                span.innerText = title;
                span.setAttribute("class", "rate");
                icon.appendChild(span);
            }

            p.appendChild(icon);

            return p;

        } catch (e) {
            showErrorMessage('render_icontext', e.message);
        }
    }
}

var icontext2 = {
    render_icontext2: function (pTagComponentNode, pTagXpath) {
        try {
            var title = pTagComponentNode.getAttribute("title");
            var subTitle = pTagComponentNode.getAttribute("subTitle");
            var theme = pTagComponentNode.getAttribute("theme");
            var subTitleTheme = pTagComponentNode.getAttribute("subTitleTheme");
            var Headertheme = pTagComponentNode.getAttribute("Headertheme");

            var p = document.createElement("p");
            p.setAttribute("class", theme);
            var titleNode = document.createTextNode(title);

            var imgSrc = pTagComponentNode.getAttribute("imgSrc");
            var iTheme = pTagComponentNode.getAttribute("iTheme");
            var iWidth = pTagComponentNode.getAttribute("iWidth");
            var iHeight = pTagComponentNode.getAttribute("iHeight");
            var i = document.createElement("img");
            i.setAttribute("src", "images/" + imgSrc);
            i.setAttribute("width", iWidth);
            i.setAttribute("height", iHeight);
            i.setAttribute("class", iTheme);
            p.appendChild(i);
            p.appendChild(titleNode);
            return p;

        } catch (e) {
            showErrorMessage('render_icontext', e.message);
        }
    }
}

var icontext3 = {
    render_icontext3: function (pTagComponentNode, pTagXpath) {
        try {
            var title = pTagComponentNode.getAttribute("title");
            var theme = pTagComponentNode.getAttribute("theme");
            var spanTheme = pTagComponentNode.getAttribute("spanTheme");
            var spanTitle = pTagComponentNode.getAttribute("spanTitle");

            var p = document.createElement("p");
            p.setAttribute("class", theme);
            var titleNode = document.createTextNode(title);

            var imgSrc = pTagComponentNode.getAttribute("imgSrc");
            var iTheme = pTagComponentNode.getAttribute("iTheme");
            var iWidth = pTagComponentNode.getAttribute("iWidth");
            var iHeight = pTagComponentNode.getAttribute("iHeight");
            var i = document.createElement("img");
            i.setAttribute("src", "images/" + imgSrc);
            i.setAttribute("width", iWidth);
            i.setAttribute("height", iHeight);
            i.setAttribute("class", iTheme);

            var span = document.createElement("span");
            span.setAttribute("class", spanTheme);
            span.innerText = spanTitle;

            p.appendChild(i);
            p.appendChild(span);
            p.appendChild(titleNode);
            return p;

        } catch (e) {
            showErrorMessage('render_icontext', e.message);
        }
    }
}

var ionCheckBox = {
    render_ionCheckBox: function (ionCheckBoxComponentNode, ionCheckBoxXpath) {
        try {

            var title = ionCheckBoxComponentNode.getAttribute("title");
            var theme = ionCheckBoxComponentNode.getAttribute("theme");
            var name = ionCheckBoxComponentNode.getAttribute("name");
            var hidden = ionCheckBoxComponentNode.getAttribute("hidden");

            var container = document.createElement("div");

            var input = document.createElement("input");
            input.setAttribute("type", 'checkbox');
            input.setAttribute("name", name);
            input.setAttribute("id", name);

            if (theme) {
                input.setAttribute("class", "css-checkbox " + theme);
            } else {
                input.setAttribute("class", "css-checkbox");
            }

            input.setAttribute("ng-model", "data." + name);

            input.setAttribute("ng-change", "onCheckboxChange('" + name + "',data." + name + "," + null + ")");

            var label = document.createElement("label");
            label.setAttribute("class", "css-label");
            label.setAttribute("for", name);

            var span = document.createElement("span");
            span.setAttribute("class", "category-text v-align-bottom");
            span.innerText = title;

            label.appendChild(span);

            container.appendChild(input);
            container.appendChild(label);

            if (hidden && hidden == "true") {
                container.style.display = "none";
            }

            return container;

        } catch (e) {
            showErrorMessage('render_ionCheckBox', e.message);
        }
    }
}


var inputFields = {
    render_fields: function (subType, componentNode, listelementXPath, listComponentXPath) {
        var html = "";
        var label = componentNode.getAttribute("label");
        var labelTheme = componentNode.getAttribute("labelTheme");
        var fieldType = componentNode.getAttribute("fieldType");
        var theme = componentNode.getAttribute("theme");
        var componentDataNode = componentNode.getAttribute("dataNode");
        var placeholder = componentNode.getAttribute("alias");
        var ngIf = componentNode.getAttribute("ng-if");
        var ngShow = componentNode.getAttribute("ng-show");
        var ngClick = componentNode.getAttribute("ng-click");
        var ngChange = componentNode.getAttribute("ng-change");
        var ngModel = componentNode.getAttribute("ng-model");
        var value = componentNode.getAttribute("value");
        var hidden = componentNode.getAttribute("hidden");
        var onfocus = componentNode.getAttribute("onfocus");
        var onblur = componentNode.getAttribute("onblur");
        var min = componentNode.getAttribute("min");
        var readOnly = componentNode.getAttribute("readOnly");
        var customTheme = componentNode.getAttribute("customTheme");
        var defaultOption = componentNode.getAttribute("defaultOption");

        if (label && label == "true") {
            html = document.createElement("label");
            html.setAttribute("class", labelTheme);
        }

        switch (subType) {
            case "select":
                var ngOptions = componentNode.getAttribute("ng-options");
                var field = document.createElement("select");
                //field.setAttribute('ng-options', ngOptions);
                field.setAttribute("class", "item item-select " + theme);
                utils.setComponentAttributes(componentNode, field, "select");
                var elementList = getNodesByXPath(gAppDefXML, listelementXPath);
                var elementTitle = elementList[0].getAttribute("title");
                var elementValue = elementList[0].getAttribute("value");
                if (ngOptions) {
                    field.setAttribute("ng-options", elementValue + ' as ' + elementTitle + ' for item in ' + ngOptions);
                } else {
                    field.setAttribute("ng-options", elementValue + ' as ' + elementTitle + ' for item in dynamicList.' + componentDataNode);
                }

                var optionTag = document.createElement('option');
                optionTag.setAttribute("value", "");
                if (defaultOption && defaultOption.length) {
                    optionTag.innerText = defaultOption;
                } else {
                    optionTag.setAttribute("style", "display:none;");
                }
                field.appendChild(optionTag);
                break;

            case "input":
                var field = document.createElement("input");
                field.setAttribute("type", fieldType);
                if (customTheme && customTheme == "true") {

                    field.setAttribute("class", theme);
                } else {
                    field.setAttribute("class", "item item-input " + theme);
                }

                if (placeholder && placeholder.length > 0) {
                    field.setAttribute("placeholder", placeholder);
                }
                if (min && min.length) {
                    field.setAttribute("min", "{{" + min + "| date:'yyyy-MM-ddTHH:mm:ss'}}"); // 2016-09-16T10:02:49.708Z for 3:33
                }
                utils.setComponentAttributes(componentNode, field, "select");
                break;

            case "customInput":
                var field = document.createElement("input");
                field.setAttribute("type", fieldType);
                field.setAttribute("class", theme);
                // field.setAttribute("class", "item item-input " + theme);
                if (placeholder && placeholder.length > 0) {
                    field.setAttribute("placeholder", placeholder);
                }
                if (min && min.length) {
                    field.setAttribute("min", "{{" + min + "| date:'yyyy-MM-ddTHH:mm:ss'}}"); // 2016-09-16T10:02:49.708Z for 3:33
                }
                utils.setComponentAttributes(componentNode, field, "select");
                break;

            case "textarea":
                var cols = componentNode.getAttribute("cols");
                var rows = componentNode.getAttribute("rows");
                var field = document.createElement("textarea");
                field.setAttribute("class", theme);
                field.setAttribute("cols", cols);
                field.setAttribute("rows", rows);
                utils.setComponentAttributes(componentNode, field, "textarea");
                break;
        }

        if (ngIf && ngIf.length) {
            field.setAttribute("ng-if", ngIf);
        }
        if (ngShow && ngShow.length) {
            field.setAttribute("ng-show", ngShow);
        }
        if (ngClick && ngClick.length) {
            field.setAttribute("ng-click", ngClick);
        }
        if (ngChange && ngChange.length) {
            field.setAttribute("ng-change", ngChange);
        }
        if (ngModel && ngModel.length) {
            field.setAttribute("ng-model", ngModel);
        }
        if (value && value.length) {
            field.setAttribute("value", value);
        }
        if (onfocus && onfocus.length) {
            field.setAttribute("onfocus", onfocus);
        }
        if (onblur && onblur.length) {
            field.setAttribute("onblur", onblur);
        }

        if (readOnly && readOnly.length) {
            field.setAttribute("readonly", true);
        }

        if (html === "") {
            html = field;
        } else {
            html.appendChild(field);
        }

        if (hidden && hidden == "true") {
            html.style.display = "none";
        }


        return html;
    }
}


var simpleList = {
    render: function (componentNode, listelementXPath, listComponentXPath) {
        var subtype = componentNode.getAttribute("subtype");
        var theme = componentNode.getAttribute("theme");
        var ngIf = componentNode.getAttribute("ng-if");
        var style = componentNode.getAttribute("style");


        var dom = "";
        switch (subtype) {
            case "ul":
                dom = document.createElement("ul");
                break;
            case "ol":
                dom = document.createElement("ol");
                break;
        }

        if (dom !== "") {
            dom.setAttribute("class", theme);
            if (ngIf && ngIf.length) {
                dom.setAttribute("ng-if", ngIf);
            }
            if (style && style.length) {
                dom.setAttribute("style", style);
            }

            var elementList = getNodesByXPath(gAppDefXML, listelementXPath);
            for (var index = 0; index < elementList.length; index++) {
                var currentNode = elementList[index];
                var orderList = document.createElement("li");
                var liTheme = currentNode.getAttribute("theme");
                var elementName = currentNode.getAttribute("name");
                var lingIf = currentNode.getAttribute("ng-if");
                var name2 = componentNode.getAttribute("name");
                orderList.setAttribute("class", liTheme);
                orderList.setAttribute("id", elementName);
                if (lingIf && lingIf.length) {
                    orderList.setAttribute("ng-if", lingIf);

                }

                var sublistelementXPath = listelementXPath + "[@name='" + elementName + "']" + gcomponentXPath;;
                var subelementList = getNodesByXPath(gAppDefXML, sublistelementXPath);

                var subelementList = getNodesByXPath(gAppDefXML, sublistelementXPath);
                for (var v = 0; v < subelementList.length; v++) {
                    create_ionControl(orderList, subelementList[v], null, sublistelementXPath);
                }

                dom.appendChild(orderList);
            }
        }
        return dom;
    }
}

var tabs = {
    render_tabs: function (componentNode, listelementXPath) {
        var subtype = componentNode.getAttribute("subtype");
        var theme = componentNode.getAttribute("theme");
        var ngIf = componentNode.getAttribute("ng-if");
        var style = componentNode.getAttribute("style");
        var name = componentNode.getAttribute("name");

        var segment = document.createElement('ti-segmented-control');
        //segment.setAttribute('id',name); 
        segment.setAttribute('on-select', 'buttonClicked($index)');
        segment.setAttribute('style', 'margin: 0 1% 0 1%');
        segment.setAttribute("class", "uploads-tabs");
        var segmentChild = document.createElement('ti-segmented-control-button');
        segmentChild.setAttribute('title', "'My Bubs'");
        segmentChild.setAttribute('selected', 'selected');
        segmentChild.setAttribute('id', 'MyBubId');

        segmentChild.setAttribute('class', 'float-left button button-medium button-positive font-titi size-14 titi-600 bubs-tab-1');
        var segmentChild2 = document.createElement('ti-segmented-control-button');
        segmentChild2.setAttribute('title', "'The Hub'");
        segmentChild2.setAttribute('id', 'HubId');
        segmentChild2.setAttribute('class', 'float-right button button-medium button-positive font-titi size-14 titi-600 bubs-tab-2');
        segment.appendChild(segmentChild);
        segment.appendChild(segmentChild2);

        return segment;
    }
}
