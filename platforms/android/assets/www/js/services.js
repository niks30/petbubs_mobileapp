// define(['angular'], function (angular) {
define([], function () {
    var appServices = angular.module('appStudioStarter.services', []);
    //console.log('appStudioStarter.services');
    var appData = getAppData();
    for (var template in appData.views) {
        appServices.value(template, appData.views[template].data);
    }

    appServices.factory('Session', function (Global, GlobalConstants) {
        return {
            isUserLoggedIn: function () {
                var isUserLoggedIn = true;
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);
                if (customerId.length == 0 || isLoggedIn == false) {
                    isUserLoggedIn = false;
                }
                return isLoggedIn;
            }
        }
    })

    appServices.service('VideoService', function ($q) {
        // Resolve the URL to the local file
        // Start the copy process
        var deferred = $q.defer();
        var promise = deferred.promise;

        promise.success = function (fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function (fn) {
            promise.then(null, fn);
            return promise;
        }

        function createFileEntry(fileURI) {
            console.log(fileURI);
            window.resolveLocalFileSystemURL(fileURI, function (entry) {
                return copyFile(entry);
            }, fail);
        }

        function copyFile(fileEntry) {
            console.log(fileEntry);
            var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
            var newName = makeid() + name;

            window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory + "/Petbubs/Videos/", function (fileSystem2) {
                fileEntry.copyTo(fileSystem2, newName, function (succ) {
                    return onCopySuccess(succ);
                }, fail);
            },
                fail
            );
        }

        // Called on successful copy process
        // Creates a thumbnail from the movie
        // The name is the moviename but with .png instead of .mov
        function onCopySuccess(entry) {
            console.log("onCopySuccess");
            var name = entry.nativeURL.slice(0, -4);


            var resize = { width: 100, height: 100 };
            var options = { "resize": resize };

            window.PKVideoThumbnail.createThumbnail(entry.nativeURL, name + '.png', options, function (prevSucc) {
                return prevImageSuccess(prevSucc);
            }, fail);
        }

        // Called on thumbnail creation success
        // Generates the currect URL to the local moviefile
        // Finally resolves the promies and returns the name
        function prevImageSuccess(succ) {
            var correctUrl = succ.slice(0, -4);
            correctUrl += '.mp4';
            deferred.resolve(correctUrl);
        }

        // Called when anything fails
        // Rejects the promise with an Error
        function fail(error) {
            console.log('FAIL: ' + error.code);
            deferred.reject('ERROR');
        }

        // Function to make a unique filename
        function makeid() {
            var text = '';
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            for (var i = 0; i < 5; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }

        // The object and functions returned from the Service
        return {
            // This is the initial function we call from our controller
            // Gets the videoData and calls the first service function
            // with the local URL of the video and returns the promise
            saveVideo: function (data) {
                window.resolveLocalFileSystemURL(data, copyFile, fail);
                return promise;
            }
        }
    });

    appServices.factory('globalService', function ($ionicLoading, $ionicPopup) {
        var gAppDefXML = null;
        var isOnline = "";
        var gServiceURL = gAppStudioServerURL; //"http://192.168.2.16:8080/Tranx/n";
        var gEncServiceURL = gAppStudioServerEncryptURL; //"http://192.168.2.16:8080/Tranx/n";
        return {
            getAppXML: function () {
                return gAppDefXML;
            },
            setAppXML: function (newAppDefXML) {
                gAppDefXML = newAppDefXML;
            },
            getServiceURL: function () {
                if (gRequestEncryptionEnabled) {
                    return gAppStudioServerEncryptURL; //gEncServiceURL;
                } else {
                    return gAppStudioServerURL; //gServiceURL;
                }

            },
            stringToXML: function (oString) {
                //code for IE
                if (window.ActiveXObject) {
                    var oXML = new ActiveXObject("Microsoft.XMLDOM");
                    oXML.loadXML(oString);
                    return oXML;
                }
                // code for Chrome, Safari, Firefox, Opera, etc.
                else {
                    return (new DOMParser()).parseFromString(oString, "text/xml");
                }
            },
            showLoading: function (message) {
                $ionicLoading.show({
                    template: message,
                    duration: 60000
                });
            },
            hideLoading: function () {
                $ionicLoading.hide();
            },
            showConfirmation: function (headerTitle, confirmMessage, confirmCallback, cancelCallback) {
                var confirmPopup = $ionicPopup.confirm({
                    title: headerTitle,
                    template: confirmMessage
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        if (confirmCallback) confirmCallback();
                    } else {
                        if (cancelCallback) cancelCallback();
                    }
                });
            },
            showCustomConfirmation: function (headerTitle, confirmMessage, confirmCallback, cancelCallback) {
                var confirmPopup = $ionicPopup.confirm({
                    title: headerTitle,
                    template: confirmMessage,
                    cancelText: 'Use existing PIN', // String (default: 'Cancel'). The text of the Cancel button.
                    cancelType: 'button-default', // String (default: 'button-default'). The type of the Cancel button.
                    okText: 'Setup New PIN', // String (default: 'OK'). The text of the OK button.
                    okType: 'button-positive' // String (default: 'button-positive'). The type of the OK button.

                });
                confirmPopup.then(function (res) {
                    if (res) {
                        if (confirmCallback) confirmCallback();
                    } else {
                        if (cancelCallback) cancelCallback();
                    }
                });
            },

            showEventConfirmation: function (headerTitle, confirmMessage, confirmCallback, cancelCallback) {
                var confirmPopup = $ionicPopup.confirm({
                    title: headerTitle,
                    template: confirmMessage,
                    cancelText: 'Cancel', // String (default: 'Cancel'). The text of the Cancel button.
                    cancelType: 'button-default', // String (default: 'button-default'). The type of the Cancel button.
                    okText: 'Add Event', // String (default: 'OK'). The text of the OK button.
                    okType: 'button-positive' // String (default: 'button-positive'). The type of the OK button.

                });
                confirmPopup.then(function (res) {
                    if (res) {
                        if (confirmCallback) confirmCallback();
                    } else {
                        if (cancelCallback) cancelCallback();
                    }
                });
            },

            showAlert: function (headerTitle, alertMessage, callback) {
                $ionicPopup.alert({
                    title: headerTitle,
                    content: alertMessage
                }).then(function (res) {
                    if (callback) callback();
                });

            },

            getOnlineStatus: function () {
                return isOnline;
            },

            setOnlineStatus: function (newOnlineStatus) {
                isOnline = newOnlineStatus;
            },

        };
    });

    appServices.factory('netCallService', function ($http, $log, $q) {

        return {
            /**
            options.url = "http://....",
            options.onSuccess = funtion onSuccess(){},
            options.onError = funtion onSuccess(){},
            **/
            httpGET: function (options) {
                var deferred = $q.defer();
                var success = options.success;
                var error = options.error;
                $http.get(options.url)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(success(data, status, headers, config));
                    }).error(function (data, status, headers, config) {
                        deferred.reject(data);
                        error(data, status, headers, config);
                        $log.error(data, status, headers, config);
                    });
                return deferred.promise;
            },

            httpPOST: function (options) {
                var deferred = $q.defer();
                var success = options.success;
                var error = options.error;
                $http.post(options.url, options.data, options.headers)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(success(data, status, headers, config));
                    }).error(function (data, status, headers, config) {
                        deferred.reject(data);
                        error(data, status, headers, config);
                        $log.error(data, status, headers, config);
                    });
                return deferred.promise;
            },
        };
    })

    appServices.factory('Global', ['$ionicLoading', '$ionicPopup', 'GlobalConstants', '$filter', globalService])

    function globalService($ionicLoading, $ionicPopup, GlobalConstants, $filter) {
        var service = {
            getObjectFromLocalStorage: getObject,
            setObjectInLocalStorage: setObject,
            clearLocalStorage: clear,
        };

        return service;

    }

    /**
    * Makes an entry in localStorage based on the key/value pair provided.
    *
    * @param {String} key
    * @param {Object} value
    */
    function setObject(key, value) {
        window.localStorage.setItem(key, JSON.stringify(value));
    }

    /**
    * Retrieves a json object from localStorage based on the key provided.
    * Returns an empty json, if specified key is not available
    *
    * @param {String} key
    */
    function getObject(key) {
        return JSON.parse(window.localStorage.getItem(key)) ? JSON.parse(window.localStorage.getItem(key)) : [];
    }

    /**
    * Removes all key/value pairs from localStorage
    *
    */
    function clear() {
        var currentEndPoint = this.getEndpointURL();
        var currentEndPointPort = this.getEndPointPort();
        window.localStorage.clear();
        this.setEndpointURL(currentEndPoint);
        this.setEndPointPort(currentEndPointPort);
    }
    window.gAppServices = appServices;
    return appServices;

});
