//define(['angular', 'angularIonic', 'controllers', 'services'], function (angular) {
  define([], function () {
  // Ionic Starter App

  
  var $stateProviderRef = null;
  var $urlRouterProviderRef = null;

  

  var appStudioChildStarter = angular.module('appStudioChildStarter', ['ionic']);

  
  appStudioChildStarter.config(function($locationProvider, $stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    //$stateProvider

    /*for (var template in appData.views)
    {
    console.log(appData.views[template].title);
      $stateProvider.state(template, {
        url: appData.views[template].url,      
        template: appData.views[template].template,
        controller: appData.views[template].controller
      })
      
    }*/
    alert('appStudioChildStarter loaded');  
    $stateProvider.state("Abstract", {
      url: "/",      
      abstract: true,
      template: "<div ui-view></div>",
      controller: "AbstractController"
    });    
      
    $urlRouterProviderRef = $urlRouterProvider;
    $stateProviderRef = $stateProvider;
    $locationProvider.html5Mode(false);
    

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');

  });

  appStudioChildStarter.run(['$q', '$rootScope', '$state', '$ionicPopup', '$ionicPlatform',
    function ($q, $rootScope, $state, $ionicPopup, $ionicPlatform) {

          var firstLeafName = localStorage.getItem('firstLeafName');
          var firstLeafNameChildApp = localStorage.getItem('firstLeafNameChildApp');
          alert('firstLeafNameChildApp ' + firstLeafNameChildApp);
          $ionicPlatform.onHardwareBackButton(function (e) {
          
          //UNCOMMENT
            /*if (dataService.getIsLoginShown()) { // here to check whether the home page, if yes, exit the application
               navigator.app.exitApp();              
            }*/
            //alert('test');
            if($state.current.name==firstLeafName){
              navigator.app.exitApp();
            }else{
              e.preventDefault();
            }
            
            
          });

            /*    
            require(['data', 'controllers','services'], function (data,controllers,services) {
              appStudioChildStarter.$inject = ['appStudioStarter.controllers', 'appStudioStarter.services'];
              var $injector = angular.injector();
              $injector.invoke(['appStudioStarter.controllers', 'appStudioStarter.services', function(controller, service){
                    
              }]);
            
                
            });*/
          var appData = getAppData();
          
          for (var template in appData.views)
          {
            //console.log(appData.views[template].title);
            $stateProviderRef.state(appData.views[template].title, {
              url: appData.views[template].url,      
              template: appData.views[template].template,
              controller: appData.views[template].controller
            });
            
          }

          
                     
          
          $state.go(firstLeafNameChildApp);
          


           // $ionicPopup.alert({
           //    title: "Internet Disconnected",
           //    content:  "The internet is disconnected on your device."
           //  }).then(function(res) {
              
           //  });

          

   }]);  


  appStudioChildStarter.directive('onValidSubmit', ['$parse', '$timeout', function($parse, $timeout) {
    return {
      require: '^form',
      restrict: 'A',
      link: function(scope, element, attrs, form) {
        form.$submitted = false;
        var fn = $parse(attrs.onValidSubmit);
        element.on('submit', function(event) {
          scope.$apply(function() {
            element.addClass('ng-submitted');
            form.$submitted = true;
            if (form.$valid) {
              if (typeof fn === 'function') {
                fn(scope, {$event: event});
              }
            }
          });
        });
      }
    }
 
  }]);

  appStudioChildStarter.directive('validated', ['$parse', function($parse) {
    return {
      restrict: 'AEC',
      require: '^form',
      link: function(scope, element, attrs, form) {
        var inputs = element.find("*");
        for(var i = 0; i < inputs.length; i++) {
          (function(input){  
            var attributes = input.attributes;
            if (attributes.getNamedItem('ng-model') != void 0 && attributes.getNamedItem('name') != void 0) {
              var field = form[attributes.name.value];
              if (field != void 0) { 
                scope.$watch(function() {
                  return form.$submitted + "_" + field.$valid;
                }, function() {
                  if (form.$submitted != true) return;
                  //if (form.$valid === true) return;
                  var inp = angular.element(input); 
                  // if (attributes.getNamedItem('customValidator') != void 0){
                  //   customValidation(form, inp, attributes.getNamedItem('customValidator'));
                  // }
                  // if (attributes.getNamedItem('controlValidator') != void 0){
                  //   controlValidation(form, inp, attributes.getNamedItem('controlValidator'));
                  // }
                  if (inp.hasClass('ng-invalid')) {
                    element.removeClass('has-success');
                    element.addClass('has-error');
                  } else {
                    element.removeClass('has-error').addClass('has-success');
                  }
                });
              }
            }
          })(inputs[i]);
        }
      }
    }
  }]);

  appStudioChildStarter.directive('numbersOnly', function(){
     return {
       require: 'ngModel',
       link: function(scope, element, attrs, modelCtrl) {
         modelCtrl.$parsers.push(function (inputValue) {
             // this next if is necessary for when using ng-required on your input. 
             // In such cases, when a letter is typed first, this parser will be called
             // again, and the 2nd time, the value will be undefined
             if (inputValue == undefined) return '' 
             var transformedInput = inputValue.replace(/[^0-9+.]/g, ''); 
             if (transformedInput!=inputValue) {
                modelCtrl.$setViewValue(transformedInput);
                modelCtrl.$render();
             }         

             return transformedInput;         
         });
       }
     };
  });

  appStudioChildStarter.directive('compareTo', function() {
    return {
      require: '?ngModel',
      restrict: 'A',
      scope: {
          compareTo: '='
      },
      link: function(scope, elem, attrs, ctrl) {
          if(!ctrl) {
              console && console.warn('Match validation requires ngModel to be on the element');
              return;
          }

          scope.$watch(function() {
              var modelValue = angular.isUndefined(ctrl.$modelValue)? ctrl.$$invalidModelValue : ctrl.$modelValue;
              return (ctrl.$pristine && angular.isUndefined(modelValue)) || scope.compareTo === modelValue;
          }, function(currentValue) {
              ctrl.$setValidity('compareTo', currentValue);
          });
      }
    };
  });

  return appStudioChildStarter;    
});