/*! DBHelper - v0.0.1 - 01 Jan 2015
 * http://www.mobilewaretech.com
 * Includes: 
 * Copyright 2014 Mobileware Technologies Pvt Ltd. 
 * 
 * Author: Rohan Deshpande
 * Date  : 01 June 2015
 * Desc  : Include all Database functions here
 */

// var db = window.sqlitePlugin.openDatabase("Database", "1.0", "Demo", -1);

// db.transaction(function(tx) {
//   tx.executeSql('DROP TABLE IF EXISTS test_table');
//   tx.executeSql('CREATE TABLE IF NOT EXISTS test_table (id integer primary key, data text, data_num integer)');

//   tx.executeSql("INSERT INTO test_table (data, data_num) VALUES (?,?)", ["test", 100], function(tx, res) {
//     alert("insertId: " + res.insertId + " -- probably 1");
//     alert("rowsAffected: " + res.rowsAffected + " -- should be 1");

//     tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
//       alert("res.rows.length: " + res.rows.length + " -- should be 1");
//       alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
//     });

//   }, function(e) {
//     alert("ERROR: " + e.message);
//   });
// });


var tables = [{
    "tableName": "Application",
    "attributes": [{
        "name": "AppId",
        "type": "INTEGER PRIMARY KEY",
    }, {
        "name": "AppVersion",
        "type": "TEXT",
    }, {
        "name": "AppName",
        "type": "TEXT",
    }, {
        "name": "AppLogo",
        "type": "TEXT",
    }, {
        "name": "AppXML",
        "type": "TEXT",
    }, {
        "name": "AppData",
        "type": "TEXT",
    }, {
        "name": "AppDownloadStatus",
        "type": "INTEGER",
    }, {
        "name": "AppUpdateStatus",
        "type": "INTEGER",
    }]
}, {
    "tableName": "Resources",
    "attributes": [{
        "name": "ResourceId",
        "type": "INTEGER PRIMARY KEY",
    }, {
        "name": "AppId",
        "type": "INTEGER",
    }, {
        "name": "ResourceName",
        "type": "TEXT",
    }, {
        "name": "ResourceContent",
        "type": "TEXT",
    }],
}, {
    "tableName": "OfflineData",
    "attributes": [{
        "name": "OfflineDataId",
        "type": "INTEGER PRIMARY KEY AUTOINCREMENT ",
    }, {
        "name": "AppId",
        "type": "INTEGER",
    }, {
        "name": "Request",
        "type": "TEXT",
    }, {
        "name": "ServiceURL",
        "type": "TEXT",
    }, {
        "name": "SyncStatus",
        "type": "INTEGER",
    }],
}];

var appStudioDB = {};
appStudioDB.instance = null;

appStudioDB.deleteDB = function() {
    if (window.sqlitePlugin !== undefined) {
        window.sqlitePlugin.deleteDatabase("AppStudioDB", appStudioDB.onTxSuccess, appStudioDB.onTxError);
    }
}

appStudioDB.openDBInstance = function() {
    if (window.sqlitePlugin !== undefined) {
        appStudioDB.instance = window.sqlitePlugin.openDatabase("AppStudioDB", appStudioDB.onTxSuccess, appStudioDB.onTxError);
    } else {
        // For debugging in simulator fallback to native SQL Lite
        // appStudioDB.instance = window.openDatabase("AppStudioDB", "1.0", "AppStudioDB", 200000);
        alert('window.sqlitePlugin missing');
    }
}

// appStudioDB.createTable = function() {
//  appStudioDB.instance.transaction(function(tx) {
//         tx.executeSql("CREATE TABLE IF NOT EXISTS MyTable (id INTEGER PRIMARY KEY ASC, text_sample TEXT, date_sample DATETIME)", []);
//     });

// }

// appStudioDB.createTables = function() {
//  var queryCreateTable = "";
//   var tblCount=0;
//   if(tblCount<tables.length){
//     // for(var tblCount=0; tblCount<tables.length; tblCount++){
//      var queryCreateTable = "CREATE TABLE IF NOT EXISTS ";
//      // if(tableName === tables[tblCount].tableName){

//      // }
//      queryCreateTable += tables[tblCount].tableName + ' (';
//      var tableAttributes = tables[tblCount].attributes;
//      for(var attrCount=0; attrCount<tableAttributes.length; attrCount++){
//        queryCreateTable += tableAttributes[attrCount].name;
//        queryCreateTable += ' ';
//        queryCreateTable += tableAttributes[attrCount].type;
//        if(attrCount !== tableAttributes.length-1){
//          queryCreateTable += ', ';
//        }else {
//          queryCreateTable += ')';
//        }       
//      }
//       alert(queryCreateTable);
//      appStudioDB.instance.transaction(function(tx) {
//          tx.executeSql(queryCreateTable, []);
//      }, appStudioDB.onTxError, appStudioDB.createTablesRecursively(tblCount));
//     // }
//   }   
// }

// appStudioDB.createTablesRecursively = function(tblCounter) {
//   var queryCreateTable = "";
//   var tblCount=tblCounter+1;
//   if(tblCount<tables.length){
//     // for(var tblCount=tblCounter+1; tblCount<tables.length; tblCount++){
//       var queryCreateTable = "CREATE TABLE IF NOT EXISTS ";
//       // if(tableName === tables[tblCount].tableName){

//       // }
//       queryCreateTable += tables[tblCount].tableName + ' (';
//       var tableAttributes = tables[tblCount].attributes;
//       for(var attrCount=0; attrCount<tableAttributes.length; attrCount++){
//         queryCreateTable += tableAttributes[attrCount].name;
//         queryCreateTable += ' ';
//         queryCreateTable += tableAttributes[attrCount].type;
//         if(attrCount !== tableAttributes.length-1){
//           queryCreateTable += ', ';
//         }else {
//           queryCreateTable += ')';
//         }       
//       }
//       alert(queryCreateTable);
//       appStudioDB.instance.transaction(function(tx) {
//           tx.executeSql(queryCreateTable, []);
//       }, appStudioDB.onTxError, appStudioDB.createTablesRecursively(tblCount));
//     // } 
//   }  
// }

appStudioDB.createTablesRecursively = function(tblCounter) {
    var queryCreateTable = "";
    var tblCount = tblCounter;
    if (tblCount < tables.length) {
        // for(var tblCount=tblCounter+1; tblCount<tables.length; tblCount++){
        var queryCreateTable = "CREATE TABLE IF NOT EXISTS ";
        // if(tableName === tables[tblCount].tableName){

        // }
        queryCreateTable += tables[tblCount].tableName + ' (';
        var tableAttributes = tables[tblCount].attributes;
        for (var attrCount = 0; attrCount < tableAttributes.length; attrCount++) {
            queryCreateTable += tableAttributes[attrCount].name;
            queryCreateTable += ' ';
            queryCreateTable += tableAttributes[attrCount].type;
            if (attrCount !== tableAttributes.length - 1) {
                queryCreateTable += ', ';
            } else {
                queryCreateTable += ')';
            }
        }
        appStudioDB.instance.transaction(function(tx) {
            tx.executeSql(queryCreateTable, []);
        }, appStudioDB.onTxError, appStudioDB.createTablesRecursively(tblCount + 1));
        // } 
    }
}

appStudioDB.dropTablesRecursively = function(tblCounter) {
    var queryCreateTable = "";
    var tblCount = tblCounter;
    if (tblCount < tables.length) {
        // for(var tblCount=tblCounter+1; tblCount<tables.length; tblCount++){
        var queryCreateTable = "DROP TABLE ";
        // if(tableName === tables[tblCount].tableName){

        // }
        queryCreateTable += tables[tblCount].tableName;

        appStudioDB.instance.transaction(function(tx) {
            tx.executeSql(queryCreateTable, []);
        }, appStudioDB.onTxError, appStudioDB.dropTablesRecursively(tblCount + 1));
        // } 
    }
}

appStudioDB.setAppListForContainter = function(appList, totalAppCount, tblCounter, callback) {
    var tblCount = tblCounter;
    var appDetails = appList;
    if (tblCount < totalAppCount) {
        appStudioDB.getAppDetails(appDetails[tblCount].appId, function(tx, rs) {
            if (rs && rs.rows.length > 0) {
                //If Exists then check appVersion
                if (rs.rows.item(0).AppVersion.toString() !== appDetails[tblCount].appVersion) {
                    appStudioDB.updateApplicationVersion(appDetails[tblCount], function() {
                        appStudioDB.setAppListForContainter(appDetails, totalAppCount, tblCount + 1, callback);
                    });
                } else {
                    appStudioDB.setAppListForContainter(appDetails, totalAppCount, tblCount + 1, callback);
                }
                //alert(JSON.stringify(rs.rows.item(0)));
            } else {

                //Insert in DB
                appStudioDB.insertApplication(appDetails[tblCount], function() {
                    appStudioDB.setAppListForContainter(appDetails, totalAppCount, tblCount + 1, callback);
                });

            }

        });
    } else {
        if (typeof callback === "function") {
            callback();
        }
    }



}


// appStudioDB.setupTable = function(tx){
//  tx.executeSql("CREATE TABLE IF NOT EXISTS notes(id INTEGER PRIMARY KEY,title,body,updated)");
// }

appStudioDB.onTxSuccess = function(tx, r) {
    // alert("Your SQLite query was successful!");
}

appStudioDB.onTxError = function(tx, e) {
    alert("SQLite Error: " + e.message);
}

appStudioDB.insertRecord = function(t) {
    appStudioDB.instance.transaction(function(tx) {
        var cDate = new Date();
        tx.executeSql("INSERT INTO MyTable(text_sample, date_sample) VALUES (?,?)", [t, cDate],
            appStudioDB.onTxSuccess,
            appStudioDB.onTxError);
    });
}

appStudioDB.updateRecord = function(id, t) {
    appStudioDB.instance.transaction(function(tx) {
        var mDate = new Date();
        tx.executeSql("UPDATE MyTable SET text_sample = ?, date_sample = ? WHERE id = ?", [t, mDate, id],
            appStudioDB.onTxSuccess,
            appStudioDB.onTxError);
    });
}

appStudioDB.deleteRecord = function(id) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("DELETE FROM MyTable WHERE id = ?", [id],
            appStudioDB.onTxSuccess,
            appStudioDB.onTxError);
    });
}

appStudioDB.selectAllRecords = function(tableName, orderBy, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("SELECT * FROM " + tableName + " ORDER BY " + orderBy, [],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Non-Container
// appStudioDB.getAppDetails = function(appId, appVersion, fnOnSuccess) {
//     appStudioDB.instance.transaction(function(tx) {
//         tx.executeSql("SELECT * FROM Application WHERE AppId = ? AND AppVersion = ?", [appId, appVersion],
//                       fnOnSuccess,
//                       appStudioDB.onTxError);
//     });
// }

//For Container
appStudioDB.getAppDetails = function(appId, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("SELECT * FROM Application WHERE AppId = ?", [appId],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.getAllApps = function(fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("SELECT * FROM Application ORDER BY AppId", [],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Non-Container
// appStudioDB.insertApplication = function(appDetails, fnOnSuccess) {  
//     appStudioDB.instance.transaction(function(tx) {
//         tx.executeSql("INSERT INTO Application(AppId, AppVersion, AppXML) VALUES (?,?,?)",
//                       [appDetails.appId, appDetails.appVersion, appDetails.appXML],
//                       fnOnSuccess,
//                       appStudioDB.onTxError);
//     });
// }

//For Container
appStudioDB.insertApplication = function(appDetails, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("INSERT INTO Application(AppId, AppVersion, AppName, AppLogo, AppDownloadStatus, AppUpdateStatus) VALUES (?,?,?,?,?,?)", [appDetails.appId, appDetails.appVersion, appDetails.appName, appDetails.appLogo, 1, 0],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Non-Container
appStudioDB.updateApplication = function(appDetails, appData, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("UPDATE Application SET AppVersion = ?, AppXML = ?, AppData = ?, AppDownloadStatus = ?, AppUpdateStatus = ? WHERE AppId = ?", [appDetails.appVersion, appDetails.appXML, appData, 0, 0, appDetails.appId],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.updateApplicationVersion = function(appDetails, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("UPDATE Application SET AppVersion = ?, AppDownloadStatus = ?, AppUpdateStatus = ? WHERE AppId = ?", [appDetails.appVersion, 0, 1, appDetails.appId],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

appStudioDB.updateApplicationDetails = function(appDetails, appXML, appData, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("UPDATE Application SET AppVersion = ?, AppXML = ?, AppData = ?, AppDownloadStatus = ?, AppUpdateStatus = ? WHERE AppId = ?", [appDetails.AppVersion, appXML, appData, 0, 0, appDetails.AppId],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.deleteApplication = function(id, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("DELETE FROM Application WHERE AppId = ?", [id],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.insertOfflineData = function(appDetails, request, serviceURL, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("INSERT INTO OfflineData(AppId, Request, ServiceURL, SyncStatus) VALUES (?,?,?,?)", [appDetails.AppId, request, serviceURL, 1],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.getOfflineDataByAppId = function(appId, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("SELECT * FROM OfflineData WHERE AppId = ? AND SyncStatus = ?", [appId, 1],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.getOfflineDataForAll = function(fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("SELECT * FROM OfflineData WHERE SyncStatus = ?", [1],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}

//For Container
appStudioDB.deleteOfflineData = function(id, fnOnSuccess) {
    appStudioDB.instance.transaction(function(tx) {
        tx.executeSql("DELETE FROM OfflineData WHERE OfflineDataId = ?", [id],
            fnOnSuccess,
            appStudioDB.onTxError);
    });
}
