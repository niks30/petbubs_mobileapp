define([], function () {
    var controllerModule = angular.module('appStudioStarter.controllers', []);
    var appData = getAppData(); // Called from common.js
    //setCustomerId(2);

    var AbstractController = function ($scope, $http, $cordovaOauth, $state, $q, $stateParams, $ionicLoading, $globalService, $netCallService, $ionicNavBarDelegate, $ionicHistory, $interval, $ionicModal, $timeout, $cordovaCamera, $cordovaFile, $ionicPopup, $cordovaLocalNotification, $ionicSlideBoxDelegate, $cordovaToast, $cordovaSocialSharing, $sce, $filter, $cordovaFileTransfer, $cordovaSQLite, VideoService, Session, $ionicScrollDelegate, Global, GlobalConstants, $ionicNativeTransitions, PetBubHttpService) {
        /*Dont change this will affect on side menu 25-11-16 By Bhupesh Bari*/
        $scope.Sidebarstate = false;

        $scope.Truetoggle = function () {
            document.getElementById('sidebarContainer').className = 'sidebar show';
            document.getElementById('overlay').className = "sidemenu-background-overlay";
            document.getElementById('overlay').style.opacity = 0.7;
        };

        $scope.falseToggle = function () {
            if ($state.current.name != "BubHubWallPostDetail") {
                document.getElementById('overlay').classList.remove("sidemenu-background-overlay");
                document.getElementById('overlay').style.opacity = 0;
                document.getElementById('sidebarContainer').className = 'sidebar';
            }
        };
        /*End*/

        $scope.baseUrl = GlobalConstants.defaultEndPoint;
        $scope.mi_customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
        $scope.mediaUrl = GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath;
        $scope.mediaUrl1 = GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath;
        $scope.userProfile = Global.getObjectFromLocalStorage(GlobalConstants.ls_profilePicture).length > 0 ? Global.getObjectFromLocalStorage(GlobalConstants.ls_profilePicture) : 'dummy.png';
        $scope.userName = Global.getObjectFromLocalStorage(GlobalConstants.ls_name);
        $scope.dataRating = {
            rating: 0,
            max: 5
        };
        $scope.globalData = gGlobalData;

        // $scope.bbHubTabActive = 0;

        if (window.cordova) {
            FCMPlugin.getToken(function (token) {
                console.log('Device token = ' + token);
                $scope.deviceTokenId = token;
            });

            FCMPlugin.onNotification(function (data) {
                if (data.wasTapped) {
                    //Notification was received on device tray and tapped by the user.
                    console.log(JSON.stringify(data));
                    $state.go('PetCalendar');
                } else {
                    //Notification was received in foreground. Maybe the user needs to be notified.
                    cordova.plugins.notification.local.schedule({
                        // id: 1,
                        title: data.title,
                        text: data.body,
                        icon: "file://img/icon.png",
                        smallIcon: "file://img/icon.png",
                        color: 'FFFFFF',
                        // icon: 'res://icon',
                        // smallIcon: 'res://icon',
                        // firstAt: monday_9_am,
                        // every: "week",
                        // sound: "file://sounds/reminder.mp3",
                        // icon: "http://icons.com/?cal_id=1",
                        // data: { meetingId: "123#fg8" }
                    });
                    console.log(JSON.stringify(data));
                }
            });

            cordova.plugins.notification.local.on("click", function (notification) {
                console.log(JSON.stringify(notification));
                $state.go('PetCalendar');
            });
        }

        $scope.forgotPassword = function(){
            console.log("Forgot Password");
        };

        //list item event
        $scope.onListItemClick = function (componentSubType, componentTargetLeaf, selectedItem) {
            //selectedItem=JSON.stringify(selectedItem);
            //console.log(selectedItem=JSON.stringify(selectedItem)); 
            if ($state.current.name == 'BubHubWallPostOne' || $state.current.name == 'BubHubWallPostTwo') {
                //////////// service call to display user Profile by Nikhil
                var dynamicList = getDynamicListData();
                dynamicList["BubPostDetail"] = {};
                dynamicList["BubPostDetail"]["postAuthorId"] = selectedItem.postAuthorId;
                setDynamicListData(dynamicList);
                $scope.noTargetServiceRequest("nextleaf", "SocialMedia");
                /////////////end 
            } else {
                for (var prop in selectedItem) {
                    gGlobalData[prop] = selectedItem[prop].toString();
                    if (componentTargetLeaf === 'SocialMedia' && prop === "customerid" && $state.current.name === 'Following' || $state.current.name === 'Follower' || $state.current.name === 'BubHubWallFindFriends' || $state.current.name === "LikedBy") {
                        var dynamicList = getDynamicListData();
                        dynamicList["BubPostDetail"] = {};
                        dynamicList["BubPostDetail"]["postAuthorId"] = selectedItem.customerid;
                        //console.log(dynamicList["BubPostDetail"]);
                        setDynamicListData(dynamicList);
                    }
                }
                $scope.onButtonClick(componentSubType, componentTargetLeaf);
                // console.log($scope.bbHubTabActive);
                window.localStorage.setItem('BUB_HUB_ACTIVE_TAB', $scope.bbHubTabActive);
            }
        };

        $scope.videoPlay = function (url) {
            VideoPlayer.play($scope.mediaUrl + url);
        }

        $scope.videoPlayBubTalk = function (url) {
            VideoPlayer.play($scope.baseUrl + url);
        }

        $scope.ChangeCity = function () {
            $scope.showModal('templates/AddCity.html');
        }

        //list item event
        $scope.New_onListItemClick = function (componentSubType, componentTargetLeaf, key, value, selectedItem) {
            var isVideo = (selectedItem && selectedItem.newstype && selectedItem.newstype === "V");

            if (componentTargetLeaf == 'SocialMedia') {
                //  Dummy Gallery Photos
                var GalleryImage = [];
                GalleryImage.push("images/landScroller1.jpg");
                GalleryImage.push("images/landScroller2.jpg");
                GalleryImage.push("images/landScroller3.jpg");
                GalleryImage.push("images/landScroller4.jpg");
                GalleryImage.push("images/landScroller5.png");
                GalleryImage.push("images/landScroller6.jpg");
                var d = getDynamicListData();
                d["GalleryImage"] = GalleryImage;
                setDynamicListData(d);
            }

            if (isVideo) {
                var videoPath = $scope.baseUrl + selectedItem.filelocation.slice(0, -3) + "mp4";
                VideoPlayer.play(videoPath);
            } else {
                if (value) {
                    gGlobalData[key] = value.toString();
                }
                for (var prop in selectedItem) {
                    gGlobalData[prop] = selectedItem[prop].toString();
                }
                $scope.onButtonClick(componentSubType, componentTargetLeaf);
            }
        }

        //onIconButton click
        $scope.onIconbuttonClick = function (type, value) {
            if (value.length > 0) {
                switch (type) {
                    case "call":
                        window.open('tel:' + value + ',_system');
                        break;
                    case "mail":

                        window.plugins.emailComposer.showEmailComposerWithCallback(function (result) { },
                            "Feedback from Petbubs", // Subject
                            "", // Body
                            [value], // To
                            null, // CC
                            null, // BCC
                            false, // isHTML
                            null, // Attachments
                            null); // Attachment Data
                        break;
                    case "website":
                        window.open("http://" + value, '_system');
                        break;
                }
            }
        }

        /*on share news button click*/
        $scope.onShareNewsClick = function (image, title) {
            var shareoptions = {
                message: title, // not supported on some apps (Facebook, Instagram)
                subject: null, // fi. for email
                files: [image], // an array of filenames either locally or remotely
                url: null,
                chooserTitle: 'Pet News Article' // Android only, you can override the default share sheet title
            }
            var onSuccess = function (result) { }
            var onError = function (msg) { }
            window.plugins.socialsharing.shareWithOptions(shareoptions, onSuccess, onError);
        }

        /*on pet icon click*/
        $scope.onBookmarkClick = function () {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);
            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.onButtonClick("nextleaf", "AddRemoveBookmark")
            } else {
                $scope.noTargetServiceRequest("nextleaf", "AddRemoveBookmark")
            }
        }

        $scope.onBookmarkRemove = function (Bookmark) {
            gGlobalData["petnewsid"] = Bookmark.petnewsid;
            $scope.noTargetServiceRequest("nextleaf", "AddRemoveBookmark")
        }

        /*on watch later*/
        $scope.onWatchLaterClick = function () {
            $scope.isWatchLater = true;
            $globalService.showAlert("Watch later");
        }

        //onRatingButtonClick
        $scope.onRatingButtonClick = function (params) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);
            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.showModal2('templates/BubTalkLogin.html');
            } else {
                $scope.dataRating.rating = 0;
                var alertPopup = $ionicPopup.alert({
                    title: 'Rate your experience on this listing',
                    cssClass: 'customPopup ',
                    // template: '<i class="icon ionIcon ion-close ratingClose"></i><rating ng-model="dataRating.rating" max="dataRating.max"></rating><div class="ratingShareLabel">Share</div><div class="row ratingRow"><div class="col15"><input type="checkbox"></div><div class="col30 ratingLabel">Facebook</div><div class="col15"><input type="checkbox"></div><div class="col30 ratingLabel">Twitter</div></div>',
                    // template: '<button class="button button-light ratingClose" ng-click="closeRating()" style="background-color:transparent;border-color:transparent"><i class="icon ionIcon ion-close"></i></button><rating ng-model="dataRating.rating" max="dataRating.max"></rating>',
                    template: '<rating ng-model="dataRating.rating" max="dataRating.max"></rating>',
                    scope: $scope,
                    buttons: [
                        { text: '<b>Cancel</b>' },
                        {
                            text: '<b>Submit</b>',
                            type: 'bar-5',
                            onTap: function (e) {
                                var serviceRequest = {};
                                var request = {};
                                var requestParams = [];
                                var serviceInfo = [];
                                requestParams.push({
                                    k: "userId",
                                    v: customerId
                                });
                                requestParams.push({
                                    k: "ratingValue",
                                    v: $scope.dataRating.rating
                                });
                                requestParams.push({
                                    k: "ModuleId",
                                    v: params.map.id.toString()
                                });
                                requestParams.push({
                                    k: "subModuleId",
                                    v: params.map.subModuleId
                                });

                                request["params"] = requestParams;
                                request["deviceId"] = getDeviceId();
                                request["initiatorId"] = customerId;

                                request["service"] = "PostRatings";
                                serviceRequest["req"] = request;

                                serviceInfo["progressInfo"] = "Please wait...";
                                $timeout(function () {
                                    doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                }, 1000);

                                alertPopup.close();
                            }
                        }]
                });
            }

            $scope.closeRating = function () {
                alertPopup.close();
            }
        }


        $scope.next = function () {
            $ionicSlideBoxDelegate.next();
        };

        $scope.previous = function () {
            $ionicSlideBoxDelegate.previous();
        };

        // Called each time the slide changes
        $scope.slideHasChanged = function (index) {
            $scope.slideIndex = index;
        };

        /*Select Category*/
        $scope.getCalandarimage = function (imagePath, title) {
            $scope.selectedCat = true;
            $scope.noDetailCatText = true;
            $scope.selectedCatImage = imagePath;
            gGlobalData["selectedCalendarimg"] = imagePath;
            gGlobalData["CatTitle"] = title;
            $scope.selectedCatTitle = title;
        }

        /*DeSelect Category*/
        $scope.UnsetCalandarimage = function () {
            $scope.selectedCat = false;
            $scope.selectedCatImage = "";
            $ionicScrollDelegate.resize();
        }

        $scope.validateEndDate = function () {
            var currentStartDate = new Date($scope.data.startDate).getTime() + (60 * 60 * 1000);
            var currentEndDate = new Date($scope.data.endDate).getTime();
            if (currentStartDate > currentEndDate) {
                var edt = new Date(currentStartDate);
                var endDate = $filter('date')(edt, 'yyyy-MM-dd HH:mm');
                $scope.data["endDate"] = new Date(endDate);
            }
        }

        $scope.onClickAddEvent = function () {
            $scope.data.petId = petId($scope.data.myPet, $scope.dynamicList.PetList);
            var result = onClickAddEvent($scope.data, gGlobalData, Global, GlobalConstants, $globalService.showAlert);
            if (result) {
                var addPetEventData = $scope.data;
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];

                requestParams.push({
                    k: "customerId",
                    v: customerId
                });

                requestParams.push({
                    k: "petid",
                    v: petId(addPetEventData.myPet, $scope.dynamicList.PetList)
                });

                requestParams.push({
                    k: "activityname",
                    v: gGlobalData.CatTitle
                });

                requestParams.push({
                    k: "activitydetails",
                    v: addPetEventData.selectActivity
                });

                requestParams.push({
                    k: "activitydate",
                    v: addPetEventData.startDate
                });

                requestParams.push({
                    k: "activityenddate",
                    v: addPetEventData.endDate
                });

                requestParams.push({
                    k: "activityreminder1",
                    v: addPetEventData.alertTimeText
                });

                requestParams.push({
                    k: "activityreminder2",
                    v: addPetEventData.choice
                });

                requestParams.push({
                    k: "stoprepeat",
                    v: addPetEventData.selectedDate === undefined ? '' : addPetEventData.selectedDate
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "addpetevent";
                serviceRequest["req"] = request;
                serviceInfo["progressInfo"] = "Please wait...";
                PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                    console.log('Result: ' + JSON.stringify(result));
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                    petEvents(customerId, "PetCalendar");
                });

                window.localStorage.removeItem('EVENT_DATE');
            }
        };


        $scope.deleteSinglePetActivity = function (eId, petid, petdeletedate, petdeletedaterepeat) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: "customerId",
                v: customerId
            });

            requestParams.push({
                k: "petid",
                v: petid
            });

            requestParams.push({
                k: "petactivityid",
                v: eId
            });

            requestParams.push({
                k: "deletedate",
                v: petdeletedate
            });

            requestParams.push({
                k: "deletedaterepeat",
                v: petdeletedaterepeat
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "deletepetevent";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";

            PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                console.log('Result: ' + JSON.stringify(result));
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                petEvents(customerId, "PetCalendar");
            });

            // doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            // $state.go("PetCalendar");
        }

        // Delete pet activity function
        $scope.deletePetActivity = function (eId, petid) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: "customerId",
                v: customerId
            });

            requestParams.push({
                k: "petid",
                v: petid
            });

            requestParams.push({
                k: "petactivityid",
                v: eId
            });


            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "deletepetevent";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";

            PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                console.log('Result: ' + JSON.stringify(result));
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                petEvents(customerId, "PetCalendar");
            });

            // doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            // $state.go("PetCalendar");
        }

        $scope.deleteEvent = function (eId, petid, index) {
            var data = $scope.dynamicList.viewEventItem;
            var result = deleteEvent(eId, Global, GlobalConstants);
            if (result) {
                // $scope.deletePetActivity(eId, petid);
                $scope.deleteSinglePetActivity(eId, petid, data.startDate, data.repeat);
                $globalService.showAlert('Petbubs', 'Event deleted successfully.');
            }
        };

        var deletePetConfirmation = function (petid) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var request = {};
            var requestParams = [];
            var serviceInfo = [];
            var serviceRequest = {};
            requestParams.push({
                k: "customerId",
                v: customerId
            }); requestParams.push({
                k: "petid",
                v: petid
            });
            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;
            request["service"] = "deletepet";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                $globalService.showAlert("Petbubs", "Pet Deleted successfully");
                myPetsService("ProfileMyPets");
            });
        };

        $scope.deletePet = function (petid) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Petbubs',
                template: 'Are you sure you want to delete your pet? All data related to the pet would be deleted forever 😞.'

            });
            confirmPopup.then(function (res) {
                if (res) {
                    deletePetConfirmation(petid)
                } else {
                    console.log('You are not sure');
                }
            });
        };



        $scope.onClickEditEvent = function () {
            if ($scope.data.editEventStarted) {
                $scope.data.petId = petId($scope.data.myPet, $scope.dynamicList.PetList);
                $scope.data.selectedDate = $scope.data.selectEndDate;
                var result = onClickAddEventFromServer($scope.data, gGlobalData, Global, GlobalConstants, $globalService.showAlert, true, $scope.dynamicList.viewEventItem.eId, true);
                if (result) {
                    var editPetEventData = $scope.data;
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];
                    var serviceRequest = {};

                    requestParams.push({
                        k: "customerId",
                        v: customerId
                    });

                    requestParams.push({
                        k: "petid",
                        v: editPetEventData.petId
                    });

                    requestParams.push({
                        k: "activityname",
                        v: gGlobalData.CatTitle
                    });

                    requestParams.push({
                        k: "activitydetails",
                        v: editPetEventData.selectActivity
                    });

                    requestParams.push({
                        k: "activitydate",
                        v: editPetEventData.startDate
                    });

                    requestParams.push({
                        k: "activityenddate",
                        v: editPetEventData.endDate
                    });

                    requestParams.push({
                        k: "activityreminder1",
                        v: editPetEventData.alertTimeText
                    });

                    requestParams.push({
                        k: "activityreminder2",
                        v: editPetEventData.choice
                    });

                    requestParams.push({
                        k: "stoprepeat",
                        v: editPetEventData.selectEndDate === null ? '' : editPetEventData.selectEndDate
                    });

                    requestParams.push({
                        k: "petactivityid",
                        v: editPetEventData.eId
                    });

                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = -1;

                    request["service"] = "editpetevent";
                    serviceRequest["req"] = request;
                    serviceInfo["progressInfo"] = "Please wait...";
                    PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                        // console.log('Result: ' + JSON.stringify(result));
                        var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                        petEvents(customerId, "PetCalendar");
                    });
                }
            }
        };


        $scope.onCheckboxChange = function (ParamName, value, key) {
            if (ParamName == "addRepeat" && value == true) {
                $scope.showModal('templates/addRepeat.html');
            } else if (ParamName == "addAlert" && value == true) {
                $scope.showModal('templates/addAlert.html');
            }
        }

        var removeDuplicates = function (keyName) {
            var array = $scope.profileInfoData;
            for (var i = 0; i < array.length; i++) {
                // console.log(array[i]);
            }
        }

        $scope.profileInfoData = [];
        $scope.onClickResetUserProfile = function () {

            $timeout(function () {
                document.getElementById('mi_image').src = 'images/profile.png';
                //$scope.data.mi_image = $scope.dynamicList.profileData.profilePicture;
                $scope.data.mi_name = null;
                $scope.data.mi_display_name = null;
                $scope.data.mi_birthdate = null;
                $scope.data.mi_city = null;
                $scope.data.mi_country = null;
                $scope.data.mi_facebook = null;
                $scope.data.mi_twitter = null;
                $scope.data.mi_instagram = null;
                gGlobalData["PROFILEPIC"] = "";
            })
        }

        $scope.onClickSaveDewormingActivity = function () {
            var dewormingVetActivity = {
                activityname: gGlobalData["title"],
                activitydate: $scope.data.addVetActivity.activitydate,
                activitydetails: '',
                activityenddate: new Date($scope.data.addVetActivity.activitydate.getTime() + (60 * 60 * 1000)),
                stoprepeat: '',
                activityReminder1: $scope.data.vetReminder1 ? $scope.data.vetReminder1 : "",
                activityReminder2: $scope.data.vetReminder2 ? $scope.data.vetReminder2 : "",
                activityRemider1_time: $scope.data.vetReminderTime1 ? $scope.data.vetReminderTime1 : "",
                activityReminder2_time: $scope.data.vetReminderTime2 ? $scope.data.vetReminderTime2 : "",
            };

            if (gGlobalData["VetActivity"].length > 0) {

                var hasUpdated = false;
                for (var index = 0; index < gGlobalData["VetActivity"].length; index++) {
                    if (gGlobalData["VetActivity"][index].activityname == gGlobalData["title"]) {
                        hasUpdated = true;
                        gGlobalData["VetActivity"][index].activitydate = $scope.data.addVetActivity.activitydate;
                        gGlobalData["VetActivity"][index].activityReminder1 = $scope.data.vetReminder1 ? $scope.data.vetReminder1 : "";
                        gGlobalData["VetActivity"][index].activityReminder2 = $scope.data.vetReminder2 ? $scope.data.vetReminder2 : "";
                        gGlobalData["VetActivity"][index].activityReminder1_time = $scope.data.vetReminderTime1 ? $scope.data.vetReminderTime1 : "";
                        gGlobalData["VetActivity"][index].activityReminder2_time = $scope.data.vetReminderTime2 ? $scope.data.vetReminderTime2 : "";
                        break;
                    }
                }

                if (!hasUpdated) {
                    gGlobalData["VetActivity"].push(dewormingVetActivity);
                }
            } else {
                gGlobalData["VetActivity"].push(dewormingVetActivity);
            }


            var dynamicList = getDynamicListData();

            if (gGlobalData["title"] === "Deworming") {
                dynamicList["dewormingVetActivity"] = dewormingVetActivity;
                dynamicList["dewormingVetActivity"].activitydate = new Date(dewormingVetActivity.activitydate);
            }

            if (gGlobalData["title"] === "Rabies Vaccine") {
                dynamicList["rabiesVetActivity"] = dewormingVetActivity;
                dynamicList["rabiesVetActivity"].activitydate = new Date(dewormingVetActivity.activitydate);
            }

            if (gGlobalData["title"] === "DHPPi & Leptosporosis") {
                dynamicList["DHPPiVetActivity"] = dewormingVetActivity;
                dynamicList["DHPPiVetActivity"].activitydate = new Date(dewormingVetActivity.activitydate);
            }
            gGlobalData["backView"] = "VetActivity";
            setDynamicListData(dynamicList);
            // $ionicHistory.goBack();
            $ionicNativeTransitions.goBack();
            //$state.go('AddAPet');
        }


        $scope.getBreedName = function (typeidvalue) {
            var data = getObjectByKeyFromArray($scope.dynamicList.PetCategory, "typeid", typeidvalue, "petbreed");
            return data;
        }

        $scope.clearText = function (argument) {
            document.getElementById('searchBar').setAttribute("style", "display:none");
            $scope.data.find_friends_text = "";
            $scope.searchText = "";
            document.querySelector('input').autofocus = false;
            var e = document.getElementById("navbarid");
            e.setAttribute("style", "display:block");
            if ($state.current.name === "PetCalendar") {
                document.getElementById('flexCalendar').setAttribute("style", "display:block");
                document.getElementById('EventData').setAttribute("style", "display:block");
            }

            if ($state.current.name === "BubTalkThreadCategories") {
                document.getElementById('threadCategoryHeading').setAttribute("style", "display:block");
            }
            if ($state.current.name === "BubTalkThreadList") {
                document.getElementById('threadListHeading').setAttribute("style", "display:block");
            }
        };

        $scope.showSearch = function () {
            document.getElementById('searchBar').setAttribute("style", "display:block");
            var e = document.getElementById("navbarid");
            e.setAttribute("style", "display:none");
            // $('#searchbox').focus().select();
            // document.getElementById('searchbox').focus();
            // document.querySelector('input').autofocus = true;
            // var $code = $('#searchbox');
            // $('.ion-search').on('mousedown', function () {
            //     $(this).data('inputFocused', $code.is(":focus"));
            // }).click(function () {
            //     if ($(this).data('inputFocused')) {
            //         $code.blur();
            //     } else {
            //         $code.focus();
            //     }
            // });
            $timeout(function () {
                var element = document.getElementById('searchbox');
                if (element) element.focus();
            }, 0);

            // window.cordova.plugins.Keyboard.show();
            if ($state.current.name === "PetCalendar") {
                document.getElementById('flexCalendar').setAttribute("style", "display:none");
                document.getElementById('EventData').setAttribute("style", "display:none");
            }
            if ($state.current.name === "BubTalkThreadCategories") {
                document.getElementById('threadCategoryHeading').setAttribute("style", "display:none");
            }
            if ($state.current.name === "BubTalkThreadList") {
                document.getElementById('threadListHeading').setAttribute("style", "display:none");
            }
            // $('#searchbox').attr("autofocus");
            // $('#searchbox').focus().select();
            // document.getElementById('searchbox').focus();
            // $("input:visible:enabled:not([readonly]),textarea:visible:enabled:not([readonly]),select:visible:enabled:not([readonly])",$state.current.name).first().focus();
            // angular.element('#searchbox').focus();
        };

        $scope.onClickAddPet = function () {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            var profileImage = gGlobalData["PROFILEPIC"];
            var name = $scope.data.addpet_name;
            var dob = $scope.data.addpet_birthday;
            var petType = $scope.data.pettype;
            var petBreed = $scope.data.addpet_breed;
            var petBreedName = $scope.data.addpet_breedname;
            var petSize = $scope.data.addpet_size;
            var petGender = $scope.data.addpet_gender;
            var petActivity = [];
            if ($scope.dynamicList.dewormingVetActivity != null) {
                petActivity.push($scope.dynamicList.dewormingVetActivity);
            }

            if ($scope.dynamicList.DHPPiVetActivity != null) {
                petActivity.push($scope.dynamicList.DHPPiVetActivity);
            }

            if ($scope.dynamicList.rabiesVetActivity != null) {
                petActivity.push($scope.dynamicList.rabiesVetActivity);
            }

            if (profileImage == undefined || profileImage == null) {
                $globalService.showAlert("Petbubs", 'Please select profile picture');
            } else if (name == undefined || name == null) {
                $globalService.showAlert("Petbubs", 'Please enter pet name');
            } else if (dob == undefined || dob == null) {
                $globalService.showAlert("Petbubs", 'Please enter date of birth');
            } else if (petType == undefined || petType == null) {
                $globalService.showAlert("Petbubs", 'Please select pet type');
            } else if (petBreed == undefined && petBreed == null) {
                $globalService.showAlert("Petbubs", 'Please select pet breed');
            } else if (petSize == undefined && petSize == null) {
                $globalService.showAlert("Petbubs", 'Please select pet size');
            } else if (petGender == undefined && petGender == null) {
                $globalService.showAlert("Petbubs", 'Please enter pet gender');
            } else {
                requestParams.push({
                    'k': 'customerId',
                    'v': customerId.toString()
                });
                requestParams.push({
                    'k': 'NAME',
                    'v': name
                });
                requestParams.push({
                    'k': 'PROFILEPIC',
                    'v': profileImage ? profileImage : ''
                });
                requestParams.push({
                    'k': 'DOB',
                    'v': dob
                });
                requestParams.push({
                    'k': 'PETTYPE',
                    'v': petType
                });
                requestParams.push({
                    'k': 'PETBREEDCATEGORY',
                    'v': petBreed
                });
                requestParams.push({
                    'k': 'PETBREEDNAME',
                    'v': petBreedName ? petBreedName : ''
                });
                requestParams.push({
                    'k': 'PETSIZE',
                    'v': petSize
                });
                requestParams.push({
                    'k': 'GENDER',
                    'v': petGender
                });
                requestParams.push({
                    'k': 'PETACTIVITY',
                    'v': petActivity ? JSON.stringify(petActivity) : []
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "addpet";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            }
        }

        var isRelativeImage = function (imageUrl) {
            if (/^(f|ht)tps?:\/\//i.test(imageUrl)) {
                return true;
            } else {
                return false;
            }
        }

        $scope.onClickSubmitEditPet = function () {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            $scope.profileInfoData.push({
                "k": "customerId",
                "v": customerId
            })

            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];
            var dynamicList = getDynamicListData();

            if (dynamicList['EditPetName']) {
                requestParams.push(dynamicList['EditPetName']);
            }

            if (dynamicList['EditPetDob']) {
                requestParams.push(dynamicList['EditPetDob']);
            }

            if (gGlobalData["EditPetBreedCategory"]) {
                requestParams.push(dynamicList['PetBreedCategory']);
            }

            if (dynamicList['EditPetBreedCategoryName']) {
                requestParams.push(dynamicList['PetBreedCategoryName']);
            }
            if (dynamicList['EditPetSize']) {
                requestParams.push(dynamicList['PetSize']);
            }
            if (dynamicList['EditPetGender']) {
                requestParams.push(dynamicList['PetGender']);
            }
            if (dynamicList['EditPetType']) {
                requestParams.push(dynamicList['PetType']);
            }

            requestParams.push({
                "k": "customerId",
                "v": customerId
            })
            requestParams.push({
                'k': 'petid',
                'v': $scope.dynamicList.PetInfo.petid
            });
            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "editpet";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        }

        $scope.showModal = function (templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        $scope.showModal2 = function (templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal2) {
                $scope.modal2 = modal2;
                $scope.modal2.show();
            });
        };

        // Change Password
        $scope.changePasswordClick = function (data) {
            //console.log(JSON.stringify(data));
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];
            if (data.newPassword != data.confirmPassword) {
                $globalService.showAlert("Petbubs", "Password and Confirm Password doesnt match");
            }
            requestParams.push({
                k: "CUSTOMERId",
                v: customerId
            });
            requestParams.push({
                k: "OLDPASSWORD",
                v: data.oldPassword
            });
            requestParams.push({
                k: "NEWPASSWORD",
                v: data.newPassword
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = "0";

            request["service"] = "ChangePassword";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            // console.log(customerId);
            if (data.newPassword === data.confirmPassword) {
                doNetworkServiceCall(serviceRequest, serviceInfo, "ProfileMyInfoEditSuccess", $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            }
        };
        // End Of change password

        $scope.setValueModal = function (key, value) {
            $scope.data[key] = value;
            $scope.modal.hide();
            $scope.modal.remove();
        };


        $scope.CloseCity = function (value) {
            // var e = document.getElementById("city_select");
            // e.innerHTML =value;
            // window.localStorage.setItem("selectedCity",value);
            var dynamicList = getDynamicListData();
            dynamicList["selectedCity"] = value;
            setDynamicListData(dynamicList);
            $scope.dynamicList.selectedCity = value;
            $scope.modal.hide();
            $scope.modal.remove();
        };

        $scope.callNumber = function (number) {
            window.open('tel:' + number + ',_system');
        }

        $scope.SaveAlert = function () {
            $scope.modal.hide();
            $scope.modal.remove();
        }

        $scope.toggleGroup = function (item) {
            if ($scope.isGroupShown(item)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = item;
            }
        };

        $scope.isGroupShown = function (item) {
            return $scope.shownGroup === item;
        };

        // Close the modal
        $scope.closeModal = function (key) {
            if (key == "addAlert") {
                $scope.data.addAlert = false;
            }
            if (key == "addRepeat") {
                $scope.data.addRepeat = false;
            }
            $scope.modal.hide();
            $scope.modal.remove()
        };

        $scope.closeModal2 = function (key) {
            if (key == "addAlert") {
                $scope.data.addAlert = false;
            }
            if (key == "addRepeat") {
                $scope.data.addRepeat = false;
            }
            $scope.modal2.hide();
            $scope.modal2.remove()
        };

        $scope.enterFollow = function () {
            $scope.closeModal();

            // $state.go('BubHubWallFindFriends');
            // $globalService.showLoading("Loading...");
            // $timeout(function () {
            $scope.buttonClicked(1);
            //     $globalService.hideLoading();
            //     $('#HubId').addClass(' active');
            //     $('#MyBubId').removeClass('active');
            // }, 2000);
        };

        $scope.onConfirmRequestClick = function (customerId) {
            gGlobalData["secondCustomerId"] = customerId;
            $scope.onButtonClick("nextleaf", "AcceptRequest");
        }

        $scope.onDeclineRequestClick = function (customerId) {
            gGlobalData["secondCustomerId"] = customerId;
            $scope.onButtonClick("nextleaf", "AcceptRequest");
        }

        //back button event
        $scope.onBackbuttonClick = function (componentTargetLeaf) {
            $state.go(componentTargetLeaf);
        }

        // Facebook Login

        // This is the success callback from the login method
        var fbLoginSuccess = function (response) {
            if (!response.authResponse) {
                fbLoginError("Cannot find the authResponse");
                return;
            }
            var authResponse = response.authResponse;
            getFacebookProfileInfo(authResponse).then(function (profileInfo) {
                // console.log(JSON.stringify(profileInfo));
                var picture = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
                $scope.showRegistrationScreen(profileInfo.name, profileInfo.email, picture);
                $ionicLoading.hide();
            }, function (fail) {
                console.log('profile info fail', fail);
            });
        };

        // This is the fail callback from the login method
        var fbLoginError = function (error) {
            console.log('fbLoginError', error);
            $ionicLoading.hide();
        };

        // This method is to get the user profile info from the facebook api
        var getFacebookProfileInfo = function (authResponse) {
            var info = $q.defer();
            facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null, function (response) {
                // console.log(response);
                info.resolve(response);
            }, function (response) {
                // console.log(response);
                info.reject(response);
            });
            return info.promise;
        };

        $scope.facebookLogin = function () {
            facebookConnectPlugin.getLoginStatus(function (success) {
                if (success.status === 'connected') {
                    getFacebookProfileInfo(success.authResponse).then(function (profileInfo) {
                        var picture = "http://graph.facebook.com/" + profileInfo.id + "/picture?type=large";
                        $scope.showRegistrationScreen(profileInfo.name, profileInfo.email, picture);
                    }, function (fail) {
                        console.log('profile info fail', fail);
                    });
                } else {
                    // console.log('getLoginStatus', success.status);
                    $ionicLoading.show({
                        template: 'Logging in...'
                    });
                    facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
                }
            });
        };
        // End of FB Login

        // Google Plus Login
        $scope.googlePlusLogin = function () {
            $ionicLoading.show({
                template: 'Logging in...'
            });

            window.plugins.googleplus.login(
                {},
                function (user_data) {
                    // For the purpose of this example I will store user data on local storage
                    // console.log(JSON.stringify(user_data));
                    var picture = user_data.imageUrl;
                    // console.log('Profile Pic = ' + picture);
                    $scope.showRegistrationScreen(user_data.displayName, user_data.email, user_data.imageUrl);
                    $ionicLoading.hide();
                }, function (msg) {
                    console.log(msg);
                    $ionicLoading.hide();
                });
        };
        // End Of Google_Plus Login

        // Twitter Login
        $scope.twitterLogin = function () {
            TwitterConnect.login(function (result) {
                // console.log('Successful login!');
                // console.log(result);
            }, function (error) {
                console.log('Error logging in');
                console.log(error);
            });
        };
        // End of twitter Login

        $scope.myGoBack = function () {
            var index = parseInt(window.localStorage.getItem("POST_INDEX"));
            var history = $ionicHistory.backView();//returns an object with data about the previous screen. console.log(history) to see the entire data.
            var previous_statename = history.stateName;//compare this
            var fullHistory = $ionicHistory.viewHistory(); // return histroy
            if (previous_statename == "Gallery") {
                $scope.noTargetServiceRequest("nextleaf", "Gallery");
            } else if (previous_statename === "ProfileMyPets") {
                $scope.noTargetServiceRequest("nextleaf", "ProfileMyPets");
            } else if (previous_statename === "DRBubsHome") {
                $ionicNativeTransitions.goBack();
            } else if ($state.current.name === "AddEventLeaf") {
                window.localStorage.removeItem('EVENT_DATE');
                $ionicNativeTransitions.goBack();
            } else if ($state.current.name === "PetCalendar") {
                $ionicNativeTransitions.goBack();
            } else if ($state.current.name === "LikedBy") {

                var prevstate = fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 3].stateName;
                if (prevstate === "BubHubWallPostDetail") {
                    $ionicNativeTransitions.stateGo(fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 4].stateName, {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $state.go(fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 4].stateName);
                } else if (prevstate === "LikedBy") {
                    $ionicNativeTransitions.stateGo(fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 5].stateName, {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $state.go(fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 5].stateName);
                } else {
                    // $scope.showPostDetails(index);
                    $ionicNativeTransitions.stateGo(prevstate, {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $ionicHistory.goBack();
                    //$state.go(prevstate,{},{reload:true});
                    // $state.noTargetServiceRequest("","BubHubWallPostOne");
                }
            } else if ($state.current.name === "BubHubWallPostOne" && previous_statename != "childView") {
                var prevstate = fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 3].stateName;

                if (previous_statename === "LikedBy") {
                    $ionicNativeTransitions.stateGo("childView", {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $state.go("childView");
                } else if (previous_statename === "BubHubWallPostTwo") {
                    $ionicNativeTransitions.stateGo("childView", {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $state.go("childView");
                } else if (previous_statename === "SocialMedia") {
                    $ionicNativeTransitions.stateGo("childView", {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $state.go("childView");
                    // $state.noTargetServiceRequest("","BubHubWallPostOne");
                } else {
                    $ionicNativeTransitions.goBack();
                }

            } else if ($state.current.name === "BubHubWallPostTwo") {

                var prevstate = fullHistory.histories.root.stack[fullHistory.histories.root.stack.length - 3].stateName;

                if (previous_statename === "LikedBy") {
                    $ionicNativeTransitions.stateGo("BubHubWallPostOne", {}, {}, {
                        "type": "slide",
                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                        "duration": 500, // in milliseconds (ms), default 400
                    });
                    // $state.go("BubHubWallPostOne");
                } else {
                    $ionicNativeTransitions.goBack();
                }

            } else if (previous_statename === "BubHubWallFindFriends") {
                $globalService.showLoading("Loading...");
                $ionicNativeTransitions.goBack();
                // var tab = parseInt(window.localStorage.getItem('BUB_HUB_ACTIVE_TAB'));

                // $timeout(function () {
                //     if (tab === 0) {
                //         $('#MyBubId').addClass(' active');
                //         $('#HubId').removeClass('active');
                //         $scope.buttonClicked(tab);
                //         $globalService.hideLoading();
                //     } else {
                //         $('#HubId').addClass(' active');
                //         $('#MyBubId').removeClass('active');
                //         $scope.buttonClicked(tab);
                //         $globalService.hideLoading();
                //     }

                // }, 2000);
            } else if (window.localStorage.getItem(GlobalConstants.ls_hasSeenBubTalkLeaf) && $state.current.name === "BubTalkCategoryList") {
                $ionicHistory.goBack(-2);
                // $ionicNativeTransitions.stateGo('childView', {}, {}, {
                //     "type": "slide",
                //     "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                //     "duration": 500, // in milliseconds (ms), default 400
                // });
            } else if (previous_statename === "BubHubWallPostDetail") {
                $ionicNativeTransitions.stateGo('BubHubWallPostOne', {}, {}, {
                    "type": "slide",
                    "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                    "duration": 500, // in milliseconds (ms), default 400
                });
            } else if ($state.current.name === "SocialMedia" && previous_statename === "BubHubWallPostTwo") {

                $ionicNativeTransitions.stateGo('BubHubWallPostTwo', {}, {}, {
                    "type": "slide",
                    "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                    "duration": 500, // in milliseconds (ms), default 400
                });
            } else {
                $ionicNativeTransitions.goBack();
            }
        }

        $scope.onClickCancelEditPet = function () {
            // $ionicHistory.goBack();
            $ionicNativeTransitions.goBack();
        };

        $scope.checkPinCode = function (address, city) {
            var pinCodeWithSpaces = address.substring(address.length - 10, address.length).replace(/ /g, "");
            var pinCode = parseInt(pinCodeWithSpaces.substring(pinCodeWithSpaces.length - 6, pinCodeWithSpaces.length));
            var withOutPin = address.substring(0, address.length - 7);
            if (angular.isNumber(pinCode) && !isNaN(pinCode)) {
                var FullPinAddress = withOutPin + " " + city + " " + pinCode;
                return FullPinAddress;
            } else {
                return address + " " + city;
            }
        }


        //Clear cached data
        $scope.events = Global.getObjectFromLocalStorage(GlobalConstants.ls_calendarEvents);
        $scope.options = {
            defaultDate: new Date(),
            minDate: "1900-01-01",
            maxDate: "2099-12-31",
            disabledDates: [],
            dayNamesLength: 1, // 1 for "M", 2 for "Mo", 3 for "Mon"; 9 will show full day names. Default is 1.
            mondayIsFirstDay: false, //set monday as first day of week. Default is false
            eventClick: function (date) {
                window.localStorage.setItem("EVENT_DATE", date.date);
            },
            dateClick: function (date) {
                window.localStorage.setItem("EVENT_DATE", date.date);
            },
            changeMonth: function (month, year) { },

            deletePetActivity: function (eId, petid) {
                $scope.deletePetActivity(eId, petid);
            },
            deleteSinglePetActivity(eId, petid, petdeletedate, petdeletedaterepeat) {
                $scope.deleteSinglePetActivity(eId, petid, petdeletedate, petdeletedaterepeat);
            }

        };

        $scope.$on('$ionicView.beforeLeave', function (ev, states) {

            if (states.stateName === "Directoryleaf" || states.stateName === "BubHubWallPostOne" || states.stateName === "BubTalkThreadList") {
                if (typeof $scope.data.oldScrollPosition === "undefined") {
                    $scope.data.oldScrollPosition = {};
                }
                $scope.data.oldScrollPosition = $ionicScrollDelegate.$getByHandle('scroller').getScrollPosition();
            }
        });

        $scope.noTargetServiceRequest = function (componentSubType, componentTargetLeaf) {
            for (var template in appData.views) {
                if (appData.views[template].title === componentTargetLeaf) {
                    var serviceInfo = appData.views[template].networkService;
                    if (serviceInfo) {
                        if (componentTargetLeaf === "Gallery") {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, componentTargetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        } else if (componentTargetLeaf === "SocialMedia") {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, componentTargetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        } else if (componentTargetLeaf === "ProfileMyInfo") {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, componentTargetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        } else if (componentTargetLeaf === "ProfileMyPets") {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, componentTargetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        } else if (componentTargetLeaf === "FindPet" || componentTargetLeaf === "PetCare" || componentTargetLeaf === "PetNeeds" || componentTargetLeaf === "PetParty" || componentTargetLeaf === "PetTravel" || componentTargetLeaf === "PetFarewell") {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, componentTargetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        } else if (componentTargetLeaf === "PetCalendar") {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, "AfterEditPetEvent", $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        } else {
                            var serviceRequest = createServiceRequest(serviceInfo, null, key, componentTargetLeaf, Global, GlobalConstants, $state);
                            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                        }
                    }
                }
            }
        };

        $('#drHomeTabs').bind('easytabs:before', function (id, val, t) {
            if (t.selector === "#tab-pw") {
                window.localStorage.setItem('ACTIVE_TAB', '#t1');
            } else if (t.selector === "#tab-qw") {
                window.localStorage.setItem('ACTIVE_TAB', '#t2');
            } else if (t.selector === "#tab-rw") {
                window.localStorage.setItem('ACTIVE_TAB', '#t3');
            }
        });

        $scope.$on("$ionicView.afterEnter", function (ev, states) {
            if (states.stateName === "BubHubWallFindFriends") {
                $timeout(function () {
                    var element = document.getElementById('find_friends_text');
                    if (element) element.focus();
                }, 0);
            }
        });

        $scope.$on("$ionicView.beforeEnter", function (ev, states) {

            //$('#drHomeTabs').easytabs({ defaultTab: "li#t2" });
            gGlobalData["EditedPost"] = undefined;
            if (states.stateName === "ProfileMyPets") {
                gGlobalData.VetActivity = [];
            }

            if (states.direction === "forward") {
                ev.targetScope.data = resetScopeData(ev.targetScope.data, "headerTitle,headerbarClass,leafName,title");
            }

            if (states.stateName === "ProfileMyInfoEdit") {
                ev.targetScope.data.mi_name = ev.targetScope.dynamicList.profileData.name;
                ev.targetScope.data.mi_mobile = ev.targetScope.dynamicList.profileData.mobile;
                ev.targetScope.data.mi_email = ev.targetScope.dynamicList.profileData.email;
                ev.targetScope.data.mi_country = ev.targetScope.dynamicList.profileData.country;
                ev.targetScope.data.mi_display_name = ev.targetScope.dynamicList.profileData.displayname;
                ev.targetScope.data.mi_facebook = ev.targetScope.dynamicList.profileData.facebookid;
                ev.targetScope.data.mi_instagram = ev.targetScope.dynamicList.profileData.googleid;
                ev.targetScope.data.mi_twitter = ev.targetScope.dynamicList.profileData.twitterid;
                ev.targetScope.data.mi_birthdate = new Date(ev.targetScope.dynamicList.profileData.dob);
                ev.targetScope.data.mi_city = ev.targetScope.dynamicList.profileData.city;
                gGlobalData["PROFILEPIC"] = ev.targetScope.dynamicList.profileData.profilepicture;
            }

            if (states.stateName === "BubHubWallFindFriends" && states.direction === "back") {
                $scope.noTargetServiceRequest("nextleaf", "BubHubWallFindFriends");
            }

            if (states.stateName === 'DRBubsHome' && states.direction === "forward") {
                $('#drHomeTabs').easytabs({ defaultTab: "li#t1" });
                window.localStorage.setItem('ACTIVE_TAB', '#t1');
            }

            if (states.stateName === 'DRBubsHome' && states.direction === "back") {
                var previousSelectedTab = window.localStorage.getItem('ACTIVE_TAB');
                if (previousSelectedTab === "#t1") {
                    $('#drHomeTabs').easytabs({ defaultTab: "li#t1" });
                } else if (previousSelectedTab === "#t2") {
                    $('#drHomeTabs').easytabs({ defaultTab: "li#t2" });
                } else if (previousSelectedTab === "#t3") {
                    $('#drHomeTabs').easytabs({ defaultTab: "li#t3" });
                }
            }

            if (states.direction === "forward" || states.direction === "back") {
                if (states.stateName === "BubHubWallPostOne") {
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];

                    requestParams.push({
                        k: 'CUSTOMERId',
                        v: customerId
                    });

                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = -1;

                    request["service"] = "TopTenPost";
                    serviceRequest["req"] = request;

                    serviceInfo["progressInfo"] = "Please wait...";
                    doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                }
            }

            if (states.direction === "forward" || states.direction === "back") {
                if (states.stateName === "BubHubWallPostTwo") {
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];

                    requestParams.push({
                        k: 'CUSTOMERId',
                        v: customerId
                    });

                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = -1;

                    request["service"] = "TopTenPBPost";
                    serviceRequest["req"] = request;

                    serviceInfo["progressInfo"] = "Please wait...";
                    doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                }
            }


            if (states.stateName === "BubTalkThreadCategories" && states.direction === "back") {
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];
                requestParams.push({
                    k: "categoryId",
                    v: gGlobalData["BubCategory"]
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = "0";

                request["service"] = "AllThreads";
                serviceRequest["req"] = request;
                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            }
            // if (states.stateName == "Gallery" && states.direction === "back") { 
            //    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            //    var serviceRequest = {};
            //     var request = {};
            //     var requestParams = [];
            //     var serviceInfo = [];
            //     requestParams.push({
            //         k: "OPPCUSTOMERID",
            //         v: customerId
            //     });

            //     requestParams.push({
            //         k: "SELFCUSTOMERID",
            //         v: customerId
            //     });

            //     request["params"] = requestParams;
            //     request["deviceId"] = getDeviceId();
            //     request["initiatorId"] = "0";

            //     request["service"] = "SocialMedia";
            //     serviceRequest["req"] = request;
            //     serviceInfo["progressInfo"] = "Please wait...";
            //     doNetworkServiceCall(serviceRequest, serviceInfo, "Gallery", $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            // }
            if (states.stateName == "BubTalkThreadList" && states.direction === "back") {
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];
                requestParams.push({
                    k: "categoryId",
                    v: gGlobalData["BubCategory"]
                });
                requestParams.push({
                    k: "topicId",
                    v: gGlobalData["topic"]
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = "0";

                request["service"] = "TopicThreadList";
                serviceRequest["req"] = request;
                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            }

            if (states.stateName === "AddAPet" && states.direction === "forward") {
                var dynamicList = getDynamicListData();
                dynamicList["dewormingVetActivity"] = undefined;
                dynamicList["rabiesVetActivity"] = undefined;
                dynamicList["DHPPiVetActivity"] = undefined;
                setDynamicListData(dynamicList);
                ev.targetScope.dynamicList.dewormingVetActivity = undefined;
                ev.targetScope.dynamicList.rabiesVetActivity = undefined;
                ev.targetScope.dynamicList.DHPPiVetActivity = undefined;
                ev.targetScope.data.pettype = '1';
                gGlobalData["PROFILEPIC"] = undefined;
                $scope.data = {};
            }

            if (states.stateName === "AddAPet" && states.direction === "back") {
                $scope.data = $scope.dynamicList['AddAPetData'];
                $scope.data.addpet_birthday = new Date($scope.dynamicList['AddAPetData'].addpet_birthday);
            }

            if (states.stateName === "AddAPetEdit") {
                ev.targetScope.data.mi_name = ev.targetScope.dynamicList.PetInfo.name;
                ev.targetScope.data.petid = ev.targetScope.dynamicList.PetInfo.petid;
                ev.targetScope.data.mi_birthdate = new Date(ev.targetScope.dynamicList.PetInfo.dob);
                gGlobalData["PROFILEPIC"] = ev.targetScope.dynamicList.PetInfo.profilepic;

                ev.targetScope.data.addpet_pettype = getObjectByKeyFromArray(ev.targetScope.dynamicList.PetCategory, "typeid", ev.targetScope.dynamicList.PetInfo.pettype, "petbreed");
                ev.targetScope.data.addpet_breed = ev.targetScope.dynamicList.PetInfo.petbreedcategory;
                ev.targetScope.data.addpet_breedname = ev.targetScope.dynamicList.PetInfo.petbreedname;
                ev.targetScope.data.addpet_size = ev.targetScope.dynamicList.PetInfo.petsize;
                ev.targetScope.data.addpet_gender = ev.targetScope.dynamicList.PetInfo.gender;
                // ev.targetScope.data.petactivity=ev.targetScope.dynamicList.PetInfo.twitterid;
            }

            if (states.stateName == "VetActivity") {
                if (gGlobalData["title"] === "Deworming") {
                    ev.targetScope.data.addVetActivity = ev.targetScope.dynamicList.dewormingVetActivity ? (ev.targetScope.dynamicList.dewormingVetActivity) : undefined;
                    if (ev.targetScope.dynamicList.dewormingVetActivity) {
                        ev.targetScope.data.addVetActivity.activitydate = getDateFromString(ev.targetScope.dynamicList.dewormingVetActivity.activitydate);
                        ev.targetScope.data.vetReminder1 = ev.targetScope.dynamicList.dewormingVetActivity.activityReminder1;
                        ev.targetScope.data.vetReminder2 = ev.targetScope.dynamicList.dewormingVetActivity.activityReminder2;
                    } else {
                        ev.targetScope.data.addVetActivity = {};
                        ev.targetScope.data.vetReminder1 = "";
                        ev.targetScope.data.vetReminder2 = "";
                    }
                }

                if (gGlobalData["title"] === "Rabies Vaccine") {
                    ev.targetScope.data.addVetActivity = ev.targetScope.dynamicList.rabiesVetActivity ? new Date(ev.targetScope.dynamicList.rabiesVetActivity.activitydate) : undefined;
                    if (ev.targetScope.dynamicList.rabiesVetActivity) {
                        ev.targetScope.data.addVetActivity.activitydate = getDateFromString(ev.targetScope.dynamicList.rabiesVetActivity.activitydate);
                        ev.targetScope.data.vetReminder1 = ev.targetScope.dynamicList.rabiesVetActivity.activityReminder1;
                        ev.targetScope.data.vetReminder2 = ev.targetScope.dynamicList.rabiesVetActivity.activityReminder2;
                    } else {
                        ev.targetScope.data.addVetActivity = {};
                        ev.targetScope.data.vetReminder1 = "";
                        ev.targetScope.data.vetReminder2 = "";
                    }
                }

                if (gGlobalData["title"] === "DHPPi & Leptosporosis") {
                    ev.targetScope.data.addVetActivity = ev.targetScope.dynamicList.DHPPiVetActivity ? new Date(ev.targetScope.dynamicList.DHPPiVetActivity.activitydate) : undefined;
                    if (ev.targetScope.dynamicList.DHPPiVetActivity) {
                        ev.targetScope.data.addVetActivity.activitydate = getDateFromString(ev.targetScope.dynamicList.DHPPiVetActivity.activitydate);
                        ev.targetScope.data.vetReminder1 = ev.targetScope.dynamicList.DHPPiVetActivity.activityReminder1;
                        ev.targetScope.data.vetReminder2 = ev.targetScope.dynamicList.DHPPiVetActivity.activityReminder2;
                    } else {
                        ev.targetScope.data.addVetActivity = {};
                        ev.targetScope.data.vetReminder1 = "";
                        ev.targetScope.data.vetReminder2 = "";
                    }
                }

            }

            if (states.stateName == "FindPet") {

                if (gGlobalData["FindPet"].toString() === "1") {
                    $scope.Title = "Adoption Agencies & Shelters";
                }
                if (gGlobalData["FindPet"] === "2") {
                    $scope.Title = "Pet Shops";
                }
                if (gGlobalData["FindPet"] === "3") {
                    $scope.Title = "Security Guard Dogs";
                }
                if (gGlobalData["FindPet"] === "0") {
                    $scope.Title = "Breeders";
                }
            }

            if (states.stateName == "PetCare") {

                if (gGlobalData["PetCare"] === "7") {
                    $scope.Title = "Veterinary Care";
                }
                if (gGlobalData["PetCare"] === "0") {
                    $scope.Title = "Pet Ambulances";
                }
                if (gGlobalData["PetCare"] === "6") {
                    $scope.Title = "Canine Behaviourists & Trainers";
                }
                if (gGlobalData["PetCare"] === "2") {
                    $scope.Title = "Pet Walkers & Sitters";
                }
            }

            if (states.stateName == "PetNeeds") {
                if (gGlobalData["PetNeeds"] === "2") {
                    $scope.Title = "Pet Supplies";
                }
                if (gGlobalData["PetNeeds"] === "1") {
                    $scope.Title = "Pet Salons & Spas";
                }
                if (gGlobalData["PetNeeds"] === "0") {
                    $scope.Title = "@Home Grooming";
                }
            }

            if (states.stateName == "PetParty") {
                if (gGlobalData["PetPartys"] === "3") {
                    $scope.Title = "Event Managers";
                }
                if (gGlobalData["PetPartys"] === "5") {
                    $scope.Title = "Restaurants / Bakeries";
                }
                if (gGlobalData["PetPartys"] === "4") {
                    $scope.Title = "Photographers";
                }
                if (gGlobalData["PetPartys"] === "6") {
                    $scope.Title = "Breeders";
                }
            }

            if (states.stateName == "PetTravel") {
                if (gGlobalData["PetTravels"] === "0") {
                    $scope.Title = "Pet Friendly Hotels";
                }
                if (gGlobalData["PetTravels"] === "1") {
                    $scope.Title = "Pet Taxis";
                }
                if (gGlobalData["PetTravels"] === "2") {
                    $scope.Title = "Boarding & Kennels";
                }
            }

            if (states.stateName == "AddEventLeaf") {
                var eventDate = window.localStorage.getItem('EVENT_DATE');
                // console.log(eventDate);
                if (eventDate) {
                    var dt = new Date(eventDate);
                    var todaydt = $filter('date')(dt, 'yyyy-MM-dd HH:mm');
                    $scope.data["startDate"] = new Date(todaydt);
                    var edt = new Date(dt.getTime() + (60 * 60 * 1000)); // 1 hour ahead of start Date
                    var endDate = $filter('date')(edt, 'yyyy-MM-dd HH:mm');
                    $scope.data["endDate"] = new Date(endDate);
                } else {
                    var dt = new Date();
                    var todaydt = $filter('date')(dt, 'yyyy-MM-dd HH:mm');
                    $scope.data["startDate"] = new Date(todaydt);
                    var edt = new Date(dt.getTime() + (60 * 60 * 1000)); // 1 hour ahead of start Date
                    var endDate = $filter('date')(edt, 'yyyy-MM-dd HH:mm');
                    $scope.data["endDate"] = new Date(endDate);
                }

                $scope.data.myPet = "";
                $scope.selectedCat = false;
                $scope.noDetailCatText = false;
                $scope.selectedCatImage = "";
                gGlobalData["selectedCalendarimg"] = "";
                gGlobalData["CatTitle"] = "";
                $scope.selectedCatTitle = "";
                $scope.data.selectActivity = "";
                $scope.data.selectEndDate = "";
                $scope.data.alertTime = "";
                $scope.data.alertTimeText = "";
                $scope.data.choice = "";

                $scope.data.addEventEligible = false;
                $scope.$watch('[data.myPet, selectedCatImage, data.startDate, data.endDate]', function (newContent, oldContent) {
                    var newName = newContent[0];
                    var newcatImg = newContent[1];
                    var newStart = newContent[2];
                    var newEnd = newContent[3];
                    if (newName && newcatImg && newStart && newEnd) {
                        $scope.data.addEventEligible = true;
                    } else {
                        $scope.data.addEventEligible = false;
                    }
                });

                $scope.addVetActivity = false;
                $scope.$watch('[data.dewormingDate]', function (newContent, oldContent) {
                    var newDate = newContent[0];
                    if (newDate) {
                        $scope.data.addVetActivity = true;
                    } else {
                        $scope.data.addVetActivity = false;
                    }
                });
            }

            if (states.stateName === "PetCalendar") {
                // callNotificationRequest();
            }

            if (states.stateName == "EditEventLeaf" && $scope.dynamicList.PetList.length > 0) {
                if (ev.targetScope === $scope) {
                    $scope.data.eId = $scope.dynamicList.viewEventItem.eId;
                    $scope.selectedCat = true;
                    $scope.noDetailCatText = true;
                    $scope.selectedCatImage = $scope.dynamicList.viewEventItem.img;
                    gGlobalData["selectedCalendarimg"] = $scope.dynamicList.viewEventItem.img;
                    gGlobalData["CatTitle"] = $scope.dynamicList.viewEventItem.type;
                    $scope.selectedCatTitle = $scope.dynamicList.viewEventItem.type;
                    $scope.data.selectActivity = $scope.dynamicList.viewEventItem.title;
                    $scope.data.startDate = new Date($scope.dynamicList.viewEventItem.startDate);
                    $scope.data.endDate = new Date($scope.dynamicList.viewEventItem.endDate);
                    $scope.data.selectEndDate = (($scope.dynamicList.viewEventItem.endRepeat) ? new Date($scope.dynamicList.viewEventItem.endRepeat) : "");
                    $scope.data.alertTime = ($scope.dynamicList.viewEventItem.alertTime === "undefined") ? "" : $scope.dynamicList.viewEventItem.alertTime;
                    $scope.data.alertTimeText = ($scope.dynamicList.viewEventItem.alert === "Never") ? "" : $scope.dynamicList.viewEventItem.alert;
                    $scope.data.choice = ($scope.dynamicList.viewEventItem.repeat === "Never") ? "" : $scope.dynamicList.viewEventItem.repeat;
                    $scope.data.editEventStarted = false;
                    $scope.data.myPet = $scope.dynamicList.viewEventItem.myPet;

                    $scope.$watch('[data.myPet, selectedCatImage, data.selectActivity, data.startDate, data.endDate, data.alertTime, data.choice, data.selectEndDate]', function (newContent, oldContent) {
                        var newName = newContent[0];
                        var oldName = oldContent[0];
                        var newcatImg = newContent[1];
                        var oldcatImg = oldContent[1];
                        var newDetail = newContent[2];
                        var oldDetail = oldContent[2];
                        var newStart = newContent[3];
                        var oldStart = oldContent[3];
                        var newEnd = newContent[4];
                        var oldEnd = oldContent[4];
                        var newAlert = newContent[5];
                        var oldAlert = oldContent[5];
                        var newRepeat = newContent[6];
                        var oldRepeat = oldContent[6];
                        var newEndRepeat = newContent[7];
                        var oldEndRepeat = oldContent[7];
                        if (newName !== oldName || newcatImg !== oldcatImg || newDetail !== oldDetail || newStart !== oldStart || newEnd !== oldEnd || newAlert !== oldAlert || newRepeat !== oldRepeat || newEndRepeat !== oldEndRepeat) {
                            $scope.data.editEventStarted = true;
                        }
                    });
                }
            }

            var leaf = states.stateName;
            if (leaf === "Directoryleaf" || leaf === "BubHubWallPostOne" || leaf === "BubTalkThreadList") {
                if ($scope.data.oldScrollPosition && $ionicScrollDelegate) {
                    $timeout(function () {
                        $ionicScrollDelegate.scrollTo(0, $scope.data.oldScrollPosition.top);
                    }, 10);
                }
            }

            // if (states.stateName === "childView" && Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId).length != 0) {
            //     var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            //     petEvents(customerId, "AfterEditPetEvent");
            // }
        });

        var callNotificationRequest = function () {
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            serviceRequest = {};
            request = {};
            requestParams = [];
            serviceInfo = [];

             requestParams.push({
                k: "stop",
                v: true
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = "0";

            request["service"] = "NotificationService";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                console.log(JSON.stringify(result));
                $globalService.hideLoading();
            });
        };

        var myPetsService = function (stateName) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            serviceRequest = {};
            request = {};
            requestParams = [];
            serviceInfo = [];

            requestParams.push({
                k: "customerId",
                v: customerId
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = "0";

            request["service"] = "mypets";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, stateName, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            $globalService.hideLoading();
        }


        var petEvents = function (customerId, stateName) {
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            serviceRequest = {};
            request = {};
            requestParams = [];
            serviceInfo = [];

            requestParams.push({
                k: "customerId",
                v: customerId
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = "0";

            request["service"] = "PetActivitylist";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, stateName, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            // $globalService.hideLoading();
        }

        $scope.enterToPetProfile = function () {
            if (Session.isUserLoggedIn()) {
                $state.go("AddAPet");
                $scope.closeModal();
            }
        }

        $scope.loadNextPbLoveList = function () {
            var currentList = $scope.dynamicList.BubPBPosts;
            var lastPostId = currentList[currentList.length - 1].postId;

            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: 'PostId',
                v: lastPostId
            });
            requestParams.push({
                k: 'customerId',
                v: $scope.mi_customerId
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "NextWallPBList";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        }

        $scope.loadNextWallPostList = function () {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var currentList = $scope.dynamicList.BubPosts;
            var lastPostId = currentList[currentList.length - 1].postId.toString();
            // var lastPostId = getBiggestValueFromArray(currentList, "postId").toString();

            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: 'CUSTOMERID',
                v: customerId
            });
            requestParams.push({
                k: 'PostId',
                v: lastPostId
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "NextWallList";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        };

        $scope.moreDataCanBeLoaded = false;

        $scope.showRegistrationScreen = function (name, email, picture) {
            $scope.data = {};
            if (name != null || email != null) {
                $scope.data.name = name;
                $scope.data.emailAdd = email;
                $scope.showModal('templates/userRegistration.html');
            } else {
                $scope.showModal('templates/userRegistration.html');
            }
        }

        $scope.onClickLike = function (postId, index) {
            var history = $ionicHistory.backView();//returns an object with data about the previous screen. console.log(history) to see the entire data.
            var previous_statename = history.stateName;//compare this
            if (index === "xyz") {
                index = parseInt(window.localStorage.getItem("POST_INDEX"));
            }
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);

            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.showModal2('templates/BubTalkLogin.html');
            }
            else {
                if (previous_statename == "BubHubWallPostOne" || previous_statename == "BubHubWallPostTwo" && $state.current.name == "BubHubWallPostDetail") {

                    if ($scope.dynamicList.BubPostDetail.isPostLikeByMe == '1' && $state.current.name == "BubHubWallPostDetail") {
                        // $scope.dynamicList.BubPosts[index].isPostLikeByMe = '0';
                        // $scope.dynamicList.BubPosts[index].totalPostLikes = parseInt($scope.dynamicList.BubPosts[index].totalPostLikes) - 1;
                        $scope.dynamicList.BubPostDetail.totalPostLikes = parseInt($scope.dynamicList.BubPostDetail.totalPostLikes) - 1;
                        $scope.dynamicList.BubPostDetail.isPostLikeByMe = '0';
                        // $scope.showPostDetails(index);
                    } else if ($scope.dynamicList.BubPostDetail.isPostLikeByMe == '0' && $state.current.name == "BubHubWallPostDetail") {
                        // $scope.dynamicList.BubPosts[index].isPostLikeByMe = '1';
                        // $scope.dynamicList.BubPosts[index].totalPostLikes = parseInt($scope.dynamicList.BubPosts[index].totalPostLikes) + 1;
                        $scope.dynamicList.BubPostDetail.totalPostLikes = parseInt($scope.dynamicList.BubPostDetail.totalPostLikes) + 1;
                        $scope.dynamicList.BubPostDetail.isPostLikeByMe = '1';
                        // $scope.showPostDetails(index);
                    }
                } else if (previous_statename == "Gallery" && $state.current.name == "BubHubWallPostDetail") {

                    if ($scope.dynamicList.BubPostDetail.isPostLikeByMe == '1' && $state.current.name == "BubHubWallPostDetail") {
                        $scope.dynamicList.BubPostDetail.totalPostLikes = parseInt($scope.dynamicList.BubPostDetail.totalPostLikes) - 1;
                        $scope.dynamicList.BubPostDetail.isPostLikeByMe = '0';
                    } else if ($scope.dynamicList.BubPostDetail.isPostLikeByMe == '0' && $state.current.name == "BubHubWallPostDetail") {
                        $scope.dynamicList.BubPostDetail.totalPostLikes = parseInt($scope.dynamicList.BubPostDetail.totalPostLikes) + 1;
                        $scope.dynamicList.BubPostDetail.isPostLikeByMe = '1';
                    }
                } else if ($scope.dynamicList.BubPosts[index].isPostLikeByMe == '1' && $state.current.name == "BubHubWallPostOne") {
                    $scope.dynamicList.BubPosts[index].isPostLikeByMe = '0';
                    $scope.dynamicList.BubPosts[index].totalPostLikes = parseInt($scope.dynamicList.BubPosts[index].totalPostLikes) - 1;

                } else if ($scope.dynamicList.BubPosts[index].isPostLikeByMe == '0' && $state.current.name == "BubHubWallPostOne") {
                    $scope.dynamicList.BubPosts[index].isPostLikeByMe = '1';
                    $scope.dynamicList.BubPosts[index].totalPostLikes = parseInt($scope.dynamicList.BubPosts[index].totalPostLikes) + 1;
                } else if ($scope.dynamicList.BubPBPosts[index].isPostLikeByMe == '1' && $state.current.name == "BubHubWallPostTwo") {
                    $scope.dynamicList.BubPBPosts[index].isPostLikeByMe = '0';
                    $scope.dynamicList.BubPBPosts[index].totalPostLikes = parseInt($scope.dynamicList.BubPBPosts[index].totalPostLikes) - 1;
                } else if ($scope.dynamicList.BubPBPosts[index].isPostLikeByMe == '0' && $state.current.name == "BubHubWallPostTwo") {
                    $scope.dynamicList.BubPBPosts[index].isPostLikeByMe = '1';
                    $scope.dynamicList.BubPBPosts[index].totalPostLikes = parseInt($scope.dynamicList.BubPBPosts[index].totalPostLikes) + 1;
                }
                // $scope.$apply();
                // var dynamicList = getDynamicListData();
                // setDynamicListData(dynamicList);                



                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];

                requestParams.push({
                    k: 'CustomerId',
                    v: customerId
                });
                requestParams.push({
                    k: 'PostId',
                    v: postId + ''
                });
                requestParams.push({
                    k: 'Date',
                    v: new Date()
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "AddLike";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);

            }
        }

        $scope.onClickResendOtp = function () {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var mobile = Global.getObjectFromLocalStorage(GlobalConstants.ls_mobile);

            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            //  Saving mobile no in local storage
            Global.setObjectInLocalStorage(GlobalConstants.ls_mobile, mobile);

            requestParams.push({
                k: 'customerId',
                v: customerId
            });
            requestParams.push({
                k: "mobile",
                v: mobile
            });
            requestParams.push({
                k: "type",
                v: '2'
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "MobileVerification";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        }

        $scope.onClickOtpSubmit = function () {

            var mobileNo = Global.getObjectFromLocalStorage(GlobalConstants.ls_mobile);
            var otp = $scope.data.otp1.toString() + $scope.data.otp2.toString() + $scope.data.otp3.toString() + $scope.data.otp4.toString() + $scope.data.otp5.toString() + $scope.data.otp6.toString();

            if (otp != undefined && otp != '' && otp.length.toString() == 6) {
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];

                requestParams.push({
                    k: 'otp',
                    v: otp
                });
                requestParams.push({
                    k: "mobile",
                    v: mobileNo
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "VerifyRegistrationOTP";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            } else {
                $globalService.showAlert("Petbubs", "Please enter valid OTP");
            }
        }

        $scope.videoToImage = function (succ) {
            var correctUrl1 = succ.slice(0, -4);
            correctUrl1 += '.png';
            return correctUrl1;
        }

        $scope.onClickRegistrationSubmit = function () {
            var name = $scope.data.name;
            var email = $scope.data.emailAdd;
            var password = $scope.data.password;
            var confirmPassword = $scope.data.confirmpassword;
            var mobileNo = $scope.data.mobile;
            var city = $scope.data.city;
            var termsAndCondition = $scope.data.termsAndCondition;

            if (name === undefined || name == '') {
                $globalService.showAlert("Petbubs", "Please enter name");
            } else if (email === undefined || email === '') {
                $globalService.showAlert("Petbubs", "Please enter email");
            } else if (mobileNo === undefined || mobileNo === '') {
                $globalService.showAlert("Petbubs", "Password enter mobile number");
            } else if (city === undefined || city === '' || city === null) {
                $globalService.showAlert("Petbubs", "Password select city");
            } else if (password === undefined || password == '') {
                $globalService.showAlert("Petbubs", "Please enter password");
            } else if (confirmPassword === undefined || confirmPassword == '') {
                $globalService.showAlert("Petbubs", "Please enter confirm password");
            } else if (password != confirmPassword) {
                $globalService.showAlert("Petbubs", "Password and Confirm Password doesnt match");
            } else if (!termsAndCondition) {
                $globalService.showAlert("Petbubs", "Please accepts T&C");
            } else {
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];

                //Saving user data
                Global.setObjectInLocalStorage(GlobalConstants.ls_name, name);
                Global.setObjectInLocalStorage(GlobalConstants.ls_email, email);
                Global.setObjectInLocalStorage(GlobalConstants.ls_city, city);
                Global.setObjectInLocalStorage(GlobalConstants.ls_mobile, $scope.data.mobile.toString());

                requestParams.push({
                    k: 'name',
                    v: $scope.data.name
                });
                requestParams.push({
                    k: "email",
                    v: $scope.data.emailAdd
                });
                requestParams.push({
                    k: "password",
                    v: $scope.data.confirmpassword
                });
                requestParams.push({
                    k: "mobile",
                    v: $scope.data.mobile.toString()
                });
                requestParams.push({
                    k: "city",
                    v: $scope.data.city
                });
                requestParams.push({
                    k: "pets",
                    v: 0
                });
                requestParams.push({
                    k: "tokenid",
                    "v": $scope.deviceTokenId
                });

                requestParams.push({
                    k: "platform",
                    "v": ionic.Platform.platform()
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "Registration";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            }
        }

        //Only for photo for edit post
        $scope.editselectMedia = function (objectKey) {
            $scope.dynamicList.fileName = [];
            // $scope.isMediaSelected = objectKey;
            $scope.dynamicList.SelectedMedia = [];
            $scope.dynamicList.editThreadMedia = [];

            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, // Camera.PictureSourceType.PHOTOLIBRARY
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                targetWidth: 600,
                targetHeight: 600,
            };

            $cordovaCamera.getPicture(options).then(function (sourcePath) {
                var fileName = getFileName(sourcePath.split("?")[0]);
                var uniqueFileName = guid() + fileName;
                $scope.dynamicList.fileName.push(uniqueFileName);
                $scope.dynamicList.SelectedMedia.push(sourcePath);
                $scope.dynamicList.editThreadMedia.push(sourcePath);
                // $scope.dynamicList.editThreadMediafromphone=true;
                $scope.mediaUrl1 = '';
                $scope.dynamicList.SelectedMediaType = 'image';
                $scope.isMediaSelectedFlag = true;
            }, function (err) {
                // An error occured. Show a message to the user
                console.log("Error: " + err);
                $scope.isMediaSelected = undefined;
            });
            //getImagesFromPicker($scope,3,objectKey);
        }
        //Only for photo
        $scope.selectMedia = function (objectKey) {
            $scope.dynamicList.fileName = [];
            $scope.isMediaSelected = objectKey;
            $scope.dynamicList.SelectedMedia = [];

            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, // Camera.PictureSourceType.PHOTOLIBRARY
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                targetWidth: 600,
                targetHeight: 600,
            };

            $cordovaCamera.getPicture(options).then(function (sourcePath) {
                var fileName = getFileName(sourcePath.split("?")[0]);
                var uniqueFileName = guid() + fileName;
                $scope.dynamicList.fileName.push(uniqueFileName);
                $scope.dynamicList.SelectedMedia.push(sourcePath);
                $scope.dynamicList.SelectedMediaType = 'image';
                $scope.isMediaSelectedFlag = true;
            }, function (err) {
                // An error occured. Show a message to the user
                console.log("Error: " + err);
                $scope.isMediaSelected = undefined;
            });
            //getImagesFromPicker($scope,3,objectKey);
        }

        $scope.onClickPostAttach = function () {
            //getImagesFromPicker($scope,1);
            $scope.dynamicList.fileName = [];
            $scope.dynamicList.SelectedMedia = [];

            var myPopup = $ionicPopup.show({
                title: 'Select media type',
                scope: $scope,
                buttons: [{
                    text: '<i class="icon ion-camera cameraIconBtn"></i>',
                    type: 'button-stable',
                    onTap: function (e) {
                        openGallery(0, 'image');
                    }
                },

                {
                    text: '<i class="icon ion-social-youtube-outline videoIconBtn"></i>',
                    type: 'button-stable',
                    onTap: function (e) {
                        openGallery(1, 'video');
                    }
                }
                ]
            });

            myPopup.then(function (res) {
                //console.log('Tapped!', res);
            });

            function openGallery(mediaType, type) {
                var options = {
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, // Camera.PictureSourceType.PHOTOLIBRARY
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    popoverOptions: CameraPopoverOptions,
                    targetWidth: 600,
                    targetHeight: 600,
                    mediaType: mediaType //ALLMEDIA//VIDEO
                };

                $cordovaCamera.getPicture(options).then(function (sourcePath) {
                    var fileName = getFileName(sourcePath.split("?")[0]);
                    var uniqueFileName = guid() + fileName;
                    if (isVideo(fileName)) {
                        VideoService.saveVideo('file://' + sourcePath).then(function (result) {
                            //console.log(result);
                            $scope.dynamicList.fileName = uniqueFileName; ////video  name 
                            $scope.dynamicList.SelectedMedia.push(prevImageSuccess(result)); ////video thumbel  path
                            // $scope.dynamicList.SelectedMedia.push(prevImageSuccess(sourcePath)); ////video thumbel  path sourcePath
                            $scope.dynamicList.videoThumbnailName = prevImageSuccess(uniqueFileName); ////video thumbel name
                            $scope.dynamicList.videoThumbnail = result; //// Video path 
                            // $scope.dynamicList.videoThumbnail = sourcePath; //// Video path sourcePath
                            $scope.dynamicList.SelectedMediaType = type;
                            $scope.dynamicList.isVideo = true;

                        });
                    } else {
                        $scope.dynamicList.fileName.push(uniqueFileName);
                        $scope.dynamicList.SelectedMedia.push(sourcePath);
                        $scope.dynamicList.SelectedMediaType = type;
                        $scope.dynamicList.isVideo = false;

                    }
                }, function (err) {
                    console.log(err);
                });

                function prevImageSuccess(succ) {
                    var correctUrl = succ.slice(0, -4);
                    correctUrl += '.png';
                    return correctUrl;
                }
            }

            function isVideo(filename) {
                var ext = getExtension(filename);
                switch (ext.toLowerCase()) {
                    case 'm4v':
                    case 'avi':
                    case 'mpg':
                    case 'mp4':
                        return true;
                }
                return false;
            }

            function getExtension(filename) {
                var parts = filename.split('.');
                return parts[parts.length - 1];
            }
        }

        var uploadImage = function (mediaList, mediaNameList, networkCall) {
            var callbackCounter = mediaList.length;
            var uri = GlobalConstants.defaultEndPoint + GlobalConstants.fileUpload;
            var success = function (response) {

                callbackCounter--;
                if (callbackCounter === 0) {
                    networkCall();
                }
            };

            var fail = function (error) {
                console.log(error);
            };

            var options = new FileUploadOptions();
            options.fileKey = "fileKey";
            options.mimeType = "image/jpg";
            options.chunkedMode = false;

            var params = {};
            params.directory = 'upload';

            var ft = new FileTransfer();
            ft.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    var percentage = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
                    $globalService.showLoading("Loading");
                } else { }
            };
            for (var i = 0; i < mediaList.length; i++) {
                params.fileName = mediaNameList[i].substr(mediaNameList[i].lastIndexOf('/') + 1);
                options.fileName = mediaNameList[i].substr(mediaNameList[i].lastIndexOf('/') + 1);
                options.params = params;
                ft.upload(mediaList[i], uri, success, fail, options);
            }

        }


        var uploadVideo = function (video, videoName, videoThumbnail, videoThumbnailName, networkCall, $globalService) {
            var uri = GlobalConstants.defaultEndPoint + GlobalConstants.fileUpload;
            var success = function (response) {

                var imageFileName = [];
                imageFileName.push(prevImageSuccess(videoName));


                function prevImageSuccess(succ) {
                    var correctUrl = succ.slice(0, -4);
                    correctUrl += '.png';
                    return correctUrl;
                }

                uploadImage(videoThumbnail, imageFileName, networkCall);
            };

            var fail = function (error) {

            };

            var options = new FileUploadOptions();
            options.fileKey = "fileKey";
            options.fileName = videoName;
            options.mimeType = "video/mp4";
            options.chunkedMode = true;

            var params = {};
            params.directory = 'upload';
            params.fileName = videoName;

            options.params = params;
            var ft = new FileTransfer();
            ft.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    var percentage = Math.floor((progressEvent.loaded / progressEvent.total) * 100);

                    $globalService.showLoading(percentage + "%");
                } else { }
            };
            ft.upload(video, uri, success, fail, options);
        }

        $scope.onClickUploadMedia = function () {
            var threadTitle = $scope.data.ThreadTitle;
            var threadMessage = $scope.data.ThreadMessage;
            var media = $scope.dynamicList.SelectedMedia;
            var mediaName = $scope.dynamicList.fileName;

            if (threadTitle === undefined || threadTitle === "") {
                //console.log("Thread title is empty");
                $globalService.showAlert('Invalid Input', 'Thread title cannot be empty.');
            } else if (threadMessage === undefined || threadTitle === "") {
                //console.log("Thread message is empty");
                $globalService.showAlert('Invalid Input', 'Thread message cannot be empty.');
            } else {
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                var categoryId = gGlobalData['BubCategory'];
                var topicId = gGlobalData['topic'];
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];

                requestParams.push({
                    k: 'topicId',
                    v: topicId
                });
                requestParams.push({
                    k: "customerId",
                    v: customerId
                });
                requestParams.push({
                    k: "title",
                    v: threadTitle
                });
                requestParams.push({
                    k: "body",
                    v: threadMessage
                });
                requestParams.push({
                    k: "categoryId",
                    v: categoryId
                });

                requestParams.push({
                    k: "date",
                    v: new Date()
                });

                if ($scope.dynamicList.SelectedMediaType == GlobalConstants.mediaTypeImage) {
                    for (var i = 0; i < mediaName.length; i++) {
                        var imageCounter = i + 1;
                        requestParams.push({
                            k: "image" + imageCounter,
                            v: mediaName[i]
                        });
                    }
                    if (mediaName.length === 1) {
                        requestParams.push({
                            k: "image2",
                            v: ""
                        });
                        requestParams.push({
                            k: "image3",
                            v: ""
                        });
                    } else if (mediaName.length === 2) {
                        requestParams.push({
                            k: "image3",
                            v: ""
                        });
                    }
                    requestParams.push({
                        k: "video1",
                        v: ""
                    });
                } else if ($scope.dynamicList.SelectedMediaType == GlobalConstants.mediaTypeVideo) {
                    requestParams.push({
                        k: "video1",
                        v: $scope.dynamicList.videoName
                    });
                    requestParams.push({
                        k: "image1",
                        v: ""
                    });
                    requestParams.push({
                        k: "image2",
                        v: ""
                    });
                    requestParams.push({
                        k: "image3",
                        v: ""
                    });
                } else {
                    requestParams.push({
                        k: "image1",
                        v: ""
                    });
                    requestParams.push({
                        k: "image2",
                        v: ""
                    });
                    requestParams.push({
                        k: "image3",
                        v: ""
                    });
                    requestParams.push({
                        k: "video1",
                        v: ""
                    });
                }

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = customerId;

                request["service"] = "NewThread";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                var networkCall = function () {
                    doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                };

                if ($scope.dynamicList.SelectedMediaType == 'image') {
                    uploadImage(media, $scope.dynamicList.fileName, networkCall);
                } else if ($scope.dynamicList.SelectedMediaType == 'video') {


                    uploadVideo($scope.dynamicList.video, $scope.dynamicList.videoName, media, $scope.dynamicList.fileName, networkCall, $globalService)
                } else {
                    networkCall();
                }
            }
        }


        $scope.onClickUploadPostMedia = function () {
            var postTitle = '';

            if ($scope.data.PostMessage === undefined) {
                var postMessage = '';
            } else {
                var postMessage = $scope.data.PostMessage;
            }

            var media = $scope.dynamicList.SelectedMedia;
            var mediaName = $scope.dynamicList.fileName;

            // if (postMessage === undefined || postMessage === "") {
            //     //console.log("Post message is empty");
            //     $globalService.showAlert('Petbubs', 'Photo\'s / Video\'s and Post message can\'t be empty.');
            // } else 
            if (media.length <= 0 || media === undefined) {
                $globalService.showAlert('Petbubs', 'Photo\'s / Video\'s can\'t be empty.');
            } else {
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];

                requestParams.push({
                    k: "CUSTOMERID",
                    v: customerId + ''
                });
                requestParams.push({
                    k: "TITLE",
                    v: postTitle
                });
                requestParams.push({
                    k: "BODY",
                    v: postMessage
                });
                requestParams.push({
                    k: "date",
                    v: new Date()
                });

                if ($scope.dynamicList.SelectedMediaType == GlobalConstants.mediaTypeImage) {
                    requestParams.push({
                        k: "IMAGE",
                        v: mediaName[0]
                    });
                    requestParams.push({
                        k: "VIDEO",
                        v: ''
                    });
                } else if ($scope.dynamicList.SelectedMediaType == GlobalConstants.mediaTypeVideo) {
                    requestParams.push({
                        k: "VIDEO",
                        v: $scope.dynamicList.fileName
                    });
                    requestParams.push({
                        k: "IMAGE",
                        v: ''
                    });
                }

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = customerId;

                request["service"] = "AddPost";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                var networkCall = function () {
                    $timeout(function () {
                        doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                    }, 1000);
                };
                if ($scope.dynamicList.SelectedMediaType == 'image') {
                    uploadImage(media, $scope.dynamicList.fileName, networkCall);
                } else if ($scope.dynamicList.SelectedMediaType == 'video') {
                    uploadVideo($scope.dynamicList.videoThumbnail, $scope.dynamicList.fileName, $scope.dynamicList.SelectedMedia, $scope.dynamicList.videoThumbnailName, networkCall, $globalService);
                    //uploadVideo($scope.dynamicList.video, $scope.dynamicList.videoName, media, $scope.dynamicList.fileName, networkCall, $globalService)
                }
            }
        }


        $scope.selectImage = function (target, storeAt) {

            if ($state.current.name === "Gallery") {
                $scope.noTargetServiceRequest("nextleaf", "ProfileMyInfo");
            }

            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, // Camera.PictureSourceType.PHOTOLIBRARY
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                targetWidth: 600,
                targetHeight: 600
            };

            $cordovaCamera.getPicture(options).then(function (sourcePath) {
                // $scope.data[storeAt] = sourcePath;
                var fileName = getFileName(sourcePath.split("?")[0]);
                var uniqueFileName = guid() + fileName;

                document.getElementById(storeAt).src = sourcePath;
                uploadImage([sourcePath], [uniqueFileName], function () {
                    $globalService.hideLoading();
                });
                $scope.data[storeAt] = sourcePath;
                gGlobalData["PROFILEPIC"] = uniqueFileName;

            }, function (err) {
                // An error occured. Show a message to the user
                console.log(err);
            });
        }

        $scope.onClickGetLikes = function (postId) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);

            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.showModal2('templates/BubTalkLogin.html');
            } else {
                var dynamicList = getDynamicListData();
                dynamicList['CurrentPostId'] = postId;
                setDynamicListData(dynamicList);
                var serviceRequest = {};
                var request = {};
                var requestParams = [];
                var serviceInfo = [];
                requestParams.push({
                    k: "PostId",
                    v: postId + ''
                });
                requestParams.push({
                    k: "customerId",
                    v: customerId + ''
                });
                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "GetLikes";
                serviceRequest["req"] = request;

                serviceInfo["progressInfo"] = "Please wait...";
                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            }
        }

        $scope.onClickUnFollowUser = function (userId) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];
            if (customerId < userId) {
                requestParams.push({
                    k: "CUSTOMERID_ONE",
                    v: customerId
                });
                requestParams.push({
                    k: "CUSTOMERID_TWO",
                    v: userId
                });
            } else {
                requestParams.push({
                    k: "CUSTOMERID_ONE",
                    v: userId
                });
                requestParams.push({
                    k: "CUSTOMERID_TWO",
                    v: customerId
                });
            }
            requestParams.push({
                k: "Action_status",
                v: customerId
            });
            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "Unfollow";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";

            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        }

        $scope.onReplyButtonclick = function (item) {
            if (gGlobalData["EditedPost"] != undefined) {
                document.getElementById("replyPostButtonMain_" + gGlobalData["EditedPost"].index).setAttribute("style", "display:block");
                document.getElementById("EditPostButtonMain_" + gGlobalData["EditedPost"].index).setAttribute("style", "display:none");
            }
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);

            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.showModal2('templates/BubTalkLogin.html');
            } else {
                $scope.isReplyVisible = item;
                $scope.data.replyPost = undefined;
                gGlobalData["EditedPost"] = undefined;

            }
        }

        $scope.getThreadImageDetail = function (name, image, body) {
            $scope.authorName = name;
            $scope.threadImage = $scope.mediaUrl + image;
            $scope.threadBody = body;
            // if ($state.current.name === "Gallery") {
            $scope.showModal('templates/ThreadsImagesUser.html');
            // } else {
            // $scope.showModal('templates/ThreadsImages.html');
            // }
        };

        $scope.EditThreadPost = function (item, index) {
            if (gGlobalData["EditedPost"]) {
                document.getElementById("replyPostButtonMain_" + gGlobalData["EditedPost"].index).setAttribute("style", "display:block");
            }
            /// edit post store
            gGlobalData["EditedPost"] = item;
            gGlobalData["EditedPost"].index = index;
            // $scope.data.editPost = undefined;
            $scope.isReplyVisible = undefined;
            $scope.showModal('templates/EditThreads.html');

        }

        $scope.onDeleteThreadPostclick = function () {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);
            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.showModal2('templates/BubTalkLogin.html');
            } else {
                $scope.isEditThreadPost = gGlobalData["EditedPost"];

                $scope.modal.hide();
                $scope.modal.remove();
                // console.log($scope.isEditThreadPost.postid+"From Delete");
                // var dynamicList = getDynamicListData();

                // // remove post from array     
                // for(i=0 ; i < dynamicList.listOfThreadDetailObj.length ; i++){
                //     if($scope.isEditThreadPost.postid===dynamicList.listOfThreadDetailObj[i].postid){
                //         dynamicList.listOfThreadDetailObj.splice(i,1);
                //         break;
                //     }    
                // }

                // setDynamicListData(dynamicList);

                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                var request = {};
                var requestParams = [];
                var serviceInfo = [];
                var serviceRequest = {};

                requestParams.push({
                    k: "customerId",
                    v: customerId
                });

                requestParams.push({
                    k: "postid",
                    v: $scope.isEditThreadPost.postid
                });

                request["params"] = requestParams;
                request["deviceId"] = getDeviceId();
                request["initiatorId"] = -1;

                request["service"] = "ThreadPostDelete";
                serviceRequest["req"] = request;
                serviceInfo["progressInfo"] = "Please wait...";
                // console.log(JSON.stringify(request));

                PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                    $globalService.showAlert("Petbubs", "Thread Post Deleted successfully");
                    // $state.go("BubTalkThreadDetail",{},{reload:true});
                    document.getElementById("threadCommentCover_12_" + $scope.isEditThreadPost.index).setAttribute("style", "display:none");
                });
            }




        }

        $scope.onEditThreadPostclick = function () {

            // Niks EditPost 
            $scope.isEditThreadPost = gGlobalData["EditedPost"];
            $scope.data.editReplyPost = gGlobalData["EditedPost"].body;
            $scope.data.editPost = gGlobalData["EditedPost"].body;
            if ($scope.mediaUrl1 === '') {
                $scope.mediaUrl1 = GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath;
            }
            $scope.dynamicList.editThreadMedia = [];
            $scope.dynamicList.editThreadMediafromphone = true;

            if (gGlobalData["EditedPost"].image1) {
                $scope.isEditThreadPostHasMedia = true;
                $scope.dynamicList.editThreadMedia[0] = gGlobalData["EditedPost"].image1;
            }

            // if (gGlobalData["EditedPost"].image2) {
            //     $scope.dynamicList.editThreadMedia[1] = gGlobalData["EditedPost"].image2 !='null' ? gGlobalData["EditedPost"].image2 : '';
            // }

            // if (gGlobalData["EditedPost"].image3) {
            //     $scope.dynamicList.editThreadMedia[2] = gGlobalData["EditedPost"].image3 !='null' ? gGlobalData["EditedPost"].image3 : '';
            // }

            $scope.modal.hide();
            $scope.modal.remove();
            document.getElementById("replyPostButtonMain_" + gGlobalData["EditedPost"].index).setAttribute("style", "display:none");
            if (document.getElementById("EditPostButtonMain_" + gGlobalData["EditedPost"].index) != null) {
                document.getElementById("EditPostButtonMain_" + gGlobalData["EditedPost"].index).setAttribute("style", "display:block");
            }
            $scope.data.editPost = gGlobalData["EditedPost"].body != "null" ? gGlobalData["EditedPost"].body : "";
            // document.getElementById("postGalleryImage1_"+gGlobalData["EditedPost"].index).setAttribute("style","display:block");
            // document.getElementById("postGalleryImage1_"+gGlobalData["EditedPost"].index).setAttribute("ng-hide","false");

        }

        $scope.onEditThreadReplyComment = function () {

            if ($scope.data.editPost || ($scope.dynamicList.fileName && $scope.dynamicList.fileName[0])) {
                function CallbackSuccess1() {
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];
                    requestParams.push({
                        k: "customerId",
                        v: customerId
                    });
                    requestParams.push({
                        k: "postid",
                        v: $scope.isEditThreadPost.postid
                    });


                    requestParams.push({
                        k: "body",
                        v: $scope.data.editPost
                    });



                    if ($scope.dynamicList.SelectedMedia && $scope.dynamicList.SelectedMedia.length > 0) {
                        if ($scope.dynamicList.fileName[0]) {
                            requestParams.push({
                                k: "image1",
                                v: $scope.dynamicList.fileName[0]
                            });
                        }

                        if ($scope.dynamicList.fileName[1]) {
                            requestParams.push({
                                k: "image2",
                                v: $scope.dynamicList.fileName[1]
                            });
                        }

                        if ($scope.dynamicList.fileName[2]) {
                            requestParams.push({
                                k: "image3",
                                v: $scope.dynamicList.fileName[2]
                            });
                        }
                    } else {
                        requestParams.push({
                            k: "image1",
                            v: $scope.dynamicList.editThreadMedia[0] == undefined ? 'null' : $scope.dynamicList.editThreadMedia[0]
                        });
                        requestParams.push({
                            k: "image2",
                            v: $scope.dynamicList.editThreadMedia[1] == undefined ? 'null' : $scope.dynamicList.editThreadMedia[1]
                        });
                        requestParams.push({
                            k: "image3",
                            v: $scope.dynamicList.editThreadMedia[2] == undefined ? 'null' : $scope.dynamicList.editThreadMedia[2]
                        });

                    }
                    document.getElementById("replyPostButtonMain_" + gGlobalData["EditedPost"].index).setAttribute("style", "display:block");

                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = customerId;

                    request["service"] = "ThreadPostEdit";
                    serviceRequest["req"] = request;
                    // console.log(JSON.stringify(request));
                    serviceInfo["progressInfo"] = "Please wait...";
                    $scope.dynamicList.SelectedMedia = [];
                    $scope.dynamicList.fileName = [];
                    $timeout(function () {
                        doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                    }, 1000);

                    $scope.isReplyVisible = false;
                    $scope.isMediaSelected = undefined;
                    gGlobalData["EditedPost"] = undefined;
                }


                if ($scope.dynamicList.SelectedMedia === undefined || $scope.dynamicList.SelectedMedia.length === 0) {
                    CallbackSuccess1();
                } else {
                    uploadImage($scope.dynamicList.SelectedMedia, $scope.dynamicList.fileName, CallbackSuccess1);
                }
            }
        }

        $scope.selectEditMedia = function (item) {
            getEditImagesFromPicker($scope, 3, objectKey);
        }

        $scope.onWriteReplyButtonclick = function () {
            if ($scope.data.replyPost || ($scope.dynamicList.fileName && $scope.dynamicList.fileName[0])) {

                function CallbackSuccess() {
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];
                    requestParams.push({
                        k: "customerId",
                        v: customerId
                    });
                    requestParams.push({
                        k: "categoryId",
                        v: gGlobalData["BubCategory"]
                    });
                    requestParams.push({
                        k: "threadId",
                        v: gGlobalData["threadId"]
                    });
                    requestParams.push({
                        k: "topicId",
                        v: gGlobalData["topic"]
                    });

                    if ($scope.data.replyPost) {
                        requestParams.push({
                            k: "body",
                            v: $scope.data.replyPost
                        });
                    }

                    requestParams.push({
                        k: "date",
                        v: new Date()
                    });
                    requestParams.push({
                        k: "isReply",
                        v: "0"
                    });
                    requestParams.push({
                        k: "title",
                        v: gGlobalData["title"]
                    });

                    if ($scope.dynamicList.SelectedMedia && $scope.dynamicList.SelectedMedia.length > 0) {
                        if ($scope.dynamicList.fileName[0]) {
                            requestParams.push({
                                k: "image1",
                                v: $scope.dynamicList.fileName[0]
                            });
                        }

                        if ($scope.dynamicList.fileName[1]) {
                            requestParams.push({
                                k: "image2",
                                v: $scope.dynamicList.fileName[1]
                            });
                        }

                        if ($scope.dynamicList.fileName[2]) {
                            requestParams.push({
                                k: "image3",
                                v: $scope.dynamicList.fileName[2]
                            });
                        }
                    }

                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = customerId;

                    request["service"] = "ThreadPostReply";
                    serviceRequest["req"] = request;

                    serviceInfo["progressInfo"] = "Please wait...";
                    $scope.dynamicList.SelectedMedia = [];
                    $scope.dynamicList.fileName = [];
                    doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                    $scope.isReplyVisible = false;
                    $scope.isMediaSelected = undefined;
                }
                if ($scope.dynamicList.SelectedMedia === undefined || $scope.dynamicList.SelectedMedia.length === 0) {
                    CallbackSuccess();
                } else {
                    uploadImage($scope.dynamicList.SelectedMedia, $scope.dynamicList.fileName, CallbackSuccess);
                }
            }
        }

        $scope.onWriteReplyCommentButtonclick = function (item) {

            if ($scope.data.replyPost || ($scope.dynamicList.fileName && $scope.dynamicList.fileName[0])) {
                function CallbackSuccess1() {
                    var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];
                    requestParams.push({
                        k: "customerId",
                        v: customerId
                    });
                    requestParams.push({
                        k: "threadId",
                        v: gGlobalData["threadId"]
                    });

                    if ($scope.data.replyPost) {
                        requestParams.push({
                            k: "body",
                            v: $scope.data.replyPost
                        });
                    }
                    requestParams.push({
                        k: "isReply",
                        v: "1"
                    });
                    requestParams.push({
                        k: "title",
                        v: gGlobalData["title"]
                    });
                    requestParams.push({
                        k: "date",
                        v: new Date()
                    });
                    requestParams.push({
                        k: "postId",
                        v: item.postid
                    });

                    if ($scope.dynamicList.SelectedMedia && $scope.dynamicList.SelectedMedia.length > 0) {
                        if ($scope.dynamicList.fileName[0]) {
                            requestParams.push({
                                k: "image1",
                                v: $scope.dynamicList.fileName[0]
                            });
                        }

                        if ($scope.dynamicList.fileName[1]) {
                            requestParams.push({
                                k: "image2",
                                v: $scope.dynamicList.fileName[1]
                            });
                        }

                        if ($scope.dynamicList.fileName[2]) {
                            requestParams.push({
                                k: "image3",
                                v: $scope.dynamicList.fileName[2]
                            });
                        }
                    }

                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = customerId;

                    request["service"] = "ThreadCommentReply";
                    serviceRequest["req"] = request;

                    serviceInfo["progressInfo"] = "Please wait...";
                    $scope.dynamicList.SelectedMedia = [];
                    $scope.dynamicList.fileName = [];
                    $timeout(function () {
                        doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                    }, 1000);

                    $scope.isReplyVisible = false;
                    $scope.isMediaSelected = undefined;
                }


                if ($scope.dynamicList.SelectedMedia === undefined || $scope.dynamicList.SelectedMedia.length === 0) {
                    CallbackSuccess1();
                } else {
                    uploadImage($scope.dynamicList.SelectedMedia, $scope.dynamicList.fileName, CallbackSuccess1);
                }
            }
        };

        $scope.onClickLogin = function () {
            var emailId = $scope.data.emailId;
            var password = $scope.data.password;

            // validation
            if (!emailId || emailId === "") {
                $globalService.showAlert("Petbubs", "Please enter Email Id");
                return false;
            }
            if (!password || password === "") {
                $globalService.showAlert("Petbubs", "Please enter password");
                return false;
            }

            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: "emailid",
                "v": emailId
            });

            requestParams.push({
                k: "password",
                "v": password
            });

            requestParams.push({
                k: "tokenid",
                "v": $scope.deviceTokenId
            });

            requestParams.push({
                k: "platform",
                "v": ionic.Platform.platform()
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "Login";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";
            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        }

        $scope.onButtonClick = function (componentSubType, componentTargetLeaf, key) {
            appData = getAppData();
            var proceed = true;
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceInfo = null;


            if (componentTargetLeaf === "SocialMedia" || componentTargetLeaf === "CreateNewPost" || componentTargetLeaf === "PetBookmark" || componentTargetLeaf === "AddRemoveBookmark" || componentTargetLeaf === "StartNewThread" || componentTargetLeaf === "ProfileUser" || componentTargetLeaf === "AddEventLeaf" || componentTargetLeaf === "BubHubWallFindFriends" || componentTargetLeaf === "Gallery") {
                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);
                var dynamicList = getDynamicListData();
                dynamicList['CurrentPage'] = componentTargetLeaf;

                setDynamicListData(dynamicList);

                if (customerId.length === 0 || isLoggedIn.length === 0) {
                    $scope.showModal2('templates/BubTalkLogin.html');
                    proceed = false;
                }
            }
            if (componentTargetLeaf === 'BubTalkLeaf') {
                var hasSeenBubTalkLeaf = window.localStorage.getItem(GlobalConstants.ls_hasSeenBubTalkLeaf);
                // var hasSeenBubTalkLeaf = Global.getObjectFromLocalStorage(GlobalConstants.ls_hasSeenBubTalkLeaf);

                // var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                if (hasSeenBubTalkLeaf === 'true') {
                    //$state.go('BubTalkCategoryList');
                    componentTargetLeaf = "BubTalkCategoryList";
                }
            }

            if (componentTargetLeaf === "BubTalkCategoryList") {
                window.localStorage.setItem(GlobalConstants.ls_hasSeenBubTalkLeaf, true);
            }

            if (componentTargetLeaf === 'editPetSuccess') {
                // $scope.data.addpet_pettype = $scope.data.petid;
                if ($scope.data.addpet_pettype != null) {
                    $scope.data.addpet_pettype = $scope.data.addpet_pettype[0].pettypeid;
                    $scope.data.petactivity = JSON.stringify(gGlobalData["VetActivity"]);
                }
            }

            if ($state.current.name === "AddAPet") {
                var dynamicList = getDynamicListData();
                dynamicList['AddAPetData'] = $scope.data;
                setDynamicListData(dynamicList);
            }

            if (componentTargetLeaf === 'VetActivity') {
                if (componentSubType === "Deworming") {
                    gGlobalData["title"] = "Deworming";
                }
                if (componentSubType === "Rabies") {
                    gGlobalData["title"] = "Rabies Vaccine";
                }
                if (componentSubType === "DHPPi") {
                    gGlobalData["title"] = "DHPPi & Leptosporosis";
                }
            }

            if (proceed) {
                for (var template in appData.views) {
                    if (appData.views[template].title === componentTargetLeaf) {
                        serviceInfo = appData.views[template].networkService;
                        setControllerData(appData.views[template].data);
                    }

                    //Reset update data
                    if (appData.views[template].title === $scope.data["leafName"]) {
                        appData.views[template].data = $scope.data;
                        setAppData(appData);
                        var dynamicList = getDynamicListData();
                        for (var prop in $scope.data) {
                            if ($scope.data[prop] && prop !== "leafName" && prop !== "title" && prop !== "headerTitle" && prop !== "headerbarClass") {
                                dynamicList[prop] = $scope.data[prop].toString();
                            }
                        }
                        setDynamicListData(dynamicList);
                    }
                }

                if (serviceInfo && serviceInfo["service"]) {
                    var serviceRequest = createServiceRequest(serviceInfo, componentSubType, key, componentTargetLeaf, Global, GlobalConstants, $state);
                    doNetworkServiceCall(serviceRequest, serviceInfo, componentTargetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, key, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                } else if (componentTargetLeaf && componentTargetLeaf !== 'null') {
                    $state.go(componentTargetLeaf);
                }

            }


        }

        $scope.onClickUnfollow = function (name, customerId, index) {
            $scope.UnfollowerName = name;
            $scope.UnfollowerId = customerId;
            gGlobalData["UnfollowerIndex"] = index;
            $scope.showModal('templates/unfollow.html');
        }

        $scope.onClickFollowUser = function (userId, index, activeTabIndex) {
            if ($state.current.name === "SocialMedia") {
                $scope.dynamicList.SocialMediaList.relationStatus = "1";
            }

            gGlobalData["followerIndex"] = index;

            var dynamicList = getDynamicListData();
            dynamicList['CurrentAuthorId'] = userId;
            setDynamicListData(dynamicList);
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: "CUSTOMER_ONE",
                v: customerId
            });
            requestParams.push({
                k: "CUSTOMER_TWO",
                v: userId
            });

            requestParams.push({
                k: "Action_status",
                v: customerId
            });
            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "AddFriend";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";

            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
            //$state.go($state.current, {}, {reload: true});

            $scope.buttonClicked(activeTabIndex);
        }

        $scope.onClickUnFollowUser = function () {
            if ($state.current.name === "SocialMedia") {
                $scope.dynamicList.SocialMediaList.relationStatus = "0";
            }
            $scope.modal.remove();
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];
            requestParams.push({
                k: "CUSTOMERID_ONE",
                v: customerId.toString()
            });
            requestParams.push({
                k: "CUSTOMERID_TWO",
                v: $scope.UnfollowerId.toString()
            });
            requestParams.push({
                k: "Action_status",
                v: customerId.toString()
            });
            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "Unfollow";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";

            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        }

        $scope.removeMediaImage = function (object, index) {
            if ($scope.dynamicList.editThreadMedia != undefined) {
                $scope.dynamicList.editThreadMedia.splice(index, 1);
            }

            if ($scope.dynamicList.SelectedMedia != undefined) {
                $scope.dynamicList.SelectedMedia.splice(index, 1);

                $scope.dynamicList.fileName = "";
                $state.go($state.current.name, {}, { reload: true });
                if ($state.current.name === "BubTalkThreadDetail" && index == 0) {
                    $scope.isMediaSelected = undefined;
                }
            }

        }

        $scope.showPostDetails = function (index, postIdFromAddComment) {
            window.localStorage.setItem("POST_INDEX", index);
            var dynamicList = getDynamicListData();
            var history = $ionicHistory.backView();//returns an object with data about the previous screen. console.log(history) to see the entire data.
            var previous_statename = history.stateName;//compare this
            if ($state.current.name === "BubHubWallPostOne") {
                dynamicList['BubPostDetail'] = $scope.dynamicList.BubPosts[index];
            } else if ($state.current.name === "Gallery" || $state.current.name === "SocialMedia") {
                if (postIdFromAddComment) {
                    dynamicList['BubPostDetail'] = $scope.dynamicList.SocialMediaList.GalleryVideo[index];
                } else {
                    dynamicList['BubPostDetail'] = $scope.dynamicList.SocialMediaList.GalleryImage[index];
                }
                postIdFromAddComment = undefined;
            } else if ($state.current.name === "BubHubWallPostDetail" && previous_statename === "BubHubWallPostOne") {
                dynamicList['BubPostDetail'] = $scope.dynamicList.BubPosts[index];
            }
            else {
                dynamicList['BubPostDetail'] = $scope.dynamicList.BubPBPosts[index];
            }
            setDynamicListData(dynamicList);
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: 'PostId',
                v: postIdFromAddComment === undefined ? dynamicList['BubPostDetail'].postId : postIdFromAddComment
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "GetComment";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";

            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
        };

        $scope.buttonClicked = function (index) {
            $scope.data.find_friends_text = "";
            // if (index === 0) {
            //     $scope.bbHubTabActive = index;
            //     var serviceRequest = {};
            //     var request = {};
            //     var requestParams = [];
            //     var serviceInfo = [];
            //     requestParams.push({
            //         k: "customerId",
            //         v: $scope.mi_customerId
            //     });
            //     request["params"] = requestParams;
            //     request["deviceId"] = getDeviceId();
            //     request["initiatorId"] = "0";

            //     request["service"] = "FriendList";
            //     serviceRequest["req"] = request;
            //     serviceInfo["progressInfo"] = "Please wait...";
            //     $globalService.showLoading("Refreshing...");
            //     $netCallService.httpPOST({
            //         url: GlobalConstants.defaultEndPoint + GlobalConstants.webServiceName,
            //         data: serviceRequest,
            //         success: function (data, status, headers, config) {
            //             $timeout(function () {
            //                 $globalService.hideLoading();
            //                 var serverResponse = JSON.parse(JSON.stringify(data));
            //                 var service = serverResponse.serv;
            //                 var output = serverResponse.op;
            //                 if (output[0].v == "1") {
            //                     var dynamicList = getDynamicListData();
            //                     dynamicList["friendList"] = output[1].v;
            //                     $scope.dynamicList["friendList"] = output[1].v;
            //                     setDynamicListData(dynamicList);
            //                 } else if (output[0].k === "Failure") {
            //                     var dynamicList = getDynamicListData();
            //                     dynamicList["friendList"] = [];
            //                     $scope.dynamicList["friendList"] = [];
            //                     setDynamicListData(dynamicList);
            //                 }
            //                 $('#tab-dw').hide(200);
            //                 $('#tab-cw').show(400);
            //             }, 1000);

            //         },
            //         error: function (data, status, headers, config) {

            //         }
            //     })

            // } else {
            //     $('#tab-cw').hide(200);
            //     $('#tab-dw').show(400);
            //     $scope.bbHubTabActive = index;

            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];
            requestParams.push({
                k: "customerId",
                v: $scope.mi_customerId
            });
            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = "0";

            request["service"] = "AllBubs";
            serviceRequest["req"] = request;
            serviceInfo["progressInfo"] = "Please wait...";
            $globalService.showLoading("Refreshing...");
            $netCallService.httpPOST({
                url: GlobalConstants.defaultEndPoint + GlobalConstants.webServiceName,
                data: serviceRequest,
                success: function (data, status, headers, config) {
                    $globalService.hideLoading();
                    var serverResponse = JSON.parse(JSON.stringify(data));
                    var service = serverResponse.serv;
                    var output = serverResponse.op;
                    if (output[0].v == "1") {
                        var dynamicList = getDynamicListData();
                        dynamicList["BubList"] = output[1].v;
                        $scope.dynamicList["BubList"] = output[1].v;
                        setDynamicListData(dynamicList);
                        if ($state.current.name === "Gallery") {
                            $state.go('BubHubWallFindFriends');
                        }
                    }
                },
                error: function (data, status, headers, config) {

                }
            })
            // }
        };

        // function strEncodeUTF16(str) {
        //     var buf = new ArrayBuffer(str.length * 2);
        //     var bufView = new Uint16Array(buf);
        //     for (var i = 0, strLen = str.length; i < strLen; i++) {
        //         bufView[i] = str.charCodeAt(i);
        //     }
        //     return bufView;
        // }

        function stringToUint(str) {
            var bytes = []; // char codes
            for (var i = 0; i < str.length; ++i) {
                var code = str.charCodeAt(i);
                bytes = bytes.concat([code]);
            }
            return bytes.join(', ');
        }

        $scope.onClickStickyPost = function (post) {
            var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);

            var isLoggedIn = Global.getObjectFromLocalStorage(GlobalConstants.ls_isLoggedIn);

            if (customerId.length === 0 || isLoggedIn.length === 0) {
                $scope.showModal2('templates/BubTalkLogin.html');
            } else {
                var comment = $scope.data.commentInput;
                // console.log('Output: ' + stringToUint(comment));
                if (comment && comment.length > 0) {
                    var serviceRequest = {};
                    var request = {};
                    var requestParams = [];
                    var serviceInfo = [];
                    requestParams.push({
                        k: 'PostId',
                        v: post.postId
                    });
                    requestParams.push({
                        k: 'BODY',
                        v: stringToUint(comment)
                    });
                    requestParams.push({
                        k: 'CUSTOMERID',
                        v: customerId + ''
                    });
                    requestParams.push({
                        k: 'DATE',
                        v: new Date()
                    });
                    request["params"] = requestParams;
                    request["deviceId"] = getDeviceId();
                    request["initiatorId"] = -1;

                    request["service"] = "AddComment";
                    serviceRequest["req"] = request;
                    serviceInfo["progressInfo"] = "Please wait...";
                    $timeout(function () {
                        doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                    }, 1000);
                }
            }
        };

        $scope.showToast = function (msg) {
            try {
                $cordovaToast.showLongBottom(msg);
            } catch (e) {
                alert(msg);
            }
        }

        var logoutConfirm = function () {
            var serviceRequest = {};
            var request = {};
            var requestParams = [];
            var serviceInfo = [];

            requestParams.push({
                k: "customerid",
                "v": Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId)
            });

            request["params"] = requestParams;
            request["deviceId"] = getDeviceId();
            request["initiatorId"] = -1;

            request["service"] = "Logout";
            serviceRequest["req"] = request;

            serviceInfo["progressInfo"] = "Please wait...";

            PetBubHttpService.calendarService(serviceRequest).then(function (result) {

                $scope.data.emailId = '';
                $scope.data.password = '';

                resetMobileUserId();
                resetAppId();
                resetPreviousStateName();
                resetSelectedAppData();
                resetApplicationListData();
                resetControllerData();
                resetDynamicListData();
                addToDynamicList(); //reintialise data
                window.localStorage.clear();
                gGlobalData["noBookmark"] = false;
                $globalService.showAlert("Petbubs", "You've been logged out successfully");
                $state.go($state.current, {}, { reload: true });
            });
        };

        $scope.onLogoutClick = function () {
            $scope.falseToggle();
            $globalService.showConfirmation("Petbubs", "Are you sure you want to logout ?", logoutConfirm);
        };

        $scope.onHomeClick = function () {
            var homeLeafName = getHomeLeafName(); //localStorage.getItem('firstLeafName');
            $state.go(homeLeafName);
        };


        $scope.onClickCollapsiblePanel = function (varID) {
            var panelControl = document.getElementById(varID);
            var ctrlStyle = panelControl.style.display;
            if (ctrlStyle == "none") {
                panelControl.style.display = "block";
            } else {
                panelControl.style.display = "none";
            }
        }

        // Get getSeconds
        var getSeconds = function (date) {
            var seconds = new Date(date).getTime() / 1000;
            return seconds;
        };
        // End of getSeconds

        var captureComboObj = function (dynamicListKeyName, chkkeyName, scopeDataVar) {
            var dynamicList = getDynamicListData();
            var cboData = dynamicList[dynamicListKeyName]
            var leafData = dynamicList[$state.current.name] == null ? {} : dynamicList[$state.current.name];
            for (var i = 0; i < cboData.length; i++) {
                for (var keyName in cboData[i]) {
                    if (keyName == chkkeyName) {
                        if (cboData[i][keyName] === scopeDataVar) {
                            leafData[dynamicListKeyName + "Object"] = cboData[i];
                        }
                    }
                }
            }
            dynamicList[$state.current.name] = leafData;
            setDynamicListData(dynamicList);
        }

    }

    for (var template in appData.views) {
        controllerModule.controller(appData.views[template].controller, ["$scope", '$http', '$q', 'PetBubHttpService', '$cordovaOauth', '$ionicNativeTransitions', '$state', '$stateParams', '$ionicLoading', 'globalService', 'netCallService', '$ionicNavBarDelegate', '$ionicHistory', '$interval', '$ionicModal', '$timeout', '$cordovaCamera', '$cordovaFile', '$ionicPopup', '$cordovaLocalNotification', '$ionicSlideBoxDelegate', '$cordovaToast', '$cordovaSocialSharing', '$sce', '$filter', '$cordovaFileTransfer', '$cordovaSQLite', 'VideoService', 'Session', '$ionicScrollDelegate', 'Global', 'GlobalConstants', template, function ($scope, $http, $q, PetBubHttpService, $cordovaOauth, $ionicNativeTransitions, $state, $stateParams, $ionicLoading, $globalService, netCallService, $ionicNavBarDelegate, $ionicHistory, $interval, $ionicModal, $timeout, $cordovaCamera, $cordovaFile, $ionicPopup, $cordovaLocalNotification, $ionicSlideBoxDelegate, $cordovaToast, $cordovaSocialSharing, $sce, $filter, $cordovaFileTransfer, $cordovaSQLite, VideoService, Session, $ionicScrollDelegate, Global, GlobalConstants, data) {
            $scope.data = data;
            $scope.data.eventSelectedDate = '';
            $scope.dynamicList = getDynamicListData();

            var $injector = angular.injector();
            $injector.invoke(AbstractController, this, {
                $scope: $scope,
                $http: $http,
                $q: $q,
                PetBubHttpService: PetBubHttpService,
                $cordovaOauth: $cordovaOauth,
                $ionicNativeTransitions: $ionicNativeTransitions,
                $state: $state,
                $stateParams: $stateParams,
                $ionicLoading: $ionicLoading,
                $globalService: $globalService,
                $netCallService: netCallService,
                $ionicNavBarDelegate: $ionicNavBarDelegate,
                $ionicHistory: $ionicHistory,
                $interval: $interval,
                $ionicModal: $ionicModal,
                $timeout: $timeout,
                $cordovaCamera: $cordovaCamera,
                $cordovaFile: $cordovaFile,
                $ionicPopup: $ionicPopup,
                $cordovaLocalNotification: $cordovaLocalNotification,
                $ionicSlideBoxDelegate: $ionicSlideBoxDelegate,
                $cordovaToast: $cordovaToast,
                $cordovaSocialSharing: $cordovaSocialSharing,
                $sce: $sce,
                $filter: $filter,
                $cordovaFileTransfer: $cordovaFileTransfer,
                $cordovaSQLite: $cordovaSQLite,
                VideoService: VideoService,
                Session: Session,
                $ionicScrollDelegate: $ionicScrollDelegate,
                Global: Global,
                GlobalConstants: GlobalConstants,
            });
        }]);
    }

    controllerModule.controller("AbstractCntrl", ["$scope", '$http', '$q', 'PetBubHttpService', '$cordovaOauth', '$ionicNativeTransitions', '$state', '$stateParams', '$ionicLoading', 'globalService', 'netCallService', '$ionicNavBarDelegate', '$ionicHistory', '$interval', '$ionicModal', '$timeout', '$cordovaCamera', '$cordovaFile', '$ionicPopup', '$cordovaLocalNotification', '$ionicSlideBoxDelegate', '$cordovaToast', '$cordovaSocialSharing', '$sce', '$filter', '$cordovaFileTransfer', '$cordovaSQLite', 'VideoService', 'Session', '$ionicScrollDelegate', 'Global', 'GlobalConstants', template, function ($scope, $http, $q, PetBubHttpService, $state, $stateParams, $ionicNativeTransitions, $ionicLoading, $globalService, netCallService, $ionicNavBarDelegate, $ionicHistory, $interval, $ionicModal, $timeout, $cordovaCamera, $cordovaFile, $ionicPopup, $cordovaLocalNotification, $ionicSlideBoxDelegate, $cordovaToast, $cordovaSocialSharing, $sce, $filter, $cordovaFileTransfer, $cordovaSQLite, VideoService, Session, $ionicScrollDelegate, Global, GlobalConstants, data) {
        $scope.data = {};
        $scope.dynamicList = getDynamicListData();

        var $injector = angular.injector();
        $injector.invoke(AbstractController, this, {
            $scope: $scope,
            $http: $http,
            $q: $q,
            PetBubHttpService: PetBubHttpService,
            $cordovaOauth: $cordovaOauth,
            $ionicNativeTransitions: $ionicNativeTransitions,
            $state: $state,
            $stateParams: $stateParams,
            $ionicLoading: $ionicLoading,
            $globalService: $globalService,
            $netCallService: netCallService,
            $ionicNavBarDelegate: $ionicNavBarDelegate,
            $ionicHistory: $ionicHistory,
            $interval: $interval,
            $ionicModal: $ionicModal,
            $timeout: $timeout,
            $cordovaCamera: $cordovaCamera,
            $cordovaFile: $cordovaFile,
            $ionicPopup: $ionicPopup,
            $cordovaLocalNotification: $cordovaLocalNotification,
            $ionicSlideBoxDelegate: $ionicSlideBoxDelegate,
            $cordovaToast: $cordovaToast,
            $cordovaSocialSharing: $cordovaSocialSharing,
            $sce: $sce,
            $filter: $filter,
            $cordovaFileTransfer: $cordovaFileTransfer,
            $cordovaSQLite: $cordovaSQLite,
            VideoService: VideoService,
            Session: Session,
            $ionicScrollDelegate: $ionicScrollDelegate,
            Global: Global,
            GlobalConstants: GlobalConstants,
        });
    }]);


    var createServiceRequest = function (serviceInfo, componentSubType, key, componentTargetLeaf, Global, GlobalConstants, $state) {
        var serviceRequest = {};
        var request = {};

        var inputParams = serviceInfo["inputParams"];
        var outputParams = serviceInfo["outputparam"];
        var requestParams = [];
        var dynamicList = getDynamicListData();

        if (inputParams && inputParams.length > 0) {

            for (var index = 0; index < inputParams.length; index++) {
                var inputParamValue = inputParams[index]["value"];
                var inputParamComponent = inputParamValue.split(".");
                var inputParamLeafName;
                var inputParamComponentName;
                if (inputParamComponent.length == 3) {
                    inputParamLeafName = inputParamComponent[0] + "." + inputParamComponent[1];
                    inputParamComponentName = inputParamComponent[2];
                } else if (inputParamComponent.length == 4) {
                    inputParamLeafName = inputParamComponent[0] + "." + inputParamComponent[1] + "." + inputParamComponent[2];
                    inputParamComponentName = inputParamComponent[3];
                } else {
                    inputParamLeafName = inputParamComponent[0];
                    inputParamComponentName = inputParamComponent[1];
                }

                if (inputParamLeafName === "DynamicData") {

                    switch (inputParamComponentName) {
                        case "BubPostDetail":
                            if ($state.current.name === "Gallery") {
                                dynamicList["BubPostDetail"] = [];
                                dynamicList["BubPostDetail"]["postAuthorId"] = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId)
                            }

                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: dynamicList[inputParamComponentName]["postAuthorId"]
                            });
                            break;

                        default:
                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: dynamicList[inputParamComponentName]
                            });
                            break;
                    }

                } else if (inputParamLeafName === "GlobalData") {

                    requestParams.push({
                        k: inputParams[index]["key"],
                        v: gGlobalData[inputParamComponentName]
                    });

                } else if (inputParamLeafName === "Special") {
                    switch (inputParamComponentName) {
                        case "customerId":
                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId)
                            });
                            break;
                        case "CustomerId":
                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: getCustomerId()
                            });
                            break;
                        case "Rating":
                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: getRating()
                            });
                            break;
                        case "ModuleId":
                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: gGlobalData["ModuleId"]
                            });
                            break;
                        case "subModuleId":
                            requestParams.push({
                                k: inputParams[index]["key"],
                                v: gGlobalData["subModuleId"]
                            });
                            break;
                    }
                } else {
                    var keyValue = getDataValueFromLeaf(appData, inputParamLeafName, inputParamComponentName);
                    requestParams.push({
                        k: inputParams[index]["key"],
                        v: keyValue
                    });
                }
            }
        }
        request["params"] = requestParams;
        request["deviceId"] = getDeviceId(); //dynamicList["deviceId"] ? dynamicList["deviceId"] : 0 ;
        request["initiatorId"] = getCNO();

        request["service"] = serviceInfo["service"];
        serviceRequest["req"] = request;
        return serviceRequest;
    }

    var doNetworkServiceCall = function (serviceRequest, serviceInfo, targetLeaf, $state, $globalService, $netCallService, $scope, $stateParams, $http, componentSubType, $interval, $timeout, $ionicSlideBoxDelegate, buttonKey, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions) {
        //debugger

        if ($globalService.getOnlineStatus()) {
            $globalService.showLoading(serviceInfo["progressInfo"]);
            //alert($globalService.getServiceURL());
            $netCallService.httpPOST({
                url: GlobalConstants.defaultEndPoint + GlobalConstants.webServiceName,
                data: serviceRequest,
                success: function (data, status, headers, config) {
                    //alert('success callback ' + JSON.stringify(data));
                    var serverResponse = JSON.parse(JSON.stringify(data));
                    var service = serverResponse.serv;
                    var output = serverResponse.op;

                    if (output.length && output[0].k.toString() === "Success" && output[0].v.toString() === "1") {
                        for (var opIndex = 1; opIndex < output.length; opIndex++) {
                            var opKey = output[opIndex].k;
                            var opValue = output[opIndex].v;
                        }
                        if ($state.current.name === "ProfileMyInfoEdit") {

                            if (output.length && output[0].k.toString() === "Success" && output[1].v.toString() === "40") {
                                $globalService.showAlert("Petbubs", "Password changes successfully");
                                $scope.closeModal();
                            }
                        }


                        switch (targetLeaf) {
                            case "PetCalendar":
                                window.localStorage.removeItem(GlobalConstants.ls_calendarEvents);
                                var EventData = [];
                                var petList = [];
                                for (var i = 0; i < output[1].v[1].length; i++) {
                                    petList.push({
                                        petid: output[1].v[1][i].petid,
                                        name: output[1].v[1][i].name,
                                        profilePic: GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath + output[1].v[1][i].profilepic
                                    });
                                }

                                var dynamicList = getDynamicListData();
                                dynamicList['PetList'] = petList;
                                setDynamicListData(dynamicList);

                                for (var i = 0; i < opValue[0].length; i++) {
                                    var data = {};
                                    data.addEventEligible = true;
                                    data.addVetActivity = false;
                                    data.alertTime = alertTime(opValue[0][i].activityReminder1);
                                    data.alertTimeText = opValue[0][i].activityReminder1 == undefined ? "" : opValue[0][i].activityReminder1;
                                    data.endDate = opValue[0][i].activityenddate == undefined ? opValue[0][i].activitydate : opValue[0][i].activityenddate;
                                    data.petId = opValue[0][i].petid;
                                    data.eventSelectedDate = opValue[0][i].activitydate;
                                    data.leafName = 'PetCalendar';
                                    data.myPet = petName(opValue[0][i].petid, petList);
                                    data.selectActivity = opValue[0][i].activitydetails == undefined ? "" : opValue[0][i].activitydetails;
                                    data.selectedDate = opValue[0][i].stoprepeat == undefined ? "" : opValue[0][i].stoprepeat;
                                    data.startDate = opValue[0][i].activitydate <= new Date().toISOString() ? new Date().toISOString() : opValue[0][i].activitydate;
                                    // data.startDate = opValue[0][i].activitydate;
                                    data.title = 'PetCalendar';
                                    data.eId = opValue[0][i].petactivityid;
                                    data.activityname = opValue[0][i].activityname;
                                    data.color = randomColor(opValue[0][i].activityname);
                                    data.choice = opValue[0][i].activityReminder2
                                    data.deletedate = opValue[0][i].deletedate == undefined ? "" : opValue[0][i].deletedate.split(", ");
                                    data.deletedaterepeat = opValue[0][i].deletedaterepeat == undefined ? "" : opValue[0][i].deletedaterepeat.split(", ");

                                    if (data.deletedate != "") {
                                        onClickAddEventFromServer(data, gGlobalData, Global, GlobalConstants, $globalService.showAlert, false, data.eId, false);
                                    }
                                    else {
                                        onClickAddEventFromServer(data, gGlobalData, Global, GlobalConstants, $globalService.showAlert, false, data.eId, false);
                                    }
                                }
                                if ($state.current.name === "EditEventLeaf" || $state.current.name === "AddEventLeaf") {
                                    $ionicNativeTransitions.stateGo(targetLeaf, {}, {}, {
                                        "type": "slide",
                                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                                        "duration": 500, // in milliseconds (ms), default 400
                                    });
                                } else {
                                    $state.go(targetLeaf);
                                }
                                $globalService.hideLoading();

                                break;

                            case "AfterEditPetEvent":
                                var EventData = [];

                                var petList = [];
                                for (var i = 0; i < output[1].v[1].length; i++) {

                                    petList.push({
                                        petid: output[1].v[1][i].petid,
                                        name: output[1].v[1][i].name
                                    });
                                }

                                for (var i = 0; i < opValue[0].length; i++) {
                                    var data = {};
                                    data.addEventEligible = true;
                                    data.addVetActivity = false;
                                    data.alertTime = alertTime(opValue[0][i].activityReminder1);
                                    data.alertTimeText = opValue[0][i].activityReminder1 == undefined ? "" : opValue[0][i].activityReminder1;
                                    data.endDate = opValue[0][i].activityenddate == undefined ? opValue[0][i].activitydate : opValue[0][i].activityenddate;
                                    data.petId = opValue[0][i].petid;
                                    data.eventSelectedDate = opValue[0][i].activitydate;
                                    data.leafName = 'PetCalendar';
                                    data.myPet = petName(opValue[0][i].petid, petList);
                                    data.selectActivity = opValue[0][i].activitydetails == undefined ? "" : opValue[0][i].activitydetails;
                                    data.selectedDate = opValue[0][i].stoprepeat == undefined ? "" : opValue[0][i].stoprepeat;
                                    data.startDate = opValue[0][i].activitydate <= new Date().toISOString() ? new Date().toISOString() : opValue[0][i].activitydate;
                                    // data.startDate = opValue[0][i].activitydate;
                                    data.title = 'PetCalendar';
                                    data.eId = opValue[0][i].petactivityid;
                                    data.activityname = opValue[0][i].activityname;
                                    data.color = randomColor(opValue[0][i].activityname);
                                    data.choice = opValue[0][i].activityReminder2
                                    data.deletedate = opValue[0][i].deletedate.split(", ");
                                    data.deletedaterepeat = opValue[0][i].deletedaterepeat.split(", ");
                                    onClickAddEventFromServer(data, gGlobalData, Global, GlobalConstants, $globalService.showAlert, false, data.eId, false);
                                }
                                $globalService.hideLoading();

                                break;
                            case "PetBookmark":
                            case "PetNews":
                                opValue = formatDate(opValue, "createddate");
                                for (index = 0; index < opValue.length; index++) {
                                    if (opValue[index].isbookmark == '1') {
                                        gGlobalData["noBookmark"] = true;
                                        break;
                                    }
                                }
                                var dynamicList = getDynamicListData();
                                dynamicList[opKey] = opValue;
                                setDynamicListData(dynamicList);
                                if (targetLeaf) {
                                    $state.go(targetLeaf);
                                } else {
                                    $scope.dynamicList[opKey] = opValue;
                                }

                                $globalService.hideLoading();

                                break;

                            case "BubTalkThreadCategories":
                                opValue = bubtalkthreadCategory(opValue);
                                var dynamicList = getDynamicListData();
                                dynamicList[opKey] = opValue;
                                setDynamicListData(dynamicList);
                                $state.go(targetLeaf);
                                $globalService.hideLoading();

                                break;

                            case "BubTalkThreadDetail":
                                if (opValue) {
                                    for (var index = 0; index < opValue.length; index++) {
                                        var Newsdate = new Date(opValue[index].date);

                                        var today = new Date();
                                        var milliseconds = Math.floor((today - (Newsdate)) / 60000);
                                        var seconds = Math.floor((today - (Newsdate)) / 1000);
                                        var minutes = Math.floor(seconds / 60);
                                        var hours = Math.floor(minutes / 60);
                                        var days = Math.floor(hours / 24);
                                        if (parseInt(days) > 0) {
                                            opValue[index].date = $filter('date')(Newsdate, 'MMM dd, yyyy')
                                        } else if (parseInt(hours) > 0) {
                                            opValue[index].date = hours + " hours ago";
                                        } else if (parseInt(minutes) > 0) {
                                            opValue[index].date = minutes + " minutes ago";
                                        } else if (parseInt(seconds) > 0) {
                                            opValue[index].date = seconds + " seconds ago";
                                        } else if (parseInt(milliseconds) > 0) {
                                            opValue[index].date = milliseconds + " seconds ago";
                                        }

                                        opValue[index].ThreadCommentReply = formatDate(opValue[index].ThreadCommentReply, "date");

                                    }
                                }

                                var dynamicList = getDynamicListData();
                                if (opValue == undefined) {
                                    dynamicList["listOfThreadDetailObj"] = [];
                                } else {
                                    dynamicList[opKey] = opValue;
                                }

                                dynamicList["totalpost"] = dynamicList["listOfThreadDetailObj"].length;
                                for (var i = 0; i < dynamicList["listOfThreadDetailObj"].length; i++) {
                                    dynamicList["totalpost"] = dynamicList["totalpost"] + dynamicList["listOfThreadDetailObj"][i].ThreadCommentReply.length;
                                    dynamicList["listOfThreadDetailObj"][i].ThreadCommentReply.reverse();
                                }
                                dynamicList["totalpost"] = dynamicList["totalpost"] + 1;
                                dynamicList["listOfThreadDetailObj"].reverse();
                                setDynamicListData(dynamicList);
                                $state.go(targetLeaf);
                                $globalService.hideLoading();

                                break;
                            case 'SocialMedia':
                            case 'Gallery':
                                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                                var dynamicList = getDynamicListData();
                                var SocialMediaList = output[1].v[0];
                                SocialMediaList["followers"] = (output[1].v[2]) === undefined ? 0 : (output[1].v[2].followers);
                                SocialMediaList["following"] = (output[1].v[2]) === undefined ? 0 : (output[1].v[2].following);
                                SocialMediaList["relationStatus"] = (output[1].v[2]) === undefined ? 0 : (output[1].v[2].relationStatus);
                                SocialMediaList["images"] = [];
                                SocialMediaList["videos"] = [];
                                SocialMediaList["postAuthorProfileThumbnail"] = SocialMediaList.profilepicture;

                                if (SocialMediaList.customerid === customerId) {
                                    SocialMediaList.postAuthorName = "My Gallery";
                                    SocialMediaList["relationStatus"] = 3;
                                } else {
                                    SocialMediaList.postAuthorName = SocialMediaList.name;
                                }
                                SocialMediaList.GalleryImage = [];
                                SocialMediaList.GalleryVideo = [];

                                if (output[1].v[1] != undefined) {
                                    for (var i = 0; i < output[1].v[1].length; i++) {

                                        if (output[1].v[1][i].isVideo === "false") {
                                            SocialMediaList.GalleryImage.push(output[1].v[1][i]);
                                            SocialMediaList["images"].push(output[1].v[1][i]);
                                        }
                                        else {
                                            SocialMediaList.GalleryVideo.push(output[1].v[1][i]);
                                            SocialMediaList["videos"].push(output[1].v[1][i]);
                                        }
                                    }
                                }

                                // if (SocialMediaList.images) {
                                //     SocialMediaList.GalleryImage = JSON.parse(SocialMediaList.images);
                                // }
                                // if (SocialMediaList.videos) {
                                //     SocialMediaList.GalleryVideo = JSON.parse(SocialMediaList.videos);
                                // }

                                SocialMediaList.GalleryImage.reverse();
                                SocialMediaList.GalleryVideo.reverse();

                                dynamicList["SocialMediaList"] = SocialMediaList;
                                setDynamicListData(dynamicList);
                                // $ionicNativeTransitions.goBack();
                                if ($state.current.name === "Follower" || $state.current.name === "Following" || $state.current.name === "CreateNewPost") {
                                    $ionicNativeTransitions.stateGo(targetLeaf, {}, {}, {
                                        "type": "slide",
                                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                                        "duration": 500, // in milliseconds (ms), default 400
                                    });
                                } else {
                                    $state.go(targetLeaf);
                                }
                                $globalService.hideLoading();
                                break;
                            case 'BubHubWallPostTwo':
                            case "BubHubWallPostOne":
                            case "BubHubWallLoves":
                                var allPosts = output[1].v;
                                var dynamicList = getDynamicListData();
                                allPosts = formatDate(allPosts, "timeSincePost");
                                allPosts.sort(function (a, b) {
                                    return parseInt(b["postId"]) - parseInt(a["postId"])
                                });

                                if (targetLeaf === "BubHubWallPostTwo") {
                                    dynamicList['BubPBPosts'] = allPosts;
                                } else if (targetLeaf === "BubHubWallLoves") {
                                    var BubsMediaList = output[1].v[0];
                                    BubsMediaList.PostAuthorName = BubsMediaList.postAuthorName;
                                    if (BubsMediaList.images) {
                                        BubsMediaList.GalleryImage = JSON.parse(BubsMediaList.images);
                                    }

                                    if (BubsMediaList.videos) {
                                        BubsMediaList.GalleryVideo = JSON.parse(BubsMediaList.videos);
                                    }
                                    dynamicList["BubsMediaList"] = BubsMediaList;
                                } else {
                                    var featuredPost = output[2].v;
                                    featuredPost = formatDate(featuredPost, "timeSincePost");
                                    allPosts.unshift(featuredPost[0]); //Adding featured post on top
                                    dynamicList['BubPosts'] = allPosts;
                                }

                                setDynamicListData(dynamicList);
                                $state.go(targetLeaf);
                                $globalService.hideLoading();
                                break;
                            case "DRBubsHome":
                                opValue = formatDate(opValue, "createddate");
                                var dynamicList = getDynamicListData();
                                dynamicList[opKey] = opValue;
                                setDynamicListData(dynamicList);
                                $state.go(targetLeaf);
                                $globalService.hideLoading();
                                break;
                            case "editPetSuccess":
                                // add event to pet calendar
                                var name = $scope.dynamicList.PetInfo.name;
                                var petid = $scope.dynamicList.PetInfo.petid;
                                var dewormingVetActivity = $scope.dynamicList["dewormingVetActivity"];
                                addEventByGivenData(dewormingVetActivity, name, petid, Global, GlobalConstants, $globalService.showAlert);
                                var rabiesVetActivity = $scope.dynamicList["rabiesVetActivity"];
                                addEventByGivenData(rabiesVetActivity, name, petid, Global, GlobalConstants, $globalService.showAlert);
                                var DHPPiVetActivity = $scope.dynamicList["DHPPiVetActivity"];
                                addEventByGivenData(DHPPiVetActivity, name, petid, Global, GlobalConstants, $globalService.showAlert);
                                $globalService.showAlert("Petbubs", "Saved");
                                $scope.onButtonClick('nextleaf', 'ProfileInfo');
                                break;
                            case "ProfileMyInfoEditSuccess":
                                /*storeFileToLocal("Petbubs/profile_img", $scope.dynamicList.profileData.profilePicture, $cordovaFile, $cordovaFileTransfer, showToast, function(path){
                                console.log(path)
                                })*/
                                var url = gGlobalData["PROFILEPIC"];
                                var sourceFileName = url.substring(url.lastIndexOf('/') + 1, url.length);
                                Global.setObjectInLocalStorage(GlobalConstants.ls_profilePicture, sourceFileName);
                                var history = $ionicHistory.backView();//returns an object with data about the previous screen. console.log(history) to see the entire data.
                                var previous_statename = history.stateName;//compare this
                                //console.log(previous_statename);
                                if (previous_statename == "Gallery") {
                                    $scope.onButtonClick('nextleaf', 'Gallery');
                                }
                                else {
                                    $scope.onButtonClick('nextleaf', 'ProfileMyInfo');
                                }
                                break;
                            case "ProfileMyInfo":
                                var profileData = output[1].v;
                                var profilePicture = GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath + profileData.profilepicture;
                                profileData.profilePicture = profilePicture;
                                var dynamicList = getDynamicListData();
                                if (profileData["dob"]) {
                                    profileData["dob"] = new Date(profileData["dob"]);
                                }
                                dynamicList['profileData'] = profileData;
                                setDynamicListData(dynamicList);
                                if ($state.current.name == "Gallery") {
                                    $state.go("ProfileMyInfoEdit");
                                } else {
                                    $state.go(targetLeaf);
                                }
                                $globalService.hideLoading();
                                break;
                            case "ProfileMyPets":
                            case "AddAPet":
                                if (output[2] == undefined) {
                                    var petCategory = output[1].v;
                                    var petList = [];
                                } else {
                                    var petCategory = output[2].v;
                                    var petList = output[1].v;
                                }

                                for (var i = 0; i < petList.length; i++) {
                                    var profilePic = GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath + petList[i].profilepic;
                                    petList[i].profilepic = profilePic;
                                }
                                var dynamicList = getDynamicListData();
                                dynamicList['PetList'] = petList;
                                dynamicList['PetCategory'] = petCategory;
                                setDynamicListData(dynamicList);
                                if ($state.current.name === "AddAPet" || $state.current.name === "ProfileInfo") {
                                    $ionicNativeTransitions.stateGo(targetLeaf, {}, {}, {
                                        "type": "slide",
                                        "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                                        "duration": 500, // in milliseconds (ms), default 400
                                    });
                                } else {
                                    $state.go(targetLeaf)
                                }
                                $globalService.hideLoading();
                                break;
                            case "AddEventLeaf":
                                if (output[2] == undefined) {
                                    var petList = [];
                                    $scope.showModal('templates/noPetFound.html');
                                } else {
                                    var petList = output[1].v;
                                    for (var i = 0; i < petList.length; i++) {
                                        var profilePic = GlobalConstants.defaultEndPoint + GlobalConstants.mediaServerPath + petList[i].profilepic;
                                        petList[i].profilepic = profilePic;
                                    }
                                    var dynamicList = getDynamicListData();
                                    dynamicList['PetList'] = petList;
                                    setDynamicListData(dynamicList);

                                    $state.go(targetLeaf)
                                }
                                $globalService.hideLoading();
                                break;
                            case "ProfileInfo":
                                var petInfo = output[1].v;
                                petInfo.profilepic = petInfo.profilepic;
                                var dynamicList = getDynamicListData();
                                var petBreeds = getObjectByKeyFromArray(dynamicList.PetCategory, "typeid", petInfo.pettype, "petbreed");
                                petInfo.petCategoryText = getObjectByKeyFromArray(petBreeds, "breedid", petInfo.petbreedcategory, "name");
                                dynamicList['PetInfo'] = petInfo;
                                // console.log(dynamicList.petactivity)
                                //for (var i = 0; i < dynamicList['PetInfo'].petactivity.length; i++) {
                                var petActivity = dynamicList["petactivity"] ? JSON.parse(dynamicList["petactivity"]) : [];
                                gGlobalData["VetActivity"] = petActivity;
                                dynamicList['dewormingVetActivity'] = getObjectByKeyFromArray(petActivity, "activityname", "Deworming");
                                dynamicList['rabiesVetActivity'] = getObjectByKeyFromArray(petActivity, "activityname", "Rabies Vaccine");
                                dynamicList['DHPPiVetActivity'] = getObjectByKeyFromArray(petActivity, "activityname", "DHPPi & Leptosporosis");
                                //}

                                for (var i = 0; i < output[1].v.petactivity.length; i++) {
                                    if (output[1].v.petactivity[i].activityname === "Deworming") {
                                        dynamicList['dewormingVetActivity'] = output[1].v.petactivity[i];
                                    } else if (output[1].v.petactivity[i].activityname === "Rabies Vaccine") {
                                        dynamicList['rabiesVetActivity'] = output[1].v.petactivity[i];
                                    } else if (output[1].v.petactivity[i].activityname === "DHPPi & Leptosporosis") {
                                        dynamicList['DHPPiVetActivity'] = output[1].v.petactivity[i];
                                    } else {
                                        dynamicList['dewormingVetActivity'] = '';
                                        dynamicList['rabiesVetActivity'] = '';
                                        dynamicList['DHPPiVetActivity'] = '';
                                    }
                                }


                                setDynamicListData(dynamicList);
                                $state.go('ProfileInfo');
                                $globalService.hideLoading();
                                break;
                            default:
                                var dynamicList = getDynamicListData();
                                dynamicList[opKey] = opValue;

                                setDynamicListData(dynamicList);
                                setPreviousStateName($state.current.name);

                                if (targetLeaf != null) {
                                    if ($state.current.name === "StartNewThread") {
                                        $ionicNativeTransitions.stateGo(targetLeaf, {}, {}, {
                                            "type": "slide",
                                            "direction": "right", // 'left|right|up|down', default 'left' (which is like 'next')
                                            "duration": 500, // in milliseconds (ms), default 400
                                        });
                                    } else if (targetLeaf === "FindPet" || targetLeaf === "PetCare" || targetLeaf === "PetNeeds" || targetLeaf === "PetParty" || targetLeaf === "PetTravel" || targetLeaf === "PetFarewell") {
                                        $state.go(targetLeaf, {}, { reload: true });

                                    }
                                    else {
                                        $state.go(targetLeaf);
                                    }

                                } else {
                                    switch (serverResponse.serv) {

                                        case "addpet":
                                            // add event to pet calendar
                                            var name = output[2].v;
                                            var petid = output[3].v;
                                            var dewormingVetActivity = $scope.dynamicList["dewormingVetActivity"];
                                            addEventByGivenData(dewormingVetActivity, name, petid, Global, GlobalConstants, $globalService.showAlert);
                                            var rabiesVetActivity = $scope.dynamicList["rabiesVetActivity"];
                                            addEventByGivenData(rabiesVetActivity, name, petid, Global, GlobalConstants, $globalService.showAlert);
                                            var DHPPiVetActivity = $scope.dynamicList["DHPPiVetActivity"];
                                            addEventByGivenData(DHPPiVetActivity, name, petid, Global, GlobalConstants, $globalService.showAlert);
                                            $globalService.showAlert("Petbubs", "Your Pet was added successfully");
                                            var history = $ionicHistory.backView();//returns an object with data about the previous screen. console.log(history) to see the entire data.
                                            var previous_statename = history.stateName;//compare this
                                            // console.log(previous_statename);
                                            if (previous_statename == "PetCalendar") {
                                                $scope.onButtonClick('nextleaf', 'AddEventLeaf');
                                            } else {
                                                $scope.onButtonClick('nextleaf', 'ProfileMyPets');
                                            }
                                            break;
                                        case "PostRatings":
                                            ///logic to refresh rating///
                                            if ($state.current.name === "FindPet") {
                                                $scope.noTargetServiceRequest("", "FindPet");
                                            } else if ($state.current.name === "PetCare") {
                                                $scope.noTargetServiceRequest("", "PetCare");
                                            } else if ($state.current.name === "PetNeeds") {
                                                $scope.noTargetServiceRequest("", "PetNeeds");
                                            } else if ($state.current.name === "PetParty") {
                                                $scope.noTargetServiceRequest("", "PetParty");
                                            } else if ($state.current.name === "PetTravel") {
                                                $scope.noTargetServiceRequest("", "PetTravel");
                                            } else if ($state.current.name === "PetFarewell") {
                                                $scope.noTargetServiceRequest("", "PetFarewell");
                                            }

                                            $globalService.showAlert("Petbubs", "Thank you for your rating");

                                            break;
                                        case "Login":
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_customerId, output[1].v);
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_name, output[2].v);
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_profilePicture, output[3].v);
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_isLoggedIn, true);
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_hasSeenBubTalkLeaf, false);
                                            $scope.closeModal2();
                                            var dynamicData = getDynamicListData();
                                            if (dynamicData['CurrentPage'] != "AddRemoveBookmark") {
                                                // if ($state.current.name === "childView") {
                                                //     // dynamicData['CurrentPage']="childView";
                                                //     $scope.onButtonClick('nextleaf', "childView");
                                                //     $state.go("childView", {}, { reload: true });
                                                // } else {
                                                $scope.onButtonClick('nextleaf', dynamicData['CurrentPage']);
                                                // }

                                                // $scope.falseToggle();
                                            }
                                            break;
                                        case "Registration":
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_OTP, output[2].v);
                                            $scope.closeModal();
                                            $scope.data.otp1 = output[2].v.toString().substring(0, 1);
                                            $scope.data.otp2 = output[2].v.toString().substring(1, 2);;
                                            $scope.data.otp3 = output[2].v.toString().substring(2, 3);;
                                            $scope.data.otp4 = output[2].v.toString().substring(3, 4);;
                                            $scope.data.otp5 = output[2].v.toString().substring(4, 5);;
                                            $scope.data.otp6 = output[2].v.toString().substring(5, 6);;
                                            $scope.data.name = Global.getObjectFromLocalStorage(GlobalConstants.ls_name);
                                            $scope.showModal('templates/enterOtp.html');
                                            break;
                                        case "VerifyRegistrationOTP":
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_customerId, output[1].v);
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_name, output[2].v);
                                            Global.setObjectInLocalStorage(GlobalConstants.ls_isLoggedIn, true);
                                            $scope.closeModal2();
                                            $scope.closeModal();
                                            var dynamicData = getDynamicListData();
                                            if (dynamicData['CurrentPage'] != "AddRemoveBookmark") {
                                                $scope.onButtonClick('nextleaf', dynamicData['CurrentPage']);
                                            }
                                            break;
                                        case "ThreadCommentReply":
                                        case "ThreadPostReply":
                                        case "ThreadPostEdit":
                                            var serviceRequest = {};
                                            var request = {};
                                            var requestParams = [];
                                            var serviceInfo = [];
                                            requestParams.push({
                                                k: "categoryId",
                                                v: gGlobalData["BubCategory"]
                                            });
                                            requestParams.push({
                                                k: "threadId",
                                                v: gGlobalData["threadId"]
                                            });
                                            requestParams.push({
                                                k: "topicId",
                                                v: gGlobalData["topic"]
                                            });

                                            request["params"] = requestParams;
                                            request["deviceId"] = getDeviceId();
                                            request["initiatorId"] = "0";

                                            request["service"] = "ThreadsDetails";
                                            serviceRequest["req"] = request;
                                            serviceInfo["progressInfo"] = "Please wait...";
                                            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            $globalService.hideLoading();
                                            // document.getElementById("EditPostButtonMain_"+gGlobalData["EditedPost"].index).setAttribute("style","display:none");
                                            break;
                                        case "ThreadsDetails":
                                            for (var index = 0; index < opValue.length; index++) {
                                                var Newsdate = new Date(opValue[index].date);
                                                var today = new Date();
                                                today.setSeconds(today.getSeconds() + 5);
                                                var milliseconds = Math.floor((today - (Newsdate)) / 60000);
                                                var seconds = Math.floor((today - (Newsdate)) / 1000);
                                                var minutes = Math.floor(seconds / 60);
                                                var hours = Math.floor(minutes / 60);
                                                var days = Math.floor(hours / 24);

                                                if (parseInt(days) > 0) {
                                                    opValue[index].date = $filter('date')(Newsdate, 'MMM dd, yyyy')
                                                } else if (parseInt(hours) < 2 && parseInt(hours) > 0) {
                                                    opValue[index].date = hours + " hour ago";
                                                } else if (parseInt(hours) > 0) {
                                                    opValue[index].date = hours + " hours ago";
                                                } else if (parseInt(minutes) < 2 && parseInt(minutes) > 0) {
                                                    opValue[index].date = minutes + " minute ago";
                                                } else if (parseInt(minutes) > 0) {
                                                    opValue[index].date = minutes + " minutes ago";
                                                } else if (parseInt(seconds) < 2 && parseInt(seconds) > 0) {
                                                    opValue[index].date = seconds + " second ago";
                                                } else if (parseInt(seconds) > 0) {
                                                    opValue[index].date = seconds + " seconds ago";
                                                } else if (parseInt(milliseconds) > 0) {
                                                    opValue[index].date = milliseconds + " seconds ago";
                                                }

                                                opValue[index].ThreadCommentReply = formatDate(opValue[index].ThreadCommentReply, "date");
                                            }

                                            setTimeout(function () {
                                                $scope.dynamicList.listOfThreadDetailObj = opValue;
                                                $scope.dynamicList.listOfThreadDetailObj.reverse();
                                                for (var i = 0; i < $scope.dynamicList["listOfThreadDetailObj"].length; i++) {
                                                    $scope.dynamicList["listOfThreadDetailObj"][i].ThreadCommentReply.reverse();
                                                }
                                                $scope.dynamicList["totalpost"] = $scope.dynamicList["totalpost"] + 1;
                                                //$scope.dynamicList.dynamicList.totalpost=$scope.dynamicList.dynamicList.totalpost+1;
                                                $scope.$apply();
                                                $ionicScrollDelegate.resize();
                                            }, 100)
                                            $globalService.hideLoading();
                                            break;
                                        case "TopicThreadList":
                                            var dynamicList = getDynamicListData();
                                            dynamicList[opKey] = opValue;
                                            setDynamicListData(dynamicList);
                                            setTimeout(function () {
                                                $scope.dynamicList[opKey] = opValue;
                                                $scope.$apply();
                                                $ionicScrollDelegate.resize();
                                            }, 100)

                                            if ($state.current.name === "StartNewThread") {
                                                $state.go("BubTalkThreadList");
                                            }

                                            break;
                                        case "NewThread":
                                            var serviceRequest = {};
                                            var request = {};
                                            var requestParams = [];
                                            var serviceInfo = [];
                                            requestParams.push({
                                                k: "categoryId",
                                                v: gGlobalData["BubCategory"]
                                            });
                                            requestParams.push({
                                                k: "topicId",
                                                v: gGlobalData["topic"]
                                            });

                                            request["params"] = requestParams;
                                            request["deviceId"] = getDeviceId();
                                            request["initiatorId"] = "0";

                                            request["service"] = "TopicThreadList";
                                            serviceRequest["req"] = request;
                                            serviceInfo["progressInfo"] = "Please wait...";
                                            doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            $globalService.hideLoading();
                                            break;

                                        case "AllThreads":
                                            opValue = bubtalkthreadCategory(opValue);
                                            var dynamicList = getDynamicListData();
                                            dynamicList[opKey] = opValue;
                                            setDynamicListData(dynamicList);
                                            $scope.dynamicList[opKey] = opValue;
                                            $globalService.hideLoading();
                                            break;

                                        case "GetComment":
                                            var comments = output[1].v;
                                            if (comments && comments.length > 0) {
                                                var dynamicList = getDynamicListData();
                                                var bubPostDetail = dynamicList['BubPostDetail'];
                                                comments = formatDate(comments, "postTime");
                                                // bubPostDetail.comments = comments.reverse();
                                                bubPostDetail.comments = comments;

                                                for (var i = 0; i < bubPostDetail.comments.length; i++) {
                                                    if (!isNaN(bubPostDetail.comments[i].comment.split(',')[0])) {
                                                        var commentBodyArray = comments[i].comment.split(',').map(Number);
                                                        var convetedBodyText = '';
                                                        for (var j = 0; j < commentBodyArray.length; j++) {
                                                            convetedBodyText = convetedBodyText + String.fromCodePoint(commentBodyArray[j]);
                                                        }
                                                        // console.log(convetedBodyText);
                                                        bubPostDetail.comments[i].comment = convetedBodyText;
                                                    }
                                                }

                                                bubPostDetail.totalPostComments = comments.length;


                                                dynamicList['BubPostDetail'] = bubPostDetail;
                                                setDynamicListData(dynamicList);
                                                $scope.dynamicList.BubPostDetail = bubPostDetail;
                                                $state.go('BubHubWallPostDetail');
                                                $globalService.hideLoading();
                                            }
                                            break;

                                        case 'AddComment':
                                            $scope.data.commentInput = "";
                                            var comments = output[1].v;
                                            if (comments && comments.length > 0) {
                                                comments = formatDate(comments, "postTime");
                                                var bubPostDetail = dynamicList['BubPostDetail'];
                                                // bubPostDetail.comments = comments.reverse();
                                                bubPostDetail.comments = comments;
                                                for (var i = 0; i < bubPostDetail.comments.length; i++) {
                                                    if (!isNaN(bubPostDetail.comments[i].comment.split(',')[0])) {
                                                        var commentBodyArray = comments[i].comment.split(',').map(Number);
                                                        var convetedBodyText = '';
                                                        for (var j = 0; j < commentBodyArray.length; j++) {
                                                            convetedBodyText = convetedBodyText + String.fromCodePoint(commentBodyArray[j]);
                                                        }
                                                        bubPostDetail.comments[i].comment = convetedBodyText;
                                                    }
                                                }
                                                bubPostDetail.totalPostComments = comments.length;

                                                $scope.dynamicList.BubPostDetail = bubPostDetail;
                                                $globalService.hideLoading();
                                            }
                                            $globalService.hideLoading();
                                            break;
                                        case 'GetLikes':
                                            var likeList = output[1].v;
                                            if (likeList.length > 0) {
                                                var dynamicList = getDynamicListData();
                                                dynamicList['LikeList'] = likeList;
                                                setDynamicListData(dynamicList);
                                                $state.go('LikedBy');
                                                $globalService.hideLoading();
                                            } else {

                                            }
                                            break;
                                        case 'AddPost':
                                            // var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                                            // var serviceRequest = {};
                                            // var request = {};
                                            // var requestParams = [];
                                            // var serviceInfo = [];
                                            // requestParams.push({
                                            //     k: "OPPCUSTOMERID",
                                            //     v: customerId
                                            // });

                                            // requestParams.push({
                                            //     k: "SELFCUSTOMERID",
                                            //     v: customerId
                                            // });

                                            // request["params"] = requestParams;
                                            // request["deviceId"] = getDeviceId();
                                            // request["initiatorId"] = "0";

                                            // request["service"] = "SocialMedia";
                                            // serviceRequest["req"] = request;
                                            // serviceInfo["progressInfo"] = "Please wait...";
                                            // doNetworkServiceCall(serviceRequest, serviceInfo, "Gallery", $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            // PetBubHttpService.calendarService(serviceRequest).then(function (result) {
                                            //    doNetworkServiceCall(serviceRequest, serviceInfo, "Gallery", $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            // });
                                            // doNetworkServiceCall(serviceRequest, serviceInfo, "Gallery", $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            // $scope.noTargetServiceRequest("null","Gallery");
                                            // $state.go("Gallery");
                                            $scope.myGoBack();
                                            $globalService.showAlert("Petbubs", "Post added successfully!");

                                            // $state.go("Gallery");
                                            // $ionicHistory.goBack();
                                            // $ionicNativeTransitions.goBack();
                                            break;
                                        case 'AddFriend':
                                            if ($state.current.name === "SocialMedia") {
                                                var dynamicList = getDynamicListData();
                                                var userId = dynamicList['CurrentAuthorId'];
                                                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                                                var dynamicList = getDynamicListData();
                                                var currentPostId = dynamicList['CurrentPostId'];
                                                var serviceRequest = {};
                                                var request = {};
                                                var requestParams = [];
                                                var serviceInfo = [];
                                                requestParams.push({
                                                    k: "OPPCUSTOMERID",
                                                    v: userId + ''
                                                });
                                                requestParams.push({
                                                    k: "SELFCUSTOMERID",
                                                    v: customerId + ''
                                                });
                                                request["params"] = requestParams;
                                                request["deviceId"] = getDeviceId();
                                                request["initiatorId"] = -1;

                                                request["service"] = "SocialMedia";
                                                serviceRequest["req"] = request;

                                                serviceInfo["progressInfo"] = "Please wait...";
                                                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            } else if ($state.current.name === "LikedBy") {
                                                var customerId = Global.getObjectFromLocalStorage(GlobalConstants.ls_customerId);
                                                var dynamicList = getDynamicListData();
                                                var currentPostId = dynamicList['CurrentPostId'];
                                                var serviceRequest = {};
                                                var request = {};
                                                var requestParams = [];
                                                var serviceInfo = [];
                                                requestParams.push({
                                                    k: "PostId",
                                                    v: currentPostId + ''
                                                });
                                                requestParams.push({
                                                    k: "customerId",
                                                    v: customerId + ''
                                                });
                                                request["params"] = requestParams;
                                                request["deviceId"] = getDeviceId();
                                                request["initiatorId"] = -1;

                                                request["service"] = "GetLikes";
                                                serviceRequest["req"] = request;

                                                serviceInfo["progressInfo"] = "Please wait...";
                                                doNetworkServiceCall(serviceRequest, serviceInfo, null, $state, $globalService, $netCallService, $scope, $stateParams, $http, null, $interval, $timeout, $ionicSlideBoxDelegate, null, $filter, $cordovaSQLite, $ionicScrollDelegate, $cordovaToast, Global, GlobalConstants, $ionicHistory, $ionicNativeTransitions);
                                            }

                                            if ($state.current.name === "Following") {
                                                var dynamicList = getDynamicListData();
                                                dynamicList.SocialMediaList.following = parseInt(dynamicList.SocialMediaList.following) - 1;
                                                $scope.dynamicList.followingList[gGlobalData["followerIndex"]].relationStatus = '1';
                                                setDynamicListData(dynamicList);
                                            }

                                            if ($state.current.name === "Follower") {
                                                var dynamicList = getDynamicListData();
                                                dynamicList.SocialMediaList.followersList = parseInt(dynamicList.SocialMediaList.following) + 1;
                                                $scope.dynamicList.followersList[gGlobalData["followerIndex"]].relationStatus = '1';
                                                setDynamicListData(dynamicList);
                                            }

                                            if ($state.current.name === "LikedBy") {
                                                $scope.dynamicList.LikeList[gGlobalData["followerIndex"]].relationStatus = '1';
                                            }

                                            if ($state.current.name === "BubHubWallFindFriends") {
                                                // console.log($scope.bbHubTabActive);
                                                // if ($scope.dynamicList.BubList && $scope.bbHubTabActive === 1) {
                                                //     $scope.dynamicList.BubList[gGlobalData["followerIndex"]].relationStatus = '1';
                                                // }

                                                // if ($scope.dynamicList.friendList && $scope.bbHubTabActive === 0) {
                                                //     $scope.dynamicList.friendList[gGlobalData["followerIndex"]].relationStatus = '1';
                                                // }
                                                $scope.noTargetServiceRequest("", "BubHubWallFindFriends");
                                                $scope.dynamicList.BubList[gGlobalData["followerIndex"]].relationStatus = '1';
                                            }

                                            break;
                                        case 'Unfollow':
                                            var dynamicList = getDynamicListData();
                                            if ($state.current.name === "Following") {
                                                var dynamicList = getDynamicListData();
                                                dynamicList.SocialMediaList.following = parseInt(dynamicList.SocialMediaList.following) - 1;
                                                $scope.dynamicList.SocialMediaList.following = parseInt(dynamicList.SocialMediaList.following) - 1;
                                                $scope.dynamicList.followingList[gGlobalData["UnfollowerIndex"]].relationStatus = '0';
                                                setDynamicListData(dynamicList);
                                            }

                                            if ($state.current.name === "Follower") {
                                                var dynamicList = getDynamicListData();
                                                dynamicList.SocialMediaList.followers = parseInt(dynamicList.SocialMediaList.followers) + 1;
                                                $scope.dynamicList.SocialMediaList.followers = parseInt(dynamicList.SocialMediaList.followers) + 1;
                                                $scope.dynamicList.followersList[gGlobalData["UnfollowerIndex"]].relationStatus = '0';
                                                setDynamicListData(dynamicList);
                                            }

                                            if ($state.current.name === "LikedBy") {
                                                $scope.dynamicList.LikeList[gGlobalData["UnfollowerIndex"]].relationStatus = '0';
                                            }

                                            if ($state.current.name === "BubHubWallFindFriends") {
                                                $scope.dynamicList.BubList[gGlobalData["UnfollowerIndex"]].relationStatus = '0';
                                                // $scope.dynamicList.BubList[gGlobalData["followerIndex"]].relationStatus = '0';
                                                // if ($scope.dynamicList.BubList && $scope.bbHubTabActive === 1) {
                                                //     $scope.dynamicList.BubList[gGlobalData["UnfollowerIndex"]].relationStatus = '0';
                                                // }
                                                // if ($scope.dynamicList.friendList && $scope.bbHubTabActive === 0) {
                                                //     $scope.dynamicList.friendList[gGlobalData["UnfollowerIndex"]].relationStatus = '0';
                                                // }
                                                $scope.noTargetServiceRequest("", "BubHubWallFindFriends");
                                            }

                                            $globalService.hideLoading();
                                            break;
                                        // case "FriendList":
                                        case "AllBubs":
                                            if (targetLeaf != "BubHubWallFindFriends") {
                                                setTimeout(function () {
                                                    $scope.dynamicList["friendList"] = output[1].v;
                                                    $scope.$apply();
                                                }, 100)
                                            }

                                            break;
                                        case 'NextWallPBList':
                                        case "NextWallList":
                                            var allPosts = output[1].v;
                                            if (allPosts.length <= 1) {
                                                $scope.moreDataCanBeLoaded = true;
                                            } else {
                                                allPosts = formatDate(allPosts, "timeSincePost");

                                                for (var i = allPosts.length - 1; i > 0; i--) {
                                                    if (serverResponse.serv === "NextWallPBList") {
                                                        $scope.dynamicList.BubPBPosts.push(allPosts[i]);
                                                    } else {
                                                        // console.log(allPosts[i]);
                                                        $scope.dynamicList.BubPosts.push(allPosts[i]);
                                                    }
                                                }
                                                $scope.$broadcast('scroll.infiniteScrollComplete');
                                            }
                                            if (serverResponse.serv === "NextWallPBList") {
                                                $scope.dynamicList.BubPBPosts.sort(function (a, b) {
                                                    return parseInt(b["postId"]) - parseInt(a["postId"])
                                                });
                                            } else {
                                                $scope.dynamicList.BubPosts.sort(function (a, b) {
                                                    return parseInt(b["postId"]) - parseInt(a["postId"])
                                                });
                                            }

                                            if (serverResponse.serv === "NextWallList") {
                                                for (var i = 0; i < $scope.dynamicList.BubPosts.length; i++) {
                                                    if ($scope.dynamicList.BubPosts[i].isFeaturedPost === "1") {
                                                        $scope.dynamicList.BubPosts.unshift($scope.dynamicList.BubPosts[i]);
                                                        $scope.dynamicList.BubPosts.splice(i + 1, 1);
                                                    }
                                                }
                                            }
                                            $globalService.hideLoading();
                                            break;

                                        case "TopTenPost":
                                            var allPosts = output[1].v;
                                            var dynamicList = getDynamicListData();
                                            allPosts = formatDate(allPosts, "timeSincePost");
                                            var featuredPost = output[2].v;
                                            featuredPost = formatDate(featuredPost, "timeSincePost");
                                            allPosts.sort(function (a, b) {
                                                return parseInt(b["postId"]) - parseInt(a["postId"])
                                            });
                                            allPosts.unshift(featuredPost[0]); //Adding featured post on top
                                            dynamicList['BubPosts'] = allPosts;
                                            $timeout(function () {
                                                $scope.dynamicList.BubPosts = allPosts;
                                                $scope.$apply();
                                            });
                                            setDynamicListData(dynamicList);

                                            $globalService.hideLoading();
                                            break;
                                        case "TopTenPBPost":
                                            var allPosts = output[1].v;
                                            var dynamicList = getDynamicListData();
                                            allPosts = formatDate(allPosts, "timeSincePost");
                                            allPosts.sort(function (a, b) {
                                                return parseInt(b["postId"]) - parseInt(a["postId"])
                                            });
                                            dynamicList['BubPBPosts'] = allPosts;
                                            $timeout(function () {
                                                $scope.dynamicList.BubPBPosts = allPosts;
                                                $scope.$apply();
                                            });
                                            setDynamicListData(dynamicList);

                                            $globalService.hideLoading();
                                            break;

                                        case "AddRemovebookmark":
                                            var dynamicList = getDynamicListData();

                                            //Added bookmark code
                                            if (output[1].v == "16") {
                                                $scope.globalData.isbookmark = '1';
                                                for (var index = 0; index < dynamicList["petnewsObj"].length; index++) {
                                                    if (dynamicList["petnewsObj"][index]["petnewsid"] === $scope.globalData["petnewsid"]) {
                                                        dynamicList["petnewsObj"][index].isbookmark = '1';
                                                        gGlobalData["noBookmark"] = true;
                                                        $globalService.showAlert("Bookmark added");
                                                        break;
                                                    }
                                                }
                                            }

                                            //Removed bookmark code
                                            if (output[1].v == "53") {
                                                for (var index = 0; index < dynamicList["petnewsObj"].length; index++) {
                                                    if (dynamicList["petnewsObj"][index]["petnewsid"] === $scope.globalData["petnewsid"]) {
                                                        dynamicList["petnewsObj"][index].isbookmark = '0';
                                                        break;
                                                    }
                                                }
                                                debugger;
                                                for (var index = 0; index < dynamicList["BookmarkList"].length; index++) {
                                                    if (dynamicList["BookmarkList"][index]["petnewsid"] === $scope.globalData["petnewsid"]) {
                                                        dynamicList["BookmarkList"].splice(index, 1);
                                                        setTimeout(function () {
                                                            $scope.dynamicList["BookmarkList"] = dynamicList["BookmarkList"];
                                                            $scope.$apply();
                                                        })
                                                        if (dynamicList["BookmarkList"].length == 0) {
                                                            gGlobalData["noBookmark"] = false;
                                                            $globalService.showAlert("No Bookmarks");
                                                        }
                                                        break;
                                                    }
                                                }
                                                //$scope.noTargetServiceRequest("nextleaf","PetNews")
                                            }

                                            setDynamicListData(dynamicList);
                                            $globalService.hideLoading();
                                            break;
                                        case "PetArticle":
                                            opValue = formatDate(opValue, "createddate");
                                            var dynamicList = getDynamicListData();
                                            dynamicList[opKey] = opValue;
                                            setTimeout(function () {
                                                $scope.dynamicList[opKey] = opValue;
                                                $scope.$apply();
                                            })
                                            setDynamicListData(dynamicList);
                                            $globalService.hideLoading();
                                            break;
                                        case 'AddLike':

                                            // if($state.current.name==="BubHubWallPostDetail"){                                                
                                            //     $state.go($state.current.name,{},{reload:true});
                                            // }

                                            $globalService.hideLoading();
                                            break;
                                    }
                                }
                                $globalService.hideLoading();
                                break;
                        }

                    } else if (output[0].k.toString() === "Exception Occured" && output[0].v.toString() === "-1") {
                        $globalService.hideLoading();
                        $globalService.showAlert('Info', "An error occured. Please try again later");
                    } else if (output[0].k.toString() === "Failure") {
                        if (targetLeaf === "BubHubWallFindFriends") {
                            $state.go(targetLeaf);
                            $globalService.hideLoading();
                        } else {
                            $globalService.hideLoading();
                        }
                    } else {
                        $globalService.hideLoading();
                        var errCode = output[0].v.toString();
                        switch (serverResponse.serv) {
                            case "GetComment":
                                $state.go('BubHubWallPostDetail');
                                break;
                            case "SocialMedia":
                                $state.go('Gallery');
                                break;
                            case "SocialMedia":
                                $state.go('Gallery');
                                break;
                            default:
                                if (targetLeaf === "BubHubWallFindFriends" && errorCodes[errCode] === "27") {
                                    $state.go('BubHubWallFindFriends');
                                } else if (targetLeaf === "ProfileMyInfoEditSuccess") {
                                    $globalService.showAlert("Petbubs", "Your Old Password is Wrong");
                                    $scope.closeModal();
                                } else if (serverResponse.serv === "GetLikes") {
                                    $globalService.showAlert("Petbubs", "No Likes yet!");
                                } else if (targetLeaf === "Follower" || targetLeaf === "Following") {
                                    $scope.showModal('templates/noFollow.html');
                                } else {
                                    $globalService.showAlert("Petbubs", errorCodes[errCode]);
                                }
                                break;
                        }
                    }
                },
                error: function (data, status, headers, config) {
                    $globalService.hideLoading();
                    console.log(status);
                    $globalService.showAlert('Connection Failure', 'Server cannot be contacted. Please try again later.');
                },
            });
        } else {
            $globalService.showAlert('Internet Disconnected', 'The internet is disconnected on your device.');

        }
    }


    var deleteApplicationMobileLogin = function (rowIndex, dbApps, appDetails, targetLeaf, $globalService, $state) {
        if (rowIndex < dbApps.length) {
            var dbAppId = dbApps.item(rowIndex).AppId;
            var appExists = false;
            for (var appIndex = 0; appIndex < appDetails.length; appIndex++) {
                var appId = appDetails[appIndex].appId;
                if (appId.toString() === dbAppId.toString()) {
                    appExists = true;
                }
            }
            if (appExists === false) {
                appStudioDB.deleteApplication(dbAppId, function () {
                    deleteApplicationMobileLogin(rowIndex + 1, dbApps, appDetails, targetLeaf, $globalService, $state);
                });
            } else {
                deleteApplicationMobileLogin(rowIndex + 1, dbApps, appDetails, targetLeaf, $globalService, $state);
            }
        } else {
            appStudioDB.setAppListForContainter(appDetails, appDetails.length, 0, function () {
                appStudioDB.selectAllRecords("Application", "AppId", function (tx, rs) {
                    //Uncomment for normal listing
                    var appList = [];
                    for (var rowIndex = 0; rowIndex < rs.rows.length; rowIndex++) {
                        var appItem = rs.rows.item(rowIndex);
                        if (appItem.AppLogo.toString().length === 0) {
                            appItem.AppLogo = "default_app_icon.png";
                        }
                        appList.push(appItem);
                    }
                    var dynamicList = getDynamicListData();
                    dynamicList["AppList"] = appList;
                    setDynamicListData(dynamicList);
                    console.log(JSON.stringify(appList));
                    if (targetLeaf && targetLeaf !== 'null') {
                        $globalService.hideLoading();
                        setPreviousStateName($state.current.name);
                        $state.go(targetLeaf);
                    } else if (errorCode.length > 0) {
                        $globalService.hideLoading();
                    }
                });

            });
        }
    }

    window.gControllerModule = controllerModule;
    return controllerModule;
});