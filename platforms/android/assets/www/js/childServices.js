// define(['angular'], function (angular) {
define([], function () {  
  var appServices = angular.module('appStudioStarter.services', []);  
  var appData = getAppData();
  for (var template in appData.views)
  {
    appServices.value(template, appData.views[template].data);
  }

  appServices.factory('globalService', function ($ionicLoading, $ionicPopup) {
    var gAppDefXML = null;
    var gServiceURL = gAppStudioServerURL;//"http://192.168.2.16:8080/Tranx/n";
    return {
      getAppXML: function(){                
        return gAppDefXML;
      },
      setAppXML: function(newAppDefXML){             
        gAppDefXML = newAppDefXML;
      },
      getServiceURL: function(){                
        return gServiceURL;
      },
      stringToXML: function(oString) {
         //code for IE
         if (window.ActiveXObject) { 
           var oXML = new ActiveXObject("Microsoft.XMLDOM"); oXML.loadXML(oString);
           return oXML;
         }
         // code for Chrome, Safari, Firefox, Opera, etc. 
         else {
          return (new DOMParser()).parseFromString(oString, "text/xml");
         }
      },
      showLoading: function(message) {
        $ionicLoading.show({
          template: message,
          duration: 15000 
        });
      },
      hideLoading: function(){
        $ionicLoading.hide();
      },
      showConfirmation: function(headerTitle, confirmMessage, confirmCallback, cancelCallback) {
       var confirmPopup = $ionicPopup.confirm({
         title: headerTitle,
         template: confirmMessage
       });
       confirmPopup.then(function(res) {
         if(res) {        
           if(confirmCallback) confirmCallback();
         } else {
           if(cancelCallback) cancelCallback();
         }
       });
     },
     showAlert: function(headerTitle, alertMessage, callback){      
            $ionicPopup.alert({
              title: headerTitle,
              content: alertMessage
            }).then(function(res) {
              if(callback) callback();
            });
          
     },
    };
  });

  appServices.factory('netCallService', function ($http, $log, $q) {

    return{
      /**
      options.url = "http://....",
      options.onSuccess = funtion onSuccess(){},
      options.onError = funtion onSuccess(){},
      **/
      httpGET: function(options){
        var deferred = $q.defer();
        var success = options.success;
        var error = options.error;
        $http.get(options.url)
          .success(function(data, status, headers, config) { 
            deferred.resolve(success(data, status, headers, config));
          }).error(function(data, status, headers, config) {
            deferred.reject(data);
            error(data, status, headers, config);
            $log.error(data, status, headers, config);
          });
        return deferred.promise;
      },

      httpPOST: function(options){
        var deferred = $q.defer();
        var success = options.success;
        var error = options.error;
        $http.post(options.url, options.data)
          .success(function(data, status, headers, config) { 
            deferred.resolve(success(data, status, headers, config));
          }).error(function(data, status, headers, config) {
            deferred.reject(data);
            error(data, status, headers, config);
            $log.error(data, status, headers, config);
          });
        return deferred.promise;
      },
    };
  });

  window.gAppServices = appServices;
  return appServices;

});