require.config({
    paths: {
        cordova: '../cordova',
        text: '../bower_components/requirejs-text/text',
        data: '../data/appData',
        serverCodes: '../data/serverCodes',
        Constants: '../js/Constants',
        commonJS: '../js/include/common-0.0.1',
        dbHelper: '../js/include/dbHelper-0.0.1',
        tripledes: '../js/include/tripledes',
        customJS: '../js/customJS',
        flexcalendar: '../js/flex-calendar', ///////////changes @04/09/2015
        angularTranslate: '../js/angular-translate',///////////changes @04/09/2015
        ngcordova: '../js/ng-cordova.min',///////////changes @04/09/2015
        ionGallery   : '../js/ion.gallery',
        ionRating   : '../js/ionic-rating'
    }
});

require([
    'cordova',
    'serverCodes',
    'dbHelper',
    'tripledes',
    'commonJS',
    'Constants',
    'customJS',
    'angularTranslate',///////////changes @04/09/2015
    'flexcalendar',///////////changes @04/09/2015
    'ngcordova',
    'ionGallery',
    'ionRating',
    'processAppXML',

], function(cordova, serverCodes, dbHelper, tripledes, commonJS,Constants,customJS, angularTranslate,flexcalendar,ngcordova,ionGallery,ionRating,processAppXML) { // angular,
    'use strict';

    fetchData().then(bootstrapApplication);
    //fetchData();//.then(bootstrapApplication);


    function fetchData() {
        //for server

        if (isLocal) {
            return initAppStudio();
        } else {
            ionic.Platform.ready(function() {
                initAppStudio();
            });

            window.dispatchEvent(new CustomEvent('load'));
        }

    }



    function bootstrapApplication() {
        require(['app'], function(app) {
            'use strict';
            // angular.element(document).ready(function() {
            //     angular.bootstrap(document, [app['name']]);
            // });

            // ionic.Platform.ready(function ()
            // {
            //   angular.bootstrap(document, [app['name']]);
            // });

            // window.dispatchEvent(new CustomEvent('load'));

            var start = function() {
                //alert('start func');
                angular.bootstrap(document, [app['name']]);
            }
            var device = ionic.Platform.device();
            (document.body && device) ? start(): ionic.Platform.ready(start);
        });

    }


    // var start  = function(){
    //     console.log('main.start');
    //     angular.bootstrap(document, [app['name']]);
    // }
    //(document.body && device) ? start() : ionic.Platform.ready(start);
    //start();
});
