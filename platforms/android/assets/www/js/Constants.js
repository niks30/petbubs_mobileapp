/**
 *
 * File name : constants.js
 *
 * Version information
 * VERSION   1.0
 * DATE      2Aug2016
 * COMMENT   Initial Version
 * AUTHOR    Bhupesh Bari
 *
 * Copyright © [2015] Mobileware Technologies Private Limited
 * All Rights Reserved.
 *
 */

angular.module('starter.constants', [])

  .constant('GlobalConstants', {
    // Application Constants
    "appName": "Petbubs",
    "appVersion": "1.0",
    "defaultEndPoint": "http://52.66.131.144:8080/PetBubs/",
    // "defaultEndPoint": "http://localhost:8080/PetBubs/",
    // "defaultEndPoint": "http://10.9.31.124:8080/PetBubs/",
    "defaultEndPointPort": "1010",
    "fileUpload": "fileUpload",
    "webServiceName": "n",
    "resourceUrl": "",
    "responseType": "json", //oData,
    "javeIntegerLowerLimit": -2147483648,
    "javeIntegerUpperLimit": 2147483648,
    "imageFolderPath": "img/",
    "mediaServerPath": "images/UserWall/",
    "userProfileServerPath": "images/User/",
    "responseTimeOut": 20000,
    "sessionExpiredErrorCode": 403,

    // LocalStorage Keys
    "ls_SystemName": "systemName",
    "ls_EndPoint": "endPoint",
    "ls_EndPointPort": "endPointPort",
    "ls_customerId": "customerId",
    "ls_name": "name",
    "ls_email": "email",
    "ls_city": "city",
    "ls_mobile": "mobile",
    "ls_profilePicture": "ProFilePicture",
    "ls_coverImage": "coverImage",
    "ls_calendarEvents": "calendarEvents",
    "ls_hasSeenBubTalkLeaf": "hasSeenBubTalkLeaf",


    "mediaTypeImage": "image",
    "mediaTypeVideo": "video",

    "ls_OTP": "OTP",
    "ls_isLoggedIn": "isLoggedIn",

    "bubCategory": {
      "dogs": 1,
      "cats": 2,
      "birds": 3,
      "other": 4
    },
    //Error Codes
    "errorCode": {}
  });
